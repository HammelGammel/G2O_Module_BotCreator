// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

local BOT_SCHEDULETICKINTERVAL = 10000;
local BOT_NEXTSCHEDULETICK = getTickCount();
local BOT_LASTTIME = getTime();
// -- ignore first minute or bots will execute 8:0h schedules twice at serverstart -- //
BOT_LASTTIME.min++;

// ------------------------------------------------------------------- //

addEventHandler("onTick", function()
{
    local tick = getTickCount();

    if (tick > BOT_NEXTSCHEDULETICK)
    {
		local gameTime = getTime();

        foreach (bot in Bot.m_Bots)
        {
            if (bot != null && !bot.isDead())
            {
                local combatComp = bot.getCombatComp();
                local isFighting = false;

                if (combatComp != null)
                    isFighting = combatComp.isFighting();

                if (!isFighting)
                    bot.getScheduleComp().findValidBetween(BOT_LASTTIME, gameTime);
            }
        }

        BOT_NEXTSCHEDULETICK = tick + BOT_SCHEDULETICKINTERVAL;
		BOT_LASTTIME = gameTime;
    }
});

// ------------------------------------------------------------------- //

class ScheduleComponent extends UniqueComponent
{
    function returnToSchedule(currentTime)
    {
        local lastScheduleIdx = null;
        local lastDist = false;

        foreach (i, schedule in m_ScheduledActions)
        {
            local currDist = Util.getDeltaGameTimeMinutes(schedule.time, currentTime);

            if (lastDist == false || currDist < lastDist)
            {
                lastScheduleIdx = i;
                lastDist = currDist;
            }
        }

        if (lastScheduleIdx != null)
        {
            execute(lastScheduleIdx);
        }
    }

    // ------------------------------------------------------------------- //

    function findValidBetween(startTime, endTime)
    {
        local lastScheduleIdx = null;
        local lastDist = false;

        foreach (i, schedule in m_ScheduledActions)
        {
            /* Find nearest schedule between starttime and endtime */
            if (Util.isTimeBetween(startTime, endTime, schedule.time))
            {
                local currDist = Util.getDeltaGameTimeMinutes(startTime, schedule.time);

                if (lastDist == false || currDist > lastDist)
                {
                    lastScheduleIdx = i;
                    lastDist = currDist;
                }
            }
        }

        if (lastScheduleIdx != null)
            execute(lastScheduleIdx);
    }

    // ------------------------------------------------------------------- //

    function findValid(hour, min)
    {
        foreach (i, schedule in m_ScheduledActions)
        {
            if (schedule.time.hour == hour && schedule.time.min == min)
            {
				if (execute(schedule, i))
                    i--;
            }
        }
    }

	// ------------------------------------------------------------------- //

    function execute(i)
    {
        local parent = getParent();

        if (!parent.isDead())
        {
            local actionComponent = parent.getActionComp();
            local time = m_ScheduledActions[i].time;
            local schedules = m_ScheduledActions[i].schedules;

            actionComponent.clear();

            for (local i = schedules.len() - 1; i >= 0; i--)
            {
                local schedule = schedules[i];
                // -- Clone params so exAction doesn't permanently change them -- //
                local params = Util.cloneTable(schedule.params);

                // -- Call scheduled method in action component -- //
                getParent().getActionComp().add(PRIORITY_LOW, schedule.funcName, params);
            }

            //if (Bot.m_LogBots)
                //print("Bot " + parent.getName() + "(" + parent.getID() + ") now executes schedule: " + time.hour + ":" + time.min);
        }
        return false
    }

	// ------------------------------------------------------------------- //

    // -- don't call directly. Use individual schedule method -- //
    function add(hour, min, funcName, params)
    {
        local newSchedule = {["funcName"] = funcName, ["params"] = params};
        local newTime = {["hour"] = hour, ["min"] = min};
        local newTable = {["time"] = newTime, ["schedules"] = [newSchedule]}

        if (m_ScheduledActions.len() == 0)
            m_ScheduledActions.append(newTable);
        else
        {
            // -- There's already a schedule at that time. Append -- //
            foreach (i, currSchedules in m_ScheduledActions)
            {
                if (isEqual(newTime, currSchedules.time))
                {
                    currSchedules.schedules.insert(0, newSchedule);

                    return;
                }
            }

            // -- Create new schedule -- //
            foreach (i, currSchedules in m_ScheduledActions)
            {
                if (isEarlier(newTime, currSchedules.time))
                {
                    m_ScheduledActions.insert(i, newTable);
                    break;
                }
                else if (i == m_ScheduledActions.len() - 1)
                {
                    m_ScheduledActions.append(newTable);
                    break;
                }
            }
        }
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Add schedule -- //

    function addGotoWP(hour, min, wp)
    {
        add(hour, min, "exGotoWP", {["wp"] = wp});
    }

    // ------------------------------------------------------------------- //

    function addGoto(hour, min, x, y, z)
    {
        add(hour, min, "exGoto", {["x"] = x, ["y"] = y, ["z"] = z, ["wp"] = null, ["path"] = null});
    }

    // ------------------------------------------------------------------- //

    function addSetAngle(hour, min, angle)
    {
        add(hour, min, "exSetAngle", {["angle"] = angle});
    }

    // ------------------------------------------------------------------- //

    function addTurn(hour, min, angle)
    {
        add(hour, min, "exTurn", {["angle"] = angle});
    }

    // ------------------------------------------------------------------- //

    function addPlayAni(hour, min, ani, aniLength = 1000)
    {
        add(hour, min, "exPlayAni", {["ani"] = ani, ["aniLength"] = aniLength, ["endAt"] = null, ["playedAni"] = false});
    }

    // ------------------------------------------------------------------- //

    function addPlayRandomAni(hour, min, ani, aniLength = 1000)
    {
        add(hour, min, "exPlayRandomAni", {["ani"] = ani, ["aniLength"] = aniLength, ["endAt"] = null, ["playedAni"] = false});
    }

    // ------------------------------------------------------------------- //

    function addRepeatAni(hour, min, ani, aniLength = 1000, repeatCount = 0)
    {
        add(hour, min, "exRepeatAni", {["ani"] = ani, ["aniLength"] = aniLength, ["endAt"] = null, ["playedAni"] = false, ["repeatCount"] = repeatCount});
    }

    // ------------------------------------------------------------------- //

    function addRepeatRandomAni(hour, min, ani, aniLength = 1000, repeatCount = 0)
    {
        add(hour, min, "exRepeatRandomAni", {["ani"] = ani, ["aniLength"] = aniLength, ["endAt"] = null, ["playedAni"] = false, ["repeatCount"] = repeatCount});
    }

    // ------------------------------------------------------------------- //

    function addSleep(hour, min, wp, angle)
    {
        add(hour, min, "exSleep", {["wp"] = wp, ["angle"] = angle});
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static function isEqual(leftTime, rightTime)
    {
        return leftTime.hour == rightTime.hour && leftTime.min == rightTime.min;
    }

    // ------------------------------------------------------------------- //

    static function isEarlier(leftTime, rightTime)
    {
        return leftTime.hour < rightTime.hour || (leftTime.hour == rightTime.hour && leftTime.min < rightTime.min);
    }

    // ------------------------------------------------------------------- //

    static function isEarlierOrEqual(leftTime, rightTime)
    {
        return leftTime.hour < rightTime.hour || (leftTime.hour == rightTime.hour && leftTime.min <= rightTime.min);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(parent)
    {
        base.constructor(parent);

        m_ScheduledActions = [];
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_ScheduledActions = null;
}
