// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class RespawnComponent extends UniqueComponent
{
    function sendRespawn()
    {
        local parent = getParent();
        local respawnPos = getSpawnPos();

        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TCRespawn);
        packet.writeInt16(parent.getID());
        packet.writeFloat(respawnPos.x);
        packet.writeFloat(respawnPos.y);
        packet.writeFloat(respawnPos.z);

        for(local pid = 0; pid < getMaxSlots(); pid++)
            if (isPlayerConnected(pid))
                packet.send(pid, RELIABLE);
    }

    // ------------------------------------------------------------------- //

    function attemptRespawn()
    {
        local pos = getParent().getPosition();
        local spawnPos = getSpawnPos();

        local nearestPid = Util.getNearestPlayer(pos.x, pos.y, pos.z, m_MinRespawnDist);
        local nearestPidAtSpawn = Util.getNearestPlayer(spawnPos.x, spawnPos.y, spawnPos.z, m_MinRespawnDist);

        if (nearestPid == -1 && nearestPidAtSpawn == -1)
            respawn();
        // -- Postpone spawn if any player is too close -- //
        else
            scheduleRespawn();
    }

    // ------------------------------------------------------------------- //

    function respawn()
    {
        local parent = getParent();

        if (getDespawn())
        {
            Bot.destroyBot(parent.getID());

            if (Bot.m_LogBots)
                print("Bot " + parent.getName() + "(" + parent.getID() + ") despawned");
        }
        else
        {
            if (Bot.m_LogBots)
                print("Bot " + parent.getName() + "(" + parent.getID() + ") respawned");

            parent.setHealth(parent.getMaxHealth());

            local spawnPos = getSpawnPos();
            getParent().setPosition(spawnPos.x, spawnPos.y, spawnPos.z);

            parent.equipMeleeWeapon(m_PreDeathMeleeWeapon);
            parent.equipRangedWeapon(m_PreDeathRangedWeapon);

            parent.getScheduleComp().returnToSchedule(getTime());

            sendRespawn();

            callEvent("onBotRespawn", parent.getID());
        }
    }

    // ------------------------------------------------------------------- //

    function scheduleRespawn()
    {
        local currDate = date();
        local dateDelta = {["day"] = 0, ["hour"] = m_RespawnTimeHour, ["min"] = m_RespawnTimeMin, ["sec"] = m_RespawnTimeSec};
        local newDate = Util.addToTime(currDate, dateDelta);

        RealTimeEvent.add(newDate, function(comp = this)
        {
            comp.attemptRespawn();
        }, "rspwn:" + getParent().getID(), true);
    }

    // ------------------------------------------------------------------- //

    function unScheduleRespawn()
    {
        RealTimeEvent.remove("rspwn:" + getParent().getID());
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //
    function getSpawnPos() {return m_SpawnPos;}
    function getSpawnAngle() {return m_SpawnAngle;}
    function getDespawn() {return m_Despawn;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Setters -- //
    function setSpawnAngle(angle){m_SpawnAngle = angle;}
    function setSpawnPos(x, y, z)
    {
        m_SpawnPos.x = x;
        m_SpawnPos.y = y;
        m_SpawnPos.z = z;
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    function setRespawnInterval(hour, min, sec)
    {
        m_RespawnTimeHour = hour;
        m_RespawnTimeMin = min;
        m_RespawnTimeSec = sec;
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(parent)
    {
        base.constructor(parent);

        local pos = parent.getPosition();
        local angle = parent.getAngle();

        m_SpawnPos = {["x"] = pos.x, ["y"] = pos.y, ["z"] = pos.z};
        setSpawnAngle(angle);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_Despawn = false;

    m_SpawnPos = null;
    m_SpawnAngle = 0;

    m_PreDeathMeleeWeapon = null;
    m_PreDeathRangedWeapon = null;

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    // -- Bots respawn will be postponed if the nearest player's distance to the bot or it's spawnpoint is smaller than this
    // -- (-> players cannot see bot appear/disappear) -- //
    static m_MinRespawnDist = 5000;

    m_RespawnTimeHour = 0;
    m_RespawnTimeMin = 5;
    m_RespawnTimeSec = 0;
}
