// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class FactionComponent extends UniqueComponent
{
    function isPlayerEnemy(pid)
    {
        foreach (i, fac in m_Factions)
            if (fac.isEnemyNameFaction("player"))
                    return true;

        return false;
    }

    // ------------------------------------------------------------------- //

    function isBotEnemy(bot)
    {
        foreach (i, fac in m_Factions)
            foreach (j, otherFac in bot.getFactionComp().getFactions())
                if (Faction.areFactionsEnemies(fac, otherFac))
                    return true;

        return false;
    }

    // ------------------------------------------------------------------- //

    function addToFaction(name)
    {
        local fac = Faction.getFaction(name);

        if (fac != null)
            m_Factions.append(fac);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //
    function getFactions(){return m_Factions;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(parent)
    {
        base.constructor(parent);

        m_Factions = [];
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_Factions = null;
}
