// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class CombatTypeComponent extends TypeComponent
{
    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //
    function getDrawnWeaponMode() {return m_DrawnWeaponMode;}
    function getDrawTime() {return m_DrawTime;}
    function getMaxMoveDist() {return m_MaxMoveDist;}
    function getMaxKeepEnemyTime() {return m_MaxKeepEnemyTime;}

    function getDoEverWarn() {return m_DoEverWarn;}
    function getWarnTurnSpeed() {return m_WarnTurnSpeed;}
    function getEnemyViewDist() {return m_EnemyViewDist;}
    function getMaxWarnTime() {return m_MaxWarnTime;}
    function getMaxWarnDist() {return m_MaxWarnDist;}
    function getMinWarnDist() {return m_MinWarnDist;}

    function getChaseTurnSpeed() {return m_ChaseTurnSpeed;}
    function getMaxChaseDist() {return m_MaxChaseDist;}
    function getMaxChaseTime() {return m_MaxChaseTime;}

    function getJumpBackRange() {return m_JumpBackRange;}
    function getAttackRange() {return m_AttackRange;}
    function getAttackTurnSpeed() {return m_AttackTurnSpeed;}

    function getAttackRecoveryTime() {return m_AttackRecoveryTime;}
    function getDamageDelay() {return m_DamageDelay;}

    function getMaxAttackAngle() {return m_MaxAttackAngle;}

    function getJumpBackChance() {return m_JumpBackChance;}
    function getJumpBackTime() {return m_JumpBackTime;}
    function getStrafeChance() {return m_StrafeChance;}
    function getStrafeTime() {return m_StrafeTime;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_DrawnWeaponMode = 2;
    m_DrawTime = 700;

    // -- cancel combat if enemy has moved at least m_MaxMoveDist units from starting pos -- //
    m_MaxMoveDist = 5000;

    // -- Attempt to find a new opponent after m_MaxKeepEnemyTime milliseconds. Otherwise, one player can lure the bot around while
    // -- another deals damage unharmed -- //
    m_MaxKeepEnemyTime = 10000;

    // -- WARN -- //
        m_DoEverWarn = true;
        m_WarnTurnSpeed = 0.006;
        m_EnemyViewDist = 1500;
        // -- Attack after m_MaxWarnTime ms -- //
        m_MaxWarnTime = 6000;
        m_MaxWarnDist = 1750;
        m_MinWarnDist = 1000;

    // -- CHASE -- //
        m_ChaseTurnSpeed = 0.006;
        // -- If enemy is farther away than m_MaxChaseDist units, cancel chase. Make sure to set this to at least the maximum range
        // -- of ranged weapons, or bots will cancel chase immediately when shot -- //
        m_MaxChaseDist = 10000;
        // -- if chase takes longer than m_MaxChaseTime ms, change combat state to warn
        m_MaxChaseTime = 20000;

    // -- ATTACK -- //
        m_JumpBackRange = 100;
        m_AttackRange = 250;
        m_AttackTurnSpeed = 0.006;

        m_AttackRecoveryTime = 1000;
        // -- After attack anim is has been started, wait m_DamageDelay ms before delivering damage -- //
        m_DamageDelay = 200;

        m_MinAttackDist = 100;
        m_MaxAttackAngle = 60;

        // -- Evade -- //
            m_JumpBackChance = 0.7;
            m_JumpBackTime = 1000;

            m_StrafeChance = 0.15;
            m_StrafeTime = 1000;
}

// -- Human fist -- //
COMBATTYPE_HUM_FIST <- CombatTypeComponent();
    COMBATTYPE_HUM_FIST.m_DrawnWeaponMode = 1;
    COMBATTYPE_HUM_FIST.m_DrawTime = 0;
    COMBATTYPE_HUM_FIST.m_MaxMoveDist = 5000;
    COMBATTYPE_HUM_FIST.m_MaxKeepEnemyTime = 10000;
    COMBATTYPE_HUM_FIST.m_DoEverWarn = false;
    COMBATTYPE_HUM_FIST.m_WarnTurnSpeed = 0.006;
    COMBATTYPE_HUM_FIST.m_EnemyViewDist = 1500;
    COMBATTYPE_HUM_FIST.m_MaxWarnTime = 0;
    COMBATTYPE_HUM_FIST.m_MaxWarnDist = 1750;
    COMBATTYPE_HUM_FIST.m_MinWarnDist = 1000;
    COMBATTYPE_HUM_FIST.m_ChaseTurnSpeed = 0.006;
    COMBATTYPE_HUM_FIST.m_MaxChaseDist = 10000;
    COMBATTYPE_HUM_FIST.m_MaxChaseTime = 20000;
    COMBATTYPE_HUM_FIST.m_JumpBackRange = 100;
    COMBATTYPE_HUM_FIST.m_AttackRange = 150;
    COMBATTYPE_HUM_FIST.m_AttackTurnSpeed = 0.006;
    COMBATTYPE_HUM_FIST.m_AttackRecoveryTime = 400;
    COMBATTYPE_HUM_FIST.m_DamageDelay = 200;
    COMBATTYPE_HUM_FIST.m_MaxAttackAngle = 60;
    COMBATTYPE_HUM_FIST.m_JumpBackChance = 0.7;
    COMBATTYPE_HUM_FIST.m_JumpBackTime = 800;
    COMBATTYPE_HUM_FIST.m_StrafeChance = 0.15;
    COMBATTYPE_HUM_FIST.m_StrafeTime = 1000;

// -- Human 1H -- //
COMBATTYPE_HUM_1H <- CombatTypeComponent();
    COMBATTYPE_HUM_1H.m_DrawnWeaponMode = 2;
    COMBATTYPE_HUM_1H.m_DrawTime = 700;
    COMBATTYPE_HUM_1H.m_MaxMoveDist = 5000;
    COMBATTYPE_HUM_1H.m_MaxKeepEnemyTime = 10000;
    COMBATTYPE_HUM_1H.m_DoEverWarn = false;
    COMBATTYPE_HUM_1H.m_WarnTurnSpeed = 0.006;
    COMBATTYPE_HUM_1H.m_EnemyViewDist = 1500;
    COMBATTYPE_HUM_1H.m_MaxWarnTime = 0;
    COMBATTYPE_HUM_1H.m_MaxWarnDist = 1750;
    COMBATTYPE_HUM_1H.m_MinWarnDist = 1000;
    COMBATTYPE_HUM_1H.m_ChaseTurnSpeed = 0.006;
    COMBATTYPE_HUM_1H.m_MaxChaseDist = 10000;
    COMBATTYPE_HUM_1H.m_MaxChaseTime = 20000;
    COMBATTYPE_HUM_1H.m_JumpBackRange = 100;
    COMBATTYPE_HUM_1H.m_AttackRange = 200;
    COMBATTYPE_HUM_1H.m_AttackTurnSpeed = 0.006;
    COMBATTYPE_HUM_1H.m_AttackRecoveryTime = 700;
    COMBATTYPE_HUM_1H.m_DamageDelay = 200;
    COMBATTYPE_HUM_1H.m_MaxAttackAngle = 60;
    COMBATTYPE_HUM_1H.m_JumpBackChance = 0.6;
    COMBATTYPE_HUM_1H.m_JumpBackTime = 800;
    COMBATTYPE_HUM_1H.m_StrafeChance = 0.15;
    COMBATTYPE_HUM_1H.m_StrafeTime = 1000;

// -- Human 2H -- //
COMBATTYPE_HUM_2H <- CombatTypeComponent();
    COMBATTYPE_HUM_2H.m_DrawnWeaponMode = 4;
    COMBATTYPE_HUM_2H.m_DrawTime = 700;
    COMBATTYPE_HUM_2H.m_MaxMoveDist = 5000;
    COMBATTYPE_HUM_2H.m_MaxKeepEnemyTime = 10000;
    COMBATTYPE_HUM_2H.m_DoEverWarn = false;
    COMBATTYPE_HUM_2H.m_WarnTurnSpeed = 0.006;
    COMBATTYPE_HUM_2H.m_EnemyViewDist = 1500;
    COMBATTYPE_HUM_2H.m_MaxWarnTime = 0;
    COMBATTYPE_HUM_2H.m_MaxWarnDist = 1750;
    COMBATTYPE_HUM_2H.m_MinWarnDist = 1000;
    COMBATTYPE_HUM_2H.m_ChaseTurnSpeed = 0.006;
    COMBATTYPE_HUM_2H.m_MaxChaseDist = 10000;
    COMBATTYPE_HUM_2H.m_MaxChaseTime = 20000;
    COMBATTYPE_HUM_2H.m_JumpBackRange = 100;
    COMBATTYPE_HUM_2H.m_AttackRange = 250;
    COMBATTYPE_HUM_2H.m_AttackTurnSpeed = 0.006;
    COMBATTYPE_HUM_2H.m_AttackRecoveryTime = 1000;
    COMBATTYPE_HUM_2H.m_DamageDelay = 200;
    COMBATTYPE_HUM_2H.m_MaxAttackAngle = 60;
    COMBATTYPE_HUM_2H.m_JumpBackChance = 0.6;
    COMBATTYPE_HUM_2H.m_JumpBackTime = 800;
    COMBATTYPE_HUM_2H.m_StrafeChance = 0.15;
    COMBATTYPE_HUM_2H.m_StrafeTime = 1000;

// -- monster -- //
COMBATTYPE_MONSTER <- CombatTypeComponent();
    COMBATTYPE_MONSTER.m_DrawnWeaponMode = 0;
    COMBATTYPE_MONSTER.m_DrawTime = 0;
    COMBATTYPE_MONSTER.m_MaxMoveDist = 5000;
    COMBATTYPE_MONSTER.m_MaxKeepEnemyTime = 10000;
    COMBATTYPE_MONSTER.m_DoEverWarn = true;
    COMBATTYPE_MONSTER.m_WarnTurnSpeed = 0.006;
    COMBATTYPE_MONSTER.m_EnemyViewDist = 1500;
    COMBATTYPE_MONSTER.m_MaxWarnTime = 6000;
    COMBATTYPE_MONSTER.m_MaxWarnDist = 1750;
    COMBATTYPE_MONSTER.m_MinWarnDist = 1000;
    COMBATTYPE_MONSTER.m_ChaseTurnSpeed = 0.006;
    COMBATTYPE_MONSTER.m_MaxChaseDist = 8000;
    COMBATTYPE_MONSTER.m_MaxChaseTime = 15000;
    COMBATTYPE_MONSTER.m_JumpBackRange = 100;
    COMBATTYPE_MONSTER.m_AttackRange = 200;
    COMBATTYPE_MONSTER.m_AttackTurnSpeed = 0.006;
    COMBATTYPE_MONSTER.m_AttackRecoveryTime = 1000;
    COMBATTYPE_MONSTER.m_DamageDelay = 200;
    COMBATTYPE_MONSTER.m_MaxAttackAngle = 60;
    COMBATTYPE_MONSTER.m_JumpBackChance = 0.6;
    COMBATTYPE_MONSTER.m_JumpBackTime = 800;
    COMBATTYPE_MONSTER.m_StrafeChance = 0.15;
    COMBATTYPE_MONSTER.m_StrafeTime = 1000;

// -- orc 2H -- //
COMBATTYPE_ORC_2H <- CombatTypeComponent();
    COMBATTYPE_ORC_2H.m_DrawnWeaponMode = 4;
    COMBATTYPE_ORC_2H.m_DrawTime = 700;
    COMBATTYPE_ORC_2H.m_MaxMoveDist = 5000;
    COMBATTYPE_ORC_2H.m_MaxKeepEnemyTime = 10000;
    COMBATTYPE_ORC_2H.m_DoEverWarn = true;
    COMBATTYPE_ORC_2H.m_WarnTurnSpeed = 0.006;
    COMBATTYPE_ORC_2H.m_EnemyViewDist = 1500;
    COMBATTYPE_ORC_2H.m_MaxWarnTime = 5000;
    COMBATTYPE_ORC_2H.m_MaxWarnDist = 1750;
    COMBATTYPE_ORC_2H.m_MinWarnDist = 1000;
    COMBATTYPE_ORC_2H.m_ChaseTurnSpeed = 0.006;
    COMBATTYPE_ORC_2H.m_MaxChaseDist = 12000;
    COMBATTYPE_ORC_2H.m_MaxChaseTime = 15000;
    COMBATTYPE_ORC_2H.m_JumpBackRange = 120;
    COMBATTYPE_ORC_2H.m_AttackRange = 230;
    COMBATTYPE_ORC_2H.m_AttackTurnSpeed = 0.006;
    COMBATTYPE_ORC_2H.m_AttackRecoveryTime = 1000;
    COMBATTYPE_ORC_2H.m_DamageDelay = 200;
    COMBATTYPE_ORC_2H.m_MaxAttackAngle = 50;
    COMBATTYPE_ORC_2H.m_JumpBackChance = 0.4;
    COMBATTYPE_ORC_2H.m_JumpBackTime = 800;
    COMBATTYPE_ORC_2H.m_StrafeChance = 0.15;
    COMBATTYPE_ORC_2H.m_StrafeTime = 1000;

// -- skeleton 2H -- //
COMBATTYPE_SKELETON_2H <- CombatTypeComponent();
    COMBATTYPE_SKELETON_2H.m_DrawnWeaponMode = 4;
    COMBATTYPE_SKELETON_2H.m_DrawTime = 700;
    COMBATTYPE_SKELETON_2H.m_MaxMoveDist = 5000;
    COMBATTYPE_SKELETON_2H.m_MaxKeepEnemyTime = 10000;
    COMBATTYPE_SKELETON_2H.m_DoEverWarn = false;
    COMBATTYPE_SKELETON_2H.m_WarnTurnSpeed = 0.006;
    COMBATTYPE_SKELETON_2H.m_EnemyViewDist = 1500;
    COMBATTYPE_SKELETON_2H.m_MaxWarnTime = 5000;
    COMBATTYPE_SKELETON_2H.m_MaxWarnDist = 1750;
    COMBATTYPE_SKELETON_2H.m_MinWarnDist = 1000;
    COMBATTYPE_SKELETON_2H.m_ChaseTurnSpeed = 0.006;
    COMBATTYPE_SKELETON_2H.m_MaxChaseDist = 12000;
    COMBATTYPE_SKELETON_2H.m_MaxChaseTime = 10000;
    COMBATTYPE_SKELETON_2H.m_JumpBackRange = 120;
    COMBATTYPE_SKELETON_2H.m_AttackRange = 220;
    COMBATTYPE_SKELETON_2H.m_AttackTurnSpeed = 0.004;
    COMBATTYPE_SKELETON_2H.m_AttackRecoveryTime = 1000;
    COMBATTYPE_SKELETON_2H.m_DamageDelay = 200;
    COMBATTYPE_SKELETON_2H.m_MaxAttackAngle = 40;
    COMBATTYPE_SKELETON_2H.m_JumpBackChance = 0.4;
    COMBATTYPE_SKELETON_2H.m_JumpBackTime = 800;
    COMBATTYPE_SKELETON_2H.m_StrafeChance = 0.15;
    COMBATTYPE_SKELETON_2H.m_StrafeTime = 1000;
