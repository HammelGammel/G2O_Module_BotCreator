// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class AnimTypeComponent extends TypeComponent
{
    function addAni(identifier, ani, time = 0)
    {
        if (!(identifier in m_AnimationSequences))
            m_AnimationSequences[identifier] <- [];

        m_AnimationSequences[identifier].append(BotAni(ani, time));
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //
    function getAnimationSequences() {return m_AnimationSequences;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor()
    {
        base.constructor();

        m_AnimationSequences = {};
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_AnimationSequences = null;
}

// ------------------------------------------------------------------- //

// -- HUMAN -- //
ANIMTYPE_HUMAN <- AnimTypeComponent();
    // -- Movement -- //
    ANIMTYPE_HUMAN.addAni("die", "T_DEAD");
    ANIMTYPE_HUMAN.addAni("die", "T_DEADB");

    ANIMTYPE_HUMAN.addAni("sprint", "HUMANS_SPRINT");

    ANIMTYPE_HUMAN.addAni("run", "S_FISTRUNL");     // -- no weapon
    ANIMTYPE_HUMAN.addAni("run", "S_FISTRUNL");     // -- fist
    ANIMTYPE_HUMAN.addAni("run", "S_1HRUNL");       // -- 1H
    ANIMTYPE_HUMAN.addAni("run", "S_2HRUNL");       // --
    ANIMTYPE_HUMAN.addAni("run", "S_2HRUNL");       // -- 2H
    ANIMTYPE_HUMAN.addAni("run", "S_2HRUNL");       // --

    ANIMTYPE_HUMAN.addAni("walk", "S_WALKL");       // -- no weapon
    ANIMTYPE_HUMAN.addAni("walk", "S_WALKL");       // -- fist
    ANIMTYPE_HUMAN.addAni("walk", "S_1HWALKL");     // -- 1H
    ANIMTYPE_HUMAN.addAni("walk", "S_2HWALKL");     // --
    ANIMTYPE_HUMAN.addAni("walk", "S_2HWALKL");     // -- 2H
    ANIMTYPE_HUMAN.addAni("walk", "S_2HWALKL");     // --

    // -- Other -- //
    ANIMTYPE_HUMAN.addAni("warn", "T_IGETYOU", 2500);
    ANIMTYPE_HUMAN.addAni("jumpBack", "T_FISTPARADEJUMPB");
    ANIMTYPE_HUMAN.addAni("strafe", "T_RUNSTRAFER");
    ANIMTYPE_HUMAN.addAni("strafe", "T_RUNSTRAFEL");
    ANIMTYPE_HUMAN.addAni("jump", "T_STAND_2_JUMP");
    ANIMTYPE_HUMAN.addAni("jump", "T_RUNL_2_JUMP");

    // -- Sleep -- //
    ANIMTYPE_HUMAN.addAni("sleep", "T_STAND_2_SLEEPGROUND");
    ANIMTYPE_HUMAN.addAni("wakeup", "T_SLEEPGROUND_2_STAND");

    // -- draw -- //
    ANIMTYPE_HUMAN.addAni("draw", "T_RUN_2_FIST");
    ANIMTYPE_HUMAN.addAni("draw", "T_RUN_2_FIST");
    ANIMTYPE_HUMAN.addAni("draw", "T_1H_2_1HRUN");
    ANIMTYPE_HUMAN.addAni("draw", "T_2H_2_2HRUN");
    ANIMTYPE_HUMAN.addAni("draw", "T_2H_2_2HRUN");
    ANIMTYPE_HUMAN.addAni("draw", "T_2H_2_2HRUN");

    ANIMTYPE_HUMAN.addAni("sheath", "T_FIST_2_RUN");
    ANIMTYPE_HUMAN.addAni("sheath", "T_FIST_2_RUN");
    ANIMTYPE_HUMAN.addAni("sheath", "T_1HRUN_2_1H");
    ANIMTYPE_HUMAN.addAni("sheath", "T_2HRUN_2_2H");
    ANIMTYPE_HUMAN.addAni("sheath", "T_2HRUN_2_2H");
    ANIMTYPE_HUMAN.addAni("sheath", "T_2HRUN_2_2H");

    // -- Attack -- //
    ANIMTYPE_HUMAN.addAni("attack1h", "S_1HATTACK");
    ANIMTYPE_HUMAN.addAni("attack1h", "T_1HATTACKR");
    ANIMTYPE_HUMAN.addAni("attack1h", "T_1HATTACKL");

    ANIMTYPE_HUMAN.addAni("attack2h", "S_2HATTACK");
    ANIMTYPE_HUMAN.addAni("attack2h", "T_2HATTACKR");
    ANIMTYPE_HUMAN.addAni("attack2h", "T_2HATTACKL");

    ANIMTYPE_HUMAN.addAni("attackFist", "S_FISTATTACK");

// -- MONSTER -- //
ANIMTYPE_MONSTER <- AnimTypeComponent();
    // -- Movement -- //
    ANIMTYPE_MONSTER.addAni("die", "T_DEAD");
    ANIMTYPE_MONSTER.addAni("die", "T_DEADB");

    ANIMTYPE_MONSTER.addAni("sprint", "S_FISTRUNL");
    ANIMTYPE_MONSTER.addAni("run", "S_FISTRUNL");
    ANIMTYPE_MONSTER.addAni("walk", "S_FISTWALKL");

    // -- Other -- //
    ANIMTYPE_MONSTER.addAni("warn", "T_WARN", 2500);
    ANIMTYPE_MONSTER.addAni("jumpBack", "T_FISTPARADEJUMPB");
    ANIMTYPE_MONSTER.addAni("strafe", "T_FISTRUNSTRAFER");
    ANIMTYPE_MONSTER.addAni("strafe", "T_FISTRUNSTRAFEL");
    ANIMTYPE_MONSTER.addAni("eat", "T_STAND_2_EAT");
    ANIMTYPE_MONSTER.addAni("jump", "T_STAND_2_JUMP");
    ANIMTYPE_MONSTER.addAni("jump", "T_RUNL_2_JUMP");

    // -- Sleep -- //
    ANIMTYPE_MONSTER.addAni("sleep", "T_STAND_2_SLEEP");
    ANIMTYPE_MONSTER.addAni("wakeup", "T_SLEEP_2_STAND");

    // -- Attack -- /
    ANIMTYPE_MONSTER.addAni("attackFist", "S_FISTATTACK");

    // -- Roam -- /
    ANIMTYPE_MONSTER.addAni("roam", "R_ROAM1");
    ANIMTYPE_MONSTER.addAni("roam", "R_ROAM2");
    ANIMTYPE_MONSTER.addAni("roam", "R_ROAM3");

// -- ORC -- //
ANIMTYPE_ORC <- AnimTypeComponent();
    // -- movement -- //
    ANIMTYPE_ORC.addAni("die", "T_DEAD");
    ANIMTYPE_ORC.addAni("die", "T_DEADB");

    ANIMTYPE_ORC.addAni("sprint", "HUMANS_SPRINT");

    ANIMTYPE_ORC.addAni("run", "S_FISTRUNL");     // -- fist/no weapon
    ANIMTYPE_ORC.addAni("run", "S_2HRUNL");       // -- 2H

    ANIMTYPE_ORC.addAni("walk", "S_WALKL");       // -- fist/no weapon
    ANIMTYPE_ORC.addAni("walk", "S_2HWALKL");     // -- 2H

    // -- Other -- //
    ANIMTYPE_ORC.addAni("warn", "T_WARN", 2500);
    ANIMTYPE_ORC.addAni("jumpBack", "T_FISTPARADEJUMPB");
    ANIMTYPE_ORC.addAni("strafe", "T_RUNSTRAFER");
    ANIMTYPE_ORC.addAni("strafe", "T_RUNSTRAFEL");
    ANIMTYPE_ORC.addAni("jump", "T_STAND_2_JUMP");
    ANIMTYPE_ORC.addAni("jump", "T_RUNL_2_JUMP");

    // -- Sleep -- //
    ANIMTYPE_ORC.addAni("sleep", "T_STAND_2_SLEEPGROUND");
    ANIMTYPE_ORC.addAni("wakeup", "T_SLEEPGROUND_2_STAND");

    // -- draw -- //
    ANIMTYPE_ORC.addAni("draw", "T_2H_2_2HRUN");

    // -- sheath -- //
    ANIMTYPE_ORC.addAni("sheath", "T_2HRUN_2_2H");

    // -- attack -- //
    ANIMTYPE_ORC.addAni("attack2h", "S_2HATTACK");
    ANIMTYPE_ORC.addAni("attack2h", "T_2HATTACKR");
    ANIMTYPE_ORC.addAni("attack2h", "T_2HATTACKL");

    ANIMTYPE_ORC.addAni("attackFist", "S_FISTATTACK");

// -- SKELETON -- //
ANIMTYPE_SKELETON <- AnimTypeComponent();
    // -- Movement -- //
    ANIMTYPE_SKELETON.addAni("die", "T_DEAD");
    ANIMTYPE_SKELETON.addAni("die", "T_DEADB");

    ANIMTYPE_SKELETON.addAni("sprint", "HUMANS_SPRINT");

    ANIMTYPE_SKELETON.addAni("run", "S_FISTRUNL");     // -- fist/no weapon
    ANIMTYPE_SKELETON.addAni("run", "S_2HRUNL");       // -- 2h

    ANIMTYPE_SKELETON.addAni("walk", "S_WALKL");       // -- fist/no weapon
    ANIMTYPE_SKELETON.addAni("walk", "S_2HWALKL");     // -- 2h

    // -- other -- //
    ANIMTYPE_SKELETON.addAni("jumpBack", "T_FISTPARADEJUMPB");
    ANIMTYPE_SKELETON.addAni("strafe", "T_RUNSTRAFER");
    ANIMTYPE_SKELETON.addAni("strafe", "T_RUNSTRAFEL");
    ANIMTYPE_SKELETON.addAni("jump", "T_STAND_2_JUMP");
    ANIMTYPE_SKELETON.addAni("jump", "T_RUNL_2_JUMP");

    // -- sleep -- //
    ANIMTYPE_SKELETON.addAni("sleep", "T_STAND_2_SLEEPGROUND");
    ANIMTYPE_SKELETON.addAni("wakeup", "T_SLEEPGROUND_2_STAND");

    // -- draw -- //
    ANIMTYPE_SKELETON.addAni("draw", "T_2H_2_2HRUN");

    // -- sheath -- //
    ANIMTYPE_SKELETON.addAni("sheath", "T_2HRUN_2_2H");

    // -- attack -- //
    ANIMTYPE_SKELETON.addAni("attack2h", "S_2HATTACK");
    ANIMTYPE_SKELETON.addAni("attack2h", "T_2HATTACKR");
    ANIMTYPE_SKELETON.addAni("attack2h", "T_2HATTACKL");

    ANIMTYPE_SKELETON.addAni("attackFist", "S_FISTATTACK");
