WayPoints.WPs["OLDWORLD\\OLDWORLD.ZEN"] <-
{
	["LOCATION_28_07"] = {x = 23871.457, y = -553.283813, z = 27821.3516, adjacent =
	[
	]},
	["PATH_AROUND_PSI21"] = {x = 44986, y = -3850.00024, z = -32940, adjacent =
	[
	]},
	["PATH_TAKE_BLOODFLY_SPAWN"] = {x = 57509.0547, y = -4042.69141, z = -24442.543, adjacent =
	[
	]},
	["PATH_WALD_OC_MOLERATSPAWN"] = {x = 17322.2109, y = 254.08345, z = 6116.21875, adjacent =
	[
	]},
	["SPAWN_OW_BLOODFLY_C3"] = {x = -25134.9434, y = -759.662964, z = 14444.8408, adjacent =
	[
	]},
	["SPAWN_OW_GOBBO_01_01"] = {x = 8760.68164, y = 7523.69043, z = 23301.748, adjacent =
	[
	]},
	["SPAWN_OW_GOBBO_WATERFALLCAVE_2"] = {x = -28920.0664, y = -509.641052, z = 14950.9443, adjacent =
	[
	]},
	["SPAWN_OW_SNAPPER_WOOD05_05"] = {x = -20598.166, y = 130.863541, z = 21888.416, adjacent =
	[
	]},
	["SPAWN_OW_WARAN_DEMON_01"] = {x = -16446.4473, y = 2519.7583, z = -30634.2129, adjacent =
	[
	]},
	["TOT"] = {x = 12259.9668, y = -1808.73792, z = 23024.4375, adjacent =
	[
	]},
	["WP_INTRO_WI13"] = {x = 6400.34229, y = 6449.36084, z = 40277.8086, adjacent =
	[
	]},
	["WP_INTRO_WI14"] = {x = 6656.55713, y = 6448.18115, z = 40247.1484, adjacent =
	[
	]},
	["OW_ADD_22"] = {x = -13854.4932, y = 344.214325, z = -29280.3262, adjacent =
	[
		"SPAWN_OW_BLOCKGOBBO_CAVE_DM6",
	]},
	["SPAWN_OW_BLOCKGOBBO_CAVE_DM6"] = {x = -12690.001, y = 304.441895, z = -28706.8867, adjacent =
	[
		"OW_ADD_22",
		"OW_PATH_06_07",
	]},
	["OW_PATH_306"] = {x = -11577.3535, y = 339.564514, z = -26875.0586, adjacent =
	[
		"OW_ADD_21",
		"SPAWN_OW_SCAVENGER_ORC_03",
		"OW_PATH_305",
		"OW_PATH_06_07",
	]},
	["OW_ADD_21"] = {x = -12771.3672, y = 842.584839, z = -25857.7129, adjacent =
	[
		"OW_PATH_306",
		"OW_ADD_20",
	]},
	["CASTLE_3"] = {x = -4610.73975, y = 2102.14893, z = -19595.5605, adjacent =
	[
		"OW_ADD_15",
		"OW_PATH_106",
		"OW_PATH_109",
		"OW_PATH_06_01",
		"CASTLE_4",
	]},
	["OW_ADD_15"] = {x = -5621.55664, y = 2172.89355, z = -19303.9141, adjacent =
	[
		"CASTLE_3",
		"OW_PATH_106",
		"OW_ADD_14",
	]},
	["OW_PATH_106"] = {x = -5473.79248, y = 1968.43677, z = -18754.6191, adjacent =
	[
		"CASTLE_3",
		"OW_ADD_15",
		"CASTLE_2",
		"OW_PATH_105",
	]},
	["OW_ADD_14"] = {x = -6224.35938, y = 2564.36597, z = -20142.168, adjacent =
	[
		"OW_ADD_15",
		"OW_ADD_13",
	]},
	["OW_ADD_13"] = {x = -7754.72266, y = 2713.71094, z = -20657.1406, adjacent =
	[
		"OW_ADD_14",
		"OW_ADD_12",
	]},
	["OW_ADD_12"] = {x = -9751.2666, y = 2829.32764, z = -21651.998, adjacent =
	[
		"OW_ADD_13",
		"OW_ADD_11",
	]},
	["OW_ADD_11"] = {x = -11521.6777, y = 2559.40015, z = -21954.9102, adjacent =
	[
		"OW_ADD_12",
		"OW_ADD_10",
	]},
	["OW_ADD_20"] = {x = -11910.5283, y = 1394.12915, z = -24215.1152, adjacent =
	[
		"OW_ADD_21",
		"OW_ADD_10",
	]},
	["OW_ADD_10"] = {x = -12507.459, y = 2087.81348, z = -22137.4082, adjacent =
	[
		"OW_ADD_11",
		"OW_ADD_20",
		"OW_ORC_LOOKOUT_2_04",
	]},
	["OW_ORC_LOOKOUT_2_04"] = {x = -13133.4336, y = 2021.72009, z = -21358.0469, adjacent =
	[
		"OW_ADD_10",
		"SPAWN_OW_WARAN_ORC_01",
		"OW_ORC_LOOKOUT_2_05",
		"OW_ORC_LOOKOUT_2_03",
	]},
	["OW_ADD_09"] = {x = -24355.9492, y = -348.783813, z = 17138.4297, adjacent =
	[
		"OW_ADD_01",
		"MOVEMENT_OW_MOLERAT_WOODOLDMINE2",
		"OW_ADD_02",
	]},
	["OW_ADD_01"] = {x = -27857.8926, y = -304.085907, z = 16901.5039, adjacent =
	[
		"OW_ADD_09",
		"OW_ADD_02",
		"OW_PATH_SCAVENGER12_SPAWN01",
		"OW_PATH_155",
	]},
	["MOVEMENT_OW_MOLERAT_WOODOLDMINE2"] = {x = -22086.6758, y = -303.388397, z = 17881.1621, adjacent =
	[
		"OW_ADD_09",
		"SPAWN_OW_MOLERAT_WOODOLDMINE2",
		"SPAWN_OW_BLOODFLY_WOOD05_01",
	]},
	["OW_ADD_02"] = {x = -25829.6621, y = -687.245361, z = 18406.0156, adjacent =
	[
		"OW_ADD_09",
		"OW_ADD_01",
		"SPAWN_OW_SCAVENGER_OCWOODEND2",
	]},
	["SPAWN_OW_SCAVENGER_OCWOODEND2"] = {x = -25870.543, y = -591.460388, z = 19653.623, adjacent =
	[
		"OW_ADD_02",
		"OW_PATH_264",
		"OW_PATH_263",
	]},
	["MOVEMENT_OW_PATH_SCAVENGER01_SPAWN01"] = {x = -10783.2168, y = -598.302185, z = 16046.2168, adjacent =
	[
		"OW_PATH_246",
		"OW_PATH_SCAVENGER01_SPAWN01",
		"OW_PATH_245",
	]},
	["OW_PATH_246"] = {x = -11969.0449, y = -619.527527, z = 15088.6602, adjacent =
	[
		"MOVEMENT_OW_PATH_SCAVENGER01_SPAWN01",
		"OW_PATH_245",
		"OW_PATH_247",
	]},
	["OW_PATH_WOLF08_SPAWN01"] = {x = -14503.3154, y = -150.200348, z = 18715.5332, adjacent =
	[
		"OW_PATH_SCAVENGER01_SPAWN01",
		"OW_ADD_08",
	]},
	["OW_PATH_SCAVENGER01_SPAWN01"] = {x = -11121.9072, y = -220.56488, z = 17929.0488, adjacent =
	[
		"MOVEMENT_OW_PATH_SCAVENGER01_SPAWN01",
		"OW_PATH_WOLF08_SPAWN01",
		"OW_PATH_183",
	]},
	["SPAWN_GOBBO_OW_PATH_1_6"] = {x = -5928.48926, y = -661.216431, z = 16549.5469, adjacent =
	[
		"OW_PATH_1_5_B",
		"OW_PATH_1_5",
		"OW_PATH_182",
		"OW_PATH_1_5_A",
	]},
	["OW_PATH_1_5_B"] = {x = -5573.01074, y = -849.186401, z = 15458.832, adjacent =
	[
		"SPAWN_GOBBO_OW_PATH_1_6",
		"OW_PATH_1_5_12_1",
		"OW_PATH_1_5_A",
	]},
	["OW_ADD_05"] = {x = -15198.3125, y = 347.648163, z = 20229.7402, adjacent =
	[
		"OW_ADD_08",
		"OW_ADD_06",
		"OW_ADD_04",
	]},
	["OW_ADD_08"] = {x = -13913.2744, y = -20.0478363, z = 19473.3711, adjacent =
	[
		"OW_PATH_WOLF08_SPAWN01",
		"OW_ADD_05",
		"OW_PATH_184",
	]},
	["OW_PATH_184"] = {x = -12710.1064, y = 249.241302, z = 20759.873, adjacent =
	[
		"OW_ADD_08",
		"OW_ADD_06",
		"OW_PATH_185",
		"OW_PATH_186",
		"OW_PATH_183",
	]},
	["OW_ADD_06"] = {x = -14179.8486, y = 345.194214, z = 20951.7422, adjacent =
	[
		"OW_ADD_05",
		"OW_PATH_184",
	]},
	["OW_ADD_04"] = {x = -15649.584, y = 286.156677, z = 19570.0391, adjacent =
	[
		"OW_ADD_05",
		"OW_ADD_03",
	]},
	["OW_ADD_03"] = {x = -16653.3516, y = 117.215302, z = 19116.1211, adjacent =
	[
		"OW_ADD_04",
		"SPAWN_OW_SCAVENGER_WOOD10_04",
	]},
	["SPAWN_OW_SCAVENGER_WOOD10_04"] = {x = -17780.6074, y = 25.0721588, z = 19399.4297, adjacent =
	[
		"OW_ADD_03",
		"SPAWN_OW_BLOODFLY_WOOD05_01",
	]},
	["OW_PATH_SCAVENGER12_SPAWN01"] = {x = -29232.1328, y = -419.315247, z = 18197.0215, adjacent =
	[
		"OW_ADD_01",
		"OW_PATH_155",
		"OW_PATH_147",
	]},
	["OW_PATH_155"] = {x = -30990.5996, y = 372.526855, z = 16507.168, adjacent =
	[
		"OW_ADD_01",
		"OW_PATH_SCAVENGER12_SPAWN01",
		"SPAWN_OW_MOLERAT_A_6_NC4",
		"OW_PATH_147",
		"OW_PATH_149",
	]},
	["MOVEMENT_OW_PATH_SCAVENGER13_SPAWN01"] = {x = -30877.1445, y = 924.96051, z = 10135.8389, adjacent =
	[
		"MOVEMENT_OW_PATH_SCAVENGER13_SPAWN02",
		"OW_PATH_153",
		"OW_PATH_SCAVENGER13_SPAWN01",
		"MOVEMENT_OW_PATH_SCAVENGER13_SPAWN03",
	]},
	["MOVEMENT_OW_PATH_SCAVENGER13_SPAWN02"] = {x = -30125.0918, y = 863.368469, z = 10757.9727, adjacent =
	[
		"MOVEMENT_OW_PATH_SCAVENGER13_SPAWN01",
	]},
	["DT_PLATFORM_01"] = {x = -11323.4658, y = 3655.93433, z = -33930.3281, adjacent =
	[
		"DT_PLATFORM_02",
		"DT_E3_03",
	]},
	["DT_PLATFORM_02"] = {x = -11432.0908, y = 3668.18408, z = -33530.1641, adjacent =
	[
		"DT_PLATFORM_01",
	]},
	["DT_E2_07"] = {x = -12073.7178, y = 2888.47925, z = -34635.2891, adjacent =
	[
		"DT_E2_06",
	]},
	["DT_E2_06"] = {x = -12381.0381, y = 2888.41479, z = -34285.9219, adjacent =
	[
		"DT_E2_07",
		"DT_LABOURY",
		"DT_E2_09",
		"DT_E2_08",
	]},
	["DT_E1_02"] = {x = -11024.6074, y = 1996.72278, z = -34675.7109, adjacent =
	[
		"DT_E1_03",
		"DT_E1_01",
	]},
	["DT_E1_03"] = {x = -10967.2764, y = 1998.01294, z = -34406.9023, adjacent =
	[
		"DT_E1_02",
		"DT_E2_01",
	]},
	["DT_E1_01"] = {x = -11294.333, y = 1994.38684, z = -34632.7461, adjacent =
	[
		"DT_E1_02",
		"DT_MAINGATE",
		"DT_E1_04",
	]},
	["DT_MAINGATE"] = {x = -11328.665, y = 1984.56079, z = -34855.2891, adjacent =
	[
		"DT_E1_01",
	]},
	["DT_E1_04"] = {x = -11482.9111, y = 1993.22717, z = -34070.3438, adjacent =
	[
		"DT_E1_01",
		"DT_E1_05",
		"DT_LIVINGROOM",
	]},
	["DT_E1_05"] = {x = -11342.8838, y = 1993.22717, z = -33515.3516, adjacent =
	[
		"DT_E1_04",
	]},
	["DT_LIVINGROOM"] = {x = -12042.1084, y = 1993.22717, z = -34122.8516, adjacent =
	[
		"DT_E1_04",
		"DT_E1_06",
	]},
	["DT_E1_06"] = {x = -12447.9639, y = 1993.22717, z = -34267.6719, adjacent =
	[
		"DT_LIVINGROOM",
		"DT_E1_08",
		"DT_E1_07",
		"DT_E1_09",
	]},
	["DT_E1_08"] = {x = -12011.1523, y = 1993.22717, z = -34845.1602, adjacent =
	[
		"DT_E1_06",
	]},
	["DT_E1_07"] = {x = -12755.8291, y = 1993.22717, z = -34507.875, adjacent =
	[
		"DT_E1_06",
	]},
	["DT_E1_09"] = {x = -12299.6377, y = 1993.22729, z = -33616.125, adjacent =
	[
		"DT_E1_06",
	]},
	["DT_E2_02"] = {x = -10927.415, y = 2805.1106, z = -34623.9727, adjacent =
	[
		"DT_E2_01",
		"DT_E2_03",
	]},
	["DT_E2_01"] = {x = -10855.501, y = 2877.25635, z = -34337.5703, adjacent =
	[
		"DT_E1_03",
		"DT_E2_02",
		"DT_E3_01",
	]},
	["DT_E2_03"] = {x = -11376.334, y = 2888.52441, z = -34493.1094, adjacent =
	[
		"DT_E2_02",
		"DT_E2_04",
	]},
	["DT_E2_04"] = {x = -11482.3047, y = 2888.47925, z = -34139.4492, adjacent =
	[
		"DT_E2_03",
		"DT_E2_05",
		"DT_LABOURY",
	]},
	["DT_E2_05"] = {x = -11453.0752, y = 2888.47925, z = -33708.7852, adjacent =
	[
		"DT_E2_04",
	]},
	["DT_LABOURY"] = {x = -11886.7852, y = 2888.47925, z = -34148.1758, adjacent =
	[
		"DT_E2_06",
		"DT_E2_04",
	]},
	["DT_E2_09"] = {x = -12179.7549, y = 2888.47925, z = -33675.4531, adjacent =
	[
		"DT_E2_06",
	]},
	["DT_E2_08"] = {x = -12656.8984, y = 2891.18408, z = -34446.4688, adjacent =
	[
		"DT_E2_06",
	]},
	["DT_E3_02"] = {x = -11082.3252, y = 3649.28491, z = -34403.8477, adjacent =
	[
		"DT_E3_01",
		"DT_E3_03",
	]},
	["DT_E3_01"] = {x = -10952.3975, y = 3649.24634, z = -34322.9531, adjacent =
	[
		"DT_E2_01",
		"DT_E3_02",
	]},
	["DT_E3_03"] = {x = -11382.543, y = 3651.06323, z = -34334.9258, adjacent =
	[
		"DT_PLATFORM_01",
		"DT_E3_02",
		"DT_LIBRARY",
	]},
	["DT_LIBRARY"] = {x = -11901.5244, y = 3641.7041, z = -34220.6367, adjacent =
	[
		"DT_E3_03",
		"DT_E3_07",
		"DT_E3_04",
	]},
	["DT_E3_05"] = {x = -12507.4434, y = 3637.63599, z = -33917.6914, adjacent =
	[
		"DT_E3_06",
		"DT_E3_04",
	]},
	["DT_E3_06"] = {x = -12566.6104, y = 3643.70215, z = -34231.4688, adjacent =
	[
		"DT_E3_05",
		"DT_E3_07",
	]},
	["DT_E3_07"] = {x = -12144.0479, y = 3645.93213, z = -34515.7617, adjacent =
	[
		"DT_LIBRARY",
		"DT_E3_06",
	]},
	["DT_E3_04"] = {x = -11989.3828, y = 3637.63574, z = -33862.7617, adjacent =
	[
		"DT_LIBRARY",
		"DT_E3_05",
	]},
	["OW_MINE3_04"] = {x = 456.000031, y = 2564.35767, z = -28044.002, adjacent =
	[
		"OW_MINE3_LEICHE_09",
		"OW_MINE3_03",
		"OW_MINE3_LEICHE_06",
	]},
	["OW_MINE3_LEICHE_09"] = {x = 633.371948, y = 2564.35767, z = -28034.5176, adjacent =
	[
		"OW_MINE3_04",
		"OW_MINE3_05",
	]},
	["OW_MINE3_06"] = {x = 1958, y = 2547.23389, z = -27877, adjacent =
	[
		"OW_MINE3_LEICHE_01",
		"OW_MINE3_05",
		"OW_MINE3_07",
	]},
	["OW_MINE3_LEICHE_01"] = {x = 1769.26648, y = 2532.32715, z = -28191.832, adjacent =
	[
		"OW_MINE3_06",
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC_MOVE"] = {x = -1088.01221, y = 255.869308, z = -13832.2803, adjacent =
	[
		"OW_WOLFHUT_01",
		"FP_ROAM_OW_SNAPPER_OW_ORC",
	]},
	["OW_WOLFHUT_01"] = {x = -1849.97839, y = 323.867523, z = -14087.3477, adjacent =
	[
		"FP_ROAM_OW_SNAPPER_OW_ORC_MOVE",
	]},
	["OW_NEWMINE_07"] = {x = -14527.4238, y = 808.556702, z = -26306.4922, adjacent =
	[
		"OW_NEWMINE_04",
		"OW_NEWMINE_08_B",
		"OW_NEWMINE_03",
		"OW_NEWMINE_11",
	]},
	["OW_NEWMINE_04"] = {x = -14944, y = 793.615234, z = -26783, adjacent =
	[
		"OW_NEWMINE_07",
		"OW_NEWMINE_08_B",
		"OW_NEWMINE_03",
		"OW_NEWMINE_02",
	]},
	["OW_NEWMINE_08_B"] = {x = -14467.6328, y = 719.792969, z = -27255.6074, adjacent =
	[
		"OW_NEWMINE_07",
		"OW_NEWMINE_04",
		"OW_NEWMINE_08",
	]},
	["OW_NEWMINE_08"] = {x = -14068.1162, y = 605.328552, z = -27493.8574, adjacent =
	[
		"OW_NEWMINE_08_B",
		"OW_PATH_06_07",
	]},
	["OW_NEWMINE_06_B"] = {x = -17434, y = 824.534424, z = -25596, adjacent =
	[
		"OW_NEWMINE_01_B",
	]},
	["OW_NEWMINE_01_B"] = {x = -16819, y = 799.531921, z = -25556, adjacent =
	[
		"OW_NEWMINE_06_B",
		"OW_NEWMINE_05",
		"OW_NEWMINE_01",
	]},
	["OW_NEWMINE_05"] = {x = -17268, y = 826.74646, z = -25171, adjacent =
	[
		"OW_NEWMINE_01_B",
	]},
	["OW_NEWMINE_01"] = {x = -16279.0654, y = 775.425415, z = -25807.9551, adjacent =
	[
		"OW_NEWMINE_01_B",
		"OW_NEWMINE_02",
	]},
	["OW_MINE3_LEICHE_07"] = {x = -269, y = 2580.72754, z = -27869.002, adjacent =
	[
		"OW_MINE3_03",
	]},
	["OW_MINE3_03"] = {x = -377, y = 2564.27515, z = -27972, adjacent =
	[
		"OW_MINE3_04",
		"OW_MINE3_LEICHE_07",
		"OW_MINE3_02",
	]},
	["OW_MINE3_LEICHE_02"] = {x = -1655, y = 2712.11353, z = -27405, adjacent =
	[
		"OW_MINE3_LEFT_01",
	]},
	["OW_MINE3_LEFT_01"] = {x = -1720, y = 2704.63501, z = -27459.002, adjacent =
	[
		"OW_MINE3_LEICHE_02",
		"OW_MINE3_01",
		"OW_MINE3_LEFT_02",
	]},
	["OW_MINE3_05"] = {x = 1071, y = 2585.02979, z = -28031, adjacent =
	[
		"OW_MINE3_LEICHE_09",
		"OW_MINE3_06",
	]},
	["OW_MINE3_07"] = {x = 2157, y = 2484.625, z = -28361, adjacent =
	[
		"OW_MINE3_06",
		"OW_MINE3_08",
	]},
	["OW_MINE3_08"] = {x = 2409, y = 2392.44946, z = -29003, adjacent =
	[
		"OW_MINE3_07",
		"OW_MINE3_09",
	]},
	["OW_MINE3_09"] = {x = 2643.00024, y = 2203.6311, z = -29985, adjacent =
	[
		"OW_MINE3_08",
		"OW_MINE3_10",
	]},
	["OW_MINE3_10"] = {x = 2250.89893, y = 2267.52979, z = -30794.2832, adjacent =
	[
		"OW_MINE3_09",
	]},
	["OW_MINE3_01"] = {x = -1763, y = 2720.77881, z = -27885, adjacent =
	[
		"OW_MINE3_LEFT_01",
		"OW_MINE3_IN",
		"OW_MINE3_02",
		"OW_MINE3_LEICHE_08",
	]},
	["OW_MINE3_LEFT_02"] = {x = -1539.00012, y = 2773.04517, z = -27098, adjacent =
	[
		"OW_MINE3_LEFT_01",
		"OW_MINE3_LEFT_03",
	]},
	["OW_MINE3_LEFT_03"] = {x = -1332, y = 2852.69727, z = -26579.002, adjacent =
	[
		"OW_MINE3_LEFT_02",
		"OW_MINE3_LEFT_04",
	]},
	["OW_MINE3_LEFT_04"] = {x = -1184.87183, y = 2883.73071, z = -26067.6758, adjacent =
	[
		"OW_MINE3_LEFT_03",
		"OW_MINE3_LEFT_05",
		"OW_MINE3_LEFT_06",
	]},
	["OW_MINE3_LEFT_05"] = {x = -1768, y = 2867.36084, z = -25779.002, adjacent =
	[
		"OW_MINE3_LEFT_04",
	]},
	["OW_MINE3_LEFT_06"] = {x = -985, y = 2883.73071, z = -25807, adjacent =
	[
		"OW_MINE3_LEFT_04",
		"OW_MINE3_LEFT_07",
	]},
	["OW_MINE3_LEFT_07"] = {x = -680, y = 2867.36084, z = -25171, adjacent =
	[
		"OW_MINE3_LEFT_06",
	]},
	["OW_MINE3_OUT"] = {x = -3167.51685, y = 2729.33545, z = -28861.707, adjacent =
	[
		"OW_MINE3_LEICHE_05",
		"OW_MINE3_IN",
		"SPAWN_OW_SCAVENGER_01_DEMONT5",
	]},
	["OW_MINE3_LEICHE_05"] = {x = -2864.73608, y = 2729.7644, z = -28987.9551, adjacent =
	[
		"OW_MINE3_OUT",
	]},
	["OW_MINE3_IN"] = {x = -2173.54053, y = 2724.87817, z = -28070.5586, adjacent =
	[
		"OW_MINE3_01",
		"OW_MINE3_OUT",
		"OW_MINE3_LEICHE_04",
	]},
	["OW_MINE3_LEICHE_04"] = {x = -1959.00012, y = 2724.87817, z = -28091, adjacent =
	[
		"OW_MINE3_IN",
	]},
	["OW_MINE3_02"] = {x = -889, y = 2593.95532, z = -27893, adjacent =
	[
		"OW_MINE3_03",
		"OW_MINE3_01",
		"OW_MINE3_LEICHE_03",
	]},
	["OW_MINE3_LEICHE_03"] = {x = -944, y = 2614.47144, z = -28083, adjacent =
	[
		"OW_MINE3_02",
	]},
	["SPAWN_OW_SCAVENGER_01_DEMONT5"] = {x = -3959.43408, y = 2690.78491, z = -29916.4121, adjacent =
	[
		"OW_MINE3_OUT",
		"OW_PATH_302",
		"OW_PATH_300",
	]},
	["OW_PATH_272"] = {x = -12863, y = 377.761688, z = -10356, adjacent =
	[
		"OW_GATE_ORCS_02",
		"OW_PATH_3_03",
		"OW_GATE_ORCS_03",
	]},
	["OW_GATE_ORCS_02"] = {x = -12297.0459, y = 224.873901, z = -9502.73633, adjacent =
	[
		"OW_PATH_272",
		"SPAWN_PATH_GUARD1",
		"OW_GATE_ORCS_01",
	]},
	["SPAWN_PATH_GUARD1"] = {x = -12106.7383, y = 68.8782196, z = -8767.74902, adjacent =
	[
		"OW_GATE_ORCS_02",
		"MOVEMENT_PATH_GUARD1",
		"MOVE_OW_SCAVENGER_AL_ORC_MOVEMENT",
		"MOVEMENT_PATH_GUARD2",
		"OW_GATE_ORCS_01",
	]},
	["OW_MINE2_04"] = {x = -24813.2246, y = -816.394226, z = 22652.8906, adjacent =
	[
		"OW_MINE2_STRF",
		"OW_MINE2_GRIMES",
		"OW_MINE2_03",
	]},
	["OW_MINE2_STRF"] = {x = -24620.8438, y = -816.394226, z = 22869.498, adjacent =
	[
		"OW_MINE2_04",
	]},
	["OW_SPAWN_SCAVENGER_01"] = {x = 10891.1816, y = -1318.51794, z = 10947.1943, adjacent =
	[
		"OW_SPAWN_SCAVENGER_02",
		"SPAWN_OW_LURKER_RIVER2_B",
	]},
	["OW_SPAWN_SCAVENGER_02"] = {x = 9927.35059, y = -1326.60864, z = 10294.1943, adjacent =
	[
		"OW_SPAWN_SCAVENGER_01",
		"SPAWN_OW_LURKER_RIVER2_B",
	]},
	["SPAWN_OW_LURKER_RIVER2_B"] = {x = 10424.4707, y = -1335.03857, z = 9746.74219, adjacent =
	[
		"OW_SPAWN_SCAVENGER_01",
		"OW_SPAWN_SCAVENGER_02",
		"OW_PATH_004",
		"SPAWN_OW_LURKER_RIVER2",
	]},
	["OW_PATH_004"] = {x = 8940.47754, y = -1333.98657, z = 8848.18262, adjacent =
	[
		"SPAWN_OW_LURKER_RIVER2_B",
		"PATH_OC_OCWOOD",
		"O_SCAVENGER_OCWOODL2_MOVEMENT",
		"OC_ROUND_5",
		"SPAWN_O_SCAVENGER_OCWOODL2",
		"OC_ROUND_6",
		"OW_PATH_OW_PATH_WARAN05_SPAWN01",
	]},
	["SPAWN_OW_LURKER_RIVER2"] = {x = 12268.0488, y = -1287.52124, z = 11005.4365, adjacent =
	[
		"SPAWN_OW_LURKER_RIVER2_B",
		"MOVEMENT_OW_SNAPPER_OCWOOD1_05_02",
		"SPAWN_OW_LURKER_RIVER2_BEACH3_2",
	]},
	["OW_MINE2_GRIMES"] = {x = -24264.0039, y = -815.19281, z = 22709.6602, adjacent =
	[
		"OW_MINE2_04",
	]},
	["OW_PATH_1_5_11"] = {x = -2023.8501, y = -913.099609, z = 13739.0283, adjacent =
	[
		"OW_STAND_JERGAN",
		"OW_PATH_1_5_12_1",
		"OW_PATH_1_5_1",
	]},
	["OW_STAND_JERGAN"] = {x = -2564.55225, y = -1079.10913, z = 13508.1582, adjacent =
	[
		"OW_PATH_1_5_11",
		"OW_PATH_1_5_12_2",
	]},
	["OW_PATH_1_5_12_2"] = {x = -3928.15674, y = -1262.08362, z = 13712.5117, adjacent =
	[
		"OW_STAND_JERGAN",
		"OW_RIVERBED_01",
		"OW_PATH_1_5_12_1",
		"OW_PATH_1_5_13",
	]},
	["OW_SPAWN_BRUDER"] = {x = 346.799866, y = 2723.42896, z = 21203.8984, adjacent =
	[
		"SPAWN_TOTURIAL_CHICKEN_2_2",
	]},
	["SPAWN_TOTURIAL_CHICKEN_2_2"] = {x = 92.2593765, y = 2692.64526, z = 21340.1191, adjacent =
	[
		"OW_SPAWN_BRUDER",
		"OW_PATH_1_12",
	]},
	["OW_MINE2_03"] = {x = -25573.166, y = -781.45813, z = 22521.8691, adjacent =
	[
		"OW_MINE2_04",
		"OW_MINE2_02",
	]},
	["OW_MINE2_02"] = {x = -26043.0684, y = -699.622314, z = 22443.8926, adjacent =
	[
		"OW_MINE2_03",
		"OW_MINE2_01",
		"OW_MINE2_05",
	]},
	["OW_MINE2_01"] = {x = -26436.4121, y = -683.534729, z = 22195.7891, adjacent =
	[
		"OW_MINE2_02",
		"OW_PATH_2_04",
		"OW_MINE2_05",
	]},
	["OW_PATH_2_04"] = {x = -26699.4199, y = -685.192505, z = 21887.8242, adjacent =
	[
		"OW_MINE2_01",
		"OW_PATH_265",
	]},
	["OW_MINE2_05"] = {x = -26227.3555, y = -702.339844, z = 22702.375, adjacent =
	[
		"OW_MINE2_02",
		"OW_MINE2_01",
		"OW_MINE2_06",
	]},
	["OW_MINE2_06"] = {x = -26200.3438, y = -636.476807, z = 23113.2832, adjacent =
	[
		"OW_MINE2_05",
		"OW_MINE2_07",
	]},
	["OW_MINE2_07"] = {x = -26099.0625, y = -562.069458, z = 23632.0957, adjacent =
	[
		"OW_MINE2_06",
		"OW_MINE2_08",
	]},
	["OW_MINE2_08"] = {x = -25948.168, y = -565.603271, z = 24223.5449, adjacent =
	[
		"OW_MINE2_07",
	]},
	["OC_ROUND_17"] = {x = -8673.86523, y = -415.637695, z = -7795.07324, adjacent =
	[
		"OC_ROUND_MOVE_TUNING_03",
		"MOVE_TUNING_01",
		"SPAWN_OW_SCAVENGER_06_04",
		"MOVEMENT_MOLERAT_06_CAVE_GUARD2",
		"OC_ROUND_18",
	]},
	["OC_ROUND_MOVE_TUNING_03"] = {x = -8165.60547, y = -568.274719, z = -7696.98096, adjacent =
	[
		"OC_ROUND_17",
		"OC_ROUND_MOVE_TUNING_02",
	]},
	["OC_ROUND_MOVE_TUNING_02"] = {x = -7058.48926, y = -560.843262, z = -8259.94727, adjacent =
	[
		"OC_ROUND_MOVE_TUNING_03",
		"OC7",
	]},
	["OC7"] = {x = -6433.06934, y = -662.408508, z = -8082.28516, adjacent =
	[
		"OC_ROUND_MOVE_TUNING_02",
		"SPAWN_OW_SCAVENGER_06_04",
		"OC6",
		"OC7_NAVIGATION",
		"OC_ORK_BACK_CAMP_07",
		"OC_ORK_BACK_CAMP_10",
	]},
	["MOVE_TUNING_01"] = {x = -6821.00439, y = -364.800293, z = -9168.6543, adjacent =
	[
		"OC_ROUND_17",
		"SPAWN_OW_SCAVENGER_06_04",
	]},
	["SPAWN_OW_SCAVENGER_06_04"] = {x = -6687.45801, y = -458.875854, z = -8784.1582, adjacent =
	[
		"OC_ROUND_17",
		"OC7",
		"MOVE_TUNING_01",
		"OC_ROUND_15",
		"LOCATION_01_01",
	]},
	["OW_PATH_BLOODFLY01_SPAWN01"] = {x = 7620.39453, y = -1306.88171, z = 14227.1104, adjacent =
	[
		"OW_RIVERBED_08",
		"OW_PATH_BLOODFLY01_MOVEMENT",
		"OW_PATH_WARAN05_SPAWN02",
	]},
	["OW_RIVERBED_08"] = {x = 6197.87988, y = -1417.83386, z = 16569.3789, adjacent =
	[
		"OW_PATH_BLOODFLY01_SPAWN01",
		"OW_RIVERBED_07",
	]},
	["OW_RIVERBED_07"] = {x = 3752.41895, y = -1318.35156, z = 18061.1816, adjacent =
	[
		"OW_RIVERBED_08",
		"OW_RIVERBED_06",
	]},
	["OW_RIVERBED_06"] = {x = 1722.22778, y = -1307.69263, z = 17267.957, adjacent =
	[
		"OW_RIVERBED_07",
		"OW_RIVERBED_05",
	]},
	["OW_RIVERBED_05"] = {x = -382.006561, y = -1457.06372, z = 13975.5771, adjacent =
	[
		"OW_RIVERBED_06",
		"OW_RIVERBED_04",
	]},
	["OW_RIVERBED_04"] = {x = -1785.88098, y = -1492.09778, z = 13265.3018, adjacent =
	[
		"OW_RIVERBED_05",
		"OW_RIVERBED_03",
	]},
	["OW_RIVERBED_03"] = {x = -2897.6936, y = -1511.83887, z = 13148.1465, adjacent =
	[
		"OW_RIVERBED_04",
		"OW_RIVERBED_02",
	]},
	["OW_RIVERBED_02"] = {x = -3901.9585, y = -1470.71655, z = 13169.7988, adjacent =
	[
		"OW_RIVERBED_03",
		"OW_RIVERBED_01",
		"MOVEMENT_OW_BLOODFLY_E_3",
	]},
	["OW_RIVERBED_01"] = {x = -4027.2478, y = -1302.68396, z = 13503.1436, adjacent =
	[
		"OW_PATH_1_5_12_2",
		"OW_RIVERBED_02",
	]},
	["MOVEMENT_OW_BLOODFLY_E_3"] = {x = -3509.72998, y = -1429.89124, z = 11778.9736, adjacent =
	[
		"OW_RIVERBED_02",
		"SPAWN_OW_BLOODFLY_E_3",
		"MOVEMENT_OW_BLOODFLY_E_4",
	]},
	["SPAWN_OW_MOLERAT_CAVE1_OC_01"] = {x = -12485.6816, y = -838.185547, z = 6133.9917, adjacent =
	[
		"SPAWN_OW_MOLERAT_CAVE1_OC",
		"PATH_OC_NC_GOBBOCAVE_01",
	]},
	["SPAWN_OW_MOLERAT_CAVE1_OC"] = {x = -12915.8604, y = -750.255859, z = 5552.53369, adjacent =
	[
		"SPAWN_OW_MOLERAT_CAVE1_OC_01",
	]},
	["PATH_OC_NC_GOBBOCAVE_01"] = {x = -12060.9004, y = -938.377991, z = 6937.50342, adjacent =
	[
		"SPAWN_OW_MOLERAT_CAVE1_OC_01",
		"SPAWN_OW_SCAVENGER_BANDIT_02",
		"PATH_OC_NC",
	]},
	["SPAWN_OW_SCAVENGER_BANDIT_02"] = {x = -13783.7568, y = -785.541199, z = 7665.77246, adjacent =
	[
		"PATH_OC_NC_GOBBOCAVE_01",
		"MOVEMENT_TALL_PATH_03",
	]},
	["PATH_OC_NC"] = {x = -10900.3193, y = -1109.91614, z = 6741.56885, adjacent =
	[
		"PATH_OC_NC_GOBBOCAVE_01",
		"PATH_OC_NC_28",
		"OW_PATH_1",
		"PATH_OC_NC_1",
	]},
	["OW_HOSHPAK_05"] = {x = 1234.63965, y = 1596.84802, z = -11910.9707, adjacent =
	[
		"OW_HOSHPAK_02",
		"OW_HOSHPAK_04",
	]},
	["OW_HOSHPAK_02"] = {x = 2190.23486, y = 1411.07495, z = -12906.0654, adjacent =
	[
		"OW_HOSHPAK_05",
		"OW_HOSHPAK_03",
		"OW_HOSHPAK_01",
	]},
	["OW_HOSHPAK_04"] = {x = 1823.6792, y = 1481.6239, z = -10948.0791, adjacent =
	[
		"OW_HOSHPAK_05",
		"OW_HOSHPAK_03",
	]},
	["OW_HOSHPAK_03"] = {x = 3249.39551, y = 1528.95715, z = -11613.999, adjacent =
	[
		"OW_HOSHPAK_02",
		"OW_HOSHPAK_04",
	]},
	["OW_HOSHPAK_01"] = {x = 2382.13037, y = 1145.21545, z = -13573.6602, adjacent =
	[
		"OW_HOSHPAK_02",
		"OW_PATH_016_B",
	]},
	["OW_PATH_016_B"] = {x = 2789.46753, y = 765.766846, z = -14389.5889, adjacent =
	[
		"OW_HOSHPAK_01",
		"OW_PATH_015",
		"OW_PATH_016",
	]},
	["OW_PATH_015"] = {x = 2057.71143, y = 780.486633, z = -14744.8701, adjacent =
	[
		"OW_PATH_016_B",
		"FP_ROAM_OW_SNAPPER_OW_ORC",
		"OW_PATH_019",
	]},
	["OW_PATH_016"] = {x = 3530.46924, y = 751.196167, z = -14170.7139, adjacent =
	[
		"OW_PATH_016_B",
		"OW_PATH_017",
	]},
	["OW_DRAGONSWAMP_006"] = {x = -21439.7813, y = -1414.7688, z = -10134.9023, adjacent =
	[
		"OW_DJG_SWAMP_WAIT2_02",
		"OW_DJG_SWAMP_WAIT2_01",
		"OW_DRAGONSWAMP_005",
		"OW_DRAGONSWAMP_019",
		"OW_DRAGONSWAMP_018",
		"OW_DRAGONSWAMP_007",
		"OW_DJG_SWAMP_WAIT2_03",
		"SWAMPDRAGON",
	]},
	["OW_DJG_SWAMP_WAIT2_02"] = {x = -21552.873, y = -1414.76855, z = -9837.40918, adjacent =
	[
		"OW_DRAGONSWAMP_006",
	]},
	["OW_DJG_SWAMP_WAIT2_01"] = {x = -21325.9883, y = -1414.76917, z = -10247.125, adjacent =
	[
		"OW_DRAGONSWAMP_006",
	]},
	["OW_PATH_063"] = {x = -34856.4766, y = 520.635132, z = 4894.02295, adjacent =
	[
		"OW_NC_ABYSS_01",
		"OW_PATH_062",
		"OW_DJG_ICECAMP_05",
		"OW_PATH_066",
	]},
	["OW_NC_ABYSS_01"] = {x = -33297.6367, y = 83.4658203, z = 4565.84863, adjacent =
	[
		"OW_PATH_063",
		"OW_PATH_062",
		"OW_NC_ABYSS",
	]},
	["OW_PATH_062"] = {x = -33815.9766, y = 401.838684, z = 5834.72363, adjacent =
	[
		"OW_PATH_063",
		"OW_NC_ABYSS_01",
		"OW_PATH_061",
	]},
	["OW_NC_ABYSS"] = {x = -32222.1914, y = -185.854965, z = 6177.44141, adjacent =
	[
		"OW_NC_ABYSS_01",
		"OW_NC_ABYSS2",
	]},
	["START"] = {x = 4366.46533, y = 5900.20508, z = 27707.8223, adjacent =
	[
		"OW_PATH_1_17",
	]},
	["OW_PATH_1_17"] = {x = 4337.5166, y = 5885.50732, z = 27470.25, adjacent =
	[
		"START",
		"OW_PATH_1_15",
		"OW_PATH_1_17_1",
		"OW_PATH_1_16",
		"OW_VM_ENTRANCE",
	]},
	["OW_DJG_STARTCAMP_01"] = {x = 1263.76135, y = 2729.9353, z = 21218.0059, adjacent =
	[
		"OW_PATH_1_5_3A",
	]},
	["OW_PATH_1_5_3A"] = {x = 1786.45068, y = 2718.88379, z = 21510.4434, adjacent =
	[
		"OW_DJG_STARTCAMP_01",
		"OW_PATH_1_5_3",
		"OW_PATH_1_5_4",
	]},
	["OW_PATH_1_5_3"] = {x = 1472.6936, y = 2830.55225, z = 22050.0039, adjacent =
	[
		"OW_PATH_1_5_3A",
		"OW_PATH_1_12",
	]},
	["OW_PATH_1_5_4"] = {x = 2690.31323, y = 2420.94458, z = 20515.7285, adjacent =
	[
		"OW_PATH_1_5_3A",
		"OW_PATH_1_5_5",
	]},
	["OW_ICEREGION_35"] = {x = -46791.2188, y = 1973.98657, z = 14273.584, adjacent =
	[
		"OW_DJG_ICEREGION_WAIT1_02",
		"OW_DJG_ICEREGION_WAIT1_01",
		"OW_ICEREGION_34",
		"OW_ICEREGION_36",
	]},
	["OW_DJG_ICEREGION_WAIT1_02"] = {x = -46837.2383, y = 1964.453, z = 14196.3945, adjacent =
	[
		"OW_ICEREGION_35",
	]},
	["OW_DJG_ICEREGION_WAIT1_01"] = {x = -46603.8672, y = 2041.31152, z = 14654.7266, adjacent =
	[
		"OW_ICEREGION_35",
	]},
	["OW_ICEREGION_40"] = {x = -48800.9609, y = 2725.82104, z = 11996.4492, adjacent =
	[
		"OW_ICEREGION_93",
		"OW_ICEREGION_39",
		"OW_ICEREGION_DAM",
		"OW_ICEREGION_95",
	]},
	["OW_ICEREGION_93"] = {x = -49521.0898, y = 2563.10059, z = 12139.9873, adjacent =
	[
		"OW_ICEREGION_40",
		"OW_ICEREGION_71",
		"OW_ICEREGION_72",
	]},
	["OW_ICEREGION_90"] = {x = -58272.5117, y = 3143.99585, z = 17214.7168, adjacent =
	[
		"OW_ICEREGION_87",
		"OW_ICEREGION_91",
		"OW_ICEREGION_92",
	]},
	["OW_ICEREGION_87"] = {x = -59366.2539, y = 3444.479, z = 19091.8984, adjacent =
	[
		"OW_ICEREGION_90",
		"OW_ICEREGION_84",
		"OW_ICEREGION_86",
	]},
	["OW_ICEDRAGON_11"] = {x = -57073.1563, y = 2608.1167, z = 5331.82324, adjacent =
	[
		"OW_ICEDRAGON_15",
		"OW_ICEDRAGON_20",
		"OW_ICEDRAGON_10",
	]},
	["OW_ICEDRAGON_15"] = {x = -58175.0859, y = 2881.16528, z = 5053.04639, adjacent =
	[
		"OW_ICEDRAGON_11",
	]},
	["OW_ICEDRAGON_20"] = {x = -56539.7656, y = 2710.28857, z = 5058.73975, adjacent =
	[
		"OW_ICEDRAGON_11",
		"OW_ICEDRAGON_18",
		"OW_ICEDRAGON_10",
	]},
	["OW_ICEDRAGON_18"] = {x = -57441.043, y = 2740.11084, z = 4512.89355, adjacent =
	[
		"OW_ICEDRAGON_20",
	]},
	["OW_ICEDRAGON_05"] = {x = -55312.9297, y = 2658.60645, z = 4897.04932, adjacent =
	[
		"OW_ICEDRAGON_01",
		"OW_ICEDRAGON_09",
		"OW_ICEDRAGON_12",
		"OW_ICEDRAGON_13",
		"OW_ICEDRAGON_22",
	]},
	["OW_ICEDRAGON_01"] = {x = -55968.7539, y = 2659.28418, z = 3870.21143, adjacent =
	[
		"OW_ICEDRAGON_05",
		"OW_ICEDRAGON_04",
		"OW_ICEDRAGON_03",
		"OW_ICEDRAGON_02",
	]},
	["OW_ICEDRAGON_09"] = {x = -54713.0156, y = 2480.56104, z = 6536.66162, adjacent =
	[
		"OW_ICEDRAGON_05",
		"OW_ICEDRAGON_12",
		"OW_ICEREGION_48",
		"OW_ICEDRAGON_22",
		"OW_ICEDRAGON_24",
	]},
	["OW_ICEDRAGON_12"] = {x = -53960.5781, y = 2514.14795, z = 5353.38623, adjacent =
	[
		"OW_ICEDRAGON_05",
		"OW_ICEDRAGON_09",
		"OW_ICEDRAGON_13",
		"OW_ICEDRAGON_25",
	]},
	["OW_ICEREGION_44"] = {x = -51312.9766, y = 2536.02734, z = 10310.2539, adjacent =
	[
		"OW_ICEREGION_42",
		"OW_ICEREGION_45",
		"OW_ICEREGION_72",
		"OW_ICEREGION_73",
	]},
	["OW_ICEREGION_42"] = {x = -50064.9453, y = 2691.58569, z = 9998.7373, adjacent =
	[
		"OW_ICEREGION_44",
		"OW_ICEREGION_41",
	]},
	["OW_ICEREGION_49"] = {x = -56018.1836, y = 2867.07544, z = 9741.87109, adjacent =
	[
		"OW_ICEREGION_56",
		"OW_ICEREGION_50",
		"OW_ICEREGION_55",
	]},
	["OW_ICEREGION_56"] = {x = -54812.0586, y = 2647.13818, z = 9629.21094, adjacent =
	[
		"OW_ICEREGION_49",
		"OW_ICEREGION_46",
		"OW_ICEREGION_47",
		"OW_ICEREGION_55",
	]},
	["OW_ICEREGION_62"] = {x = -61170.3203, y = 2536.02734, z = 14011.2783, adjacent =
	[
		"OW_ICEREGION_78",
		"OW_ICEREGION_64",
		"OW_ICEREGION_60",
	]},
	["OW_ICEREGION_78"] = {x = -58485.4922, y = 2536.02734, z = 14249.498, adjacent =
	[
		"OW_ICEREGION_62",
		"OW_ICEREGION_64",
		"OW_ICEREGION_59",
		"OW_ICEREGION_65",
	]},
	["OW_ICEREGION_64"] = {x = -58977.3438, y = 2536.02734, z = 15451.792, adjacent =
	[
		"OW_ICEREGION_62",
		"OW_ICEREGION_78",
		"OW_ICEREGION_63",
		"OW_ICEREGION_65",
	]},
	["OW_ICEREGION_13"] = {x = -41016.1719, y = 646.638672, z = 11165.6777, adjacent =
	[
		"OW_ICEREGION_19",
	]},
	["OW_ICEREGION_19"] = {x = -40768.4023, y = 646.638733, z = 9886.14844, adjacent =
	[
		"OW_ICEREGION_13",
		"OW_ICEREGION_10",
		"OW_ICEREGION_12",
		"OW_ICEREGION_14",
	]},
	["WP_INTRO03"] = {x = 6498.57861, y = 6512.92236, z = 40097.0586, adjacent =
	[
		"WP_INTRO23",
		"WP_INTRO04",
		"WP_INTRO07",
		"WP_INTRO06",
		"WP_INTRO09",
	]},
	["WP_INTRO23"] = {x = 7687.38672, y = 6644.38477, z = 39739.293, adjacent =
	[
		"WP_INTRO03",
		"WP_INTRO22",
	]},
	["OW_PATH_274"] = {x = -37749.7734, y = 3524.56641, z = -13457.6133, adjacent =
	[
		"OW_DJG_WATCH_STONEHENGE_01",
		"OW_PATH_275",
		"OW_PATH_273",
		"OW_PATH_274_RIGHT",
	]},
	["OW_DJG_WATCH_STONEHENGE_01"] = {x = -36616.457, y = 3499.0564, z = -13195.5254, adjacent =
	[
		"OW_PATH_274",
		"OW_PATH_275",
	]},
	["OW_PATH_275"] = {x = -36778.7734, y = 3330.02588, z = -14233.5605, adjacent =
	[
		"OW_PATH_274",
		"OW_DJG_WATCH_STONEHENGE_01",
		"OW_PATH_276",
	]},
	["OW_WARAN_G_SPAWN"] = {x = -30686.123, y = 8.64510727, z = 1218.11072, adjacent =
	[
		"PATH_OC_NC_24",
		"PATH_OC_NC_23",
	]},
	["PATH_OC_NC_24"] = {x = -32459.4355, y = 389.725494, z = 1275.5376, adjacent =
	[
		"OW_WARAN_G_SPAWN",
		"OW_BLOODFLY_NEW_COAST_SPAWN",
	]},
	["OW_DJG_ICECAMP_05"] = {x = -35668.6016, y = 593.727905, z = 4119.98633, adjacent =
	[
		"OW_PATH_063",
		"OW_PATH_066",
		"OW_PATH_156",
		"OW_DJG_ICECAMP_02",
	]},
	["OW_PATH_066"] = {x = -36648.8828, y = 670.661621, z = 4519.81348, adjacent =
	[
		"OW_PATH_063",
		"OW_DJG_ICECAMP_05",
		"OW_PATH_156",
		"OW_PATH_065",
	]},
	["OW_PATH_156"] = {x = -36873.4766, y = 192.78801, z = 1941.39429, adjacent =
	[
		"OW_DJG_ICECAMP_05",
		"OW_PATH_066",
		"OW_DJG_ICECAMP_03",
		"OW_BLOODFLY_NEW_COAST_SPAWN",
		"OW_PATH_065",
		"OW_PATH_157",
	]},
	["OW_DJG_ICECAMP_02"] = {x = -35174.7422, y = 347.611786, z = 3051.13916, adjacent =
	[
		"OW_DJG_ICECAMP_05",
		"OW_DJG_ICECAMP_03",
	]},
	["OW_DJG_ICECAMP_03"] = {x = -35315.2305, y = 306.216827, z = 2717.89014, adjacent =
	[
		"OW_PATH_156",
		"OW_DJG_ICECAMP_02",
		"OW_DJG_ICECAMP_04",
	]},
	["OW_BLOODFLY_NEW_COAST_SPAWN"] = {x = -34793.3477, y = 231.169922, z = 1740.27124, adjacent =
	[
		"PATH_OC_NC_24",
		"OW_PATH_156",
		"OW_DJG_ICECAMP_04",
	]},
	["OW_DJG_ICECAMP_04"] = {x = -34874.5781, y = 305.223206, z = 2505.4082, adjacent =
	[
		"OW_DJG_ICECAMP_03",
		"OW_BLOODFLY_NEW_COAST_SPAWN",
		"OW_DJG_ICECAMP_01",
		"ICECAMP",
	]},
	["OW_DJG_ICECAMP_01"] = {x = -34870.7969, y = 322.53421, z = 2712.9231, adjacent =
	[
		"OW_DJG_ICECAMP_04",
	]},
	["OW_PATH_161"] = {x = -39980.5273, y = 690.991699, z = 3242.14209, adjacent =
	[
		"OW_PATH_064",
		"OW_PATH_160",
	]},
	["OW_PATH_064"] = {x = -38631.2969, y = 798.108215, z = 4363.92676, adjacent =
	[
		"OW_PATH_161",
		"OW_PATH_065",
		"OW_ICEREGION_ENTRANCE_01",
	]},
	["LOCATION_19_03_PATH_RUIN9"] = {x = 20518.0801, y = 7806.86475, z = -36155.6328, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN19",
		"LOCATION_19_03_PATH_RUIN18",
		"LOCATION_19_03_PATH_RUIN11",
		"LOCATION_19_03_PATH_RUIN10",
		"LOCATION_19_03_PATH_RUIN8",
		"LOCATION_19_03_ENTRANCE_HARPYE",
	]},
	["LOCATION_19_03_PATH_RUIN19"] = {x = 19998.2031, y = 7807.01172, z = -36142.2813, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN9",
		"LOCATION_19_03_PATH_RUIN18",
	]},
	["LOCATION_19_03_PATH_RUIN18"] = {x = 19444.6641, y = 7806.979, z = -36528.9609, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN9",
		"LOCATION_19_03_PATH_RUIN19",
	]},
	["LOCATION_19_03_PATH_RUIN13"] = {x = 21803.2051, y = 8262.30859, z = -36233.9805, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN17",
		"LOCATION_19_03_PATH_RUIN15",
		"LOCATION_19_03_PATH_RUIN12",
	]},
	["LOCATION_19_03_PATH_RUIN17"] = {x = 22451.5137, y = 8262.3125, z = -35965.6719, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN13",
		"LOCATION_19_03_PATH_RUIN16",
	]},
	["LOCATION_19_03_PATH_RUIN16"] = {x = 22622.4277, y = 8262.30762, z = -35441.3242, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN17",
		"LOCATION_19_03_PATH_RUIN15",
	]},
	["LOCATION_19_03_PATH_RUIN15"] = {x = 21949.7246, y = 8262.30859, z = -35512.1094, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN13",
		"LOCATION_19_03_PATH_RUIN16",
	]},
	["LOCATION_19_03_PATH_RUIN12"] = {x = 21503.6191, y = 7856.17139, z = -36158.0664, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN13",
		"LOCATION_19_03_PATH_RUIN11",
	]},
	["LOCATION_19_03_PATH_RUIN11"] = {x = 20985.0059, y = 7806.10693, z = -36267.3633, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN9",
		"LOCATION_19_03_PATH_RUIN12",
		"LOCATION_19_03_PATH_RUIN10",
	]},
	["OW_ROCKDRAGON_15"] = {x = 18913.332, y = 9096.36621, z = -37025.4453, adjacent =
	[
		"OW_ROCKDRAGON_14",
	]},
	["OW_ROCKDRAGON_14"] = {x = 18405.125, y = 9096.40137, z = -37510.6016, adjacent =
	[
		"OW_ROCKDRAGON_15",
		"OW_ROCKDRAGON_13",
	]},
	["OW_ROCKDRAGON_13"] = {x = 18388.8184, y = 9096.36328, z = -38263.1211, adjacent =
	[
		"OW_ROCKDRAGON_14",
		"OW_ROCKDRAGON_12",
	]},
	["OW_ROCKDRAGON_12"] = {x = 18905.9629, y = 9196.52832, z = -38851.0703, adjacent =
	[
		"OW_ROCKDRAGON_13",
		"OW_ROCKDRAGON_03",
	]},
	["OW_ROCKDRAGON_03"] = {x = 19902.9512, y = 9652.85449, z = -39850.9141, adjacent =
	[
		"OW_ROCKDRAGON_12",
		"OW_ROCKDRAGON_02",
		"OW_ROCKDRAGON_10",
	]},
	["OW_DRAGONSWAMP_005"] = {x = -19682.0918, y = -1364.78027, z = -9084.10449, adjacent =
	[
		"OW_DRAGONSWAMP_006",
		"OW_DRAGONSWAMP_019",
		"OW_DRAGONSWAMP_021",
		"OW_DRAGONSWAMP_020",
		"OW_DRAGONSWAMP_018",
		"OW_DRAGONSWAMP_004",
		"OW_DJG_SWAMP_WAIT2_03",
	]},
	["OW_DRAGONSWAMP_019"] = {x = -21108.248, y = -1414.76721, z = -10736.5469, adjacent =
	[
		"OW_DRAGONSWAMP_006",
		"OW_DRAGONSWAMP_005",
		"OW_DRAGONSWAMP_017",
	]},
	["OW_DRAGONSWAMP_021"] = {x = -17274.7207, y = -1364.76648, z = -9280.4043, adjacent =
	[
		"OW_DRAGONSWAMP_005",
		"OW_DRAGONSWAMP_022",
		"OW_DRAGONSWAMP_020",
	]},
	["OW_PATH_046"] = {x = -16113.3066, y = -1543.94702, z = -3090.97461, adjacent =
	[
		"OW_DRAGONSWAMP_027",
		"OW_DRAGONSWAMP_001",
		"OW_PATH_047",
		"OW_DJG_SWAMP_WAIT1_01",
		"OW_DJG_SWAMP_WAIT1_02",
	]},
	["OW_DRAGONSWAMP_027"] = {x = -15590.1992, y = -1338.09619, z = -3503.27417, adjacent =
	[
		"OW_PATH_046",
		"OW_DRAGONSWAMP_026",
	]},
	["OW_DRAGONSWAMP_026"] = {x = -14957.4482, y = -1107.07532, z = -4361.38721, adjacent =
	[
		"OW_DRAGONSWAMP_027",
		"OW_DRAGONSWAMP_025",
	]},
	["OW_DRAGONSWAMP_025"] = {x = -14635.1289, y = -1357.72437, z = -5675.61279, adjacent =
	[
		"OW_DRAGONSWAMP_026",
		"OW_DRAGONSWAMP_024",
	]},
	["OW_DRAGONSWAMP_024"] = {x = -13798.3984, y = -1364.40576, z = -6889.1709, adjacent =
	[
		"OW_DRAGONSWAMP_025",
		"OW_DRAGONSWAMP_023",
	]},
	["OW_DRAGONSWAMP_023"] = {x = -14005.0732, y = -1364.40576, z = -7486.95215, adjacent =
	[
		"OW_DRAGONSWAMP_024",
		"OW_DRAGONSWAMP_022",
	]},
	["OW_DRAGONSWAMP_022"] = {x = -16060.5303, y = -1364.76575, z = -8740.36719, adjacent =
	[
		"OW_DRAGONSWAMP_021",
		"OW_DRAGONSWAMP_023",
	]},
	["OW_DRAGONSWAMP_020"] = {x = -18375.0703, y = -1364.77698, z = -9950.96387, adjacent =
	[
		"OW_DRAGONSWAMP_005",
		"OW_DRAGONSWAMP_021",
		"OW_DJG_SWAMP_WAIT2_03",
	]},
	["OW_DRAGONSWAMP_018"] = {x = -21609.0195, y = -1414.76941, z = -9236.16309, adjacent =
	[
		"OW_DRAGONSWAMP_006",
		"OW_DRAGONSWAMP_005",
	]},
	["OW_DRAGONSWAMP_017"] = {x = -21908.5254, y = -1364.76575, z = -11498.6563, adjacent =
	[
		"OW_DRAGONSWAMP_019",
		"OW_DRAGONSWAMP_015",
		"OW_DRAGONSWAMP_007",
	]},
	["OW_DRAGONSWAMP_015"] = {x = -23285.5273, y = -1364.76575, z = -11908.0615, adjacent =
	[
		"OW_DRAGONSWAMP_017",
		"OW_DRAGONSWAMP_007",
		"OW_DRAGONSWAMP_008",
	]},
	["OW_DRAGONSWAMP_007"] = {x = -22696.1836, y = -1364.76575, z = -10311.0527, adjacent =
	[
		"OW_DRAGONSWAMP_006",
		"OW_DRAGONSWAMP_017",
		"OW_DRAGONSWAMP_015",
		"OW_DRAGONSWAMP_008",
	]},
	["OW_SWAMPDRAGON_01"] = {x = -25015.9004, y = -1364.6521, z = -10635.5068, adjacent =
	[
		"OW_DRAGONSWAMP_008",
		"OW_DRAGONSWAMP_010",
		"OW_DRAGONSWAMP_011",
	]},
	["OW_DRAGONSWAMP_008"] = {x = -24044.9219, y = -1364.76575, z = -10669.1582, adjacent =
	[
		"OW_DRAGONSWAMP_015",
		"OW_DRAGONSWAMP_007",
		"OW_SWAMPDRAGON_01",
		"OW_DRAGONSWAMP_013",
		"OW_DRAGONSWAMP_009",
	]},
	["OW_DRAGONSWAMP_010"] = {x = -25556.7207, y = -1364.47363, z = -9746.44043, adjacent =
	[
		"OW_SWAMPDRAGON_01",
	]},
	["OW_DRAGONSWAMP_011"] = {x = -25714.9102, y = -1364.46289, z = -10994.7197, adjacent =
	[
		"OW_SWAMPDRAGON_01",
	]},
	["OW_DRAGONSWAMP_004"] = {x = -20037.457, y = -1364.76575, z = -6651.6416, adjacent =
	[
		"OW_DRAGONSWAMP_005",
		"OW_DRAGONSWAMP_003",
	]},
	["OW_DRAGONSWAMP_003"] = {x = -19324.541, y = -1414.35498, z = -5448.13574, adjacent =
	[
		"OW_DRAGONSWAMP_004",
		"OW_DRAGONSWAMP_002",
	]},
	["OW_DRAGONSWAMP_002"] = {x = -17950.5781, y = -1364.38342, z = -4869.50293, adjacent =
	[
		"OW_DRAGONSWAMP_003",
		"OW_DRAGONSWAMP_001",
	]},
	["OW_DRAGONSWAMP_001"] = {x = -16905.8027, y = -1185.48706, z = -4494.98535, adjacent =
	[
		"OW_PATH_046",
		"OW_DRAGONSWAMP_002",
	]},
	["OW_PATH_340"] = {x = -17136.5938, y = 2279.92944, z = -26027.877, adjacent =
	[
		"OW_PATH_334",
	]},
	["OW_PATH_334"] = {x = -17120.9863, y = 2518.72461, z = -24834.2246, adjacent =
	[
		"OW_PATH_340",
		"OW_PATH_333",
		"OW_PATH_337",
	]},
	["OW_PATH_338"] = {x = -17250.3535, y = 2718.0708, z = -22821.4727, adjacent =
	[
		"OW_PATH_333",
		"OW_PATH_337",
		"OW_PATH_339",
	]},
	["OW_PATH_333"] = {x = -16950.043, y = 2568.95337, z = -23948.0859, adjacent =
	[
		"OW_PATH_334",
		"OW_PATH_338",
		"OW_PATH_332",
	]},
	["OW_PATH_337"] = {x = -18191.8457, y = 2939.91357, z = -23362.7285, adjacent =
	[
		"OW_PATH_334",
		"OW_PATH_338",
		"OW_PATH_336",
	]},
	["OW_PATH_330"] = {x = -15872.1328, y = 2315.28052, z = -21620.4883, adjacent =
	[
		"OW_PATH_339",
		"OW_PATH_331",
		"OW_PATH_099",
	]},
	["OW_PATH_339"] = {x = -16762.998, y = 2538.92627, z = -22313.7207, adjacent =
	[
		"OW_PATH_338",
		"OW_PATH_330",
	]},
	["OW_PATH_336"] = {x = -19399.5996, y = 2971.38135, z = -23978.084, adjacent =
	[
		"OW_PATH_337",
	]},
	["OW_PATH_332"] = {x = -16006.4463, y = 2214.84131, z = -23325.9551, adjacent =
	[
		"OW_PATH_333",
		"OW_PATH_331",
	]},
	["OW_PATH_331"] = {x = -15709.8672, y = 2310.02051, z = -22241.6465, adjacent =
	[
		"OW_PATH_330",
		"OW_PATH_332",
	]},
	["OW_PATH_099"] = {x = -16705.7695, y = 2361.30444, z = -20341.0918, adjacent =
	[
		"OW_PATH_330",
		"SPAWN_OW_WARAN_ORC_01",
		"OW_PATH_100",
	]},
	["SPAWN_OW_WARAN_ORC_01"] = {x = -14934.7646, y = 2041.9812, z = -20127.6133, adjacent =
	[
		"OW_ORC_LOOKOUT_2_04",
		"OW_PATH_099",
		"OW_PATH_100",
	]},
	["LOCATION_14_02"] = {x = 9960.14746, y = 383.108765, z = -11307.9727, adjacent =
	[
		"SPAWN_OW_BLOODFLY_OC_PSI",
		"LOCATION_14_04",
		"LOCATION_14_01",
	]},
	["SPAWN_OW_BLOODFLY_OC_PSI"] = {x = 11372.2715, y = 397.142975, z = -11525.7686, adjacent =
	[
		"LOCATION_14_02",
		"OW_ORCBARRIER_08",
		"OW_ORCBARRIER_07",
	]},
	["LOCATION_14_04"] = {x = 8974.34277, y = 424.243195, z = -11130.1807, adjacent =
	[
		"LOCATION_14_02",
		"LOCATION_14_01",
		"OW_PATH_018",
		"SPAWN_OW_WARAN_OC_PSI3",
		"OW_PATH_001",
	]},
	["LOCATION_14_01"] = {x = 9825.88379, y = 336.647736, z = -10433.3203, adjacent =
	[
		"LOCATION_14_02",
		"LOCATION_14_04",
		"PATH_OC_PSI_01",
		"SPAWN_OW_WARAN_OC_PSI3",
	]},
	["OW_ORCBARRIER_08"] = {x = 12977.9502, y = 694.815186, z = -10770.3213, adjacent =
	[
		"SPAWN_OW_BLOODFLY_OC_PSI",
		"OW_ORCBARRIER_09",
		"OW_ORCBARRIER_07",
		"OW_ORCBARRIER_08_01",
	]},
	["OW_ORCBARRIER_18"] = {x = 21828.8945, y = -602.12085, z = 4631.04395, adjacent =
	[
		"OW_PATH_291",
		"OW_ORCBARRIER_17",
		"OW_ORCBARRIER_19",
	]},
	["OW_PATH_291"] = {x = 20166.1523, y = -691.305176, z = 2513.94727, adjacent =
	[
		"OW_ORCBARRIER_18",
		"OW_ORCBARRIER_16",
		"OW_ORCBARRIER_17",
		"OW_PATH_292",
	]},
	["OW_ORCBARRIER_16"] = {x = 18776.4473, y = -517.147461, z = 1457.89612, adjacent =
	[
		"OW_PATH_291",
		"OW_ORCBARRIER_17",
		"OW_ORCBARRIER_15",
	]},
	["OW_ORCBARRIER_17"] = {x = 21000.1016, y = -561.281616, z = 2461.88281, adjacent =
	[
		"OW_ORCBARRIER_18",
		"OW_PATH_291",
		"OW_ORCBARRIER_16",
	]},
	["OW_PATH_166"] = {x = 18382.498, y = -858.534546, z = 8925.6748, adjacent =
	[
		"OW_ORCBARRIER_21",
		"SPAWN_OW_SNAPPER_OCWOOD1_05_02",
		"PATH_OC_FOGTOWER05",
	]},
	["OW_ORCBARRIER_21"] = {x = 20319.5137, y = -981.594727, z = 9672.35645, adjacent =
	[
		"OW_PATH_166",
		"OW_ORCBARRIER_20",
	]},
	["OW_ORCBARRIER_20"] = {x = 21322.3711, y = -902.135254, z = 8587.83691, adjacent =
	[
		"OW_ORCBARRIER_21",
		"OW_ORCBARRIER_19",
	]},
	["OW_ORCBARRIER_19"] = {x = 21869.4219, y = -731.740417, z = 7022.9585, adjacent =
	[
		"OW_ORCBARRIER_18",
		"OW_ORCBARRIER_20",
	]},
	["OW_ORCBARRIER_15"] = {x = 17288.0996, y = -504.233704, z = 347.895599, adjacent =
	[
		"OW_ORCBARRIER_16",
		"OW_ORCBARRIER_14",
	]},
	["OW_ORCBARRIER_14"] = {x = 16875.1465, y = -319.116394, z = -599.638306, adjacent =
	[
		"OW_ORCBARRIER_15",
		"OW_ORCBARRIER_13",
	]},
	["OW_ORCBARRIER_13"] = {x = 15721.9404, y = 455.220245, z = -3027.3855, adjacent =
	[
		"OW_ORCBARRIER_14",
		"OW_ORCBARRIER_12",
	]},
	["OW_ORCBARRIER_12"] = {x = 14679.5293, y = 613.519653, z = -4569.39014, adjacent =
	[
		"OW_ORCBARRIER_13",
		"OW_ORCBARRIER_11",
	]},
	["OW_ORCBARRIER_11"] = {x = 14366.8359, y = 698.637451, z = -6522.29736, adjacent =
	[
		"OW_ORCBARRIER_12",
		"OW_ORCBARRIER_10",
	]},
	["OW_ORCBARRIER_10"] = {x = 13857.125, y = 633.887512, z = -8022.26123, adjacent =
	[
		"OW_ORCBARRIER_11",
		"OW_ORCBARRIER_09",
	]},
	["OW_ORCBARRIER_09"] = {x = 13394.2256, y = 649.262817, z = -9454.73828, adjacent =
	[
		"OW_ORCBARRIER_08",
		"OW_ORCBARRIER_10",
	]},
	["OW_ORCBARRIER_07"] = {x = 12615.7178, y = 824.2677, z = -12532.9844, adjacent =
	[
		"SPAWN_OW_BLOODFLY_OC_PSI",
		"OW_ORCBARRIER_08",
		"OW_ORCBARRIER_06",
	]},
	["OW_ORCBARRIER_06"] = {x = 12937.1182, y = 943.816284, z = -13933.5928, adjacent =
	[
		"OW_ORCBARRIER_07",
		"OW_ORCBARRIER_05",
		"OW_ORCBARRIER_08_01",
	]},
	["OW_ORCBARRIER_05"] = {x = 15710.9971, y = 1202.76819, z = -15926.0439, adjacent =
	[
		"OW_ORCBARRIER_06",
		"OW_ORCBARRIER_04",
		"OW_ORCBARRIER_04_01",
	]},
	["OW_ORCBARRIER_04"] = {x = 17638.75, y = 1234.25732, z = -16717.8789, adjacent =
	[
		"OW_ORCBARRIER_05",
		"OW_ORCBARRIER_03",
		"OW_WATERFALL_GOBBO10",
		"OW_ORCBARRIER_04_01",
	]},
	["OW_ORCBARRIER_03"] = {x = 19735.8535, y = 1288.49963, z = -16794.5547, adjacent =
	[
		"OW_ORCBARRIER_04",
		"OW_ORCBARRIER_02",
		"OW_WATERFALL_GOBBO10",
	]},
	["OW_ORCBARRIER_02"] = {x = 21833.7891, y = 1570.59131, z = -17017.043, adjacent =
	[
		"OW_ORCBARRIER_03",
		"OW_ORCBARRIER_01",
		"OW_WATERFALL_GOBBO10",
	]},
	["OW_ORCBARRIER_01"] = {x = 23460.5078, y = 1819.50732, z = -16686.8711, adjacent =
	[
		"OW_ORCBARRIER_02",
		"PATH_TO_PLATEAU07",
	]},
	["PATH_TO_PLATEAU07"] = {x = 26051.5566, y = 1833.97778, z = -17317.2266, adjacent =
	[
		"OW_ORCBARRIER_01",
		"PATH_TO_PLATEAU06",
	]},
	["PATH_TO_PLATEAU06"] = {x = 25867.6797, y = 2433.74561, z = -18908.4434, adjacent =
	[
		"PATH_TO_PLATEAU07",
		"PATH_TO_PLATEAU05",
	]},
	["PATH_TO_PLATEAU05"] = {x = 24792.7383, y = 3202.10425, z = -20724.7871, adjacent =
	[
		"PATH_TO_PLATEAU06",
		"PATH_TO_PLATEAU02",
	]},
	["PATH_TO_PLATEAU02"] = {x = 24097, y = 3822.84351, z = -21754, adjacent =
	[
		"PATH_TO_PLATEAU05",
		"PATH_TO_PLATEAU03",
		"SPAWN_OW_WARAN_EBENE2_02_05",
	]},
	["CASTLE_33"] = {x = 3938.9165, y = 6988.12793, z = -21764.2832, adjacent =
	[
		"CASTLE_36",
		"CASTLE_34",
		"CASTLE_32",
	]},
	["CASTLE_36"] = {x = 4963.93506, y = 6925.69287, z = -21516.5293, adjacent =
	[
		"CASTLE_33",
		"CASTLE_34",
		"CASTLE_30",
		"CASTLE_31",
		"CASTLE_32",
		"CASTLE_35",
	]},
	["CASTLE_34"] = {x = 4031.46436, y = 6967.57666, z = -20914.377, adjacent =
	[
		"CASTLE_33",
		"CASTLE_36",
		"CASTLE_35",
	]},
	["CASTLE_30"] = {x = 5748.81738, y = 6985.6748, z = -21012.7949, adjacent =
	[
		"CASTLE_36",
		"CASTLE_31",
		"CASTLE_35",
		"CASTLE_29",
	]},
	["CASTLE_31"] = {x = 5516.4834, y = 7015.74316, z = -22011.9434, adjacent =
	[
		"CASTLE_36",
		"CASTLE_30",
		"CASTLE_32",
	]},
	["CASTLE_32"] = {x = 4937.56738, y = 6962.2832, z = -22463.6543, adjacent =
	[
		"CASTLE_33",
		"CASTLE_36",
		"CASTLE_31",
	]},
	["CASTLE_35"] = {x = 5017.36328, y = 6912.20801, z = -20660.8281, adjacent =
	[
		"CASTLE_36",
		"CASTLE_34",
		"CASTLE_30",
	]},
	["CASTLE_29"] = {x = 6055.86377, y = 7264.99707, z = -19904.582, adjacent =
	[
		"CASTLE_30",
		"CASTLE_28",
	]},
	["CASTLE_28"] = {x = 6582.88623, y = 7275.13916, z = -19785.0527, adjacent =
	[
		"CASTLE_29",
		"CASTLE_27",
	]},
	["CASTLE_27"] = {x = 6454.68604, y = 7304.35449, z = -19443.4844, adjacent =
	[
		"CASTLE_28",
		"CASTLE_26",
	]},
	["CASTLE_26"] = {x = 5964.4248, y = 7378.13232, z = -18665.0313, adjacent =
	[
		"CASTLE_27",
		"CASTLE_25",
	]},
	["CASTLE_25"] = {x = 5606.83154, y = 7397.93066, z = -18442.5449, adjacent =
	[
		"CASTLE_26",
		"CASTLE_24",
	]},
	["CASTLE_24"] = {x = 4960.37158, y = 7260.63623, z = -18744.2695, adjacent =
	[
		"CASTLE_25",
		"CASTLE_23",
	]},
	["CASTLE_23"] = {x = 4362.30518, y = 7175.97217, z = -18549.9531, adjacent =
	[
		"CASTLE_24",
		"CASTLE_22",
	]},
	["CASTLE_22"] = {x = 4147.7124, y = 7098.53809, z = -18935.5859, adjacent =
	[
		"CASTLE_23",
		"CASTLE_21",
		"FIREDRAGON",
	]},
	["CASTLE_21"] = {x = 3558.19946, y = 7071.0498, z = -19498.584, adjacent =
	[
		"CASTLE_22",
		"CASTLE_20",
	]},
	["CASTLE_20"] = {x = 2640.22046, y = 7100.95703, z = -19284.502, adjacent =
	[
		"CASTLE_21",
		"CASTLE_19",
	]},
	["CASTLE_19"] = {x = 2305.35181, y = 7160.39893, z = -19615.8184, adjacent =
	[
		"CASTLE_20",
		"CASTLE_18",
	]},
	["CASTLE_18"] = {x = 1788.76172, y = 7038.02686, z = -20177.3203, adjacent =
	[
		"CASTLE_19",
		"CASTLE_17",
	]},
	["CASTLE_17"] = {x = 1016.51703, y = 6678.86133, z = -20688.6523, adjacent =
	[
		"CASTLE_18",
		"CASTLE_16",
	]},
	["CASTLE_16"] = {x = 321.298859, y = 6683.15283, z = -21547.5859, adjacent =
	[
		"CASTLE_17",
		"CASTLE_15",
	]},
	["CASTLE_15"] = {x = -627.990845, y = 6784.24902, z = -21956.8887, adjacent =
	[
		"CASTLE_16",
		"CASTLE_14",
	]},
	["CASTLE_14"] = {x = -1352.1969, y = 6592.47168, z = -21042.5469, adjacent =
	[
		"CASTLE_15",
		"CASTLE_13",
	]},
	["WP_INTRO12"] = {x = 8182.96777, y = 5506.64063, z = 36193.1992, adjacent =
	[
		"WP_INTRO14",
		"WP_INTRO16",
		"WP_INTRO13",
		"WP_INTRO11",
	]},
	["WP_INTRO14"] = {x = 7277.39063, y = 5577.72314, z = 37039.6445, adjacent =
	[
		"WP_INTRO12",
		"WP_INTRO13",
		"WP_INTRO15",
	]},
	["WP_INTRO16"] = {x = 8424.47461, y = 5525.72314, z = 35766.6758, adjacent =
	[
		"WP_INTRO12",
		"WP_INTRO17",
	]},
	["WP_INTRO13"] = {x = 8096.54443, y = 5577.72314, z = 37036.2422, adjacent =
	[
		"WP_INTRO12",
		"WP_INTRO14",
		"WP_INTRO15",
	]},
	["WP_INTRO15"] = {x = 7772.90039, y = 5577.72314, z = 37540.6719, adjacent =
	[
		"WP_INTRO14",
		"WP_INTRO13",
	]},
	["WP_INTRO22"] = {x = 8409.29492, y = 6682.50439, z = 39612.8125, adjacent =
	[
		"WP_INTRO23",
		"WP_INTRO21",
	]},
	["WP_INTRO21"] = {x = 9148.69434, y = 6608.92041, z = 39471.0547, adjacent =
	[
		"WP_INTRO22",
		"WP_INTRO20",
	]},
	["WP_INTRO20"] = {x = 9433.95117, y = 6542.26904, z = 38946.7773, adjacent =
	[
		"WP_INTRO21",
		"WP_INTRO19",
	]},
	["WP_INTRO19"] = {x = 9533.77148, y = 6443.354, z = 38247.0781, adjacent =
	[
		"WP_INTRO20",
		"WP_INTRO26",
	]},
	["WP_INTRO18"] = {x = 9424.1709, y = 5970.30078, z = 36777.457, adjacent =
	[
		"WP_INTRO17",
		"WP_INTRO25",
	]},
	["WP_INTRO17"] = {x = 8957.78516, y = 5677.25732, z = 36126.7539, adjacent =
	[
		"WP_INTRO16",
		"WP_INTRO18",
	]},
	["WP_INTRO11"] = {x = 7636.94678, y = 5544.22803, z = 36078.5781, adjacent =
	[
		"WP_INTRO12",
		"WP_INTRO10",
	]},
	["WP_INTRO10"] = {x = 6796.5415, y = 5436.30957, z = 36552.5156, adjacent =
	[
		"WP_INTRO11",
		"WP_INTRO_WI07",
	]},
	["WP_INTRO_WI07"] = {x = 6081.73828, y = 5423.11328, z = 36181.2227, adjacent =
	[
		"WP_INTRO10",
		"WP_INTRO_WI06",
		"WP_INTRO_FALL3",
	]},
	["WP_INTRO_WI06"] = {x = 5913.01221, y = 5464.87695, z = 36030.9609, adjacent =
	[
		"WP_INTRO_WI07",
		"OW_PATH_1_16_8",
	]},
	["OW_PATH_185"] = {x = -13384.3594, y = 1004.32385, z = 22605.2734, adjacent =
	[
		"OW_PATH_184",
		"OW_PATH_186",
	]},
	["OW_PATH_187"] = {x = -12207.6455, y = 1458.55542, z = 23511.3203, adjacent =
	[
		"OW_PATH_190",
		"OW_PATH_188",
		"OW_PATH_186",
	]},
	["OW_PATH_190"] = {x = -13077.2764, y = 2063.49609, z = 25440.8867, adjacent =
	[
		"OW_PATH_187",
		"OW_PATH_188",
	]},
	["OW_PATH_188"] = {x = -13221.209, y = 1611.27368, z = 24405.832, adjacent =
	[
		"OW_PATH_187",
		"OW_PATH_190",
		"OW_PATH_189",
	]},
	["OW_PATH_189"] = {x = -14203.3135, y = 1700.15344, z = 25107.2051, adjacent =
	[
		"OW_PATH_188",
	]},
	["OW_PATH_186"] = {x = -12186.5068, y = 1060.44189, z = 22340.5293, adjacent =
	[
		"OW_PATH_184",
		"OW_PATH_185",
		"OW_PATH_187",
	]},
	["SPAWN_TALL_PATH_BANDITOS2_02_04"] = {x = -18692.209, y = -141.490494, z = 4626.81055, adjacent =
	[
		"SPAWN_TALL_PATH_BANDITOS2_02",
	]},
	["SPAWN_TALL_PATH_BANDITOS2_02"] = {x = -18348.7695, y = -213.994995, z = 4739.19678, adjacent =
	[
		"SPAWN_TALL_PATH_BANDITOS2_02_04",
		"SPAWN_TALL_PATH_BANDITOS2",
		"SPAWN_TALL_PATH_BANDITOS2_03",
	]},
	["OC1_MOVE"] = {x = -7011.50342, y = -1292.02722, z = 6712.96045, adjacent =
	[
		"OC1",
		"OC_ROUND_2",
	]},
	["OC1"] = {x = -7857.36133, y = -1404.08032, z = 6116.32373, adjacent =
	[
		"OC1_MOVE",
		"OC_ROUND_1",
		"OC3",
		"OC_ORK_MAIN_CAMP_03",
		"OC_PATH_03",
	]},
	["OC_ROUND_2"] = {x = -5950.93945, y = -1178.85254, z = 7753.86475, adjacent =
	[
		"OC1_MOVE",
		"OW_PATH_191",
		"PATH_OC_NC_28",
		"OC_ROUND_1",
		"OC_ROUND_22_CF_2_MOVEMENT2",
	]},
	["OW_ICEREGION_02"] = {x = -40027.6953, y = 836.96759, z = 7077.81445, adjacent =
	[
		"OW_ICEREGION_05",
		"OW_ICEREGION_01",
		"OW_ICEREGION_03",
		"OW_ICEREGION_06",
	]},
	["OW_ICEREGION_05"] = {x = -40597.7695, y = 793.951294, z = 7312.70215, adjacent =
	[
		"OW_ICEREGION_02",
		"OW_ICEREGION_04",
		"OW_ICEREGION_06",
	]},
	["OW_ICEREGION_ENTRANCE"] = {x = -39513.2344, y = 919.402405, z = 5763.77832, adjacent =
	[
		"OW_ICEREGION_01",
		"OW_PATH_07_20",
		"OW_ICEREGION_ENTRANCE_01",
	]},
	["OW_ICEREGION_01"] = {x = -39766.2422, y = 873.552429, z = 6424.97412, adjacent =
	[
		"OW_ICEREGION_02",
		"OW_ICEREGION_ENTRANCE",
	]},
	["OW_ICEREGION_03"] = {x = -39707.4453, y = 799.519897, z = 7734.75098, adjacent =
	[
		"OW_ICEREGION_02",
		"OW_ICEREGION_04",
	]},
	["OW_ICEREGION_04"] = {x = -40271.2148, y = 758.849243, z = 7711.62158, adjacent =
	[
		"OW_ICEREGION_05",
		"OW_ICEREGION_03",
		"OW_ICEREGION_10",
	]},
	["PATH_OC_NC_13"] = {x = -24714.668, y = -342.476868, z = 2148.73657, adjacent =
	[
		"OW_PATH_02_SPAWN_HOGEWOLF",
		"OW_PATH_02",
		"OW_WOODRUIN_FOR_WOLF_SPAWN",
		"PATH_OC_NC_12",
		"PATH_OC_NC_14",
	]},
	["OW_PATH_02_SPAWN_HOGEWOLF"] = {x = -26141.5293, y = 153.085953, z = 706.60675, adjacent =
	[
		"PATH_OC_NC_13",
		"OW_PATH_02",
	]},
	["OW_PATH_02"] = {x = -26786.0352, y = -23.2271271, z = 1482.86304, adjacent =
	[
		"PATH_OC_NC_13",
		"OW_PATH_02_SPAWN_HOGEWOLF",
		"PATH_OC_NC_14",
		"PATH_OC_NC_21",
		"PATH_OC_NC_22",
	]},
	["LOCATION_19_03_SECOND_ETAGE"] = {x = 19093.2832, y = 8886.24316, z = -38272.707, adjacent =
	[
		"LOCATION_19_03_LADDER2",
		"LOCATION_19_03_SECOND_HARPYE5",
		"LOCATION_19_03_SECOND_HARPYE6",
		"LOCATION_19_03_SECOND_ETAGE2",
	]},
	["LOCATION_19_03_LADDER2"] = {x = 18838.0195, y = 8886.24316, z = -38067.6602, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE",
		"LOCATION_19_03_SECOND_HARPYE5",
		"LOCATION_19_03_PEMTAGRAM2",
	]},
	["LOCATION_19_03_SECOND_HARPYE5"] = {x = 19537.9121, y = 8926.28906, z = -38423.1406, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE",
		"LOCATION_19_03_LADDER2",
		"LOCATION_19_03_SECOND_ETAGE2",
	]},
	["LOCATION_19_03_PEMTAGRAM2"] = {x = 18833.4277, y = 7876.23779, z = -37937.7383, adjacent =
	[
		"LOCATION_19_03_LADDER2",
		"LOCATION_19_03_PEMTAGRAM_MOVEMENT",
	]},
	["LOCATION_19_03_SECOND_HARPYE6"] = {x = 19569.7598, y = 8886.24316, z = -37495.2266, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE",
		"LOCATION_19_03_SECOND_ETAGE2",
		"OW_ROCKDRAGON_01",
	]},
	["LOCATION_19_03_SECOND_ETAGE2"] = {x = 19798.7578, y = 8886.24316, z = -37973.8828, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE",
		"LOCATION_19_03_SECOND_HARPYE5",
		"LOCATION_19_03_SECOND_HARPYE6",
		"LOCATION_19_03_SECOND_ETAGE3",
	]},
	["LOCATION_19_03_SECOND_ETAGE3"] = {x = 20749.3555, y = 8876.24316, z = -38145.2383, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE2",
		"LOCATION_19_03_SECOND_ETAGE4",
		"LOCATION_19_03_SECOND_ETAGE_BALCON",
	]},
	["LOCATION_19_03_SECOND_ETAGE7"] = {x = 21966.2441, y = 8876.24316, z = -39309.2461, adjacent =
	[
		"LOCATION_19_03_SECOND_HARPYE3",
		"LOCATION_19_03_SECOND_HARPYE4",
		"LOCATION_19_03_SECOND_ETAGE4",
	]},
	["LOCATION_19_03_SECOND_HARPYE3"] = {x = 22157.6641, y = 8876.24316, z = -39573.4805, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE7",
	]},
	["LOCATION_19_03_SECOND_HARPYE4"] = {x = 21608.4434, y = 8876.24316, z = -39508.9648, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE7",
	]},
	["LOCATION_19_03_SECOND_ETAGE4"] = {x = 22046.6484, y = 8876.24316, z = -38386.293, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE3",
		"LOCATION_19_03_SECOND_ETAGE7",
		"LOCATION_19_03_SECOND_ETAGE5",
	]},
	["LOCATION_19_03_SECOND_ETAGE6"] = {x = 23235.2969, y = 8876.24316, z = -37297.4141, adjacent =
	[
		"LOCATION_19_03_SECOND_HARPYE1",
		"LOCATION_19_03_SECOND_HARPYE2",
		"LOCATION_19_03_SECOND_ETAGE5",
	]},
	["LOCATION_19_03_SECOND_HARPYE1"] = {x = 23304.8848, y = 8876.24316, z = -37563.4844, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE6",
	]},
	["LOCATION_19_03_SECOND_HARPYE2"] = {x = 23537.9961, y = 8876.24316, z = -37038.6094, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE6",
	]},
	["LOCATION_19_03_SECOND_ETAGE5"] = {x = 22238.3242, y = 8876.24316, z = -37041.6641, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE4",
		"LOCATION_19_03_SECOND_ETAGE6",
	]},
	["LOCATION_19_03_SECOND_ETAGE_BALCON"] = {x = 20883.9473, y = 8881.47949, z = -37419.8594, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE3",
		"LOCATION_19_03_SECOND_ETAGE_BALCON3",
	]},
	["LOCATION_19_03_SECOND_ETAGE_BALCON3"] = {x = 20829.998, y = 8881.47852, z = -37080.3242, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE_BALCON",
		"LOCATION_19_03_SECOND_ETAGE_BALCON2",
	]},
	["LOCATION_19_03_SECOND_ETAGE_BALCON2"] = {x = 20148.1055, y = 8881.47754, z = -37131.1992, adjacent =
	[
		"LOCATION_19_03_SECOND_ETAGE_BALCON3",
	]},
	["OW_SAWHUT_SLEEP_01"] = {x = -14981.8975, y = -1204.75574, z = 4849.01123, adjacent =
	[
		"PATH_OC_NC_3",
		"OW_SAWHUT_MOLERAT_MOVEMENT8",
	]},
	["PATH_OC_NC_3"] = {x = -15207, y = -1238.95325, z = 3959, adjacent =
	[
		"OW_SAWHUT_SLEEP_01",
		"OW_SAWHUT_GREENGOBBO_SPAWN",
		"OW_SAWHUT_MOLERAT_MOVEMENT4",
		"PATH_OC_NC_4",
		"OW_SAWHUT_MEATBUG_SPAWN",
	]},
	["OW_SAWHUT_MOLERAT_MOVEMENT8"] = {x = -14768.5, y = -1209.42773, z = 5499.14795, adjacent =
	[
		"OW_SAWHUT_SLEEP_01",
		"OW_SAWHUT_MOLERAT_MOVEMENT3",
		"OW_SAWHUT_MOLERAT_MOVEMENT5",
	]},
	["OW_SAWHUT_GREENGOBBO_SPAWN"] = {x = -15020.5205, y = -1337.42505, z = 2889.12524, adjacent =
	[
		"PATH_OC_NC_3",
		"OW_CAVALORN_01",
		"OW_SAWHUT_MOLERAT_MOVEMENT6",
		"HELPPOINT8",
		"PATH_OC_NC_4",
		"PATH_OC_NC_5",
		"CAVALORN",
	]},
	["OW_CAVALORN_01"] = {x = -14541.9561, y = -1345.15247, z = 2926.04712, adjacent =
	[
		"OW_SAWHUT_GREENGOBBO_SPAWN",
	]},
	["PLATEAU_ROUND02_CAVE"] = {x = 13351.0957, y = 6732.30762, z = -27855.459, adjacent =
	[
		"PLATEAU_ROUND02_CAVE_MOVE",
		"PLATEAU_ROUND02",
	]},
	["PLATEAU_ROUND02_CAVE_MOVE"] = {x = 14063.9971, y = 6725.7666, z = -30138.4238, adjacent =
	[
		"PLATEAU_ROUND02_CAVE",
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC"] = {x = -847.927979, y = 230.37886, z = -15401.667, adjacent =
	[
		"FP_ROAM_OW_SNAPPER_OW_ORC_MOVE",
		"OW_PATH_015",
		"OW_PATH_019",
		"OW_PATH_013",
	]},
	["LOCATION_19_03_ROOM6"] = {x = 22973.5566, y = 7876.24365, z = -39505.4727, adjacent =
	[
		"LOCATION_19_03_ROOM6_BARRELCHAMBER",
		"LOCATION_19_03_ROOM4",
	]},
	["LOCATION_19_03_ROOM6_BARRELCHAMBER"] = {x = 22637.8633, y = 7876.24365, z = -39876.6563, adjacent =
	[
		"LOCATION_19_03_ROOM6",
		"LOCATION_19_03_ROOM6_BARRELCHAMBER3",
	]},
	["LOCATION_19_03_ENTRANCE"] = {x = 20643.1387, y = 7801.24414, z = -37707.0039, adjacent =
	[
		"LOCATION_19_03_PEMTAGRAM_ENTRANCE",
		"LOCATION_19_03_ROOM2",
		"LOCATION_19_03_ENTRANCE_HARPYE",
		"LOCATION_19_03_ENTRANCE_HARPYE2",
		"LOCATION_19_03_ENTRANCE_HARPYE3",
	]},
	["LOCATION_19_03_PEMTAGRAM_ENTRANCE"] = {x = 19547.5391, y = 7876.24365, z = -37521.6875, adjacent =
	[
		"LOCATION_19_03_ENTRANCE",
		"LOCATION_19_03_PENTAGRAMM",
		"LOCATION_19_03_PEMTAGRAM_MOVEMENT",
	]},
	["LOCATION_19_03_ROOM4"] = {x = 21540.1523, y = 7876.24365, z = -39153.6523, adjacent =
	[
		"LOCATION_19_03_ROOM6",
		"LOCATION_19_03_ROOM5",
		"LOCATION_19_03_ROOM2",
	]},
	["LOCATION_19_03_ROOM5"] = {x = 19773.0137, y = 7876.24365, z = -38852.6094, adjacent =
	[
		"LOCATION_19_03_ROOM4",
		"LOCATION_19_03_PENTAGRAMM",
	]},
	["LOCATION_19_03_PENTAGRAMM"] = {x = 19292.8516, y = 7876.24365, z = -38783.2813, adjacent =
	[
		"LOCATION_19_03_PEMTAGRAM_ENTRANCE",
		"LOCATION_19_03_ROOM5",
		"LOCATION_19_03_PEMTAGRAM_MOVEMENT",
	]},
	["LOCATION_19_03_ROOM2"] = {x = 21723.8379, y = 7876.24316, z = -37884.6602, adjacent =
	[
		"LOCATION_19_03_ENTRANCE",
		"LOCATION_19_03_ROOM4",
		"LOCATION_19_03_ROOM3",
	]},
	["LOCATION_19_03_ROOM3"] = {x = 22777.457, y = 7876.24365, z = -38045.6016, adjacent =
	[
		"LOCATION_19_03_ROOM2",
	]},
	["LOCATION_19_03_PATH_RUIN10"] = {x = 20757.0176, y = 7805.85254, z = -36833.043, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN9",
		"LOCATION_19_03_PATH_RUIN11",
		"LOCATION_19_03_ENTRANCE_HARPYE3",
	]},
	["LOCATION_19_03_PATH_RUIN7"] = {x = 20838.5723, y = 7613.93311, z = -35086.3398, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN8",
		"LOCATION_19_03_PATH_RUIN6",
	]},
	["LOCATION_19_03_PATH_RUIN8"] = {x = 20640.9063, y = 7568.85449, z = -35738.4844, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN9",
		"LOCATION_19_03_PATH_RUIN7",
	]},
	["LOCATION_19_03_PATH_RUIN6"] = {x = 20280.8477, y = 7612.2207, z = -34825.8711, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN7",
		"LOCATION_19_03_PATH_RUIN5",
	]},
	["PATH_TO_PLATEAU04_RIGHT"] = {x = 18068.6152, y = 6438.3252, z = -25355.5703, adjacent =
	[
		"PATH_TO_PLATEAU04_SMALLPATH",
		"PATH_TO_PLATEAU04",
	]},
	["PATH_TO_PLATEAU04_SMALLPATH"] = {x = 18493.2891, y = 6110.12402, z = -24713.0703, adjacent =
	[
		"PATH_TO_PLATEAU04_RIGHT",
		"PATH_TO_PLATEAU04_SMALLPATH2",
	]},
	["PATH_TO_PLATEAU04_SMALLPATH2"] = {x = 19896.1914, y = 5779.4043, z = -24714.7695, adjacent =
	[
		"PATH_TO_PLATEAU04_SMALLPATH",
		"PATH_TO_PLATEAU04_SMALLPATH3",
	]},
	["PATH_TO_PLATEAU04_SMALLPATH3"] = {x = 20959.3164, y = 5207.48486, z = -24699.9199, adjacent =
	[
		"PATH_TO_PLATEAU04_SMALLPATH2",
		"PATH_TO_PLATEAU03",
	]},
	["PATH_TO_PLATEAU03"] = {x = 22227.4355, y = 4503.35498, z = -23494.8359, adjacent =
	[
		"PATH_TO_PLATEAU02",
		"PATH_TO_PLATEAU04_SMALLPATH3",
		"SPAWN_OW_WARAN_EBENE_02_05",
		"OW_DJG_ROCKCAMP_03",
	]},
	["PATH_TO_PLATEAU04_BRIDGE"] = {x = 17163.1953, y = 5961.08643, z = -30182.7715, adjacent =
	[
		"PATH_TO_PLATEAU04_BRIDGE2",
		"PATH_TO_PLATEAU04",
	]},
	["PATH_TO_PLATEAU04_BRIDGE2"] = {x = 17775.7734, y = 5602.67334, z = -32314.3574, adjacent =
	[
		"PATH_TO_PLATEAU04_BRIDGE",
		"PATH_TO_PLATEAU04_BRIDGE3",
	]},
	["PATH_TO_PLATEAU04_BRIDGE3"] = {x = 18184.8457, y = 5216.76709, z = -33737.8086, adjacent =
	[
		"PATH_TO_PLATEAU04_BRIDGE2",
		"PLATEAU_ROUND07",
		"ROCKDRAGON",
	]},
	["PLATEAU_ROUND07"] = {x = 18725.3164, y = 5162.89355, z = -34101.1133, adjacent =
	[
		"PATH_TO_PLATEAU04_BRIDGE3",
		"LOCATION_19_01",
	]},
	["PATH_TO_PLATEAU04"] = {x = 16948.3809, y = 6448.10645, z = -27889.8789, adjacent =
	[
		"PATH_TO_PLATEAU04_RIGHT",
		"PATH_TO_PLATEAU04_BRIDGE",
		"PLATEAU_ROUND01",
	]},
	["PLATEAU_ROUND01"] = {x = 15530.5391, y = 6311.83203, z = -27162.084, adjacent =
	[
		"PATH_TO_PLATEAU04",
		"LOCATION_18_OUT",
		"PLATEAU_ROUND02",
	]},
	["LOCATION_18_OUT"] = {x = 11884, y = 6933.84326, z = -25989, adjacent =
	[
		"PLATEAU_ROUND01",
		"PLATEAU_ROUND02",
		"LOCATION_18_IN",
		"PLATEAU_ROUND03",
		"PLATEAU_ROUND_TO_HUT",
	]},
	["PLATEAU_ROUND02"] = {x = 13204, y = 6740.41748, z = -26909, adjacent =
	[
		"PLATEAU_ROUND02_CAVE",
		"PLATEAU_ROUND01",
		"LOCATION_18_OUT",
		"LOCATION_18_IN",
	]},
	["LOCATION_18_IN"] = {x = 12039, y = 6918.875, z = -27185, adjacent =
	[
		"LOCATION_18_OUT",
		"PLATEAU_ROUND02",
	]},
	["OW_DEADWOOD_WOLF_SPAWN01"] = {x = -23327.5977, y = 513.153381, z = -4711.68066, adjacent =
	[
		"OW_PATH_WOLF03_SPAWN02",
		"OW_PATH_042",
		"OW_PATH_041",
	]},
	["OW_PATH_WOLF03_SPAWN02"] = {x = -24541.7109, y = 578.850403, z = -5838.83496, adjacent =
	[
		"OW_DEADWOOD_WOLF_SPAWN01",
		"OW_PATH_040",
	]},
	["SPAWN_OW_BLACKGOBBO_A1"] = {x = -28735.5527, y = -718.95929, z = 9991.27441, adjacent =
	[
		"OW_MOVEMENT_BGOBBO2",
		"OW_MOVEMENT_LURKER_NEARBGOBBO03",
		"OW_MOVEMENT_BGOBBO1",
		"OW_DJG_VORPOSTEN_01",
		"VORPOSTEN",
	]},
	["OW_MOVEMENT_BGOBBO2"] = {x = -28332.6289, y = -747.216736, z = 9324.44336, adjacent =
	[
		"SPAWN_OW_BLACKGOBBO_A1",
		"SPAWN_OW_BLACKGOBBO_A2",
		"OW_MOVEMENT_BGOBBO3",
		"OW_MOVEMENT_BGOBBO1",
	]},
	["OW_PATH_1_2"] = {x = -11419.6338, y = -750.953735, z = 11551.668, adjacent =
	[
		"OW_PATH_1_1_TO_WASH",
		"SPAWN_OW_SCA_05_01",
		"OW_PATH_245",
		"OW_PATH_1_3",
		"OW_PATH_1_1",
	]},
	["OW_PATH_1_1_TO_WASH"] = {x = -11592.3809, y = -857.210266, z = 10439.7559, adjacent =
	[
		"OW_PATH_1_2",
		"SPAWN_OW_SCA_05_01",
		"OW_PATH_1_1_WASH",
		"OW_PATH_1_1",
	]},
	["SPAWN_OW_SCA_05_01"] = {x = -12293.2539, y = -668.335999, z = 12061.9336, adjacent =
	[
		"OW_PATH_1_2",
		"OW_PATH_1_1_TO_WASH",
		"SPAWN_OW_BLOODFLY_06_01",
	]},
	["SPAWN_OW_BLOODFLY_06_01"] = {x = -15162.0166, y = -506.416748, z = 11015.6377, adjacent =
	[
		"SPAWN_OW_SCA_05_01",
		"OW_PATH_1_1_WASH",
		"SPAWN_OW_SCAVENGER_OCWOOD1",
	]},
	["OW_PATH_1_1_WASH"] = {x = -12100.3555, y = -980.072021, z = 9671.16992, adjacent =
	[
		"OW_PATH_1_1_TO_WASH",
		"SPAWN_OW_BLOODFLY_06_01",
	]},
	["OW_PATH_1_5"] = {x = -6838.99658, y = -895.322632, z = 14790.0508, adjacent =
	[
		"SPAWN_GOBBO_OW_PATH_1_6",
		"SPAWN_OW_STARTSCAVNGERBO_01_02",
		"OW_PATH_1_4",
		"OW_PATH_1_5_13",
	]},
	["SPAWN_OW_SHADOWBEAST_10_03"] = {x = -29362.4004, y = 2496.18823, z = -14380.3135, adjacent =
	[
		"OW_PATH_STONEHENGE_7",
		"OW_PATH_3_09",
	]},
	["OW_PATH_STONEHENGE_7"] = {x = -27604.7891, y = 1943.76123, z = -13465.0654, adjacent =
	[
		"SPAWN_OW_SHADOWBEAST_10_03",
		"OW_PATH_STONEHENGE_6",
		"OW_PATH_3_07",
	]},
	["OW_PATH_3_03"] = {x = -13017, y = 520.266174, z = -11089, adjacent =
	[
		"OW_PATH_272",
		"OW_ORC_ORCDOG_MOVEMENT_03",
		"OW_PATH_162",
	]},
	["OW_ORC_ORCDOG_MOVEMENT_03"] = {x = -13630.4307, y = 715.622192, z = -11346.7168, adjacent =
	[
		"OW_PATH_3_03",
		"OW_ORC_ORCDOG_MOVEMENT_02",
	]},
	["OW_ORC_ORCDOG_SPAWN01_MOVEMENT"] = {x = -17877.377, y = 1198.4823, z = -12375.2842, adjacent =
	[
		"OW_ORC_ORCDOG_MOVEMENT_01",
		"OW_ORC_ORCDOG_SPAWN01",
		"OW_PATH_3_05",
		"OW_ORC_LOOKOUT_01",
	]},
	["OW_ORC_ORCDOG_MOVEMENT_01"] = {x = -15479.8223, y = 1093.53723, z = -11912.373, adjacent =
	[
		"OW_ORC_ORCDOG_SPAWN01_MOVEMENT",
		"OW_ORC_ORCDOG_MOVEMENT_02",
		"OW_ORC_ORCDOG_SPAWN01",
	]},
	["OW_ORC_ORCDOG_MOVEMENT_02"] = {x = -14406.2236, y = 953.940063, z = -11673.3984, adjacent =
	[
		"OW_ORC_ORCDOG_MOVEMENT_03",
		"OW_ORC_ORCDOG_MOVEMENT_01",
	]},
	["OW_ORC_ORCDOG_SPAWN01"] = {x = -16222.0879, y = 1114.27991, z = -12954.4785, adjacent =
	[
		"OW_ORC_ORCDOG_SPAWN01_MOVEMENT",
		"OW_ORC_ORCDOG_MOVEMENT_01",
		"OW_ORC_LOOKOUT_02",
	]},
	["MOVEMENT_MOLERAT_06_CAVE_GUARD2"] = {x = -9224.51563, y = -94.8815308, z = -8604.88281, adjacent =
	[
		"OC_ROUND_17",
		"OC_ROUND_18",
		"MOVE_OW_SCAVENGER_AL_ORC_MOVEMENT",
		"MOVEMENT_MOLERAT_06_CAVE_GUARD3",
	]},
	["OC_ROUND_18"] = {x = -9645.99219, y = -640.654053, z = -6656.77197, adjacent =
	[
		"OC_ROUND_17",
		"MOVEMENT_MOLERAT_06_CAVE_GUARD2",
		"MOVE_OW_SCAVENGER_AL_ORC_MOVEMENT",
		"SPAWN_OW_SCAVENGER_AL_ORC",
		"OC6",
	]},
	["MOVEMENT_PATH_GUARD1"] = {x = -12152.5029, y = 0.439300537, z = -8363.59082, adjacent =
	[
		"SPAWN_PATH_GUARD1",
		"MOVE_OW_SCAVENGER_AL_ORC_MOVEMENT",
		"MOVEMENT_PATH_GUARD2",
	]},
	["MOVE_OW_SCAVENGER_AL_ORC_MOVEMENT"] = {x = -11248.0488, y = -296.992737, z = -7388.92188, adjacent =
	[
		"SPAWN_PATH_GUARD1",
		"MOVEMENT_MOLERAT_06_CAVE_GUARD2",
		"OC_ROUND_18",
		"MOVEMENT_PATH_GUARD1",
		"MOVEMENT_PATH_GUARD2",
		"SPAWN_OW_SCAVENGER_AL_ORC",
	]},
	["MOVEMENT_PATH_GUARD2"] = {x = -11718.6357, y = 3.58300781, z = -8606.20898, adjacent =
	[
		"SPAWN_PATH_GUARD1",
		"MOVEMENT_PATH_GUARD1",
		"MOVE_OW_SCAVENGER_AL_ORC_MOVEMENT",
	]},
	["OW_PATH_3_05"] = {x = -20040.5215, y = 1184.3656, z = -13731.8555, adjacent =
	[
		"OW_ORC_ORCDOG_SPAWN01_MOVEMENT",
		"OW_PATH_3_06",
	]},
	["SPAWN_OW_SHADOWBEAST_NEAR_SHADOW4"] = {x = -29480.2988, y = 1478.45947, z = -8525.09375, adjacent =
	[
		"OW_PATH_STONEHENGE_4",
		"OW_PATH_037",
	]},
	["OW_PATH_STONEHENGE_4"] = {x = -28281.0605, y = 1446.20032, z = -9634.60938, adjacent =
	[
		"SPAWN_OW_SHADOWBEAST_NEAR_SHADOW4",
		"OW_PATH_STONEHENGE_3",
		"OW_PATH_STONEHENGE_5",
	]},
	["OW_PATH_038"] = {x = -28885.0684, y = 949.358398, z = -6049.49365, adjacent =
	[
		"OW_PATH_STONEHENGE_1",
		"OW_PATH_039",
		"OW_PATH_WARAN06_SPAWN01",
		"OW_PATH_037",
	]},
	["OW_PATH_STONEHENGE_1"] = {x = -28084.0625, y = 906.228088, z = -6580.85547, adjacent =
	[
		"OW_PATH_038",
		"OW_PATH_039",
		"OW_PATH_STONEHENGE_2",
	]},
	["OW_PATH_039"] = {x = -26920.0254, y = 719.713379, z = -6062.79932, adjacent =
	[
		"OW_PATH_038",
		"OW_PATH_STONEHENGE_1",
		"OW_PATH_WARAN06_SPAWN01",
		"OW_PATH_040",
	]},
	["OW_PATH_STONEHENGE_2"] = {x = -28347.7734, y = 1115.4906, z = -7421.29785, adjacent =
	[
		"OW_PATH_STONEHENGE_1",
		"OW_PATH_STONEHENGE_3",
	]},
	["OW_PATH_STONEHENGE_3"] = {x = -27735.7051, y = 1202.43665, z = -8146.74707, adjacent =
	[
		"OW_PATH_STONEHENGE_4",
		"OW_PATH_STONEHENGE_2",
	]},
	["OW_PATH_STONEHENGE_5"] = {x = -28420.2832, y = 1701.19812, z = -10939.2959, adjacent =
	[
		"OW_PATH_STONEHENGE_4",
		"OW_PATH_STONEHENGE_6",
	]},
	["OW_PATH_STONEHENGE_6"] = {x = -28386.4199, y = 2129.41138, z = -12320.4238, adjacent =
	[
		"OW_PATH_STONEHENGE_7",
		"OW_PATH_STONEHENGE_5",
	]},
	["OW_PATH_3_07"] = {x = -26389.6797, y = 1815.8667, z = -13776.6777, adjacent =
	[
		"OW_PATH_STONEHENGE_7",
		"OW_PATH_3_06",
	]},
	["OW_PATH_WARAN06_SPAWN01"] = {x = -28645.7148, y = 798.214966, z = -4865.43164, adjacent =
	[
		"OW_PATH_038",
		"OW_PATH_039",
		"OW_LAKE_NC_BLOODFLY_SPAWN01",
	]},
	["OW_LAKE_NC_BLOODFLY_SPAWN01"] = {x = -31463.0918, y = 250.719696, z = -3183.24097, adjacent =
	[
		"OW_PATH_WARAN06_SPAWN01",
		"OW_LAKE_NC_LURKER_SPAWN01_MOVEMENT",
	]},
	["OW_LAKE_NC_LURKER_SPAWN01_MOVEMENT"] = {x = -33861.3008, y = 43.804348, z = -5799.09424, adjacent =
	[
		"OW_LAKE_NC_BLOODFLY_SPAWN01",
		"OW_LAKE_NC_LURKER_SPAWN01_MOVEMENT2",
		"OW_LAKE_NC_LURKER_SPAWN01",
	]},
	["OW_LAKE_NC_LURKER_SPAWN01_MOVEMENT2"] = {x = -32910.207, y = 87.636734, z = -4557.89941, adjacent =
	[
		"OW_LAKE_NC_LURKER_SPAWN01_MOVEMENT",
	]},
	["OW_LAKE_NC_LURKER_SPAWN01"] = {x = -35421.2383, y = -69.1514206, z = -5944.44189, adjacent =
	[
		"OW_LAKE_NC_LURKER_SPAWN01_MOVEMENT",
	]},
	["OW_PATH_153"] = {x = -32485.4805, y = 993.366028, z = 9775.62988, adjacent =
	[
		"MOVEMENT_OW_PATH_SCAVENGER13_SPAWN01",
		"OW_PATH_SCAVENGER13_SPAWN01",
		"OW_PATH_152K",
		"OW_PATH_154",
	]},
	["OW_PATH_SCAVENGER13_SPAWN01"] = {x = -30993.3047, y = 897.863953, z = 8963.64063, adjacent =
	[
		"MOVEMENT_OW_PATH_SCAVENGER13_SPAWN01",
		"OW_PATH_153",
		"OW_PATH_152K",
	]},
	["MOVEMENT_OW_PATH_SCAVENGER13_SPAWN03"] = {x = -31105.0215, y = 795.105347, z = 11891.2549, adjacent =
	[
		"MOVEMENT_OW_PATH_SCAVENGER13_SPAWN01",
		"OW_PATH_151",
	]},
	["OW_PATH_151"] = {x = -31807.0195, y = 658.606018, z = 12286.5205, adjacent =
	[
		"MOVEMENT_OW_PATH_SCAVENGER13_SPAWN03",
		"SPAWN_OW_BLOODFLYS_152",
		"OW_PATH_152K",
		"OW_PATH_150",
		"OW_SPAWN_TRACK_LEICHE_00",
	]},
	["SPAWN_OW_MINICOAST_LURKER_A1"] = {x = -21641.6406, y = -931.771362, z = 10296.5693, adjacent =
	[
		"MOVEMENT_OW_MINICOAST_LURKER_A1",
	]},
	["MOVEMENT_OW_MINICOAST_LURKER_A1"] = {x = -22881.2539, y = -891.222168, z = 10540.2764, adjacent =
	[
		"SPAWN_OW_MINICOAST_LURKER_A1",
		"SPAWN_OW_NEARBGOBBO_LURKER_A1",
	]},
	["OW_PATH_245"] = {x = -11029.7002, y = -755.06012, z = 12986.0664, adjacent =
	[
		"MOVEMENT_OW_PATH_SCAVENGER01_SPAWN01",
		"OW_PATH_246",
		"OW_PATH_1_2",
		"OW_PATH_1_3",
	]},
	["OW_PATH_OC_NC3"] = {x = -12516.5078, y = -1753.01636, z = -1945.31689, adjacent =
	[
		"HELPPOINT2",
		"OW_PATH_OC_NC4",
		"OW_PATH_OC_NC2_TO_TOWER_01",
	]},
	["HELPPOINT2"] = {x = -13383.4316, y = -2023.93628, z = -1241.06531, adjacent =
	[
		"OW_PATH_OC_NC3",
		"OW_PATH_OC_NC4",
		"OW_PATH_OC_NC5",
	]},
	["OW_SAWHUT_MOLERAT_MOVEMENT"] = {x = -15979.0098, y = -1260.69543, z = 5836.70752, adjacent =
	[
		"OW_SAWHUT_MOLERAT_MOVEMENT3",
		"OW_SAWHUT_MOLERAT_MOVEMENT2",
		"OW_STAND_GUARDS",
	]},
	["OW_SAWHUT_MOLERAT_MOVEMENT3"] = {x = -15496.7832, y = -1215.31201, z = 5483.66992, adjacent =
	[
		"OW_SAWHUT_MOLERAT_MOVEMENT8",
		"OW_SAWHUT_MOLERAT_MOVEMENT",
		"OW_SAWHUT_MOLERAT_MOVEMENT2",
	]},
	["OW_SAWHUT_MOLERAT_MOVEMENT2"] = {x = -15677.585, y = -1223.68396, z = 5071.06738, adjacent =
	[
		"OW_SAWHUT_MOLERAT_MOVEMENT",
		"OW_SAWHUT_MOLERAT_MOVEMENT3",
		"OW_SAWHUT_MOLERAT_MOVEMENT4",
	]},
	["OW_SAWHUT_MOLERAT_MOVEMENT5"] = {x = -14039.6436, y = -1192.30261, z = 5235.26318, adjacent =
	[
		"OW_SAWHUT_MOLERAT_MOVEMENT8",
		"OW_SAWHUT_MOLERAT_MOVEMENT6",
	]},
	["OW_SAWHUT_MOLERAT_MOVEMENT6"] = {x = -14248.6367, y = -1330.3822, z = 3669.6311, adjacent =
	[
		"OW_SAWHUT_GREENGOBBO_SPAWN",
		"OW_SAWHUT_MOLERAT_MOVEMENT5",
	]},
	["MOVEMENT_OW_SNAPPER_OCWOOD1_05_02"] = {x = 14661.3213, y = -1299.53284, z = 11041.2266, adjacent =
	[
		"SPAWN_OW_LURKER_RIVER2",
		"SPAWN_OW_SNAPPER_OCWOOD1_05_02",
	]},
	["SPAWN_OW_SNAPPER_OCWOOD1_05_02"] = {x = 17437.502, y = -1015.10376, z = 9942.75488, adjacent =
	[
		"OW_PATH_166",
		"MOVEMENT_OW_SNAPPER_OCWOOD1_05_02",
	]},
	["SPAWN_OW_BLOODFLYS_152"] = {x = -33165.1641, y = 784.500671, z = 11434.1396, adjacent =
	[
		"OW_PATH_151",
		"MOVEMENT_OW_BLOODFLYS_152",
		"OW_PATH_152K",
	]},
	["MOVEMENT_OW_BLOODFLYS_152"] = {x = -33899.8789, y = 737.547546, z = 11483.1074, adjacent =
	[
		"SPAWN_OW_BLOODFLYS_152",
	]},
	["OW_MOVEMENT_LURKER_NEARBGOBBO03"] = {x = -27982.75, y = -530.414124, z = 11713.1885, adjacent =
	[
		"SPAWN_OW_BLACKGOBBO_A1",
		"OW_MOVEMENT_LURKER_NEARBGOBBO01",
		"SPAWN_OW_NEARBGOBBO_LURKER_A1",
	]},
	["OW_MOVEMENT_LURKER_NEARBGOBBO01"] = {x = -26448.3418, y = -406.25354, z = 11114.835, adjacent =
	[
		"OW_MOVEMENT_LURKER_NEARBGOBBO03",
		"OW_MOVEMENT_LURKER_NEARBGOBBO02",
		"SPAWN_OW_NEARBGOBBO_LURKER_A1",
	]},
	["SPAWN_OW_WARAN_CAVE1_1"] = {x = -27021.4375, y = -548.413574, z = 9487.80273, adjacent =
	[
		"OW_MOVEMENT_LURKER_NEARBGOBBO02",
		"OW_MOVEMENT_BGOBBO3",
	]},
	["OW_MOVEMENT_LURKER_NEARBGOBBO02"] = {x = -26602.6094, y = -409.719757, z = 10548.1465, adjacent =
	[
		"OW_MOVEMENT_LURKER_NEARBGOBBO01",
		"SPAWN_OW_WARAN_CAVE1_1",
		"OW_MOVEMENT_BGOBBO3",
		"OW_MOVEMENT_BGOBBO1",
	]},
	["SPAWN_OW_BLACKGOBBO_A2"] = {x = -28521.3008, y = -763.064697, z = 8241.63086, adjacent =
	[
		"OW_MOVEMENT_BGOBBO2",
		"OW_MOVEMENT_BGOBBO3",
		"MOVEMENT_OW_BLACKGOBBO_A2",
	]},
	["OW_MOVEMENT_BGOBBO3"] = {x = -27615.0449, y = -701.535461, z = 9438.50879, adjacent =
	[
		"OW_MOVEMENT_BGOBBO2",
		"SPAWN_OW_WARAN_CAVE1_1",
		"OW_MOVEMENT_LURKER_NEARBGOBBO02",
		"SPAWN_OW_BLACKGOBBO_A2",
		"OW_MOVEMENT_BGOBBO1",
	]},
	["MOVEMENT_OW_BLACKGOBBO_A2"] = {x = -28607.6465, y = -788.904236, z = 7779.05469, adjacent =
	[
		"SPAWN_OW_BLACKGOBBO_A2",
		"OW_NC_ABYSS4",
	]},
	["OW_NC_ABYSS4"] = {x = -29243.4082, y = -584.743347, z = 7374.78027, adjacent =
	[
		"MOVEMENT_OW_BLACKGOBBO_A2",
		"OW_NC_ABYSS3",
	]},
	["OW_NC_ABYSS3"] = {x = -29750.668, y = -511.452454, z = 7152.30908, adjacent =
	[
		"OW_NC_ABYSS4",
		"OW_NC_ABYSS2",
	]},
	["OW_NC_ABYSS2"] = {x = -31027.2168, y = -379.637268, z = 6893.46777, adjacent =
	[
		"OW_NC_ABYSS",
		"OW_NC_ABYSS3",
	]},
	["OW_WOODRUIN_FOR_WOLF_SPAWN"] = {x = -23981.25, y = -380.869751, z = 2896.26392, adjacent =
	[
		"PATH_OC_NC_13",
		"PATH_OC_NC_11_MOVEMENT",
		"PATH_OC_NC_12",
		"PATH_OC_NC_14",
	]},
	["PATH_OC_NC_11_MOVEMENT"] = {x = -22807.3242, y = -132.287399, z = 3053.17993, adjacent =
	[
		"OW_WOODRUIN_FOR_WOLF_SPAWN",
		"PATH_OC_NC_11",
	]},
	["PATH_OC_NC_11"] = {x = -22493.4941, y = -138.406845, z = 2565.34863, adjacent =
	[
		"PATH_OC_NC_11_MOVEMENT",
		"PATH_OC_NC_10",
		"PATH_OC_NC_12",
	]},
	["PATH_WALD_OC_WOLFSPAWN"] = {x = 15306.9736, y = -39.7515411, z = 7151.76416, adjacent =
	[
		"PATH_OC_OCWOOD_4",
		"PATH_OC_OCWOOD_3",
		"PATH_OC_OCWOOD_5",
		"PATH_WALD_OC_WOLFSPAWN2",
	]},
	["PATH_OC_OCWOOD_4"] = {x = 14136.9971, y = -89.24617, z = 6856.41846, adjacent =
	[
		"PATH_WALD_OC_WOLFSPAWN",
		"PATH_OC_OCWOOD_3",
		"PATH_WALD_OC_WOLFSPAWN_MOVEMENT2",
		"PATH_OC_OCWOOD_5",
		"PATH_WALD_OC_WOLFSPAWN2",
	]},
	["PATH_OC_OCWOOD"] = {x = 12218.2813, y = -509.98584, z = 8276.29102, adjacent =
	[
		"OW_PATH_004",
		"SPAWN_OW_SCAVENGER_INWALD_OC2",
		"PATH_OC_OCWOOD_2",
		"OC_ROUND_6",
	]},
	["SPAWN_OW_SCAVENGER_INWALD_OC2"] = {x = 12566.2988, y = -218.031525, z = 7358.76074, adjacent =
	[
		"PATH_OC_OCWOOD",
		"PATH_OC_OCWOOD_3",
		"PATH_OC_OCWOOD_2",
	]},
	["PATH_OC_OCWOOD_3"] = {x = 13564.4824, y = -69.3276443, z = 7379.21533, adjacent =
	[
		"PATH_WALD_OC_WOLFSPAWN",
		"PATH_OC_OCWOOD_4",
		"SPAWN_OW_SCAVENGER_INWALD_OC2",
		"PATH_WALD_OC_WOLFSPAWN_MOVEMENT2",
		"PATH_OC_OCWOOD_2",
	]},
	["SPAWN_OW_WOLF2_WALD_OC3"] = {x = 13746.4844, y = 133.151001, z = 5510.24805, adjacent =
	[
		"SPAWN_OW_SCAVENGER_LONE_WALD_OC3",
		"PATH_WALD_OC_WOLFSPAWN_MOVEMENT2",
		"SPAWN_WALD_OC_BLOODFLY01",
		"PATH_WALD_OC_WOLFSPAWN_MOVEMENT",
	]},
	["SPAWN_OW_SCAVENGER_LONE_WALD_OC3"] = {x = 15021.7285, y = 52.7052498, z = 5513.08789, adjacent =
	[
		"SPAWN_OW_WOLF2_WALD_OC3",
		"PATH_OC_OCWOOD_5",
		"PATH_WALD_OC_WOLFSPAWN_MOVEMENT",
	]},
	["PATH_WALD_OC_WOLFSPAWN_MOVEMENT2"] = {x = 13655.2266, y = -18.0200424, z = 6478.90527, adjacent =
	[
		"PATH_OC_OCWOOD_4",
		"PATH_OC_OCWOOD_3",
		"SPAWN_OW_WOLF2_WALD_OC3",
		"PATH_WALD_OC_WOLFSPAWN_MOVEMENT",
		"PATH_WALD_OC_WOLFSPAWN2",
	]},
	["PATH_OC_PSI_01"] = {x = 10394.0107, y = 4.81902695, z = -6323.79004, adjacent =
	[
		"LOCATION_14_01",
		"OC_ROUND_27",
		"OC_ROUND_10",
		"OC_ROUND_11",
	]},
	["OC_ROUND_27"] = {x = 7399.84229, y = -608.019653, z = -6187.896, adjacent =
	[
		"PATH_OC_PSI_01",
		"SPAWN_OW_SCAVENGER_OC_PSI_RUIN1",
		"OC_ROUND_11",
		"OC_ROUND_12",
		"OC10",
	]},
	["SPAWN_OW_SCAVENGER_OC_PSI_RUIN1"] = {x = 7468.58838, y = -288.795349, z = -8251.3623, adjacent =
	[
		"OC_ROUND_27",
		"SPAWN_OW_WARAN_OC_PSI3",
		"OW_PATH_001",
	]},
	["OC_ROUND_26"] = {x = 4961.17578, y = -557.643433, z = -10072.5391, adjacent =
	[
		"OC_ROUND_13",
		"OC_ROUND_12",
		"SPAWN_OW_SMALLCAVE01_MOLERAT",
	]},
	["OC_ROUND_13"] = {x = 4147.63232, y = -561.476807, z = -8654.13379, adjacent =
	[
		"OC_ROUND_26",
		"OC_ROUND_12",
		"OC_ROUND_14",
		"SPAWN_OW_SMALLCAVE01_MOLERAT",
		"OC9",
	]},
	["OC_ROUND_15"] = {x = -3725.0271, y = -783.80127, z = -9837.38867, adjacent =
	[
		"SPAWN_OW_SCAVENGER_06_04",
		"LOCATION_01_01",
		"OC_ROUND_14",
		"OC8",
	]},
	["LOCATION_01_01"] = {x = -5479.51904, y = -419.358948, z = -10154.3584, adjacent =
	[
		"SPAWN_OW_SCAVENGER_06_04",
		"OC_ROUND_15",
		"LOCATION_01_02",
	]},
	["SPAWN_OW_SCAVENGER_AL_ORC"] = {x = -10773.6387, y = -683.509949, z = -5351.24609, adjacent =
	[
		"OC_ROUND_18",
		"MOVE_OW_SCAVENGER_AL_ORC_MOVEMENT",
		"OW_PATH_3_01",
		"OW_PATH_268",
		"OC5",
	]},
	["O_SCAVENGER_OCWOODL2_MOVEMENT3"] = {x = 6226.35547, y = -1188.56091, z = 7677.33447, adjacent =
	[
		"SPAWN_O_SCAVENGER_05_02",
		"OC_ROUND_24",
		"O_SCAVENGER_OCWOODL2_MOVEMENT2",
		"OC_ROUND_5",
	]},
	["SPAWN_O_SCAVENGER_05_02"] = {x = 2443.01611, y = -1009.66846, z = 7895.66943, adjacent =
	[
		"O_SCAVENGER_OCWOODL2_MOVEMENT3",
		"OC_ROUND_24",
		"OC_ROUND_23",
		"OC13",
	]},
	["O_SCAVENGER_OCWOODL2_MOVEMENT"] = {x = 7739.35498, y = -1037.08105, z = 7138.14014, adjacent =
	[
		"OW_PATH_004",
		"O_SCAVENGER_OCWOODL2_MOVEMENT2",
		"SPAWN_O_SCAVENGER_OCWOODL2",
		"OC_ROUND_7",
	]},
	["OC_ROUND_24"] = {x = 2410.23584, y = -1078.38586, z = 8551.00488, adjacent =
	[
		"O_SCAVENGER_OCWOODL2_MOVEMENT3",
		"SPAWN_O_SCAVENGER_05_02",
		"OC_ROUND_5",
		"OC_ROUND_23",
		"SPAWN_OW_BLOODFLY_OC_WOOD03",
	]},
	["O_SCAVENGER_OCWOODL2_MOVEMENT2"] = {x = 7087.38281, y = -1043.70581, z = 7009.88574, adjacent =
	[
		"O_SCAVENGER_OCWOODL2_MOVEMENT3",
		"O_SCAVENGER_OCWOODL2_MOVEMENT",
		"OC12",
	]},
	["OC_ROUND_5"] = {x = 7391.13135, y = -1322.47339, z = 8433.31348, adjacent =
	[
		"OW_PATH_004",
		"O_SCAVENGER_OCWOODL2_MOVEMENT3",
		"OC_ROUND_24",
		"SPAWN_O_SCAVENGER_OCWOODL2",
		"OW_PATH_OW_PATH_WARAN05_SPAWN01",
		"OW_PATH_WARAN05_SPAWN02",
	]},
	["SPAWN_O_SCAVENGER_OCWOODL2"] = {x = 7851.76904, y = -1173.48535, z = 7958.92334, adjacent =
	[
		"OW_PATH_004",
		"O_SCAVENGER_OCWOODL2_MOVEMENT",
		"OC_ROUND_5",
	]},
	["OW_PATH_158"] = {x = -39926.1055, y = 50.0149994, z = -3220.26953, adjacent =
	[
		"LOCATION_23_01",
		"OW_PATH_159",
		"LOCATION_23_CAVE_1_OUT",
		"OW_SCAVENGER_COAST_NEWCAMP_SPAWN",
	]},
	["LOCATION_23_01"] = {x = -38039.4063, y = 44.6227875, z = -2619.25977, adjacent =
	[
		"OW_PATH_158",
		"OW_SCAVENGER_COAST_NEWCAMP_SPAWN",
	]},
	["OW_PATH_265"] = {x = -27159.3555, y = -687.182983, z = 21385.4902, adjacent =
	[
		"OW_PATH_2_04",
		"OW_PATH_264",
		"OW_PATH_266",
	]},
	["OW_PATH_264"] = {x = -26007.3613, y = -562.152466, z = 20638.4922, adjacent =
	[
		"SPAWN_OW_SCAVENGER_OCWOODEND2",
		"OW_PATH_265",
		"OW_PATH_263",
		"OW_PATH_266",
	]},
	["OW_PATH_263"] = {x = -24945.7188, y = -587.441895, z = 19792.7617, adjacent =
	[
		"SPAWN_OW_SCAVENGER_OCWOODEND2",
		"OW_PATH_264",
		"SPAWN_OW_MOLERATS_WOOD_OM",
		"OW_PATH_262",
	]},
	["SPAWN_OW_MOLERATS_WOOD_OM"] = {x = -23525.832, y = -134.71521, z = 20825.2578, adjacent =
	[
		"OW_PATH_263",
		"SPAWN_OW_MOLERAT_WOODOLDMINE2",
	]},
	["SPAWN_OW_MOLERAT_WOODOLDMINE2"] = {x = -22849.3535, y = -92.8881378, z = 19335.8789, adjacent =
	[
		"MOVEMENT_OW_MOLERAT_WOODOLDMINE2",
		"SPAWN_OW_MOLERATS_WOOD_OM",
	]},
	["SPAWN_OW_BLOODFLY_WOOD05_01"] = {x = -21064.4844, y = -207.022736, z = 18254.1797, adjacent =
	[
		"MOVEMENT_OW_MOLERAT_WOODOLDMINE2",
		"SPAWN_OW_SCAVENGER_WOOD10_04",
		"SPAWN_OW_SCAVENGER_OLDWOOD_C3",
	]},
	["SPAWN_OW_BLOODFLY_E_3"] = {x = -4074.61743, y = -1427.89355, z = 11490.0879, adjacent =
	[
		"MOVEMENT_OW_BLOODFLY_E_3",
		"MOVEMENT_OW_BLOODFLY_E_5",
	]},
	["MOVEMENT_OW_BLOODFLY_E_5"] = {x = -4987.77197, y = -1423.67834, z = 11267.1514, adjacent =
	[
		"SPAWN_OW_BLOODFLY_E_3",
	]},
	["MOVEMENT_OW_BLOODFLY_E_4"] = {x = -2623.60083, y = -1410.56519, z = 11933.2471, adjacent =
	[
		"MOVEMENT_OW_BLOODFLY_E_3",
		"OW_PATH_WARAN05_SPAWN02_04",
	]},
	["SPAWN_OW_LURKER_RIVER2_BEACH3_2"] = {x = 12740.4229, y = -1365.18567, z = 13678.6074, adjacent =
	[
		"SPAWN_OW_LURKER_RIVER2",
		"SPAWN_OW_LURKER_RIVER2_BEACH3",
	]},
	["OW_PATH_BLOODFLY01_MOVEMENT"] = {x = 10159.5674, y = -1383.19421, z = 15238.8164, adjacent =
	[
		"OW_PATH_BLOODFLY01_SPAWN01",
		"SPAWN_OW_LURKER_RIVER2_BEACH3",
	]},
	["SPAWN_OW_LURKER_RIVER2_BEACH3"] = {x = 11591.1621, y = -1377.90576, z = 14378.8057, adjacent =
	[
		"SPAWN_OW_LURKER_RIVER2_BEACH3_2",
		"OW_PATH_BLOODFLY01_MOVEMENT",
	]},
	["PATH_OC_OCWOOD_2"] = {x = 12941.4238, y = -269.876251, z = 7825.20801, adjacent =
	[
		"PATH_OC_OCWOOD",
		"SPAWN_OW_SCAVENGER_INWALD_OC2",
		"PATH_OC_OCWOOD_3",
	]},
	["OC_ROUND_23"] = {x = -604.737671, y = -1151.72778, z = 8111.18018, adjacent =
	[
		"SPAWN_O_SCAVENGER_05_02",
		"OC_ROUND_24",
		"SPAWN_OW_BLOODFLY_OC_WOOD03",
		"OC_ROUND_3",
	]},
	["SPAWN_OW_BLOODFLY_OC_WOOD03"] = {x = -335.809296, y = -1189.97717, z = 9084.10254, adjacent =
	[
		"OC_ROUND_24",
		"OC_ROUND_23",
	]},
	["OC_ROUND_22"] = {x = -3829.94238, y = -1130.90771, z = 8347.41016, adjacent =
	[
		"OW_PATH_191",
		"OC_ROUND_3",
		"SPAWN_A_OW_OC_NC_AMBIENTNSC2",
		"OC_ROUND_22_CF_2_MOVEMENT2",
	]},
	["OW_PATH_191"] = {x = -6501.97314, y = -1311.48047, z = 8617.23047, adjacent =
	[
		"OC_ROUND_2",
		"OC_ROUND_22",
		"OW_PATH_192",
	]},
	["OW_SAWHUT_MOLERAT_MOVEMENT4"] = {x = -15748.2793, y = -1275.0011, z = 3951.18652, adjacent =
	[
		"PATH_OC_NC_3",
		"OW_SAWHUT_MOLERAT_MOVEMENT2",
		"PATH_OC_NC_5_1",
		"PATH_OC_NC_4",
	]},
	["PATH_OC_NC_5_1"] = {x = -16123.6113, y = -1431.67542, z = 2670.6001, adjacent =
	[
		"OW_SAWHUT_MOLERAT_MOVEMENT4",
		"PATH_OC_NC_6_1",
		"PATH_OC_NC_4",
		"PATH_OC_NC_5",
	]},
	["OW_PATH_3_01"] = {x = -11515, y = -880.842102, z = -3200, adjacent =
	[
		"SPAWN_OW_SCAVENGER_AL_ORC",
		"OW_PATH_268",
		"CROSSING_PATH_2_3_4",
	]},
	["OW_PATH_268"] = {x = -11912.0322, y = -1003.40991, z = -3935.12915, adjacent =
	[
		"SPAWN_OW_SCAVENGER_AL_ORC",
		"OW_PATH_3_01",
		"OW_PATH_OC_NC",
	]},
	["OW_PATH_018"] = {x = 6851.20801, y = 172.169601, z = -12796.9473, adjacent =
	[
		"LOCATION_14_04",
		"OW_PATH_003",
		"OW_PATH_017",
	]},
	["SPAWN_OW_WARAN_OC_PSI3"] = {x = 8295.08105, y = 224.864258, z = -10630.9219, adjacent =
	[
		"LOCATION_14_04",
		"LOCATION_14_01",
		"SPAWN_OW_SCAVENGER_OC_PSI_RUIN1",
		"OW_PATH_001",
	]},
	["SPAWN_WALD_OC_BLOODFLY01"] = {x = 14060.4658, y = 132.040833, z = 3920.81079, adjacent =
	[
		"SPAWN_OW_WOLF2_WALD_OC3",
		"SPAWN_OW_MOLERAT2_WALD_OC1",
		"OW_PATH_WARAN01_SPAWN02",
	]},
	["SPAWN_OW_MOLERAT2_WALD_OC1"] = {x = 11996.1523, y = 18.7809334, z = 3027.4834, adjacent =
	[
		"SPAWN_WALD_OC_BLOODFLY01",
	]},
	["PATH_OC_FOGTOWER03"] = {x = 16835.3691, y = 141.310913, z = 2588.72754, adjacent =
	[
		"PATH_OC_OCWOOD_8",
		"OW_PATH_294",
		"OW_PATH_293",
	]},
	["PATH_OC_OCWOOD_8"] = {x = 17478.7148, y = 41.8319206, z = 3571.47412, adjacent =
	[
		"PATH_OC_FOGTOWER03",
		"OW_PATH_SCAVENGER03_SPAWN01",
		"PATH_OC_OCWOOD_7",
		"OW_PATH_293",
	]},
	["OW_PATH_SCAVENGER03_SPAWN01"] = {x = 18298.4473, y = -85.7326355, z = 4467.09717, adjacent =
	[
		"PATH_OC_OCWOOD_8",
		"PATH_OC_OCWOOD_7",
		"PATH_OC_FOGTOWER05",
		"PATH_OC_FOGTOWER04",
	]},
	["PATH_OC_OCWOOD_7"] = {x = 17171.3203, y = 137.999039, z = 4371.63965, adjacent =
	[
		"PATH_OC_OCWOOD_8",
		"OW_PATH_SCAVENGER03_SPAWN01",
		"PATH_OC_OCWOOD_6",
	]},
	["OC_ROUND_6"] = {x = 9908.92773, y = -972.770325, z = 7166.23584, adjacent =
	[
		"OW_PATH_004",
		"PATH_OC_OCWOOD",
		"OC_ROUND_7",
	]},
	["OW_PATH_1_3"] = {x = -10631.6738, y = -820.640015, z = 12408.1367, adjacent =
	[
		"OW_PATH_1_2",
		"OW_PATH_245",
		"SPAWN_OW_STARTSCAVNGERBO_01_02",
		"SPAWN_OW_STARTSCAVENGER_02_01",
		"OW_PATH_1_4",
	]},
	["SPAWN_OW_STARTSCAVNGERBO_01_02"] = {x = -8194.06836, y = -1056.34753, z = 13293.498, adjacent =
	[
		"OW_PATH_1_5",
		"OW_PATH_1_3",
		"OW_PATH_1_4",
	]},
	["SPAWN_OW_SCAVENGER_OCWOOD1"] = {x = -16194.749, y = -471.882935, z = 12979.9814, adjacent =
	[
		"SPAWN_OW_BLOODFLY_06_01",
		"MOVMENT_MOLERATS_OCWOOD",
	]},
	["SPAWN_OW_BLACKWOLF_02_01"] = {x = -11931.374, y = 922.011902, z = -13764.5039, adjacent =
	[
		"LOCATION_02_01",
		"OW_PATH_194",
		"OW_PATH_162",
	]},
	["LOCATION_02_01"] = {x = -11317.4463, y = 930.896667, z = -13100.3418, adjacent =
	[
		"SPAWN_OW_BLACKWOLF_02_01",
		"OW_PATH_162",
		"LOCATION_02_02",
		"OW_RITTER_LEICHE_02",
	]},
	["OW_PATH_194"] = {x = -12913.2227, y = 1038.10034, z = -14688.3652, adjacent =
	[
		"SPAWN_OW_BLACKWOLF_02_01",
		"OW_PATH_195",
		"OW_PATH_162",
	]},
	["OW_PATH_102"] = {x = -13521.8174, y = 1620.99512, z = -17242.8125, adjacent =
	[
		"SPAWN_OW_MOLERAT_ORC_04",
		"OW_PATH_195",
		"OW_PATH_103",
		"OW_PATH_101",
	]},
	["SPAWN_OW_MOLERAT_ORC_04"] = {x = -12384.3096, y = 1470.04419, z = -16419.875, adjacent =
	[
		"OW_PATH_102",
		"OW_PATH_195",
		"OW_PATH_103",
	]},
	["OW_PATH_195"] = {x = -13167.0439, y = 1198.64246, z = -15469.5889, adjacent =
	[
		"OW_PATH_194",
		"OW_PATH_102",
		"SPAWN_OW_MOLERAT_ORC_04",
	]},
	["OW_PATH_103"] = {x = -11488.1025, y = 1863.26746, z = -18053.9082, adjacent =
	[
		"OW_PATH_102",
		"SPAWN_OW_MOLERAT_ORC_04",
		"OW_PATH_104",
	]},
	["SPAWN_OW_SCAVENGER_ORC_03"] = {x = -11524.6953, y = 419.177673, z = -25895.9395, adjacent =
	[
		"OW_PATH_306",
		"OW_PATH_305",
	]},
	["OW_PATH_305"] = {x = -10982.9092, y = 354.043427, z = -25685.334, adjacent =
	[
		"OW_PATH_306",
		"SPAWN_OW_SCAVENGER_ORC_03",
		"OW_PATH_06_06",
	]},
	["OW_PATH_06_07"] = {x = -13140.834, y = 458.387054, z = -27835.2129, adjacent =
	[
		"SPAWN_OW_BLOCKGOBBO_CAVE_DM6",
		"OW_PATH_306",
		"OW_NEWMINE_08",
	]},
	["SPAWN_OW_SHADOWBEAST_10_01"] = {x = -33093.0898, y = 2896.80835, z = -14112.5713, adjacent =
	[
		"OW_PATH_034",
		"OW_PATH_033",
		"OW_PATH_3_09",
		"OW_PATH_033_TO_CAVE",
	]},
	["OW_PATH_034"] = {x = -34412.2734, y = 3136.06763, z = -13718.5381, adjacent =
	[
		"SPAWN_OW_SHADOWBEAST_10_01",
		"OW_PATH_035",
		"OW_PATH_276",
		"OW_PATH_033",
		"OW_PATH_3_13",
	]},
	["OW_PATH_036"] = {x = -32907.1484, y = 2574.93311, z = -10946.4824, adjacent =
	[
		"SPAWN_OW_WOLF_NEAR_SHADOW3",
		"OW_PATH_037",
		"OW_PATH_035",
	]},
	["SPAWN_OW_WOLF_NEAR_SHADOW3"] = {x = -30882.9883, y = 2601.28101, z = -11986.2461, adjacent =
	[
		"OW_PATH_036",
	]},
	["OW_PATH_037"] = {x = -31346.7305, y = 1670.86658, z = -7999.70801, adjacent =
	[
		"SPAWN_OW_SHADOWBEAST_NEAR_SHADOW4",
		"OW_PATH_038",
		"OW_PATH_036",
	]},
	["OW_PATH_152K"] = {x = -32695.7656, y = 834.845825, z = 11338.707, adjacent =
	[
		"OW_PATH_153",
		"OW_PATH_SCAVENGER13_SPAWN01",
		"OW_PATH_151",
		"SPAWN_OW_BLOODFLYS_152",
	]},
	["SPAWN_OW_MOLERAT_A_6_NC4"] = {x = -33041.9336, y = 588.414795, z = 14959.6357, adjacent =
	[
		"OW_PATH_155",
	]},
	["SPAWN_OW_SCAVENGER_OLDWOOD_C3"] = {x = -21284.3906, y = -177.780685, z = 15201.8867, adjacent =
	[
		"SPAWN_OW_BLOODFLY_WOOD05_01",
	]},
	["OW_PATH_160"] = {x = -41118.4648, y = 363.763672, z = 1277.28137, adjacent =
	[
		"OW_PATH_161",
		"SPAWN_OW_WARAN_NC_03",
		"SPAWN_OW_MOLERAT_03_04",
	]},
	["SPAWN_OW_WARAN_NC_03"] = {x = -40473.5352, y = 320.807373, z = 906.536438, adjacent =
	[
		"OW_PATH_160",
		"OW_PATH_159",
	]},
	["OW_PATH_1_12"] = {x = 1209.86133, y = 3042.91553, z = 22763.8359, adjacent =
	[
		"SPAWN_TOTURIAL_CHICKEN_2_2",
		"OW_PATH_1_5_3",
		"OW_PATH_1_13",
	]},
	["SPAWN_OW_STARTSCAVENGER_02_01"] = {x = -10013.4463, y = -831.040955, z = 11310.3799, adjacent =
	[
		"OW_PATH_1_3",
		"OW_PATH_1_1",
	]},
	["OW_PATH_1_1"] = {x = -11111.7568, y = -891.312134, z = 10526.9287, adjacent =
	[
		"OW_PATH_1_2",
		"OW_PATH_1_1_TO_WASH",
		"SPAWN_OW_STARTSCAVENGER_02_01",
		"OW_PATH_1",
	]},
	["OW_PATH_1_4"] = {x = -8281.76758, y = -958.940674, z = 13967.0176, adjacent =
	[
		"OW_PATH_1_5",
		"OW_PATH_1_3",
		"SPAWN_OW_STARTSCAVNGERBO_01_02",
	]},
	["PATH_OC_OCWOOD_5"] = {x = 15225.1895, y = 20.0172348, z = 5871.11084, adjacent =
	[
		"PATH_WALD_OC_WOLFSPAWN",
		"PATH_OC_OCWOOD_4",
		"SPAWN_OW_SCAVENGER_LONE_WALD_OC3",
		"PATH_OC_OCWOOD_6",
	]},
	["PATH_WALD_OC_WOLFSPAWN_MOVEMENT"] = {x = 14528.2471, y = 10.453289, z = 5740.90674, adjacent =
	[
		"SPAWN_OW_WOLF2_WALD_OC3",
		"SPAWN_OW_SCAVENGER_LONE_WALD_OC3",
		"PATH_WALD_OC_WOLFSPAWN_MOVEMENT2",
		"PATH_WALD_OC_WOLFSPAWN2",
	]},
	["PATH_OC_NC_28"] = {x = -9655.0166, y = -1224.51917, z = 7021.94971, adjacent =
	[
		"PATH_OC_NC",
		"OC_ROUND_2",
		"OW_PATH_1",
		"OC_ROUND_1",
		"OW_PATH_193",
	]},
	["OW_PATH_250"] = {x = -14664.1895, y = -815.073914, z = 16323.9668, adjacent =
	[
		"SPAWN_OW_MOLERAT_OLDWOOD1_M",
		"OW_PATH_249",
		"OW_PATH_251",
	]},
	["SPAWN_OW_MOLERAT_OLDWOOD1_M"] = {x = -13320.001, y = -657.680786, z = 14905.0166, adjacent =
	[
		"OW_PATH_250",
		"OW_PATH_248",
	]},
	["OW_PATH_248"] = {x = -13082.123, y = -711.718079, z = 15934.6064, adjacent =
	[
		"SPAWN_OW_MOLERAT_OLDWOOD1_M",
		"OW_PATH_247",
		"OW_PATH_249",
	]},
	["MOVMENT_MOLERATS_OCWOOD"] = {x = -18770.6641, y = -315.893219, z = 14122.0488, adjacent =
	[
		"SPAWN_OW_SCAVENGER_OCWOOD1",
		"SPAWN_OW_MOLERAT_OCWOOD_OC2",
	]},
	["SPAWN_OW_MOLERAT_OCWOOD_OC2"] = {x = -19226.7324, y = -388.913544, z = 13438.4482, adjacent =
	[
		"MOVMENT_MOLERATS_OCWOOD",
	]},
	["OW_PATH_162"] = {x = -11968.3633, y = 837.366211, z = -13002.876, adjacent =
	[
		"OW_PATH_3_03",
		"SPAWN_OW_BLACKWOLF_02_01",
		"LOCATION_02_01",
		"OW_PATH_194",
	]},
	["OW_PATH_254"] = {x = -16860.8945, y = -909.724854, z = 16752.4805, adjacent =
	[
		"SPAWN_OW_WOLF_WOOD05_02",
		"OW_PATH_253",
		"OW_PATH_255",
	]},
	["SPAWN_OW_WOLF_WOOD05_02"] = {x = -16556.8516, y = -906.841919, z = 17052.5078, adjacent =
	[
		"OW_PATH_254",
		"OW_PATH_253",
	]},
	["OW_PATH_253"] = {x = -16502.5215, y = -910.869812, z = 16673.2539, adjacent =
	[
		"OW_PATH_254",
		"SPAWN_OW_WOLF_WOOD05_02",
		"OW_PATH_252",
	]},
	["OW_PATH_042"] = {x = -22019.793, y = 542.952332, z = -4575.02734, adjacent =
	[
		"OW_DEADWOOD_WOLF_SPAWN01",
		"OW_PATH_041",
		"OW_PATH_52",
	]},
	["OW_PATH_041"] = {x = -23456.0215, y = 567.938354, z = -5538.71143, adjacent =
	[
		"OW_DEADWOOD_WOLF_SPAWN01",
		"OW_PATH_042",
		"OW_PATH_040",
	]},
	["OW_SCAVENGER_CAVE3_SPAWN"] = {x = -18978.4746, y = -755.585083, z = 2613.07617, adjacent =
	[
		"PATH_OC_NC_8_1",
		"PATH_OC_NC_9_1",
		"OW_MOLERAT_FORCAVE_SPAWN",
		"OW_PATH_48",
		"OW_PATH_53",
		"OW_SCAVENGER_TREE_SPAWN",
	]},
	["PATH_OC_NC_8_1"] = {x = -17898.5605, y = -738.803528, z = 2594.13892, adjacent =
	[
		"OW_SCAVENGER_CAVE3_SPAWN",
		"PATH_OC_NC_9_1",
		"PATH_OC_NC_7_1",
		"OW_SCAVENGER_TREE_SPAWN",
	]},
	["PATH_OC_NC_9_1"] = {x = -18012.7578, y = -832.608582, z = 1896.47803, adjacent =
	[
		"OW_SCAVENGER_CAVE3_SPAWN",
		"PATH_OC_NC_8_1",
		"PATH_OC_NC_7",
	]},
	["PATH_OC_NC_7"] = {x = -18815.9297, y = -861.903931, z = 855.446777, adjacent =
	[
		"PATH_OC_NC_9_1",
		"OW_PATH_48",
		"OW_SPAWN_TRACK_LEICHE_04",
	]},
	["PATH_OC_NC_7_1"] = {x = -17564.7754, y = -849.589844, z = 2824.74487, adjacent =
	[
		"PATH_OC_NC_8_1",
		"PATH_OC_NC_6_1",
	]},
	["PATH_OC_NC_6_1"] = {x = -16833.9063, y = -1223.62451, z = 2778.99414, adjacent =
	[
		"PATH_OC_NC_5_1",
		"PATH_OC_NC_7_1",
		"OW_SPAWN_TRACK_LEICHE_03",
	]},
	["HELPPOINT8"] = {x = -14258.4424, y = -1343.97388, z = 1314.78259, adjacent =
	[
		"OW_SAWHUT_GREENGOBBO_SPAWN",
		"PATH_OC_NC_5",
		"PATH_OC_NC_6",
		"HELPPOINT6",
	]},
	["PATH_OC_NC_4"] = {x = -15581.9336, y = -1383.94507, z = 2812.09668, adjacent =
	[
		"PATH_OC_NC_3",
		"OW_SAWHUT_GREENGOBBO_SPAWN",
		"OW_SAWHUT_MOLERAT_MOVEMENT4",
		"PATH_OC_NC_5_1",
		"PATH_OC_NC_5",
	]},
	["OW_MOLERAT_FORCAVE_SPAWN"] = {x = -19581.1289, y = -553.243774, z = 3609.77832, adjacent =
	[
		"OW_SCAVENGER_CAVE3_SPAWN",
		"OW_PATH_53",
		"LOCATION_21_IN",
	]},
	["OW_PATH_48"] = {x = -19695.4531, y = -932.812317, z = 1109.5144, adjacent =
	[
		"OW_SCAVENGER_CAVE3_SPAWN",
		"PATH_OC_NC_7",
		"OW_PATH_53",
		"PATH_OC_NC_9",
	]},
	["OW_PATH_53"] = {x = -19975.252, y = -681.433289, z = 2804.79492, adjacent =
	[
		"OW_SCAVENGER_CAVE3_SPAWN",
		"OW_MOLERAT_FORCAVE_SPAWN",
		"OW_PATH_48",
		"PATH_OC_NC_10",
		"PATH_OC_NC_9",
	]},
	["PATH_OC_NC_23"] = {x = -29393.7949, y = 446.206848, z = 903.157532, adjacent =
	[
		"OW_WARAN_G_SPAWN",
		"PATH_OC_NC_22",
	]},
	["PLATEAU_ROUND03"] = {x = 10839, y = 7142.29834, z = -25777, adjacent =
	[
		"LOCATION_18_OUT",
		"BRIDGE_PLATEAU_TO_CASTLE",
		"PLATEAU_ROUND_TO_HUT",
	]},
	["PATH_OC_NC_5"] = {x = -15399.5889, y = -1433.27625, z = 1889.78003, adjacent =
	[
		"OW_SAWHUT_GREENGOBBO_SPAWN",
		"PATH_OC_NC_5_1",
		"HELPPOINT8",
		"PATH_OC_NC_4",
		"PATH_OC_NC_6",
	]},
	["OC_ROUND_21"] = {x = -10488.4453, y = -678.714783, z = 3309.84375, adjacent =
	[
		"PATH_OC_NC_2",
		"OC_ROUND_1",
		"OC_ROUND_20",
		"OW_SCAVENGER_SPAWN_TREE",
		"OC3",
	]},
	["PATH_OC_NC_2"] = {x = -11013, y = -849.028748, z = 4827, adjacent =
	[
		"OC_ROUND_21",
		"PATH_OC_NC_1",
	]},
	["OW_PATH_1_5_12_1"] = {x = -4199.59277, y = -1222.70947, z = 13893.8008, adjacent =
	[
		"OW_PATH_1_5_B",
		"OW_PATH_1_5_11",
		"OW_PATH_1_5_12_2",
		"OW_PATH_1_5_13",
	]},
	["OW_PATH_1_15"] = {x = 3454.03003, y = 5720.64453, z = 27600.1621, adjacent =
	[
		"OW_PATH_1_17",
		"OW_PATH_1_17_1",
		"OW_PATH_1_16",
		"OW_PATH_1_14",
	]},
	["OW_PATH_1_17_1"] = {x = 3923.63745, y = 5812.11182, z = 27172.5879, adjacent =
	[
		"OW_PATH_1_17",
		"OW_PATH_1_15",
		"OW_PATH_1_17_2",
		"OW_PATH_1_16_1",
		"OW_PATH_1_16",
	]},
	["OW_PATH_1_17_2"] = {x = 3454.0603, y = 5996.26758, z = 26616.2344, adjacent =
	[
		"OW_PATH_1_17_1",
		"OW_PATH_1_17_3",
	]},
	["OW_PATH_1_17_3"] = {x = 3061.61035, y = 6139.49365, z = 25920.5879, adjacent =
	[
		"OW_PATH_1_17_2",
		"OW_PATH_1_17_4",
	]},
	["OW_PATH_1_17_4"] = {x = 2338.18506, y = 6072.05762, z = 26390.4434, adjacent =
	[
		"OW_PATH_1_17_3",
		"OW_PATH_1_17_5",
	]},
	["OW_PATH_1_17_5"] = {x = 1582.70776, y = 6183.14258, z = 27084.7656, adjacent =
	[
		"OW_PATH_1_17_4",
		"SPAWN_MOLERAT02_SPAWN01",
	]},
	["SPAWN_MOLERAT02_SPAWN01"] = {x = 306.450684, y = 6168.60303, z = 27128.5762, adjacent =
	[
		"OW_PATH_1_17_5",
	]},
	["OW_PATH_1_5_13"] = {x = -5679.16162, y = -984.575073, z = 14304.2891, adjacent =
	[
		"OW_PATH_1_5_12_2",
		"OW_PATH_1_5",
		"OW_PATH_1_5_12_1",
		"OW_PATH_1_5_A",
	]},
	["OW_PATH_1_16_6"] = {x = 3341.89917, y = 6066.89453, z = 32513.5781, adjacent =
	[
		"OW_PATH_1_16_A",
		"OW_PATH_1_16_5",
		"OW_PATH_1_16_5_1",
	]},
	["OW_PATH_1_16_A"] = {x = 3446.32642, y = 6073.32422, z = 32346.0781, adjacent =
	[
		"OW_PATH_1_16_6",
		"OW_PATH_1_16_B",
	]},
	["OW_PATH_1_16_B"] = {x = 3622.93042, y = 6475.43799, z = 32172.7754, adjacent =
	[
		"OW_PATH_1_16_A",
		"OW_PATH_1_16_C",
	]},
	["OW_PATH_1_16_C"] = {x = 3204.68408, y = 6475.43799, z = 31995.1035, adjacent =
	[
		"OW_PATH_1_16_B",
	]},
	["OW_PATH_1_16_5"] = {x = 3236.01904, y = 6066.70996, z = 32210.123, adjacent =
	[
		"OW_PATH_1_16_6",
		"OW_PATH_1_16_4",
	]},
	["OW_PATH_1_16_5_1"] = {x = 3634.47778, y = 6091.07861, z = 32797.4766, adjacent =
	[
		"OW_PATH_1_16_6",
		"OW_PATH_1_16_7",
	]},
	["WP_INTRO_SHORE"] = {x = 5529.53711, y = 5313.67529, z = 36490.5391, adjacent =
	[
		"WP_INTRO_FALL3",
	]},
	["WP_INTRO_FALL3"] = {x = 5460.47168, y = 5381.80176, z = 36251.7656, adjacent =
	[
		"WP_INTRO_WI07",
		"WP_INTRO_SHORE",
		"OW_PATH_1_16_8",
		"WP_INTRO_WI15",
	]},
	["OW_PATH_1_5_1"] = {x = -1262.00085, y = -686.640015, z = 13935.9258, adjacent =
	[
		"OW_PATH_1_5_11",
		"OW_PATH_1_5_10",
	]},
	["OW_PATH_1_5_10"] = {x = -770.234985, y = -477.836975, z = 14343.2236, adjacent =
	[
		"OW_PATH_1_5_1",
		"OW_PATH_1_5_2",
	]},
	["OW_PATH_1_5_9"] = {x = 409.358154, y = 412.114532, z = 16002.4287, adjacent =
	[
		"OW_PATH_1_5_2",
		"OW_PATH_1_5_8",
	]},
	["OW_PATH_1_5_2"] = {x = -210.40863, y = -149.143143, z = 14915.6504, adjacent =
	[
		"OW_PATH_1_5_10",
		"OW_PATH_1_5_9",
	]},
	["OW_PATH_1_5_8"] = {x = 664.892517, y = 756.632141, z = 17062.8164, adjacent =
	[
		"OW_PATH_1_5_9",
		"OW_PATH_1_5_7",
	]},
	["OW_PATH_1_5_7"] = {x = 1237.75659, y = 1092.38391, z = 18215.3633, adjacent =
	[
		"OW_PATH_1_5_8",
		"OW_PATH_1_5_6",
	]},
	["OW_PATH_1_5_6"] = {x = 2711.729, y = 1732.42407, z = 18934.6797, adjacent =
	[
		"OW_PATH_1_5_7",
		"OW_PATH_1_5_5",
	]},
	["OW_PATH_1_5_5"] = {x = 2922.08545, y = 2044.74219, z = 19564.6758, adjacent =
	[
		"OW_PATH_1_5_4",
		"OW_PATH_1_5_6",
	]},
	["OW_PATH_183"] = {x = -10903.3535, y = -12.6567535, z = 19932.3594, adjacent =
	[
		"OW_PATH_SCAVENGER01_SPAWN01",
		"OW_PATH_184",
		"OW_PATH_182",
	]},
	["MT07"] = {x = 2128.17334, y = 4378.50293, z = -36242.1641, adjacent =
	[
		"MT08",
	]},
	["MT08"] = {x = 1407.96423, y = 5430.53564, z = -34033.1211, adjacent =
	[
		"MT07",
		"MT09",
		"MT13",
	]},
	["MT09"] = {x = 1354.06885, y = 5870.54932, z = -31711.5703, adjacent =
	[
		"MT08",
		"OW_PATH_210",
	]},
	["OW_PATH_210"] = {x = 1280.94592, y = 5746.61914, z = -26025.5547, adjacent =
	[
		"MT09",
		"OW_PATH_209",
	]},
	["LOCATION_23_CAVE_1_IN"] = {x = -43531.1953, y = 67.9605255, z = -4473.41699, adjacent =
	[
		"LOCATION_23_CAVE_1_IN_1",
		"LOCATION_23_CAVE_1_OUT",
	]},
	["LOCATION_23_CAVE_1_IN_1"] = {x = -43818.1797, y = 33.6143456, z = -4236.41748, adjacent =
	[
		"LOCATION_23_CAVE_1_IN",
	]},
	["OW_PATH_059"] = {x = -32199.8574, y = 754.330688, z = 7410.63721, adjacent =
	[
		"OW_PATH_061",
		"OW_PATH_058",
		"OW_PATH_154",
	]},
	["OW_PATH_061"] = {x = -32752.5586, y = 500.868713, z = 6987.74951, adjacent =
	[
		"OW_PATH_062",
		"OW_PATH_059",
		"OW_PATH_154",
	]},
	["OW_PATH_035"] = {x = -33921.9492, y = 2838.22046, z = -11998.2324, adjacent =
	[
		"OW_PATH_034",
		"OW_PATH_036",
		"STONES",
	]},
	["OW_PATH_276"] = {x = -35842.9453, y = 3204.95654, z = -14021.8115, adjacent =
	[
		"OW_PATH_275",
		"OW_PATH_034",
		"OW_PATH_033",
		"OW_PATH_3_13",
	]},
	["OW_PATH_003"] = {x = 6917.38086, y = 165.809769, z = -12032.9639, adjacent =
	[
		"OW_PATH_018",
		"OW_PATH_001",
	]},
	["OW_PATH_06_06"] = {x = -10139.6318, y = 563.703491, z = -25242.9824, adjacent =
	[
		"OW_PATH_305",
		"OW_PATH_06_05",
	]},
	["OW_PATH_06_03"] = {x = -6859.08545, y = 1183.98877, z = -23942.8555, adjacent =
	[
		"OW_PATH_304",
		"OW_PATH_06_04",
		"OW_PATH_06_02",
	]},
	["OW_PATH_304"] = {x = -7538, y = 1595.65918, z = -26500, adjacent =
	[
		"OW_PATH_06_03",
		"OW_PATH_303",
		"OW_PATH_06_04",
		"OW_PATH_SNAPPER02_SPAWN01",
	]},
	["OW_PATH_303"] = {x = -6746.23633, y = 1947.89319, z = -27851.3457, adjacent =
	[
		"OW_PATH_304",
		"OW_PATH_302",
		"OW_PATH_SNAPPER02_SPAWN02",
	]},
	["OW_PATH_06_04"] = {x = -8080.23047, y = 1317.47131, z = -25583.3066, adjacent =
	[
		"OW_PATH_06_03",
		"OW_PATH_304",
		"OW_PATH_06_05",
	]},
	["OW_PATH_302"] = {x = -4887.43994, y = 2463.29761, z = -28818.291, adjacent =
	[
		"SPAWN_OW_SCAVENGER_01_DEMONT5",
		"OW_PATH_303",
	]},
	["OW_PATH_300"] = {x = -3897.63477, y = 3055.4563, z = -31488.6582, adjacent =
	[
		"SPAWN_OW_SCAVENGER_01_DEMONT5",
		"OW_PATH_121",
	]},
	["OW_PATH_121"] = {x = -4053.11377, y = 3184.13379, z = -31894.7754, adjacent =
	[
		"OW_PATH_300",
		"OW_PATH_120",
	]},
	["PATH_OC_FOGTOWER05"] = {x = 19893.0195, y = -614.96283, z = 7331.69922, adjacent =
	[
		"OW_PATH_166",
		"OW_PATH_SCAVENGER03_SPAWN01",
		"PATH_OC_FOGTOWER04",
	]},
	["OC_ROUND_9"] = {x = 10400.0654, y = -87.2878418, z = 1045.89429, adjacent =
	[
		"OW_PATH_298",
		"OC_ROUND_10",
		"OC_ROUND_8",
		"SPAWN_OW_WOLF2_WALD_OC2",
	]},
	["OW_PATH_298"] = {x = 10674.3428, y = -273.282196, z = -1383.56213, adjacent =
	[
		"OC_ROUND_9",
		"OC_ROUND_10",
		"OW_PATH_297",
	]},
	["OC_ROUND_10"] = {x = 9178.58789, y = -719.505737, z = -1641.55981, adjacent =
	[
		"PATH_OC_PSI_01",
		"OC_ROUND_9",
		"OW_PATH_298",
		"OC_ROUND_28",
		"OC11",
	]},
	["OW_PATH_297"] = {x = 12895.4932, y = 367.418579, z = -1929.54517, adjacent =
	[
		"OW_PATH_298",
		"OW_PATH_296",
	]},
	["OW_PATH_296"] = {x = 14242.2939, y = 316.046051, z = -952.581726, adjacent =
	[
		"OW_PATH_297",
		"OW_PATH_295",
	]},
	["OW_PATH_295"] = {x = 15568.6963, y = 208.642731, z = 118.498932, adjacent =
	[
		"OW_PATH_296",
		"OW_PATH_294",
	]},
	["OW_PATH_294"] = {x = 16042.4424, y = 175.344574, z = 883.578186, adjacent =
	[
		"PATH_OC_FOGTOWER03",
		"OW_PATH_295",
	]},
	["OW_PATH_293"] = {x = 18205.0488, y = -111.450424, z = 3656.27954, adjacent =
	[
		"PATH_OC_FOGTOWER03",
		"PATH_OC_OCWOOD_8",
		"PATH_OC_FOGTOWER04",
		"OW_PATH_292",
	]},
	["PATH_OC_FOGTOWER04"] = {x = 18724.2969, y = -152.982483, z = 4489.88574, adjacent =
	[
		"OW_PATH_SCAVENGER03_SPAWN01",
		"PATH_OC_FOGTOWER05",
		"OW_PATH_293",
	]},
	["OW_PATH_292"] = {x = 19210.8242, y = -478.139221, z = 2963.43774, adjacent =
	[
		"OW_PATH_291",
		"OW_PATH_293",
	]},
	["WP_INTRO04"] = {x = 6423.01123, y = 6503.20996, z = 39442.7656, adjacent =
	[
		"WP_INTRO03",
		"WP_INTRO07",
		"WP_INTRO06",
		"WP_INTRO_FALL",
	]},
	["WP_INTRO07"] = {x = 6753.29004, y = 6524.375, z = 39654.5078, adjacent =
	[
		"WP_INTRO03",
		"WP_INTRO04",
	]},
	["OW_PATH_019"] = {x = -124.085518, y = 362.62912, z = -15786.2627, adjacent =
	[
		"OW_PATH_015",
		"FP_ROAM_OW_SNAPPER_OW_ORC",
		"OW_PATH_020",
	]},
	["OW_PATH_033"] = {x = -35304.5938, y = 3040.08276, z = -14956.1738, adjacent =
	[
		"SPAWN_OW_SHADOWBEAST_10_01",
		"OW_PATH_034",
		"OW_PATH_276",
		"OW_PATH_274_RIGHT3",
		"OW_PATH_033_TO_CAVE",
	]},
	["OW_PATH_273"] = {x = -38392.9766, y = 3591.88403, z = -13304.9131, adjacent =
	[
		"OW_PATH_274",
		"OW_PATH_07_03",
	]},
	["OW_PATH_07_03"] = {x = -39180.875, y = 3558.23682, z = -12966.3535, adjacent =
	[
		"OW_PATH_273",
		"OW_PATH_07_04",
	]},
	["OW_PATH_3_STONES"] = {x = -34825.5977, y = 2881.77148, z = -12576.6885, adjacent =
	[
		"OW_UNDEAD_DUNGEON_ENTRANCE",
		"STONES",
		"OW_PATH_3_13",
	]},
	["OW_UNDEAD_DUNGEON_ENTRANCE"] = {x = -34672.9609, y = 2755.82056, z = -12820.5361, adjacent =
	[
		"OW_PATH_3_STONES",
		"OW_UNDEAD_DUNGEON_01",
	]},
	["OW_PATH_182"] = {x = -8292.5918, y = -465.730591, z = 19316.957, adjacent =
	[
		"SPAWN_GOBBO_OW_PATH_1_6",
		"OW_PATH_183",
	]},
	["OW_PATH_127"] = {x = -9549.8418, y = 2517.8584, z = -37059.5039, adjacent =
	[
		"OW_PATH_178",
		"OW_PATH_128",
	]},
	["OW_PATH_178"] = {x = -8551.93457, y = 2587.14087, z = -37552.7383, adjacent =
	[
		"OW_PATH_127",
	]},
	["OW_PATH_1_16_2"] = {x = 4543.10498, y = 6082.40869, z = 29380.8848, adjacent =
	[
		"OW_PATH_1_16_1",
		"SPAWN_OW_MEATBUG_01_01",
	]},
	["OW_PATH_1_16_1"] = {x = 4491.27979, y = 6021.0083, z = 28537.6387, adjacent =
	[
		"OW_PATH_1_17_1",
		"OW_PATH_1_16_2",
		"OW_PATH_1_16",
	]},
	["LOCATION_02_08"] = {x = -8122.41016, y = 1211.86548, z = -13647.2637, adjacent =
	[
		"LOCATION_02_05",
		"LOCATION_02_09",
		"LOCATION_02_04",
	]},
	["LOCATION_02_05"] = {x = -7456.29639, y = 1198.56543, z = -12908.6133, adjacent =
	[
		"LOCATION_02_08",
		"LOCATION_02_04",
		"LOCATION_02_06",
	]},
	["LOCATION_02_15"] = {x = -6425.78564, y = 2350.01465, z = -11934.7988, adjacent =
	[
		"LOCATION_02_16",
		"LOCATION_02_14",
	]},
	["LOCATION_02_16"] = {x = -6292.73682, y = 2335.79614, z = -11357.7695, adjacent =
	[
		"LOCATION_02_15",
	]},
	["LOCATION_02_14"] = {x = -7248.40576, y = 2194.40405, z = -12357.5088, adjacent =
	[
		"LOCATION_02_15",
		"LOCATION_02_13",
	]},
	["LOCATION_02_13"] = {x = -7752.07324, y = 2131.78589, z = -12168.5791, adjacent =
	[
		"LOCATION_02_14",
		"LOCATION_02_12",
	]},
	["LOCATION_02_12"] = {x = -9131.19043, y = 1869.50061, z = -12136.0713, adjacent =
	[
		"LOCATION_02_13",
		"LOCATION_02_11",
	]},
	["LOCATION_02_11"] = {x = -9399.02441, y = 1775.73352, z = -12664.1045, adjacent =
	[
		"LOCATION_02_12",
		"LOCATION_02_10",
	]},
	["LOCATION_02_10"] = {x = -9057.01367, y = 1625.37683, z = -13603.7715, adjacent =
	[
		"LOCATION_02_11",
		"LOCATION_02_09",
	]},
	["LOCATION_02_09"] = {x = -8397.48535, y = 1610.66309, z = -13995.7354, adjacent =
	[
		"LOCATION_02_08",
		"LOCATION_02_10",
	]},
	["LOCATION_02_04"] = {x = -8428.55273, y = 1149.12683, z = -12717.7324, adjacent =
	[
		"LOCATION_02_08",
		"LOCATION_02_05",
		"LOCATION_02_03",
	]},
	["LOCATION_02_06"] = {x = -6813.33057, y = 1222.80847, z = -13300.0947, adjacent =
	[
		"LOCATION_02_05",
		"LOCATION_02_07",
	]},
	["LOCATION_02_07"] = {x = -6361.65234, y = 1184.44495, z = -13607.1836, adjacent =
	[
		"LOCATION_02_06",
	]},
	["LOCATION_02_03"] = {x = -9557.1543, y = 1230.3147, z = -12388.3789, adjacent =
	[
		"LOCATION_02_04",
		"LOCATION_02_02",
	]},
	["LOCATION_02_02"] = {x = -10788.3516, y = 1119.88611, z = -12508.2666, adjacent =
	[
		"LOCATION_02_01",
		"LOCATION_02_03",
		"OW_RITTER_LEICHE_01",
	]},
	["SPAWN_OW_MOLERAT_03_04"] = {x = -42401.6328, y = 210.944107, z = -374.995972, adjacent =
	[
		"OW_PATH_160",
		"LOCATION_24_IN",
		"OW_PATH_159",
	]},
	["LOCATION_24_IN"] = {x = -43303.8945, y = -32.9784622, z = -1327.20056, adjacent =
	[
		"SPAWN_OW_MOLERAT_03_04",
	]},
	["OW_PATH_159"] = {x = -41196.2969, y = 271.595276, z = -2360.0542, adjacent =
	[
		"OW_PATH_158",
		"SPAWN_OW_WARAN_NC_03",
		"SPAWN_OW_MOLERAT_03_04",
		"LOCATION_23_CAVE_1_OUT",
	]},
	["LOCATION_23_CAVE_1_OUT"] = {x = -42388.7813, y = 83.5569687, z = -4489.74414, adjacent =
	[
		"OW_PATH_158",
		"LOCATION_23_CAVE_1_IN",
		"OW_PATH_159",
	]},
	["OW_SCAVENGER_COAST_NEWCAMP_SPAWN"] = {x = -37512.6641, y = 103.196594, z = -545.631165, adjacent =
	[
		"OW_PATH_158",
		"LOCATION_23_01",
		"OW_PATH_157",
	]},
	["OW_PATH_147"] = {x = -30692.3262, y = -395.741852, z = 19166.5332, adjacent =
	[
		"OW_PATH_SCAVENGER12_SPAWN01",
		"OW_PATH_155",
		"OW_PATH_148",
	]},
	["OW_PATH_149"] = {x = -31126.334, y = 566.206543, z = 14036.0439, adjacent =
	[
		"OW_PATH_155",
		"OW_PATH_150",
	]},
	["OW_PATH_142"] = {x = -16784.2441, y = 2817.02222, z = -28786.8652, adjacent =
	[
		"OW_PATH_143",
		"OW_PATH_141",
	]},
	["OW_PATH_143"] = {x = -15052.6211, y = 2651.81519, z = -28717.084, adjacent =
	[
		"OW_PATH_142",
	]},
	["OW_PATH_141"] = {x = -16909.2227, y = 2355.07837, z = -31299.6133, adjacent =
	[
		"OW_PATH_142",
		"SPAWN_OW_WARAN_DEMON_02_01",
	]},
	["SPAWN_OW_WARAN_DEMON_02_01"] = {x = -16756.1367, y = 1460.61633, z = -33371.0742, adjacent =
	[
		"OW_PATH_141",
		"OW_PATH_139",
	]},
	["OW_PATH_139"] = {x = -17373.2246, y = 1457.39978, z = -33443.5898, adjacent =
	[
		"SPAWN_OW_WARAN_DEMON_02_01",
		"OW_PATH_138",
	]},
	["OW_PATH_138"] = {x = -18258.0313, y = 1626.60596, z = -33537.9414, adjacent =
	[
		"OW_PATH_139",
		"OW_PATH_137",
	]},
	["OW_PATH_137"] = {x = -18422.6738, y = 2097.07837, z = -34780.457, adjacent =
	[
		"OW_PATH_138",
		"OW_PATH_136",
	]},
	["OW_PATH_136"] = {x = -17864.8945, y = 2357.50586, z = -35589.707, adjacent =
	[
		"OW_PATH_137",
		"OW_PATH_135",
	]},
	["OW_PATH_135"] = {x = -17044.3145, y = 2445.49194, z = -36143.7227, adjacent =
	[
		"OW_PATH_136",
		"OW_PATH_134",
	]},
	["OW_PATH_134"] = {x = -15910.333, y = 2549.85864, z = -36216.2344, adjacent =
	[
		"OW_PATH_135",
		"OW_PATH_133",
		"OW_DT_BLOODFLY_01",
	]},
	["OW_PATH_133"] = {x = -13939.3398, y = 2516.87866, z = -36274.9258, adjacent =
	[
		"OW_PATH_134",
		"OW_PATH_132",
	]},
	["OW_PATH_132"] = {x = -12685.0303, y = 2337.57764, z = -37232.3008, adjacent =
	[
		"OW_PATH_133",
		"OW_PATH_131",
	]},
	["OW_PATH_131"] = {x = -12170.8018, y = 2329.16895, z = -36619.4648, adjacent =
	[
		"OW_PATH_132",
		"OW_PATH_130",
	]},
	["OW_PATH_130"] = {x = -11785.2598, y = 2244.62231, z = -36159.7383, adjacent =
	[
		"OW_PATH_131",
		"LOCATION_03_OUT",
		"OW_PATH_129",
	]},
	["LOCATION_03_OUT"] = {x = -11576.9229, y = 2088.05469, z = -35474.9063, adjacent =
	[
		"OW_PATH_130",
		"OW_PATH_129",
		"LOCATION_03_IN",
		"DT",
	]},
	["OW_PATH_129"] = {x = -11378.8193, y = 2215.64185, z = -36113.7578, adjacent =
	[
		"OW_PATH_130",
		"LOCATION_03_OUT",
		"OW_PATH_128",
	]},
	["LOCATION_03_IN"] = {x = -11245.2285, y = 1993.67236, z = -34463.5586, adjacent =
	[
		"LOCATION_03_OUT",
	]},
	["OW_PATH_128"] = {x = -10534.707, y = 2441.83643, z = -36885.6055, adjacent =
	[
		"OW_PATH_127",
		"OW_PATH_129",
		"OW_PATH_202",
	]},
	["OW_PATH_120"] = {x = -3535.56592, y = 3602.05151, z = -32811.3594, adjacent =
	[
		"OW_PATH_121",
		"OW_PATH_119",
	]},
	["OW_PATH_119"] = {x = -3555.34326, y = 3999.83936, z = -34010.7539, adjacent =
	[
		"OW_PATH_120",
		"OW_PATH_118",
	]},
	["OW_PATH_118"] = {x = -3323.07031, y = 4138.79932, z = -34622.8438, adjacent =
	[
		"OW_PATH_119",
		"OW_PATH_117",
	]},
	["OW_PATH_117"] = {x = -2071.67578, y = 4368.77246, z = -34822.8281, adjacent =
	[
		"OW_PATH_118",
		"OW_PATH_116",
	]},
	["OW_PATH_116"] = {x = -1033.16772, y = 4681.69189, z = -33666.9961, adjacent =
	[
		"OW_PATH_117",
		"OW_PATH_115",
	]},
	["OW_PATH_115"] = {x = -1237.24133, y = 4734.07373, z = -31643.0117, adjacent =
	[
		"OW_PATH_116",
		"OW_PATH_114",
	]},
	["OW_PATH_114"] = {x = -813.134583, y = 4982.39697, z = -29315.377, adjacent =
	[
		"OW_PATH_115",
		"OW_PATH_113",
	]},
	["OW_PATH_113"] = {x = -484.339752, y = 5126.99414, z = -26974.002, adjacent =
	[
		"OW_PATH_114",
		"OW_PATH_112",
	]},
	["OW_PATH_112"] = {x = -358.034424, y = 5118.53418, z = -26019.502, adjacent =
	[
		"OW_PATH_113",
		"OW_PATH_111",
		"OW_PATH_209",
	]},
	["OW_PATH_111"] = {x = -833.552612, y = 4826.39941, z = -24703.2246, adjacent =
	[
		"OW_PATH_112",
		"OW_PATH_110",
	]},
	["OW_PATH_110"] = {x = -3795.55664, y = 3018.9563, z = -22112.4902, adjacent =
	[
		"OW_PATH_111",
		"OW_PATH_109",
	]},
	["OW_PATH_109"] = {x = -3891.302, y = 2293.23438, z = -20243.0664, adjacent =
	[
		"CASTLE_3",
		"OW_PATH_110",
		"CASTLE_4",
	]},
	["OW_PATH_108"] = {x = 2485.3064, y = 5608.93652, z = -18454.3516, adjacent =
	[
		"CASTLE_9",
		"OW_PATH_107",
	]},
	["CASTLE_9"] = {x = 5185.68701, y = 5350.48828, z = -17811.9648, adjacent =
	[
		"OW_PATH_108",
		"CASTLE_8",
	]},
	["OW_PATH_107"] = {x = 922.513916, y = 5342.37842, z = -18538.9629, adjacent =
	[
		"OW_PATH_108",
		"CASTLE_12",
	]},
	["CASTLE_12"] = {x = -588.210815, y = 5685.96289, z = -18947.5586, adjacent =
	[
		"OW_PATH_107",
		"CASTLE_13",
	]},
	["CASTLE_2"] = {x = -4776.646, y = 1160.65405, z = -16877.5566, adjacent =
	[
		"OW_PATH_106",
		"OW_PATH_013",
	]},
	["OW_PATH_013"] = {x = -2621.26123, y = 447.597168, z = -15566.4512, adjacent =
	[
		"FP_ROAM_OW_SNAPPER_OW_ORC",
		"CASTLE_2",
	]},
	["OW_PATH_105"] = {x = -6918.25098, y = 2260.83179, z = -17987.0488, adjacent =
	[
		"OW_PATH_106",
		"OW_PATH_104",
	]},
	["OW_PATH_104"] = {x = -8595, y = 2182.2854, z = -18422, adjacent =
	[
		"OW_PATH_103",
		"OW_PATH_105",
		"OW_PATH_BLACKWOLF07_SPAWN01",
	]},
	["OW_PATH_101"] = {x = -14029.5771, y = 1713.55603, z = -17795.6797, adjacent =
	[
		"OW_PATH_102",
		"OW_PATH_100",
	]},
	["OW_PATH_100"] = {x = -15491.6768, y = 2022.28809, z = -19112.8379, adjacent =
	[
		"OW_PATH_099",
		"SPAWN_OW_WARAN_ORC_01",
		"OW_PATH_101",
	]},
	["OW_PATH_092"] = {x = -36194.9844, y = 3434.41284, z = -9759.09277, adjacent =
	[
		"OW_PATH_093",
		"OW_PATH_091",
	]},
	["OW_PATH_093"] = {x = -36142.0469, y = 3285.63672, z = -8721.00488, adjacent =
	[
		"OW_PATH_092",
	]},
	["OW_PATH_091"] = {x = -36004.4375, y = 3232.31909, z = -10897.5918, adjacent =
	[
		"OW_PATH_092",
		"OW_PATH_090",
	]},
	["OW_PATH_090"] = {x = -36070.25, y = 3081.45215, z = -11862.6904, adjacent =
	[
		"OW_PATH_091",
		"OW_PATH_07_01",
	]},
	["OW_PATH_07_01"] = {x = -36060.9102, y = 2977.53174, z = -12664.5654, adjacent =
	[
		"OW_PATH_090",
		"OW_PATH_3_13",
	]},
	["OW_ICEREGION_06"] = {x = -41484.9961, y = 784.270569, z = 7036.39648, adjacent =
	[
		"OW_ICEREGION_02",
		"OW_ICEREGION_05",
		"OW_ICEREGION_07",
		"OW_ICEREGION_15",
	]},
	["OW_ICEREGION_07"] = {x = -43028.0742, y = 868.191284, z = 7922.35352, adjacent =
	[
		"OW_ICEREGION_06",
		"OW_ICEREGION_08",
		"OW_ICEREGION_15",
	]},
	["OW_PATH_065"] = {x = -38016.6914, y = 774.083801, z = 4420.44482, adjacent =
	[
		"OW_PATH_066",
		"OW_PATH_156",
		"OW_PATH_064",
		"OW_ICEREGION_ENTRANCE_01",
	]},
	["OW_PATH_058"] = {x = -32008.9512, y = 763.150513, z = 7090.22559, adjacent =
	[
		"OW_PATH_059",
		"OW_PATH_057",
	]},
	["OW_PATH_057"] = {x = -31771.0723, y = 716.483887, z = 6618.79053, adjacent =
	[
		"OW_PATH_058",
		"OW_PATH_056",
	]},
	["OW_PATH_056"] = {x = -31421.9395, y = 642.089722, z = 5757.40332, adjacent =
	[
		"OW_PATH_057",
		"PATH_AROUND_HILL03",
		"PATH_AROUND_HILL02",
	]},
	["PATH_AROUND_HILL03"] = {x = -30634.209, y = 718.553467, z = 4772.26611, adjacent =
	[
		"OW_PATH_056",
		"PATH_AROUND_HILL02",
		"OW_PATH_055",
		"OW_GOBBO_PLACE_SPAWN",
		"OW_SPAWN_TRACK_LEICHE_01",
	]},
	["PATH_AROUND_HILL02"] = {x = -31899.5977, y = 747.419189, z = 4856.1875, adjacent =
	[
		"OW_PATH_056",
		"PATH_AROUND_HILL03",
	]},
	["OW_PATH_055"] = {x = -30018.6309, y = 652.87561, z = 4959.45654, adjacent =
	[
		"PATH_AROUND_HILL03",
		"OW_PATH_054",
		"OW_GOBBO_PLACE_SPAWN",
	]},
	["OW_PATH_054"] = {x = -29624.2754, y = 541.458496, z = 4891.93555, adjacent =
	[
		"OW_PATH_055",
		"PATH_OC_NC_18",
	]},
	["PATH_OC_NC_18"] = {x = -28745.5508, y = 196.601639, z = 4514.67383, adjacent =
	[
		"OW_PATH_054",
		"PATH_OC_NC_17",
		"PATH_OC_NC_19",
	]},
	["PATH_OC_NC_10"] = {x = -20887.3359, y = -448.672241, z = 2164.77173, adjacent =
	[
		"PATH_OC_NC_11",
		"OW_PATH_53",
		"PATH_OC_NC_9",
	]},
	["LOCATION_21_IN"] = {x = -20070.5977, y = -623.081726, z = 4830.83984, adjacent =
	[
		"OW_MOLERAT_FORCAVE_SPAWN",
		"OW_MOLERAT_CAVE_SPAWN",
	]},
	["OW_PATH_1_16_7"] = {x = 3761.17456, y = 5949.35791, z = 34137.082, adjacent =
	[
		"OW_PATH_1_16_5_1",
		"OW_PATH_1_16_8",
	]},
	["OW_PATH_1_16_8"] = {x = 5165.8584, y = 5649.91504, z = 35118.9102, adjacent =
	[
		"WP_INTRO_WI06",
		"WP_INTRO_FALL3",
		"OW_PATH_1_16_7",
	]},
	["OW_PATH_1_16_4"] = {x = 3354.25708, y = 6078.92725, z = 31648.2402, adjacent =
	[
		"OW_PATH_1_16_5",
		"SPAWN_OW_MEATBUG_01_01",
	]},
	["SPAWN_OW_MEATBUG_01_01"] = {x = 4286.49365, y = 6091.91357, z = 30636.2813, adjacent =
	[
		"OW_PATH_1_16_2",
		"OW_PATH_1_16_4",
	]},
	["OW_PATH_1_16"] = {x = 4061.42822, y = 5898.44043, z = 28001.0508, adjacent =
	[
		"OW_PATH_1_17",
		"OW_PATH_1_15",
		"OW_PATH_1_17_1",
		"OW_PATH_1_16_1",
	]},
	["OW_ICEREGION_08"] = {x = -43457.3984, y = 1004.19647, z = 9848.7373, adjacent =
	[
		"OW_ICEREGION_07",
		"OW_ICEREGION_09",
		"OW_ICEREGION_15",
		"OW_ICEREGION_14",
	]},
	["OW_ICEREGION_09"] = {x = -43160.2383, y = 1150.9679, z = 11064.7842, adjacent =
	[
		"OW_ICEREGION_08",
		"OW_ICEREGION_20",
	]},
	["OW_PATH_06_05"] = {x = -9162.85645, y = 1076.04272, z = -25632.8496, adjacent =
	[
		"OW_PATH_06_06",
		"OW_PATH_06_04",
	]},
	["OW_PATH_06_02"] = {x = -6781.83398, y = 1523.84583, z = -22610.5469, adjacent =
	[
		"OW_PATH_06_03",
		"OW_PATH_06_01",
	]},
	["OW_PATH_06_01"] = {x = -5300.46826, y = 2301.91675, z = -21128.0703, adjacent =
	[
		"CASTLE_3",
		"OW_PATH_06_02",
	]},
	["OW_PATH_05"] = {x = -15700.3643, y = -1826.15332, z = 738.133179, adjacent =
	[
		"OW_PATH_05_1",
		"PATH_OC_NC_6",
		"HELPPOINT4",
	]},
	["OW_PATH_05_1"] = {x = -15253.9697, y = -1851.06177, z = 336.542786, adjacent =
	[
		"OW_PATH_05",
		"OW_PATH_OC_NC8",
	]},
	["PATH_OC_NC_6"] = {x = -15550, y = -1531.63953, z = 1404, adjacent =
	[
		"HELPPOINT8",
		"PATH_OC_NC_5",
		"OW_PATH_05",
	]},
	["STONES"] = {x = -34522.3672, y = 2912.91821, z = -12086.4727, adjacent =
	[
		"OW_PATH_035",
		"OW_PATH_3_STONES",
		"STONEHENGE",
	]},
	["OW_PATH_3_13"] = {x = -35464.707, y = 2924.73389, z = -13012.8486, adjacent =
	[
		"OW_PATH_034",
		"OW_PATH_276",
		"OW_PATH_3_STONES",
		"OW_PATH_07_01",
	]},
	["OW_PATH_3_09"] = {x = -30463.1641, y = 2491.97046, z = -15360.4395, adjacent =
	[
		"SPAWN_OW_SHADOWBEAST_10_03",
		"SPAWN_OW_SHADOWBEAST_10_01",
	]},
	["OW_PATH_3_06"] = {x = -22415.5254, y = 1363.46277, z = -14096.0605, adjacent =
	[
		"OW_PATH_3_05",
		"OW_PATH_3_07",
	]},
	["OC_ROUND_11"] = {x = 8180.15625, y = -609.030396, z = -5393.06445, adjacent =
	[
		"PATH_OC_PSI_01",
		"OC_ROUND_27",
		"OC_ROUND_28",
	]},
	["CASTLE_13"] = {x = -1306.13855, y = 6077.34521, z = -19591.1523, adjacent =
	[
		"CASTLE_14",
		"CASTLE_12",
	]},
	["CASTLE_8"] = {x = 5118.85352, y = 5091.18799, z = -17097.8047, adjacent =
	[
		"CASTLE_9",
		"CASTLE_7",
		"PATH_CASTLE_TO_WATERFALL",
		"OW_PATH_012",
		"CASTLE_8_1",
	]},
	["CASTLE_7"] = {x = 4227.41992, y = 4898.42725, z = -17109.4551, adjacent =
	[
		"CASTLE_8",
		"CASTLE_6",
	]},
	["CASTLE_6"] = {x = 2594.94849, y = 4240.21729, z = -17847.7754, adjacent =
	[
		"CASTLE_7",
		"CASTLE_5",
	]},
	["CASTLE_5"] = {x = -985.260559, y = 3405.71313, z = -18275.4668, adjacent =
	[
		"CASTLE_6",
		"CASTLE_4",
		"CASTLE_5_1",
	]},
	["CASTLE_4"] = {x = -3581.96362, y = 2251.72656, z = -20051.748, adjacent =
	[
		"CASTLE_3",
		"OW_PATH_109",
		"CASTLE_5",
	]},
	["CROSSING_PATH_2_3_4"] = {x = -11137.9111, y = -814.227417, z = -2037.78711, adjacent =
	[
		"OW_PATH_3_01",
		"OC_ROUND_20",
	]},
	["OW_PATH_1"] = {x = -10947.6426, y = -1106.03186, z = 7269.26318, adjacent =
	[
		"PATH_OC_NC",
		"OW_PATH_1_1",
		"PATH_OC_NC_28",
	]},
	["OW_PATH_1_13"] = {x = 1539.51892, y = 3893.15283, z = 24640.1191, adjacent =
	[
		"OW_PATH_1_12",
		"OW_PATH_1_14",
	]},
	["OW_PATH_1_14"] = {x = 2131.90552, y = 4778.96191, z = 26103.8945, adjacent =
	[
		"OW_PATH_1_15",
		"OW_PATH_1_13",
	]},
	["OC_ROUND_1"] = {x = -8393.72266, y = -1351.36926, z = 6500.26221, adjacent =
	[
		"OC1",
		"OC_ROUND_2",
		"PATH_OC_NC_28",
		"OC_ROUND_21",
		"OW_PATH_193",
	]},
	["OC_ROUND_7"] = {x = 9540.51367, y = -480.272156, z = 4318.91553, adjacent =
	[
		"O_SCAVENGER_OCWOODL2_MOVEMENT",
		"OC_ROUND_6",
		"OC_ROUND_8",
	]},
	["OC_ROUND_8"] = {x = 9720.96484, y = -512.674316, z = 3639.5061, adjacent =
	[
		"OC_ROUND_9",
		"OC_ROUND_7",
	]},
	["OC_ROUND_12"] = {x = 5410.0127, y = -758.089478, z = -7402.50928, adjacent =
	[
		"OC_ROUND_27",
		"OC_ROUND_26",
		"OC_ROUND_13",
		"OC2",
		"OW_PATH_001",
	]},
	["OC_ROUND_14"] = {x = -654.197083, y = -972.638672, z = -9855.40332, adjacent =
	[
		"OC_ROUND_13",
		"OC_ROUND_15",
		"OC_ROUND_25",
	]},
	["OC_ROUND_20"] = {x = -10493.374, y = -632.936279, z = 279.418274, adjacent =
	[
		"OC_ROUND_21",
		"CROSSING_PATH_2_3_4",
		"SPAWN_SIT_OW",
		"OC4",
	]},
	["PATH_OC_NC_1"] = {x = -11423, y = -928.478882, z = 5738, adjacent =
	[
		"PATH_OC_NC",
		"PATH_OC_NC_2",
	]},
	["PATH_OC_NC_9"] = {x = -20219.6152, y = -774.197754, z = 1895.62244, adjacent =
	[
		"OW_PATH_48",
		"OW_PATH_53",
		"PATH_OC_NC_10",
	]},
	["PATH_OC_NC_12"] = {x = -23746.6602, y = -270.753784, z = 2094.36401, adjacent =
	[
		"PATH_OC_NC_13",
		"OW_WOODRUIN_FOR_WOLF_SPAWN",
		"PATH_OC_NC_11",
	]},
	["PATH_OC_NC_14"] = {x = -25401.7676, y = -267.816437, z = 2767.21875, adjacent =
	[
		"PATH_OC_NC_13",
		"OW_PATH_02",
		"OW_WOODRUIN_FOR_WOLF_SPAWN",
		"PATH_OC_NC_15",
		"OW_WOODRUIN_WOLF_SPAWN",
	]},
	["PATH_OC_NC_15"] = {x = -26114.7207, y = -185.785522, z = 3900.75952, adjacent =
	[
		"PATH_OC_NC_14",
		"PATH_OC_NC_16",
	]},
	["PATH_OC_NC_16"] = {x = -27181.7383, y = -324.739197, z = 4538.91016, adjacent =
	[
		"PATH_OC_NC_15",
		"PATH_OC_NC_17",
	]},
	["PATH_OC_NC_17"] = {x = -27913.9629, y = -171.342361, z = 4765.59229, adjacent =
	[
		"PATH_OC_NC_18",
		"PATH_OC_NC_16",
	]},
	["PATH_OC_NC_19"] = {x = -29107.4824, y = 322.511414, z = 3965.8418, adjacent =
	[
		"PATH_OC_NC_18",
		"PATH_OC_NC_20_X",
	]},
	["PATH_OC_NC_20_X"] = {x = -29170.4258, y = 268.30069, z = 3150.6731, adjacent =
	[
		"PATH_OC_NC_19",
		"PATH_OC_NC_21",
		"OW_PATH_03",
	]},
	["PATH_OC_NC_21"] = {x = -27537.8789, y = -185.945251, z = 2287.67114, adjacent =
	[
		"OW_PATH_02",
		"PATH_OC_NC_20_X",
		"PATH_OC_NC_22",
		"OW_WOODRUIN_WOLF_SPAWN",
	]},
	["PATH_OC_NC_22"] = {x = -27990.4199, y = 373.41983, z = 874.115479, adjacent =
	[
		"OW_PATH_02",
		"PATH_OC_NC_23",
		"PATH_OC_NC_21",
	]},
	["SPAWN_OW_WOLF2_WALD_OC2"] = {x = 12703.8125, y = 328.358826, z = 714.723694, adjacent =
	[
		"OC_ROUND_9",
		"PATH_OC_FOGTOWER02",
	]},
	["PATH_OC_FOGTOWER02"] = {x = 13794.5439, y = 198.181381, z = 1586.73706, adjacent =
	[
		"SPAWN_OW_WOLF2_WALD_OC2",
		"OW_PATH_WARAN01_SPAWN01",
	]},
	["PATH_CASTLE_TO_WATERFALL"] = {x = 9996.27539, y = 5516.06006, z = -20552.1426, adjacent =
	[
		"CASTLE_8",
		"WATERFALL_TOP",
		"OW_PATH_012",
	]},
	["WATERFALL_TOP"] = {x = 11597.5967, y = 5050.74268, z = -21442.1465, adjacent =
	[
		"PATH_CASTLE_TO_WATERFALL",
	]},
	["OW_PATH_03"] = {x = -29760.5859, y = 592.286682, z = 2553.00806, adjacent =
	[
		"PATH_OC_NC_20_X",
		"OW_PATH_04",
	]},
	["OW_PATH_04"] = {x = -31090.6113, y = 1012.83362, z = 2442.15332, adjacent =
	[
		"OW_PATH_03",
		"PATH_AROUND_HILL01",
	]},
	["PATH_AROUND_HILL01"] = {x = -31950.7461, y = 1034.20813, z = 2861.28662, adjacent =
	[
		"OW_PATH_04",
	]},
	["OW_PATH_07_04"] = {x = -40012.6797, y = 3585.13379, z = -11701.5635, adjacent =
	[
		"OW_PATH_07_03",
		"OW_PATH_07_05",
	]},
	["OW_PATH_07_05"] = {x = -40244.4023, y = 3523.25146, z = -10429.4209, adjacent =
	[
		"OW_PATH_07_04",
		"OW_PATH_07_06",
	]},
	["OW_PATH_07_06"] = {x = -39648.5703, y = 3184.93408, z = -9104.99902, adjacent =
	[
		"OW_PATH_07_05",
		"OW_PATH_07_07",
	]},
	["OW_PATH_07_07"] = {x = -39522.3359, y = 2973.89624, z = -7903.58301, adjacent =
	[
		"OW_PATH_07_06",
		"OW_PATH_07_08",
	]},
	["OW_PATH_07_08"] = {x = -39904.9023, y = 2868.35889, z = -6798.9292, adjacent =
	[
		"OW_PATH_07_07",
		"OW_PATH_07_09",
	]},
	["OW_PATH_07_09"] = {x = -41236.4961, y = 3026.57935, z = -6202.72852, adjacent =
	[
		"OW_PATH_07_08",
		"OW_PATH_07_10",
	]},
	["OW_PATH_07_10"] = {x = -42228.9688, y = 3229.45337, z = -6152.38721, adjacent =
	[
		"OW_PATH_07_09",
		"OW_PATH_07_11",
	]},
	["OW_PATH_07_11"] = {x = -43108.8594, y = 3255.375, z = -5450.9873, adjacent =
	[
		"OW_PATH_07_10",
		"OW_PATH_07_12",
	]},
	["OW_PATH_07_12"] = {x = -43527.1836, y = 3193.16846, z = -4071.42139, adjacent =
	[
		"OW_PATH_07_11",
		"OW_PATH_07_13",
	]},
	["OW_PATH_07_13"] = {x = -43012.7852, y = 3324.81396, z = -3115.78296, adjacent =
	[
		"OW_PATH_07_12",
		"OW_PATH_07_14",
	]},
	["OW_PATH_07_14"] = {x = -43051.7734, y = 3397.74805, z = -2222.62866, adjacent =
	[
		"OW_PATH_07_13",
		"OW_PATH_07_15",
	]},
	["OW_PATH_07_15"] = {x = -43106.3008, y = 3377.63208, z = -1854.0293, adjacent =
	[
		"OW_PATH_07_14",
		"OW_PATH_07_16",
		"OW_PATH_07_15_CAVE",
	]},
	["OW_PATH_07_16"] = {x = -43950.5313, y = 2935.88623, z = 6.08091831, adjacent =
	[
		"OW_PATH_07_15",
		"OW_PATH_07_17",
	]},
	["OW_PATH_07_17"] = {x = -43975.8789, y = 2844.71021, z = 918.460083, adjacent =
	[
		"OW_PATH_07_16",
		"OW_PATH_07_18",
	]},
	["OW_PATH_07_18"] = {x = -43766.4258, y = 2707.59033, z = 2325.94043, adjacent =
	[
		"OW_PATH_07_17",
		"OW_PATH_07_19A",
	]},
	["OW_PATH_07_19"] = {x = -43080.6133, y = 2238.61255, z = 4346.69678, adjacent =
	[
		"OW_PATH_07_20",
		"OW_PATH_07_19A",
	]},
	["OW_PATH_07_20"] = {x = -40486.7734, y = 1260.31506, z = 4986.09424, adjacent =
	[
		"OW_ICEREGION_ENTRANCE",
		"OW_PATH_07_19",
	]},
	["OC2"] = {x = 5166.03369, y = -863.418701, z = -7093.9873, adjacent =
	[
		"OC_ROUND_12",
		"OC9",
		"OC10",
	]},
	["OC_ROUND_3"] = {x = -1926.64331, y = -1097.57214, z = 8263.74707, adjacent =
	[
		"OC_ROUND_23",
		"OC_ROUND_22",
	]},
	["OC_ROUND_25"] = {x = 1845.83447, y = -737.052551, z = -10549.4746, adjacent =
	[
		"OC_ROUND_14",
		"SPAWN_OW_SMALLCAVE01_MOLERAT",
	]},
	["SPAWN_OW_SMALLCAVE01_MOLERAT"] = {x = 3392.10645, y = -760.101379, z = -11171.9492, adjacent =
	[
		"OC_ROUND_26",
		"OC_ROUND_13",
		"OC_ROUND_25",
		"LOCATION_15_IN",
	]},
	["LOCATION_15_IN"] = {x = 3305.47363, y = -727.936462, z = -11773.165, adjacent =
	[
		"SPAWN_OW_SMALLCAVE01_MOLERAT",
		"LOCATION_15_IN_2",
	]},
	["LOCATION_15_IN_2"] = {x = 3038.73535, y = -770.463135, z = -12330.3867, adjacent =
	[
		"LOCATION_15_IN",
	]},
	["OW_PATH_001"] = {x = 6903.98779, y = -21.6812134, z = -10525.9912, adjacent =
	[
		"LOCATION_14_04",
		"SPAWN_OW_SCAVENGER_OC_PSI_RUIN1",
		"SPAWN_OW_WARAN_OC_PSI3",
		"OW_PATH_003",
		"OC_ROUND_12",
		"ORKS",
	]},
	["OC_ROUND_28"] = {x = 8853.42676, y = -660.012512, z = -3747.31519, adjacent =
	[
		"OC_ROUND_10",
		"OC_ROUND_11",
	]},
	["BRIDGE_PLATEAU_TO_CASTLE"] = {x = 10022.1406, y = 7261.59912, z = -25283.5801, adjacent =
	[
		"PLATEAU_ROUND03",
	]},
	["OW_PATH_012"] = {x = 8044.72656, y = 5817.86865, z = -21073.1641, adjacent =
	[
		"CASTLE_8",
		"PATH_CASTLE_TO_WATERFALL",
	]},
	["OW_PATH_017"] = {x = 5963.51318, y = 269.987305, z = -13489.0342, adjacent =
	[
		"OW_PATH_016",
		"OW_PATH_018",
	]},
	["OW_PATH_020"] = {x = -192.455017, y = 554.855469, z = -15974.1055, adjacent =
	[
		"OW_PATH_019",
		"OW_PATH_021",
	]},
	["OW_PATH_021"] = {x = -124.395714, y = 803.809204, z = -16361.8848, adjacent =
	[
		"OW_PATH_020",
		"OW_PATH_022",
	]},
	["OW_PATH_022"] = {x = 48.704895, y = 1098.28003, z = -16462.2539, adjacent =
	[
		"OW_PATH_021",
		"OW_PATH_023",
	]},
	["OW_PATH_023"] = {x = 303.57843, y = 1399.68005, z = -16797.6836, adjacent =
	[
		"OW_PATH_022",
		"OW_PATH_024",
	]},
	["OW_PATH_024"] = {x = -29.5419044, y = 1612.12524, z = -17005.043, adjacent =
	[
		"OW_PATH_023",
		"OW_PATH_025",
	]},
	["OW_PATH_025"] = {x = -460.726349, y = 1858.54163, z = -16601.123, adjacent =
	[
		"OW_PATH_024",
		"OW_PATH_026",
	]},
	["OW_PATH_026"] = {x = -526.35791, y = 2138.67041, z = -16989.8359, adjacent =
	[
		"OW_PATH_025",
		"OW_PATH_027",
	]},
	["OW_PATH_027"] = {x = -390.972137, y = 2457.7688, z = -17364.7363, adjacent =
	[
		"OW_PATH_026",
		"OW_PATH_028",
	]},
	["OW_PATH_028"] = {x = -427.010895, y = 2742.4646, z = -17919.1152, adjacent =
	[
		"OW_PATH_027",
		"OW_PATH_029",
	]},
	["OW_PATH_029"] = {x = 708.665344, y = 2667.45386, z = -17584.9141, adjacent =
	[
		"OW_PATH_028",
		"LOCATION_16_OUT",
	]},
	["LOCATION_16_OUT"] = {x = 1110.25806, y = 2973.86572, z = -17563.4492, adjacent =
	[
		"OW_PATH_029",
		"LOCATION_16_IN",
	]},
	["LOCATION_16_IN"] = {x = 1213.06519, y = 2902.64575, z = -18763.4473, adjacent =
	[
		"LOCATION_16_OUT",
	]},
	["OW_PATH_040"] = {x = -24603.9082, y = 587.154358, z = -6052.1665, adjacent =
	[
		"OW_PATH_WOLF03_SPAWN02",
		"OW_PATH_039",
		"OW_PATH_041",
	]},
	["OW_PATH_047"] = {x = -15884.6016, y = -1600.35571, z = -1926.30737, adjacent =
	[
		"OW_PATH_046",
		"OW_ENTRANCE_SNAPPER_SPAWN",
		"OW_PATH_OC_NC_2",
	]},
	["OW_PATH_49"] = {x = -20354.6074, y = -230.568298, z = 6.92746449, adjacent =
	[
		"OW_PATH_50",
	]},
	["OW_PATH_50"] = {x = -20009.834, y = 118.696335, z = -1217.59631, adjacent =
	[
		"OW_PATH_49",
		"OW_PATH_51",
	]},
	["OW_PATH_51"] = {x = -20738.8613, y = 498.534637, z = -3010.59766, adjacent =
	[
		"OW_PATH_50",
		"OW_PATH_52",
	]},
	["OW_PATH_52"] = {x = -21429.2344, y = 543.564697, z = -4007.59277, adjacent =
	[
		"OW_PATH_042",
		"OW_PATH_51",
	]},
	["OW_OM_ENTRANCE05"] = {x = -20403.6855, y = -85.4398041, z = 31924.4805, adjacent =
	[
		"OW_OM_ENTRANCE06",
	]},
	["OW_OM_ENTRANCE06"] = {x = -21777.8008, y = -414.461853, z = 33304.8984, adjacent =
	[
		"OW_OM_ENTRANCE05",
	]},
	["OW_PATH_148"] = {x = -30465.8047, y = -725.035034, z = 20193.627, adjacent =
	[
		"OW_PATH_147",
		"OW_PATH_146",
		"OW_PATH_148_A",
	]},
	["OW_PATH_146"] = {x = -31936.9355, y = -588.995422, z = 20876.8594, adjacent =
	[
		"OW_PATH_148",
	]},
	["OW_PATH_150"] = {x = -31528.3926, y = 556.673706, z = 13015.0068, adjacent =
	[
		"OW_PATH_151",
		"OW_PATH_149",
	]},
	["OW_PATH_154"] = {x = -32324.8066, y = 891.715576, z = 8287.09961, adjacent =
	[
		"OW_PATH_153",
		"OW_PATH_059",
		"OW_PATH_061",
	]},
	["OW_PATH_157"] = {x = -37244.7773, y = 53.3619194, z = 1449.30408, adjacent =
	[
		"OW_PATH_156",
		"OW_SCAVENGER_COAST_NEWCAMP_SPAWN",
	]},
	["OW_PATH_192"] = {x = -7705.42822, y = -1248.61426, z = 8592.99609, adjacent =
	[
		"OW_PATH_191",
		"OW_PATH_193",
	]},
	["OW_PATH_193"] = {x = -9349.35742, y = -1088.71973, z = 8138.10986, adjacent =
	[
		"PATH_OC_NC_28",
		"OC_ROUND_1",
		"OW_PATH_192",
	]},
	["LOCATION_01_02"] = {x = -6036.53613, y = -4.97748566, z = -10720.3506, adjacent =
	[
		"LOCATION_01_01",
		"LOCATION_01_03",
	]},
	["LOCATION_01_03"] = {x = -5489.9043, y = 236.225311, z = -10806.9043, adjacent =
	[
		"LOCATION_01_02",
		"LOCATION_01_04",
	]},
	["LOCATION_01_04"] = {x = -5396.99707, y = 525.795654, z = -11129.7217, adjacent =
	[
		"LOCATION_01_03",
		"LOCATION_01_05",
	]},
	["LOCATION_01_05"] = {x = -5791.354, y = 869.220337, z = -11194.8076, adjacent =
	[
		"LOCATION_01_04",
		"LOCATION_01_06",
	]},
	["LOCATION_01_06"] = {x = -6379.08691, y = 915.461182, z = -11105.8203, adjacent =
	[
		"LOCATION_01_05",
		"LOCATION_01_07",
	]},
	["LOCATION_01_07"] = {x = -6307.49414, y = 941.393738, z = -11785.1992, adjacent =
	[
		"LOCATION_01_06",
	]},
	["LOCATION_19_01"] = {x = 20127.8926, y = 5051.60254, z = -33487.9648, adjacent =
	[
		"PLATEAU_ROUND07",
		"LOCATION_19_02",
	]},
	["LOCATION_19_02"] = {x = 19073.834, y = 5598.14404, z = -35073.3945, adjacent =
	[
		"LOCATION_19_01",
		"LOCATION_19_02_1",
	]},
	["OW_PATH_202"] = {x = -9948.47852, y = 2605.30835, z = -36598.8867, adjacent =
	[
		"OW_PATH_128",
		"OW_PATH_203",
	]},
	["OW_PATH_203"] = {x = -9751.52734, y = 2986.69897, z = -36633.6133, adjacent =
	[
		"OW_PATH_202",
		"OW_PATH_204",
	]},
	["OW_PATH_204"] = {x = -8964.44727, y = 3186.10376, z = -36384.2656, adjacent =
	[
		"OW_PATH_203",
		"OW_PATH_205",
	]},
	["OW_PATH_205"] = {x = -9034.54004, y = 3390.60425, z = -36196.9883, adjacent =
	[
		"OW_PATH_204",
		"OW_PATH_206",
	]},
	["OW_PATH_206"] = {x = -8929.58691, y = 3683.85498, z = -35738.5391, adjacent =
	[
		"OW_PATH_205",
		"OW_PATH_207",
	]},
	["OW_PATH_207"] = {x = -9514.42773, y = 3596.6084, z = -35144.6367, adjacent =
	[
		"OW_PATH_206",
		"OW_PATH_208",
	]},
	["OW_PATH_208"] = {x = -9446.11914, y = 3441.76416, z = -34555.4805, adjacent =
	[
		"OW_PATH_207",
	]},
	["OW_PATH_209"] = {x = 429.501373, y = 5653.0874, z = -26004.4883, adjacent =
	[
		"OW_PATH_210",
		"OW_PATH_112",
	]},
	["OW_PATH_247"] = {x = -12521.5977, y = -663.988159, z = 15715.8936, adjacent =
	[
		"OW_PATH_246",
		"OW_PATH_248",
	]},
	["OW_PATH_249"] = {x = -13991.8535, y = -753.77533, z = 16246.3086, adjacent =
	[
		"OW_PATH_250",
		"OW_PATH_248",
	]},
	["OW_PATH_251"] = {x = -14959.2119, y = -880.455444, z = 16488.8359, adjacent =
	[
		"OW_PATH_250",
		"OW_PATH_252",
	]},
	["OW_PATH_252"] = {x = -15573.8486, y = -974.609375, z = 16468.4922, adjacent =
	[
		"OW_PATH_253",
		"OW_PATH_251",
	]},
	["OW_PATH_255"] = {x = -17827.75, y = -946.882874, z = 16977.3887, adjacent =
	[
		"OW_PATH_254",
		"OW_PATH_256",
	]},
	["OW_PATH_256"] = {x = -19081.6914, y = -905.254578, z = 17360.0371, adjacent =
	[
		"OW_PATH_255",
		"OW_PATH_257",
	]},
	["OW_PATH_257"] = {x = -19480.1035, y = -860.565308, z = 17700.4395, adjacent =
	[
		"OW_PATH_256",
		"OW_PATH_258",
	]},
	["OW_PATH_258"] = {x = -19808.2246, y = -907.263306, z = 17978.0684, adjacent =
	[
		"OW_PATH_257",
		"OW_PATH_259",
		"SPAWN_OW_MOLERAT_OCWOOD_OLDMINE3",
	]},
	["OW_PATH_259"] = {x = -20836.709, y = -1066.36462, z = 18382.9551, adjacent =
	[
		"OW_PATH_258",
		"SPAWN_OW_MOLERAT_OCWOOD_OLDMINE3",
	]},
	["OW_PATH_260"] = {x = -22421.1465, y = -919.594971, z = 19003.8379, adjacent =
	[
		"OW_PATH_261",
	]},
	["OW_PATH_261"] = {x = -23505.6211, y = -835.398132, z = 19127.2754, adjacent =
	[
		"OW_PATH_260",
		"OW_PATH_262",
	]},
	["OW_PATH_262"] = {x = -24176.7207, y = -773.582336, z = 19383.7676, adjacent =
	[
		"OW_PATH_263",
		"OW_PATH_261",
	]},
	["OW_PATH_266"] = {x = -27448, y = -713.191772, z = 21037, adjacent =
	[
		"OW_PATH_265",
		"OW_PATH_264",
		"OW_PATH_148_A",
		"OW_MINE2_STAND_JERGAN",
	]},
	["WP_INTRO06"] = {x = 6155.6709, y = 6566.79883, z = 39776.6602, adjacent =
	[
		"WP_INTRO03",
		"WP_INTRO04",
	]},
	["WP_INTRO_FALL"] = {x = 6392.76904, y = 6496.44336, z = 39300.0547, adjacent =
	[
		"WP_INTRO04",
	]},
	["PLATEAU_ROUND04"] = {x = 9711, y = 7251.00195, z = -26952, adjacent =
	[
		"PLATEAU_ROUND_TO_HUT",
	]},
	["PLATEAU_ROUND_TO_HUT"] = {x = 10190.9883, y = 7356.04102, z = -26620.6191, adjacent =
	[
		"LOCATION_18_OUT",
		"PLATEAU_ROUND03",
		"PLATEAU_ROUND04",
	]},
	["OW_VM_ENTRANCE"] = {x = 4827.68555, y = 5965.37354, z = 26795.1563, adjacent =
	[
		"OW_PATH_1_17",
	]},
	["OW_PATH_07_19A"] = {x = -43606.0469, y = 2506.11035, z = 3329.68799, adjacent =
	[
		"OW_PATH_07_18",
		"OW_PATH_07_19",
	]},
	["LOCATION_29_04"] = {x = 20205.6055, y = 1029.12415, z = -24721.3594, adjacent =
	[
		"SPAWN_GOBBO_LOCATION_29_03",
		"MOVEMENT_GOBBO_LOCATION_29_01",
	]},
	["SPAWN_GOBBO_LOCATION_29_03"] = {x = 19426.0918, y = 900.701233, z = -22210.6523, adjacent =
	[
		"LOCATION_29_04",
		"MOVEMENT_GOBBO_LOCATION_29_03",
		"MOVEMENT_GOBBO_LOCATION_29_01",
		"OW_WATERFALL_GOBBO6",
	]},
	["WP_INTRO_WI15"] = {x = 4871.95313, y = 5461.7959, z = 36723.832, adjacent =
	[
		"WP_INTRO_FALL3",
	]},
	["WP_INTRO"] = {x = 6888.6084, y = 6540.16992, z = 42092.6055, adjacent =
	[
		"WP_INTRO09",
		"WP_INTRO02",
	]},
	["WP_INTRO09"] = {x = 6696.94727, y = 6518.50977, z = 41589.7305, adjacent =
	[
		"WP_INTRO03",
		"WP_INTRO",
	]},
	["WP_INTRO02"] = {x = 7695.66699, y = 6509.26807, z = 42836.3281, adjacent =
	[
		"WP_INTRO",
	]},
	["OW_PATH_SNAPPER02_SPAWN01"] = {x = -6311.77588, y = 1878.40491, z = -26704.6523, adjacent =
	[
		"OW_PATH_304",
		"OW_PATH_SNAPPER02_SPAWN02",
	]},
	["OW_PATH_SNAPPER02_SPAWN02"] = {x = -6532, y = 1874.4176, z = -27063, adjacent =
	[
		"OW_PATH_303",
		"OW_PATH_SNAPPER02_SPAWN01",
	]},
	["OW_PATH_OW_PATH_WARAN05_SPAWN01"] = {x = 7904.96387, y = -1256.4469, z = 9880.86328, adjacent =
	[
		"OW_PATH_004",
		"OC_ROUND_5",
	]},
	["OW_PATH_WARAN01_SPAWN01"] = {x = 14362.2852, y = 303.039246, z = 2166.2251, adjacent =
	[
		"PATH_OC_FOGTOWER02",
		"OW_PATH_WARAN01_SPAWN02",
	]},
	["OW_PATH_WARAN01_SPAWN02"] = {x = 14062.2324, y = 208.358063, z = 3181.27832, adjacent =
	[
		"SPAWN_WALD_OC_BLOODFLY01",
		"OW_PATH_WARAN01_SPAWN01",
	]},
	["OW_PATH_WARAN05_SPAWN02"] = {x = 5218.70752, y = -1504.55078, z = 11107.1328, adjacent =
	[
		"OW_PATH_BLOODFLY01_SPAWN01",
		"OC_ROUND_5",
		"OW_PATH_WARAN05_SPAWN02_01",
	]},
	["OW_PATH_BLACKWOLF07_SPAWN01"] = {x = -7942.66211, y = 1784.38794, z = -16255.2637, adjacent =
	[
		"OW_PATH_104",
	]},
	["PATH_WALD_OC_WOLFSPAWN2"] = {x = 14977.04, y = -41.6404724, z = 6683.62354, adjacent =
	[
		"PATH_WALD_OC_WOLFSPAWN",
		"PATH_OC_OCWOOD_4",
		"PATH_WALD_OC_WOLFSPAWN_MOVEMENT2",
		"PATH_WALD_OC_WOLFSPAWN_MOVEMENT",
	]},
	["PATH_OC_OCWOOD_6"] = {x = 16448.8555, y = 241.144638, z = 5042.56934, adjacent =
	[
		"PATH_OC_OCWOOD_7",
		"PATH_OC_OCWOOD_5",
	]},
	["OW_SAWHUT_MEATBUG_SPAWN"] = {x = -14826.7539, y = -1238.82251, z = 4179.11084, adjacent =
	[
		"PATH_OC_NC_3",
	]},
	["OW_WOODRUIN_WOLF_SPAWN"] = {x = -26780.2383, y = -178.457489, z = 2526.62646, adjacent =
	[
		"PATH_OC_NC_14",
		"PATH_OC_NC_21",
	]},
	["OW_GOBBO_PLACE_SPAWN"] = {x = -30152.4336, y = 691.028809, z = 3805.78613, adjacent =
	[
		"PATH_AROUND_HILL03",
		"OW_PATH_055",
	]},
	["OW_MOLERAT_CAVE_SPAWN"] = {x = -20844.1211, y = -738.277344, z = 5303.12891, adjacent =
	[
		"LOCATION_21_IN",
	]},
	["OW_ENTRANCE_SNAPPER_SPAWN"] = {x = -15774.8877, y = -1626.23474, z = -1426.29456, adjacent =
	[
		"OW_PATH_047",
		"OW_PATH_OC_NC_2",
	]},
	["OW_SCAVENGER_SPAWN_TREE"] = {x = -10785.9766, y = -613.472595, z = 3231.53198, adjacent =
	[
		"OC_ROUND_21",
	]},
	["OW_PATH_OC_NC"] = {x = -12662.0654, y = -1216.60498, z = -3549.07324, adjacent =
	[
		"OW_PATH_268",
		"OW_PATH_OC_NC2",
	]},
	["OW_PATH_OC_NC2"] = {x = -12797.874, y = -1317.98279, z = -3275.9187, adjacent =
	[
		"OW_PATH_OC_NC",
		"OW_PATH_OC_NC2_TO_TOWER_01",
	]},
	["OW_PATH_OC_NC4"] = {x = -12730.6992, y = -1947.37329, z = -1171.28174, adjacent =
	[
		"OW_PATH_OC_NC3",
		"HELPPOINT2",
		"OW_PATH_OC_NC5",
		"HELPPOINT",
	]},
	["OW_PATH_OC_NC5"] = {x = -13342.0889, y = -2014.51733, z = -468.393616, adjacent =
	[
		"HELPPOINT2",
		"OW_PATH_OC_NC4",
		"OW_SCAVENGER_AL_NL_SPAWN",
		"OW_PATH_OC_NC6",
	]},
	["OW_SCAVENGER_AL_NL_SPAWN"] = {x = -13422.0898, y = -1944.59021, z = 271.837677, adjacent =
	[
		"OW_PATH_OC_NC5",
		"OW_PATH_OC_NC6",
		"OW_PATH_OC_NC7",
	]},
	["OW_PATH_OC_NC6"] = {x = -13699.5732, y = -1971.85852, z = -180.883926, adjacent =
	[
		"OW_PATH_OC_NC5",
		"OW_SCAVENGER_AL_NL_SPAWN",
		"OW_PATH_OC_NC7",
	]},
	["OW_PATH_OC_NC8"] = {x = -14708.0674, y = -1835.62097, z = -375.069122, adjacent =
	[
		"OW_PATH_05_1",
		"OW_PATH_OC_NC7",
		"HELPPOINT4",
		"OW_PATH_OC_NC_1",
		"HELPPOINT9",
	]},
	["OW_PATH_OC_NC7"] = {x = -14260.6064, y = -1911.46741, z = -134.874664, adjacent =
	[
		"OW_SCAVENGER_AL_NL_SPAWN",
		"OW_PATH_OC_NC6",
		"OW_PATH_OC_NC8",
		"HELPPOINT9",
	]},
	["OW_PATH_OC_NC_2"] = {x = -15463.8086, y = -1647.78223, z = -1357.31189, adjacent =
	[
		"OW_PATH_047",
		"OW_ENTRANCE_SNAPPER_SPAWN",
		"OW_PATH_OC_NC_1",
	]},
	["HELPPOINT"] = {x = -12293.4873, y = -1505.64282, z = 592.545471, adjacent =
	[
		"OW_PATH_OC_NC4",
		"HELPPOINT6",
	]},
	["HELPPOINT4"] = {x = -15686.668, y = -1639.12256, z = -494.764832, adjacent =
	[
		"OW_PATH_05",
		"OW_PATH_OC_NC8",
		"OW_PATH_OC_NC_1",
		"HELPPOINT10",
	]},
	["OW_PATH_OC_NC_1"] = {x = -15128.418, y = -1731.16833, z = -982.719238, adjacent =
	[
		"OW_PATH_OC_NC8",
		"OW_PATH_OC_NC_2",
		"HELPPOINT4",
	]},
	["HELPPOINT6"] = {x = -13381.0225, y = -1439.83447, z = 969.646545, adjacent =
	[
		"HELPPOINT8",
		"HELPPOINT",
	]},
	["HELPPOINT9"] = {x = -14357.3818, y = -1850.06604, z = -909.572144, adjacent =
	[
		"OW_PATH_OC_NC8",
		"OW_PATH_OC_NC7",
		"OW_DJG_SWAMPCAMP_01",
		"SWAMPCAMP",
	]},
	["HELPPOINT10"] = {x = -16668.7402, y = -1286.66052, z = 304.515961, adjacent =
	[
		"HELPPOINT4",
	]},
	["OW_MOVEMENT_BGOBBO1"] = {x = -28098.3184, y = -659.64563, z = 10238.7461, adjacent =
	[
		"SPAWN_OW_BLACKGOBBO_A1",
		"OW_MOVEMENT_BGOBBO2",
		"OW_MOVEMENT_LURKER_NEARBGOBBO02",
		"OW_MOVEMENT_BGOBBO3",
	]},
	["SPAWN_OW_NEARBGOBBO_LURKER_A1"] = {x = -26570.5215, y = -511.491699, z = 11857.6865, adjacent =
	[
		"MOVEMENT_OW_MINICOAST_LURKER_A1",
		"OW_MOVEMENT_LURKER_NEARBGOBBO03",
		"OW_MOVEMENT_LURKER_NEARBGOBBO01",
	]},
	["MOVEMENT_GOBBO_LOCATION_29_03"] = {x = 18935.7852, y = 980.223328, z = -22822.0449, adjacent =
	[
		"SPAWN_GOBBO_LOCATION_29_03",
	]},
	["MOVEMENT_GOBBO_LOCATION_29_01"] = {x = 20389.25, y = 869.048828, z = -22692.9746, adjacent =
	[
		"LOCATION_29_04",
		"SPAWN_GOBBO_LOCATION_29_03",
		"OW_WATERFALL_GOBBO5",
		"OW_WATERFALL_GOBBO4",
	]},
	["SPAWN_OW_MOLERAT_OCWOOD_OLDMINE3"] = {x = -20028.4961, y = -995.117554, z = 18317.2324, adjacent =
	[
		"OW_PATH_258",
		"OW_PATH_259",
	]},
	["MOVEMENT_TALL_PATH_BANDITOS2"] = {x = -18239.8125, y = -500.260498, z = 6055.28955, adjacent =
	[
		"SPAWN_TALL_PATH_BANDITOS2",
		"SPAWN_OW_BLOODFLY_12",
		"MOVEMENT_TALL_PATH_01",
	]},
	["SPAWN_TALL_PATH_BANDITOS2"] = {x = -18435.7695, y = -341.308136, z = 5381.87842, adjacent =
	[
		"SPAWN_TALL_PATH_BANDITOS2_02",
		"MOVEMENT_TALL_PATH_BANDITOS2",
	]},
	["SPAWN_OW_BLOODFLY_12"] = {x = -19154.7832, y = -898.518494, z = 7202.15723, adjacent =
	[
		"MOVEMENT_TALL_PATH_BANDITOS2",
	]},
	["SPAWN_SIT_OW"] = {x = -10371.3945, y = -659.068909, z = -166.879913, adjacent =
	[
		"OC_ROUND_20",
	]},
	["OW_SCAVENGER_TREE_SPAWN"] = {x = -17613.502, y = -742.436401, z = 3302.38403, adjacent =
	[
		"OW_SCAVENGER_CAVE3_SPAWN",
		"PATH_OC_NC_8_1",
	]},
	["SPAWN_OW_MOLERAT_06_CAVE_GUARD3"] = {x = -9655.74805, y = 83.3879242, z = -10013.8252, adjacent =
	[
		"MOVEMENT_MOLERAT_06_CAVE_GUARD3",
	]},
	["MOVEMENT_MOLERAT_06_CAVE_GUARD3"] = {x = -9306.80078, y = 19.8908958, z = -9276.43359, adjacent =
	[
		"MOVEMENT_MOLERAT_06_CAVE_GUARD2",
		"SPAWN_OW_MOLERAT_06_CAVE_GUARD3",
	]},
	["OW_PATH_1_5_A"] = {x = -5741.16113, y = -880.126282, z = 15227.2119, adjacent =
	[
		"SPAWN_GOBBO_OW_PATH_1_6",
		"OW_PATH_1_5_B",
		"OW_PATH_1_5_13",
	]},
	["SPAWN_OW_WARAN_EBENE2_02_05"] = {x = 24749.1758, y = 3780.75415, z = -22465.623, adjacent =
	[
		"PATH_TO_PLATEAU02",
		"SPAWN_OW_WARAN_EBENE_02_05",
	]},
	["SPAWN_OW_WARAN_EBENE_02_05"] = {x = 25271.375, y = 3947.62134, z = -24239.5273, adjacent =
	[
		"PATH_TO_PLATEAU03",
		"SPAWN_OW_WARAN_EBENE2_02_05",
	]},
	["OW_WATERFALL_GOBBO5"] = {x = 19975.3379, y = 886.634827, z = -22108.498, adjacent =
	[
		"MOVEMENT_GOBBO_LOCATION_29_01",
		"OW_WATERFALL_GOBBO4",
	]},
	["OW_WATERFALL_GOBBO4"] = {x = 20729.9277, y = 886.505432, z = -22324.1172, adjacent =
	[
		"MOVEMENT_GOBBO_LOCATION_29_01",
		"OW_WATERFALL_GOBBO5",
	]},
	["LOCATION_19_03_PATH_RUIN5"] = {x = 19661.6992, y = 6992.56934, z = -35860.7109, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN6",
		"LOCATION_19_03_PATH_RUIN4",
	]},
	["LOCATION_19_03_PATH_RUIN4"] = {x = 18728.0156, y = 6849.72021, z = -36255.8867, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN5",
		"LOCATION_19_03_PATH_RUIN3",
	]},
	["LOCATION_19_03_PATH_RUIN3"] = {x = 17978.5742, y = 6431.8042, z = -37122.9063, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN4",
		"LOCATION_19_03_PATH_RUIN2",
	]},
	["LOCATION_19_03_PATH_RUIN2"] = {x = 17434.8203, y = 6555.2876, z = -38772.9609, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN3",
		"LOCATION_19_03_PATH_RUIN",
	]},
	["LOCATION_19_03_PATH_RUIN"] = {x = 16866.9512, y = 6401.79004, z = -38718.4219, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN2",
		"LOCATION_19_03",
	]},
	["LOCATION_19_03"] = {x = 17253.9316, y = 5793.55811, z = -36848.7734, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN",
		"LOCATION_19_02_2",
	]},
	["OW_PATH_274_RIGHT"] = {x = -38059.9805, y = 3472.93823, z = -14121.6133, adjacent =
	[
		"OW_PATH_274",
		"OW_PATH_274_RIGHT2",
	]},
	["OW_PATH_274_RIGHT2"] = {x = -37690.0664, y = 3291.25122, z = -15130.9619, adjacent =
	[
		"OW_PATH_274_RIGHT",
		"OW_PATH_274_RIGHT3",
	]},
	["OW_PATH_274_RIGHT3"] = {x = -36789.7422, y = 3148.04004, z = -15238.0225, adjacent =
	[
		"OW_PATH_033",
		"OW_PATH_274_RIGHT2",
	]},
	["OW_PATH_033_TO_CAVE3"] = {x = -33383.3789, y = 2680.57349, z = -16842.1641, adjacent =
	[
		"OW_PATH_033_TO_CAVE5",
		"OW_PATH_033_TO_CAVE2",
	]},
	["OW_PATH_033_TO_CAVE5"] = {x = -33221.7266, y = 2741.67871, z = -17975.7656, adjacent =
	[
		"OW_PATH_033_TO_CAVE3",
	]},
	["OW_PATH_033_TO_CAVE2"] = {x = -34204.1055, y = 2872.83643, z = -15810.7295, adjacent =
	[
		"OW_PATH_033_TO_CAVE3",
		"OW_PATH_033_TO_CAVE",
	]},
	["OW_PATH_033_TO_CAVE"] = {x = -34543.2227, y = 2993.75391, z = -14974.1699, adjacent =
	[
		"SPAWN_OW_SHADOWBEAST_10_01",
		"OW_PATH_033",
		"OW_PATH_033_TO_CAVE2",
	]},
	["LOCATION_19_03_ENTRANCE_HARPYE"] = {x = 20350.459, y = 7801.24365, z = -37384.5859, adjacent =
	[
		"LOCATION_19_03_PATH_RUIN9",
		"LOCATION_19_03_ENTRANCE",
		"LOCATION_19_03_ENTRANCE_HARPYE2",
	]},
	["LOCATION_19_03_ENTRANCE_HARPYE2"] = {x = 20571.6367, y = 7801.24365, z = -37451.3242, adjacent =
	[
		"LOCATION_19_03_ENTRANCE",
		"LOCATION_19_03_ENTRANCE_HARPYE",
		"LOCATION_19_03_ENTRANCE_HARPYE3",
	]},
	["LOCATION_19_03_ENTRANCE_HARPYE3"] = {x = 20728.6992, y = 7801.24414, z = -37479.6602, adjacent =
	[
		"LOCATION_19_03_ENTRANCE",
		"LOCATION_19_03_PATH_RUIN10",
		"LOCATION_19_03_ENTRANCE_HARPYE2",
	]},
	["OW_PATH_07_15_CAVE2"] = {x = -44534.332, y = 3441.90967, z = -1825.23718, adjacent =
	[
		"OW_PATH_07_15_CAVE3",
		"OW_PATH_07_15_CAVE",
	]},
	["OW_PATH_07_15_CAVE3"] = {x = -45186.5938, y = 3319.95508, z = -1049.05554, adjacent =
	[
		"OW_PATH_07_15_CAVE2",
	]},
	["OW_PATH_07_15_CAVE"] = {x = -43919.2148, y = 3423.76147, z = -1915.52234, adjacent =
	[
		"OW_PATH_07_15",
		"OW_PATH_07_15_CAVE2",
	]},
	["LOCATION_19_03_PEMTAGRAM_MOVEMENT"] = {x = 19144.9648, y = 7876.23779, z = -38009.1914, adjacent =
	[
		"LOCATION_19_03_PEMTAGRAM2",
		"LOCATION_19_03_PEMTAGRAM_ENTRANCE",
		"LOCATION_19_03_PENTAGRAMM",
	]},
	["OC_ROUND_22_CF_1"] = {x = -4040.81689, y = -1116.50403, z = 7862.84717, adjacent =
	[
		"OC_ROUND_22_CF_2",
		"SPAWN_A_OW_OC_NC_AMBIENTNSC2",
	]},
	["OC_ROUND_22_CF_2"] = {x = -4324.42139, y = -1125.06885, z = 7624.53076, adjacent =
	[
		"OC_ROUND_22_CF_1",
		"OC_ROUND_22_CF_2_MOVEMENT",
		"OC14",
	]},
	["SPAWN_A_OW_OC_NC_AMBIENTNSC2"] = {x = -4262.85596, y = -1134.19922, z = 8068.3916, adjacent =
	[
		"OC_ROUND_22",
		"OC_ROUND_22_CF_1",
		"OC_ROUND_22_CF_2_MOVEMENT",
		"OC_ROUND_22_CF_2_MOVEMENT2",
	]},
	["OC_ROUND_22_CF_2_MOVEMENT"] = {x = -4701.78027, y = -1135.41711, z = 7844.0957, adjacent =
	[
		"OC_ROUND_22_CF_2",
		"SPAWN_A_OW_OC_NC_AMBIENTNSC2",
		"OC_ROUND_22_CF_2_MOVEMENT2",
	]},
	["OC_ROUND_22_CF_2_MOVEMENT2"] = {x = -4720.78027, y = -1159.87268, z = 8106.06982, adjacent =
	[
		"OC_ROUND_2",
		"OC_ROUND_22",
		"SPAWN_A_OW_OC_NC_AMBIENTNSC2",
		"OC_ROUND_22_CF_2_MOVEMENT",
	]},
	["OW_ORC_LOOKOUT_04"] = {x = -15396.9482, y = 1612.21533, z = -17297.6953, adjacent =
	[
		"OW_ORC_LOOKOUT_05",
		"OW_ORC_LOOKOUT_03",
	]},
	["OW_ORC_LOOKOUT_05"] = {x = -14615.0166, y = 1960.0459, z = -16560.6758, adjacent =
	[
		"OW_ORC_LOOKOUT_04",
	]},
	["OW_ORC_LOOKOUT_03"] = {x = -16657.8379, y = 1520.25598, z = -16578.8516, adjacent =
	[
		"OW_ORC_LOOKOUT_04",
		"OW_ORC_LOOKOUT_02",
	]},
	["OW_ORC_LOOKOUT_02"] = {x = -17426.3965, y = 942.428223, z = -15355.209, adjacent =
	[
		"OW_ORC_ORCDOG_SPAWN01",
		"OW_ORC_LOOKOUT_03",
		"OW_ORC_LOOKOUT_01",
	]},
	["OW_ORC_LOOKOUT_01"] = {x = -18004.2773, y = 1001.52557, z = -13714.8945, adjacent =
	[
		"OW_ORC_ORCDOG_SPAWN01_MOVEMENT",
		"OW_ORC_LOOKOUT_02",
	]},
	["OW_ORC_LOOKOUT_2_05"] = {x = -13906.6719, y = 1588.25867, z = -22268.6016, adjacent =
	[
		"OW_ORC_LOOKOUT_2_04",
		"OW_NEWMINE_10",
		"NEWMINE",
	]},
	["OW_ORC_LOOKOUT_2_03"] = {x = -12668.5713, y = 2256.41113, z = -20882.5156, adjacent =
	[
		"OW_ORC_LOOKOUT_2_04",
		"OW_ORC_LOOKOUT_2_02",
	]},
	["OW_ORC_LOOKOUT_2_02"] = {x = -11743.4521, y = 2651.09961, z = -19974.8926, adjacent =
	[
		"OW_ORC_LOOKOUT_2_03",
		"OW_ORC_LOOKOUT_2_01",
		"OW_ORC_LOOKOUT_2_02_MOVE",
	]},
	["OW_NEWMINE_10"] = {x = -14311.7813, y = 1382.10266, z = -23182.7266, adjacent =
	[
		"OW_ORC_LOOKOUT_2_05",
		"OW_NEWMINE_09_B",
	]},
	["OW_ORC_LOOKOUT_2_01"] = {x = -11167.1563, y = 2651.95923, z = -20187.4941, adjacent =
	[
		"OW_ORC_LOOKOUT_2_02",
	]},
	["OW_ORC_LOOKOUT_2_02_MOVE"] = {x = -11323.4951, y = 2655.57373, z = -19285.2617, adjacent =
	[
		"OW_ORC_LOOKOUT_2_02",
		"OW_ORC_LOOKOUT_2_02_MOVE2",
	]},
	["OW_ORC_LOOKOUT_2_02_MOVE2"] = {x = -10177.0586, y = 3059.78687, z = -19989.0645, adjacent =
	[
		"OW_ORC_LOOKOUT_2_02_MOVE",
	]},
	["SPAWN_TALL_PATH_BANDITOS2_03"] = {x = -18459.3633, y = -120.050339, z = 4307.07471, adjacent =
	[
		"SPAWN_TALL_PATH_BANDITOS2_02",
	]},
	["LOCATION_19_02_1"] = {x = 18388.6699, y = 5857.53662, z = -35511.4141, adjacent =
	[
		"LOCATION_19_02",
		"LOCATION_19_02_2",
	]},
	["LOCATION_19_02_2"] = {x = 17570.9063, y = 5949.375, z = -36311.332, adjacent =
	[
		"LOCATION_19_03",
		"LOCATION_19_02_1",
	]},
	["OW_WATERFALL_GOBBO7"] = {x = 19584.1953, y = 941.534607, z = -19984.9707, adjacent =
	[
		"OW_WATERFALL_GOBBO6",
		"OW_WATERFALL_GOBBO9",
	]},
	["OW_WATERFALL_GOBBO6"] = {x = 19564.6816, y = 809.969604, z = -21568.0898, adjacent =
	[
		"SPAWN_GOBBO_LOCATION_29_03",
		"OW_WATERFALL_GOBBO7",
	]},
	["OW_WATERFALL_GOBBO9"] = {x = 19679.2031, y = 1069.09863, z = -18862.832, adjacent =
	[
		"OW_WATERFALL_GOBBO7",
		"OW_WATERFALL_GOBBO10",
	]},
	["OW_WATERFALL_GOBBO10"] = {x = 19859.4551, y = 1161.27002, z = -18287.3125, adjacent =
	[
		"OW_ORCBARRIER_04",
		"OW_ORCBARRIER_03",
		"OW_ORCBARRIER_02",
		"OW_WATERFALL_GOBBO9",
	]},
	["OW_ROCKDRAGON_01"] = {x = 19663.5742, y = 8886.24316, z = -37525.5117, adjacent =
	[
		"LOCATION_19_03_SECOND_HARPYE6",
		"OW_ROCKDRAGON_09",
	]},
	["OW_ROCKDRAGON_02"] = {x = 19925.9023, y = 9707.59961, z = -39018.5391, adjacent =
	[
		"OW_ROCKDRAGON_03",
		"OW_ROCKDRAGON_09",
		"OW_ROCKDRAGON_10",
	]},
	["OW_ROCKDRAGON_06"] = {x = 22533.6914, y = 9660.51367, z = -41031.3086, adjacent =
	[
		"OW_ROCKDRAGON_05",
		"OW_ROCKDRAGON_04",
		"OW_ROCKDRAGON_11",
		"OW_ROCKDRAGON_10",
	]},
	["OW_ROCKDRAGON_05"] = {x = 22266.5, y = 9688.97852, z = -41354.0703, adjacent =
	[
		"OW_ROCKDRAGON_06",
	]},
	["OW_ROCKDRAGON_04"] = {x = 22824.127, y = 9730.20313, z = -40081.6719, adjacent =
	[
		"OW_ROCKDRAGON_06",
	]},
	["OW_ROCKDRAGON_08"] = {x = 21349.7012, y = 9727.25391, z = -38209.3242, adjacent =
	[
		"OW_ROCKDRAGON_07",
		"OW_ROCKDRAGON_09",
	]},
	["OW_ROCKDRAGON_07"] = {x = 22038.5918, y = 9735.07031, z = -37441.125, adjacent =
	[
		"OW_ROCKDRAGON_08",
	]},
	["OW_ROCKDRAGON_09"] = {x = 19994.9473, y = 9741.24316, z = -38130.5664, adjacent =
	[
		"OW_ROCKDRAGON_01",
		"OW_ROCKDRAGON_02",
		"OW_ROCKDRAGON_08",
		"OW_ROCKDRAGON_10",
	]},
	["OW_ROCKDRAGON_11"] = {x = 24140.6895, y = 9602.4375, z = -42059.7031, adjacent =
	[
		"OW_ROCKDRAGON_06",
	]},
	["OW_ROCKDRAGON_10"] = {x = 20899.0605, y = 9704.43848, z = -39598.6094, adjacent =
	[
		"OW_ROCKDRAGON_03",
		"OW_ROCKDRAGON_02",
		"OW_ROCKDRAGON_06",
		"OW_ROCKDRAGON_09",
	]},
	["OW_ICEREGION_10"] = {x = -40580.4883, y = 646.638672, z = 8652.25977, adjacent =
	[
		"OW_ICEREGION_19",
		"OW_ICEREGION_04",
		"OW_ICEREGION_15",
	]},
	["OW_ICEREGION_12"] = {x = -39400.7109, y = 646.638672, z = 10909.3076, adjacent =
	[
		"OW_ICEREGION_19",
		"OW_ICEREGION_11",
	]},
	["OW_ICEREGION_11"] = {x = -38642.8516, y = 646.638733, z = 10009.418, adjacent =
	[
		"OW_ICEREGION_12",
	]},
	["OW_ICEREGION_15"] = {x = -41745.707, y = 646.638611, z = 8499.05664, adjacent =
	[
		"OW_ICEREGION_06",
		"OW_ICEREGION_07",
		"OW_ICEREGION_08",
		"OW_ICEREGION_10",
	]},
	["OW_ICEREGION_14"] = {x = -42043.8594, y = 646.638672, z = 9816.36523, adjacent =
	[
		"OW_ICEREGION_19",
		"OW_ICEREGION_08",
	]},
	["OW_ICEREGION_20"] = {x = -43338.4648, y = 1332.11267, z = 11747.8057, adjacent =
	[
		"OW_ICEREGION_09",
		"OW_ICEREGION_21",
	]},
	["OW_ICEREGION_21"] = {x = -43965.2109, y = 1461.26648, z = 11904.4346, adjacent =
	[
		"OW_ICEREGION_20",
		"OW_ICEREGION_22",
		"OW_ICEREGION_27",
	]},
	["OW_ICEREGION_22"] = {x = -44112.1055, y = 1427.15771, z = 11569.4805, adjacent =
	[
		"OW_ICEREGION_21",
		"OW_ICEREGION_23",
	]},
	["OW_ICEREGION_23"] = {x = -44475.4219, y = 1391.31726, z = 10486.1572, adjacent =
	[
		"OW_ICEREGION_22",
		"OW_ICEREGION_26",
	]},
	["OW_ICEREGION_25"] = {x = -45229.8281, y = 1404.31299, z = 8747.56152, adjacent =
	[
		"OW_ICEREGION_24",
		"OW_ICEREGION_26",
	]},
	["OW_ICEREGION_24"] = {x = -45362.9883, y = 1391.32312, z = 9239.26074, adjacent =
	[
		"OW_ICEREGION_25",
		"OW_ICEREGION_26",
	]},
	["OW_ICEREGION_26"] = {x = -44763.9414, y = 1399.22693, z = 9473.96191, adjacent =
	[
		"OW_ICEREGION_23",
		"OW_ICEREGION_25",
		"OW_ICEREGION_24",
	]},
	["OW_ICEREGION_27"] = {x = -44997.4336, y = 1610.50513, z = 12293.1992, adjacent =
	[
		"OW_ICEREGION_21",
		"OW_ICEREGION_28",
	]},
	["OW_ICEREGION_28"] = {x = -45334.4453, y = 1634.37683, z = 12132.8408, adjacent =
	[
		"OW_ICEREGION_27",
		"OW_ICEREGION_29",
		"OW_ICEREGION_34",
	]},
	["OW_ICEREGION_29"] = {x = -45213.3047, y = 1553.35657, z = 11484.9209, adjacent =
	[
		"OW_ICEREGION_28",
		"OW_ICEREGION_30",
	]},
	["OW_ICEREGION_30"] = {x = -45348.668, y = 1553.34192, z = 10340.2783, adjacent =
	[
		"OW_ICEREGION_29",
		"OW_ICEREGION_31",
	]},
	["OW_ICEREGION_31"] = {x = -46138.9023, y = 1553.34436, z = 9795.08301, adjacent =
	[
		"OW_ICEREGION_30",
	]},
	["OW_ICEREGION_34"] = {x = -46097.4648, y = 1701.68689, z = 12123.9033, adjacent =
	[
		"OW_ICEREGION_35",
		"OW_ICEREGION_28",
		"OW_ICEREGION_32",
	]},
	["OW_ICEREGION_32"] = {x = -46258.0703, y = 1653.51221, z = 10740.416, adjacent =
	[
		"OW_ICEREGION_34",
		"OW_ICEREGION_33",
	]},
	["OW_ICEREGION_33"] = {x = -47407.457, y = 1641.75171, z = 10004.9033, adjacent =
	[
		"OW_ICEREGION_32",
	]},
	["OW_ICEREGION_36"] = {x = -47918.2852, y = 2609.31714, z = 15061.7969, adjacent =
	[
		"OW_ICEREGION_35",
		"OW_ICEREGION_37",
	]},
	["OW_ICEREGION_37"] = {x = -48494.2852, y = 2669.13037, z = 14909.9932, adjacent =
	[
		"OW_ICEREGION_36",
		"OW_ICEREGION_38",
	]},
	["OW_ICEREGION_38"] = {x = -48566.7695, y = 2613.21338, z = 13963.6318, adjacent =
	[
		"OW_ICEREGION_37",
		"OW_ICEREGION_39",
	]},
	["OW_ICEREGION_39"] = {x = -48542.543, y = 2650.31128, z = 12687.7666, adjacent =
	[
		"OW_ICEREGION_40",
		"OW_ICEREGION_38",
	]},
	["OW_ICEREGION_DAM"] = {x = -49016.1992, y = 2621.24292, z = 11154.71, adjacent =
	[
		"OW_ICEREGION_40",
		"OW_ICEREGION_41",
	]},
	["OW_ICEREGION_41"] = {x = -49228.5234, y = 2649.14063, z = 10412.2246, adjacent =
	[
		"OW_ICEREGION_42",
		"OW_ICEREGION_DAM",
	]},
	["OW_ICEREGION_45"] = {x = -53152.6563, y = 2536.02686, z = 9991.49121, adjacent =
	[
		"OW_ICEREGION_44",
		"OW_ICEREGION_46",
	]},
	["OW_ICEREGION_46"] = {x = -53586.9141, y = 2649.57007, z = 9555.72949, adjacent =
	[
		"OW_ICEREGION_56",
		"OW_ICEREGION_45",
		"OW_ICEREGION_47",
		"OW_DJG_ICEREGION_WAIT2_01",
		"OW_DJG_ICEREGION_WAIT2_02",
	]},
	["OW_ICEREGION_47"] = {x = -54603.6328, y = 2507.79346, z = 8511.69629, adjacent =
	[
		"OW_ICEREGION_56",
		"OW_ICEREGION_46",
		"OW_ICEREGION_48",
		"ICEDRAGON",
	]},
	["OW_ICEREGION_48"] = {x = -54731.4805, y = 2400.70337, z = 7476.05273, adjacent =
	[
		"OW_ICEDRAGON_09",
		"OW_ICEREGION_47",
		"OW_ICEDRAGON_24",
	]},
	["OW_ICEDRAGON_04"] = {x = -55538.0391, y = 2603.43823, z = 3765.16992, adjacent =
	[
		"OW_ICEDRAGON_01",
	]},
	["OW_ICEDRAGON_03"] = {x = -56510.2695, y = 2710.87891, z = 3493.08569, adjacent =
	[
		"OW_ICEDRAGON_01",
	]},
	["OW_ICEDRAGON_02"] = {x = -56078.8477, y = 2624.24487, z = 4344.26025, adjacent =
	[
		"OW_ICEDRAGON_01",
	]},
	["OW_ICEDRAGON_10"] = {x = -56544.3711, y = 2704.63525, z = 5792.69189, adjacent =
	[
		"OW_ICEDRAGON_11",
		"OW_ICEDRAGON_20",
		"OW_ICEDRAGON_22",
	]},
	["OW_ICEDRAGON_13"] = {x = -54529.6484, y = 2639.54053, z = 4395.9707, adjacent =
	[
		"OW_ICEDRAGON_05",
		"OW_ICEDRAGON_12",
	]},
	["OW_ICEDRAGON_22"] = {x = -55945.0977, y = 2655.05688, z = 5769.46582, adjacent =
	[
		"OW_ICEDRAGON_05",
		"OW_ICEDRAGON_09",
		"OW_ICEDRAGON_10",
	]},
	["OW_ICEDRAGON_23"] = {x = -56347.707, y = 2692.37427, z = 6760.06934, adjacent =
	[
		"OW_ICEDRAGON_17",
		"OW_ICEDRAGON_24",
	]},
	["OW_ICEDRAGON_17"] = {x = -57212.1211, y = 2887.28125, z = 6350.479, adjacent =
	[
		"OW_ICEDRAGON_23",
	]},
	["OW_ICEDRAGON_24"] = {x = -55452.7891, y = 2507.69531, z = 7066.98047, adjacent =
	[
		"OW_ICEDRAGON_09",
		"OW_ICEREGION_48",
		"OW_ICEDRAGON_23",
	]},
	["OW_ICEDRAGON_25"] = {x = -53619.2383, y = 2609.44873, z = 4975.02783, adjacent =
	[
		"OW_ICEDRAGON_12",
	]},
	["OW_ICEREGION_50"] = {x = -58018.6016, y = 3239.97607, z = 10214.543, adjacent =
	[
		"OW_ICEREGION_49",
		"OW_ICEREGION_51",
	]},
	["OW_ICEREGION_51"] = {x = -59459.918, y = 3582.00757, z = 10554.8057, adjacent =
	[
		"OW_ICEREGION_50",
		"OW_ICEREGION_52",
		"OW_ICEREGION_54",
	]},
	["OW_ICEREGION_52"] = {x = -60215.1914, y = 3797.58447, z = 10927.5947, adjacent =
	[
		"OW_ICEREGION_51",
		"OW_ICEREGION_53",
	]},
	["OW_ICEREGION_53"] = {x = -60541.1523, y = 3791.29395, z = 10183.2256, adjacent =
	[
		"OW_ICEREGION_52",
	]},
	["OW_ICEREGION_54"] = {x = -59939.8203, y = 3634.08276, z = 9792.80371, adjacent =
	[
		"OW_ICEREGION_51",
	]},
	["OW_ICEREGION_55"] = {x = -55774.6758, y = 2672.78735, z = 10923.8662, adjacent =
	[
		"OW_ICEREGION_49",
		"OW_ICEREGION_56",
		"OW_ICEREGION_57",
		"OW_ICEREGION_81",
	]},
	["OW_ICEREGION_57"] = {x = -56758.2969, y = 2644.28735, z = 11514.4736, adjacent =
	[
		"OW_ICEREGION_55",
		"OW_ICEREGION_58",
		"OW_ICEREGION_81",
	]},
	["OW_ICEREGION_58"] = {x = -56795.1836, y = 2536.02734, z = 12441.3887, adjacent =
	[
		"OW_ICEREGION_57",
		"OW_ICEREGION_59",
		"OW_ICEREGION_79",
	]},
	["OW_ICEREGION_59"] = {x = -58383.6328, y = 2536.02734, z = 12762.1982, adjacent =
	[
		"OW_ICEREGION_78",
		"OW_ICEREGION_58",
		"OW_ICEREGION_60",
	]},
	["OW_ICEREGION_60"] = {x = -59901.7109, y = 2536.02734, z = 13322.8916, adjacent =
	[
		"OW_ICEREGION_62",
		"OW_ICEREGION_59",
		"OW_ICEREGION_61",
	]},
	["OW_ICEREGION_61"] = {x = -61360.8047, y = 2536.02734, z = 12290.2939, adjacent =
	[
		"OW_ICEREGION_60",
	]},
	["OW_ICEREGION_63"] = {x = -60566.7148, y = 2536.02734, z = 15526.8789, adjacent =
	[
		"OW_ICEREGION_64",
	]},
	["OW_ICEREGION_65"] = {x = -57359.7266, y = 2536.0271, z = 15726.9912, adjacent =
	[
		"OW_ICEREGION_78",
		"OW_ICEREGION_64",
		"OW_ICEREGION_66",
	]},
	["OW_ICEREGION_66"] = {x = -56169.4688, y = 2536.02686, z = 16578.3164, adjacent =
	[
		"OW_ICEREGION_65",
		"OW_ICEREGION_67",
		"OW_ICEREGION_79",
		"OW_ICEREGION_82",
	]},
	["OW_ICEREGION_67"] = {x = -54089.0117, y = 2536.02686, z = 16381.9785, adjacent =
	[
		"OW_ICEREGION_66",
		"OW_ICEREGION_69",
	]},
	["OW_ICEREGION_69"] = {x = -51923.3359, y = 2536.02686, z = 16299.5586, adjacent =
	[
		"OW_ICEREGION_67",
		"OW_ICEREGION_68",
		"OW_ICEREGION_70",
	]},
	["OW_ICEREGION_68"] = {x = -52857.9883, y = 2536.02686, z = 18074.9961, adjacent =
	[
		"OW_ICEREGION_69",
	]},
	["OW_ICEREGION_70"] = {x = -50531.2852, y = 2536.0271, z = 14843.2275, adjacent =
	[
		"OW_ICEREGION_69",
		"OW_ICEREGION_71",
		"OW_ICEREGION_96",
		"OW_ICEREGION_99",
	]},
	["OW_ICEREGION_71"] = {x = -50280.7227, y = 2536.02734, z = 12820.5176, adjacent =
	[
		"OW_ICEREGION_93",
		"OW_ICEREGION_70",
		"OW_ICEREGION_73",
	]},
	["OW_ICEREGION_72"] = {x = -50055.6289, y = 2536.02734, z = 11475.1338, adjacent =
	[
		"OW_ICEREGION_93",
		"OW_ICEREGION_44",
		"OW_ICEREGION_73",
	]},
	["OW_ICEREGION_73"] = {x = -50889.2031, y = 2536.02734, z = 12066.8037, adjacent =
	[
		"OW_ICEREGION_44",
		"OW_ICEREGION_71",
		"OW_ICEREGION_72",
	]},
	["OW_ICEREGION_79"] = {x = -56617.0352, y = 2536.0271, z = 14020.8262, adjacent =
	[
		"OW_ICEREGION_58",
		"OW_ICEREGION_66",
	]},
	["OW_ICEREGION_81"] = {x = -57288.9219, y = 2779.09399, z = 10892.4473, adjacent =
	[
		"OW_ICEREGION_55",
		"OW_ICEREGION_57",
	]},
	["OW_ICEREGION_82"] = {x = -56228.6875, y = 2536.02686, z = 17415.8535, adjacent =
	[
		"OW_ICEREGION_66",
		"OW_ICEREGION_83",
	]},
	["OW_ICEREGION_83"] = {x = -57082.6016, y = 2944.07666, z = 18770.5977, adjacent =
	[
		"OW_ICEREGION_82",
		"OW_ICEREGION_84",
	]},
	["OW_ICEREGION_84"] = {x = -58978.9453, y = 3447.25586, z = 19624.9082, adjacent =
	[
		"OW_ICEREGION_87",
		"OW_ICEREGION_83",
	]},
	["OW_ICEREGION_86"] = {x = -60541.2539, y = 3733.21997, z = 19708.1914, adjacent =
	[
		"OW_ICEREGION_87",
		"OW_ICEREGION_85",
	]},
	["OW_ICEREGION_85"] = {x = -60695.7813, y = 3878.62329, z = 20618.5547, adjacent =
	[
		"OW_ICEREGION_86",
	]},
	["OW_ICEREGION_91"] = {x = -57990.3555, y = 3147.03442, z = 16602.8418, adjacent =
	[
		"OW_ICEREGION_90",
	]},
	["OW_ICEREGION_92"] = {x = -57494.0313, y = 3150.01416, z = 17272.7891, adjacent =
	[
		"OW_ICEREGION_90",
	]},
	["OW_ICEREGION_95"] = {x = -49375.2383, y = 2706.39063, z = 12583.6094, adjacent =
	[
		"OW_ICEREGION_40",
	]},
	["OW_ICEREGION_96"] = {x = -49717.3672, y = 2678.50928, z = 15112.1729, adjacent =
	[
		"OW_ICEREGION_70",
		"OW_ICEREGION_97",
	]},
	["OW_ICEREGION_97"] = {x = -50042.625, y = 2949.8501, z = 16445.1055, adjacent =
	[
		"OW_ICEREGION_96",
		"OW_ICEREGION_98",
	]},
	["OW_ICEREGION_98"] = {x = -49806.6055, y = 3150.17969, z = 17252.0234, adjacent =
	[
		"OW_ICEREGION_97",
	]},
	["WP_INTRO25"] = {x = 9553.60254, y = 6124.09619, z = 37257.3672, adjacent =
	[
		"WP_INTRO18",
		"WP_INTRO26",
	]},
	["WP_INTRO26"] = {x = 9576.9043, y = 6278.82324, z = 37681.3828, adjacent =
	[
		"WP_INTRO19",
		"WP_INTRO25",
	]},
	["OW_DJG_SWAMPCAMP_01"] = {x = -14183.9609, y = -1841.39795, z = -1072.86865, adjacent =
	[
		"HELPPOINT9",
		"OW_DJG_SWAMPCAMP_02",
	]},
	["OW_DJG_SWAMPCAMP_02"] = {x = -14068.3838, y = -1840.82056, z = -1308.12415, adjacent =
	[
		"OW_DJG_SWAMPCAMP_01",
	]},
	["OW_DJG_SWAMP_WAIT1_01"] = {x = -16324.2314, y = -1549.6897, z = -3106.76636, adjacent =
	[
		"OW_PATH_046",
	]},
	["OW_DJG_SWAMP_WAIT1_02"] = {x = -16089.7314, y = -1525.21509, z = -3210.48853, adjacent =
	[
		"OW_PATH_046",
	]},
	["OW_DJG_SWAMP_WAIT2_03"] = {x = -19625.9863, y = -1364.78149, z = -9571.95898, adjacent =
	[
		"OW_DRAGONSWAMP_006",
		"OW_DRAGONSWAMP_005",
		"OW_DRAGONSWAMP_020",
	]},
	["OW_DJG_ROCKCAMP_03"] = {x = 21833.7832, y = 4463.36963, z = -23060.3398, adjacent =
	[
		"PATH_TO_PLATEAU03",
		"OW_DJG_ROCKCAMP_02",
	]},
	["OW_DJG_ROCKCAMP_02"] = {x = 21086.666, y = 4514.37305, z = -23264.1973, adjacent =
	[
		"OW_DJG_ROCKCAMP_03",
		"OW_DJG_ROCKCAMP_01",
	]},
	["OW_DJG_ROCKCAMP_01"] = {x = 20624.8047, y = 4468.11816, z = -23929.543, adjacent =
	[
		"OW_DJG_ROCKCAMP_02",
		"ROCKCAMP",
	]},
	["OC3"] = {x = -10012.166, y = -771.830688, z = 3116.20093, adjacent =
	[
		"OC1",
		"OC_ROUND_21",
		"OC4",
		"OC_PATH_04",
	]},
	["OC4"] = {x = -9995.11426, y = -688.99707, z = 565.868164, adjacent =
	[
		"OC_ROUND_20",
		"OC3",
		"OC5",
	]},
	["OC5"] = {x = -9890.45605, y = -642.42041, z = -5072.72852, adjacent =
	[
		"SPAWN_OW_SCAVENGER_AL_ORC",
		"OC4",
		"OC_ORK_BACK_CAMP_13",
		"OC_ORK_BACK_CAMP_15",
	]},
	["OC6"] = {x = -9186.57227, y = -644.016724, z = -6386.97021, adjacent =
	[
		"OC7",
		"OC_ROUND_18",
		"OC_ORK_BACK_CAMP_10",
		"OC_ORK_BACK_CAMP_11",
		"OC_ORK_BACK_CAMP_13",
	]},
	["OC8"] = {x = -3644.03345, y = -956.498047, z = -9065.97559, adjacent =
	[
		"OC_ROUND_15",
		"OC8_NAVIGATION2",
		"OC_ORK_BACK_CAMP_06",
		"OC8_NAVIGATION",
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT11",
	]},
	["OC9"] = {x = 3071.73364, y = -824.078064, z = -8331.87012, adjacent =
	[
		"OC_ROUND_13",
		"OC2",
		"OC_ORK_BACK_CAMP_03",
		"OC_ORK_BACK_CAMP_16",
		"OC_ORK_BACK_CAMP_02",
	]},
	["OC10"] = {x = 7418.15771, y = -629.836243, z = -5967.97217, adjacent =
	[
		"OC_ROUND_27",
		"OC2",
		"OC_ORK_BACK_CAMP_01",
		"OC11_OC12",
	]},
	["OC11"] = {x = 8817.94043, y = -778.333313, z = -1448.30518, adjacent =
	[
		"OC_ROUND_10",
		"OC_ORK_BETWEEN_CAMPS_02",
		"OC_ORK_LITTLE_CAMP_04",
		"OC11_OC12",
	]},
	["OC12"] = {x = 6992.33643, y = -1055.3385, z = 6982.45947, adjacent =
	[
		"O_SCAVENGER_OCWOODL2_MOVEMENT2",
		"OC_ORK_LITTLE_CAMP_03",
		"OC_ORK_LITTLE_CAMP_01",
	]},
	["OC13"] = {x = 2221.33716, y = -1068.99902, z = 7447.56592, adjacent =
	[
		"SPAWN_O_SCAVENGER_05_02",
		"OC13_NAVIGATION",
		"OC_ORK_LITTLE_CAMP_01",
		"OC_ORK_BETWEEN_CAMPS_01",
	]},
	["OC14"] = {x = -4272.49756, y = -1124.0293, z = 7338.27002, adjacent =
	[
		"OC_ROUND_22_CF_2",
		"OC_ORK_MAIN_CAMP_01",
		"OC14_MOVEMENT",
		"OC_ORK_MAIN_CAMP_06",
	]},
	["OW_ICEREGION_ENTRANCE_ICEGOLEM_02"] = {x = -38908.1797, y = 922.313782, z = 5245.01514, adjacent =
	[
		"OW_ICEREGION_ENTRANCE_01",
	]},
	["OW_ICEREGION_ENTRANCE_01"] = {x = -39133.7734, y = 929.150146, z = 5273.10107, adjacent =
	[
		"OW_PATH_064",
		"OW_ICEREGION_ENTRANCE",
		"OW_PATH_065",
		"OW_ICEREGION_ENTRANCE_ICEGOLEM_02",
		"OW_ICEREGION_ENTRANCE_ICEGOLEM_01",
	]},
	["OW_ICEREGION_ENTRANCE_ICEGOLEM_01"] = {x = -39348.0742, y = 959.519165, z = 5054.9668, adjacent =
	[
		"OW_ICEREGION_ENTRANCE_01",
	]},
	["OW_DJG_ICEREGION_WAIT2_01"] = {x = -53449.7734, y = 2626.49976, z = 9329.59473, adjacent =
	[
		"OW_ICEREGION_46",
	]},
	["OW_DJG_ICEREGION_WAIT2_02"] = {x = -53817.2148, y = 2624.04053, z = 9479.04395, adjacent =
	[
		"OW_ICEREGION_46",
	]},
	["OW_ICEREGION_99"] = {x = -53055.6406, y = 3391.84961, z = 13132.9365, adjacent =
	[
		"OW_ICEREGION_70",
		"OW_ICEREGION_17",
	]},
	["OW_ICEREGION_17"] = {x = -53617.4766, y = 3497.13892, z = 13842.7725, adjacent =
	[
		"OW_ICEREGION_99",
	]},
	["OW_DRAGONSWAMP_013"] = {x = -24291.6035, y = -1364.76575, z = -9868.65527, adjacent =
	[
		"OW_DRAGONSWAMP_008",
	]},
	["OW_DRAGONSWAMP_009"] = {x = -24934.5566, y = -1364.69348, z = -11610.4639, adjacent =
	[
		"OW_DRAGONSWAMP_008",
	]},
	["OW_NEWMINE_03"] = {x = -15052.5215, y = 809.257141, z = -26022.4414, adjacent =
	[
		"OW_NEWMINE_07",
		"OW_NEWMINE_04",
		"OW_NEWMINE_02",
		"OW_NEWMINE_03_B",
	]},
	["OW_NEWMINE_02"] = {x = -15554.5645, y = 799.169617, z = -26166.8848, adjacent =
	[
		"OW_NEWMINE_04",
		"OW_NEWMINE_01",
		"OW_NEWMINE_03",
	]},
	["OW_NEWMINE_09"] = {x = -14455, y = 1057.87659, z = -24467, adjacent =
	[
		"OW_NEWMINE_06",
		"OW_NEWMINE_11",
		"OW_NEWMINE_09_B",
	]},
	["OW_NEWMINE_06"] = {x = -14788.8291, y = 1009.1225, z = -24546.418, adjacent =
	[
		"OW_NEWMINE_09",
		"OW_NEWMINE_03_B",
		"OW_NEWMINE_SAW",
	]},
	["OW_NEWMINE_11"] = {x = -14481.7227, y = 895.222656, z = -25306.2012, adjacent =
	[
		"OW_NEWMINE_07",
		"OW_NEWMINE_09",
		"OW_NEWMINE_03_B",
	]},
	["MT13"] = {x = 5633.19287, y = 5300.00488, z = -33104.3242, adjacent =
	[
		"MT08",
		"MT14",
	]},
	["MT14"] = {x = 6442.27734, y = 5439.47021, z = -30968.8809, adjacent =
	[
		"MT13",
		"MT15",
	]},
	["MT15"] = {x = 7575.82178, y = 5739.17334, z = -29982.418, adjacent =
	[
		"MT14",
		"MT16",
	]},
	["MT16"] = {x = 7073.44727, y = 5477.91895, z = -28208.8555, adjacent =
	[
		"MT15",
	]},
	["OW_DT_BLOODFLY_01"] = {x = -16181.8838, y = 2929.13428, z = -41216.6289, adjacent =
	[
		"OW_PATH_134",
	]},
	["OW_ORCBARRIER_08_01"] = {x = 14682.1504, y = 1357.7821, z = -11522.793, adjacent =
	[
		"OW_ORCBARRIER_08",
		"OW_ORCBARRIER_06",
		"OW_ORCBARRIER_08_02",
	]},
	["OW_ORCBARRIER_04_01"] = {x = 17111.4746, y = 1660.00098, z = -14081.8545, adjacent =
	[
		"OW_ORCBARRIER_05",
		"OW_ORCBARRIER_04",
	]},
	["OW_ORCBARRIER_08_02"] = {x = 15250.6758, y = 1487.46765, z = -10915.9482, adjacent =
	[
		"OW_ORCBARRIER_08_01",
	]},
	["CASTLE_5_1"] = {x = -2241.44922, y = 3066.72632, z = -17678.0762, adjacent =
	[
		"CASTLE_5",
	]},
	["CASTLE_8_1"] = {x = 8893.20898, y = 4785.57764, z = -17708.1387, adjacent =
	[
		"CASTLE_8",
	]},
	["OW_PATH_OC_NC2_TO_TOWER_02"] = {x = -13475.5781, y = -1209.84338, z = -2379.81177, adjacent =
	[
		"OW_PATH_OC_NC2_TO_TOWER_01",
		"OW_PATH_OC_NC2_TO_TOWER_03",
	]},
	["OW_PATH_OC_NC2_TO_TOWER_01"] = {x = -12746.8965, y = -1411.08264, z = -3026.97852, adjacent =
	[
		"OW_PATH_OC_NC3",
		"OW_PATH_OC_NC2",
		"OW_PATH_OC_NC2_TO_TOWER_02",
	]},
	["OW_PATH_OC_NC2_TO_TOWER_04"] = {x = -14830.3477, y = -892.173828, z = -2027.65857, adjacent =
	[
		"OW_PATH_OC_NC2_TO_TOWER_03",
		"OW_PATH_OC_NC2_TO_TOWER_07",
	]},
	["OW_PATH_OC_NC2_TO_TOWER_03"] = {x = -14001.21, y = -1061.65747, z = -1828.38745, adjacent =
	[
		"OW_PATH_OC_NC2_TO_TOWER_02",
		"OW_PATH_OC_NC2_TO_TOWER_04",
	]},
	["OW_PATH_OC_NC2_TO_TOWER_06"] = {x = -17089.6563, y = -656.490295, z = -1448.18945, adjacent =
	[
		"OW_PATH_OC_NC2_TO_TOWER_05",
		"OW_TOWER_JERGAN",
	]},
	["OW_PATH_OC_NC2_TO_TOWER_05"] = {x = -16790.6582, y = -788.10907, z = -1841.66992, adjacent =
	[
		"OW_PATH_OC_NC2_TO_TOWER_06",
		"OW_PATH_OC_NC2_TO_TOWER_07",
	]},
	["OW_TOWER_JERGAN"] = {x = -17513.7324, y = -656.490295, z = -1266.45886, adjacent =
	[
		"OW_PATH_OC_NC2_TO_TOWER_06",
	]},
	["OW_PATH_OC_NC2_TO_TOWER_07"] = {x = -15910.1699, y = -864.130188, z = -1958.21118, adjacent =
	[
		"OW_PATH_OC_NC2_TO_TOWER_04",
		"OW_PATH_OC_NC2_TO_TOWER_05",
	]},
	["OW_DJG_VORPOSTEN_01"] = {x = -29540.541, y = -727.103027, z = 9783.35742, adjacent =
	[
		"SPAWN_OW_BLACKGOBBO_A1",
	]},
	["VORPOSTEN"] = {x = -29177.2578, y = -725.604797, z = 10158.0205, adjacent =
	[
		"SPAWN_OW_BLACKGOBBO_A1",
	]},
	["ICECAMP"] = {x = -34601.5508, y = 312.012451, z = 2407.61182, adjacent =
	[
		"OW_DJG_ICECAMP_04",
	]},
	["ICEDRAGON"] = {x = -54367.7266, y = 2438.86328, z = 8432.99023, adjacent =
	[
		"OW_ICEREGION_47",
	]},
	["SWAMPDRAGON"] = {x = -21323.7305, y = -1414.77014, z = -9791.72559, adjacent =
	[
		"OW_DRAGONSWAMP_006",
	]},
	["NEWMINE"] = {x = -13756.417, y = 1613.98877, z = -22373.3379, adjacent =
	[
		"OW_ORC_LOOKOUT_2_05",
	]},
	["DT"] = {x = -11718.0918, y = 2078.0542, z = -35421.7813, adjacent =
	[
		"LOCATION_03_OUT",
	]},
	["ORKS"] = {x = 7238.83154, y = -31.8556061, z = -10343.6631, adjacent =
	[
		"OW_PATH_001",
	]},
	["FIREDRAGON"] = {x = 4116.01465, y = 7119.70605, z = -18798.6895, adjacent =
	[
		"CASTLE_22",
	]},
	["ROCKCAMP"] = {x = 20420.498, y = 4418.51758, z = -23936.4648, adjacent =
	[
		"OW_DJG_ROCKCAMP_01",
	]},
	["ROCKDRAGON"] = {x = 18359.3984, y = 5233.75977, z = -33634.6133, adjacent =
	[
		"PATH_TO_PLATEAU04_BRIDGE3",
	]},
	["SWAMPCAMP"] = {x = -14448.7314, y = -1864.16943, z = -618.244873, adjacent =
	[
		"HELPPOINT9",
	]},
	["CAVALORN"] = {x = -15165.0244, y = -1348.03418, z = 2821.61108, adjacent =
	[
		"OW_SAWHUT_GREENGOBBO_SPAWN",
	]},
	["STONEHENGE"] = {x = -34495.2813, y = 2897.78809, z = -11889, adjacent =
	[
		"STONES",
	]},
	["OW_UNDEAD_DUNGEON_01"] = {x = -34554.3242, y = 2360.65576, z = -13725.6094, adjacent =
	[
		"OW_UNDEAD_DUNGEON_ENTRANCE",
		"OW_UNDEAD_DUNGEON_02",
	]},
	["OW_UNDEAD_DUNGEON_02"] = {x = -34673.7266, y = 2333.7749, z = -14600.0977, adjacent =
	[
		"OW_UNDEAD_DUNGEON_01",
		"OW_UNDEAD_DUNGEON_03",
	]},
	["OW_UNDEAD_DUNGEON_03"] = {x = -34981.4414, y = 2340.66016, z = -15206.0566, adjacent =
	[
		"OW_UNDEAD_DUNGEON_02",
	]},
	["LOCATION_19_03_ROOM6_BARRELCHAMBER2"] = {x = 20368.6133, y = 7876.24365, z = -40249.1484, adjacent =
	[
		"LOCATION_19_03_ROOM6_BARRELCHAMBER3",
	]},
	["LOCATION_19_03_ROOM6_BARRELCHAMBER3"] = {x = 21619.5938, y = 7876.24365, z = -39764.4336, adjacent =
	[
		"LOCATION_19_03_ROOM6_BARRELCHAMBER",
		"LOCATION_19_03_ROOM6_BARRELCHAMBER2",
	]},
	["MOVEMENT_TALL_PATH_01"] = {x = -17080.6582, y = -359.610138, z = 7078.86865, adjacent =
	[
		"MOVEMENT_TALL_PATH_BANDITOS2",
		"MOVEMENT_TALL_PATH_02",
	]},
	["MOVEMENT_TALL_PATH_02"] = {x = -15966.4932, y = -373.461243, z = 7771.7251, adjacent =
	[
		"MOVEMENT_TALL_PATH_01",
		"MOVEMENT_TALL_PATH_03",
	]},
	["MOVEMENT_TALL_PATH_03"] = {x = -14683.7256, y = -583.339111, z = 7475.3916, adjacent =
	[
		"SPAWN_OW_SCAVENGER_BANDIT_02",
		"MOVEMENT_TALL_PATH_02",
	]},
	["OW_PATH_WARAN05_SPAWN02_01"] = {x = 3466.98364, y = -1451.20032, z = 11310.0576, adjacent =
	[
		"OW_PATH_WARAN05_SPAWN02",
		"OW_PATH_WARAN05_SPAWN02_02",
	]},
	["OW_PATH_WARAN05_SPAWN02_02"] = {x = 2402.2019, y = -1369.646, z = 12295.2139, adjacent =
	[
		"OW_PATH_WARAN05_SPAWN02_01",
		"OW_PATH_WARAN05_SPAWN02_03",
	]},
	["OW_PATH_WARAN05_SPAWN02_03"] = {x = 446.92981, y = -1428.93604, z = 12882.3496, adjacent =
	[
		"OW_PATH_WARAN05_SPAWN02_02",
		"OW_PATH_WARAN05_SPAWN02_04",
	]},
	["OW_PATH_WARAN05_SPAWN02_04"] = {x = -1391.23425, y = -1438.43237, z = 12885.4834, adjacent =
	[
		"MOVEMENT_OW_BLOODFLY_E_4",
		"OW_PATH_WARAN05_SPAWN02_03",
	]},
	["OW_PATH_148_A"] = {x = -28814.2305, y = -880.930237, z = 20626.0605, adjacent =
	[
		"OW_PATH_148",
		"OW_PATH_266",
	]},
	["OW_SPAWN_TRACK_LEICHE_01"] = {x = -31030.2656, y = 746.869995, z = 4400.31006, adjacent =
	[
		"PATH_AROUND_HILL03",
	]},
	["OW_SPAWN_TRACK_LEICHE_00"] = {x = -31877.6445, y = 797.864746, z = 11664.0605, adjacent =
	[
		"OW_PATH_151",
	]},
	["OW_GATE_ORCS_01"] = {x = -12392.2764, y = 201.560562, z = -9173.02148, adjacent =
	[
		"OW_GATE_ORCS_02",
		"SPAWN_PATH_GUARD1",
		"OW_GATE_ORCS_03",
	]},
	["OW_GATE_ORCS_03"] = {x = -12635.6006, y = 288.433838, z = -9779.96973, adjacent =
	[
		"OW_PATH_272",
		"OW_GATE_ORCS_01",
	]},
	["OW_SAWHUT_MOLERAT_SPAWN01"] = {x = -17023.9395, y = -1222.66724, z = 6378.26709, adjacent =
	[
		"OW_STAND_MARCOS",
	]},
	["OW_STAND_MARCOS"] = {x = -16672.4453, y = -1246.66589, z = 6177.99756, adjacent =
	[
		"OW_SAWHUT_MOLERAT_SPAWN01",
		"OW_STAND_GUARDS",
	]},
	["OW_SPAWN_TRACK_LEICHE_03"] = {x = -16633.6777, y = -1277.552, z = 3251.23682, adjacent =
	[
		"PATH_OC_NC_6_1",
	]},
	["OW_SPAWN_TRACK_LEICHE_04"] = {x = -18892.8184, y = -896.748596, z = 1281.09229, adjacent =
	[
		"PATH_OC_NC_7",
	]},
	["OW_MINE2_STAND_JERGAN"] = {x = -27398, y = -715.546509, z = 20842, adjacent =
	[
		"OW_PATH_266",
	]},
	["OW_MINE3_LEICHE_06"] = {x = 362, y = 2564.35767, z = -27696, adjacent =
	[
		"OW_MINE3_04",
	]},
	["OW_MINE3_LEICHE_08"] = {x = -2004.00012, y = 2724.87817, z = -27728, adjacent =
	[
		"OW_MINE3_01",
	]},
	["OW_NEWMINE_09_B"] = {x = -14267.793, y = 1204.38843, z = -23987.748, adjacent =
	[
		"OW_NEWMINE_10",
		"OW_NEWMINE_09",
	]},
	["OW_NEWMINE_03_B"] = {x = -14892.6895, y = 933.538086, z = -25045.1211, adjacent =
	[
		"OW_NEWMINE_03",
		"OW_NEWMINE_06",
		"OW_NEWMINE_11",
		"OW_NEWMINE_SAW",
	]},
	["OW_NEWMINE_SAW"] = {x = -15233.9199, y = 908.773132, z = -24811.6621, adjacent =
	[
		"OW_NEWMINE_06",
		"OW_NEWMINE_03_B",
	]},
	["OW_STAND_GUARDS"] = {x = -16232.916, y = -1270.48047, z = 5953.83838, adjacent =
	[
		"OW_SAWHUT_MOLERAT_MOVEMENT",
		"OW_STAND_MARCOS",
	]},
	["OW_RITTER_LEICHE_01"] = {x = -10751.5654, y = 1096.10193, z = -12666.7568, adjacent =
	[
		"LOCATION_02_02",
	]},
	["OW_RITTER_LEICHE_02"] = {x = -11094.0234, y = 974.604919, z = -13086.5908, adjacent =
	[
		"LOCATION_02_01",
	]},
	["OC_RAMP_11"] = {x = 4948.8335, y = -1151.78772, z = -1892.97034, adjacent =
	[
		"OC_RAMP_19",
		"OC_RAMP_10",
	]},
	["OC_RAMP_19"] = {x = 4854.00244, y = -1102.40625, z = -1416.02686, adjacent =
	[
		"OC_RAMP_11",
	]},
	["OC_RAMP_14"] = {x = 5152.50244, y = -1082.57532, z = -454.307861, adjacent =
	[
		"OC_RAMP_20",
		"OC_RAMP_13",
	]},
	["OC_RAMP_20"] = {x = 4945.84961, y = -1075.72119, z = -847.896545, adjacent =
	[
		"OC_RAMP_14",
	]},
	["OC_RAMP_16"] = {x = 3525.60449, y = 363.194672, z = -1058.84912, adjacent =
	[
		"OC_RAMP_17",
		"OC_RAMP_01",
	]},
	["OC_RAMP_17"] = {x = 3291.56177, y = 272.125916, z = -972.629517, adjacent =
	[
		"OC_RAMP_16",
		"OC_CENTER_GUARD_02",
	]},
	["OC_CENTER_GUARD_02"] = {x = 3394.17603, y = 316.763794, z = -789.345032, adjacent =
	[
		"OC_RAMP_17",
		"OC_CENTER_GUARD_03",
		"OC_CENTER_GUARD_01",
	]},
	["OC_RAMP_01"] = {x = 3809.27905, y = 45.804184, z = -1153.65918, adjacent =
	[
		"OC_RAMP_16",
		"OC_RAMP_02",
	]},
	["OC_RAMP_13"] = {x = 5703.72607, y = -938.467651, z = -541.613586, adjacent =
	[
		"OC_RAMP_14",
		"OC_RAMP_12",
	]},
	["OC_RAMP_12"] = {x = 6316.10498, y = -904.262573, z = -1008.22241, adjacent =
	[
		"OC_RAMP_13",
		"OC_RAMP_09",
	]},
	["OC_RAMP_09"] = {x = 6276.2085, y = -933.859131, z = -1670.00708, adjacent =
	[
		"OC_RAMP_12",
		"OC_RAMP_10",
		"OC_ORK_BETWEEN_CAMPS_02",
		"OC_RAMP_08",
	]},
	["OC_RAMP_10"] = {x = 5682.86279, y = -1083.30017, z = -1910.26575, adjacent =
	[
		"OC_RAMP_11",
		"OC_RAMP_09",
	]},
	["OC_ORK_BETWEEN_CAMPS_02"] = {x = 7640.11279, y = -761.40863, z = -1348.13928, adjacent =
	[
		"OC11",
		"OC_RAMP_09",
		"OC_ORK_BETWEEN_CAMPS_02_MOVEMENT",
		"OC_ORK_BETWEEN_CAMPS_02_MOVEMENT2",
	]},
	["OC_RAMP_08"] = {x = 5840.56738, y = -970.918091, z = -1508.8811, adjacent =
	[
		"OC_RAMP_09",
		"OC_RAMP_07",
	]},
	["OC_RAMP_07"] = {x = 5667.79785, y = -903.836548, z = -1436.37854, adjacent =
	[
		"OC_RAMP_08",
		"OC_RAMP_06",
	]},
	["OC_RAMP_06"] = {x = 5541.18408, y = -849.12677, z = -1381.38245, adjacent =
	[
		"OC_RAMP_07",
		"OC_RAMP_05",
	]},
	["OC_RAMP_05"] = {x = 4132.91992, y = -247.138641, z = -871.868713, adjacent =
	[
		"OC_RAMP_06",
		"OC_RAMP_04",
	]},
	["OC_RAMP_04"] = {x = 3986.07446, y = -207.521942, z = -840.593262, adjacent =
	[
		"OC_RAMP_05",
		"OC_RAMP_03",
	]},
	["OC_RAMP_03"] = {x = 3796.08838, y = -195.035919, z = -865.257935, adjacent =
	[
		"OC_RAMP_04",
		"OC_RAMP_02",
	]},
	["OC_RAMP_02"] = {x = 3775.9895, y = 48.5826187, z = -974.707703, adjacent =
	[
		"OC_RAMP_01",
		"OC_RAMP_03",
	]},
	["OC_CENTER_05"] = {x = 2032.18213, y = 199.886063, z = -706.493469, adjacent =
	[
		"OC_CENTER_05_B",
		"OC_STORE_ENTRANCE",
		"OC_MAGE_ENTRANCE",
		"OC_CAMPFIRE_BARBQ",
		"OC_CAMPFIRE_OUT_01",
		"OC_TRAIN_01",
		"OC_TRAIN_02",
		"OC_CENTER_GUARD_01",
	]},
	["OC_CENTER_05_B"] = {x = 1780.8103, y = 199.75769, z = -489.602722, adjacent =
	[
		"OC_CENTER_05",
	]},
	["OC_STAND_TANDOR_02"] = {x = 1753.06189, y = 199.847687, z = 137.651947, adjacent =
	[
		"OC_STAND_TANDOR_01",
		"OC_STORE_ENTRANCE",
		"OC_SMITH_01",
	]},
	["OC_STAND_TANDOR_01"] = {x = 1735.1449, y = 199.917999, z = 358.992462, adjacent =
	[
		"OC_STAND_TANDOR_02",
	]},
	["OC_STORE_ENTRANCE"] = {x = 2433.55566, y = 200.00705, z = 186.473206, adjacent =
	[
		"OC_CENTER_05",
		"OC_STAND_TANDOR_02",
		"OC_CENTER_04",
		"OC_GUARD_STORE_01",
		"OC_STORE_IN",
		"OC_TRAIN_02",
	]},
	["OC_SMITH_01"] = {x = 1021.26831, y = 199.707825, z = 368.337097, adjacent =
	[
		"OC_STAND_TANDOR_02",
		"OC_SMITH_COOL",
		"OC_CENTER_04",
		"OC_SMITH_ANVIL",
		"OC_GUARD_OUT_01",
	]},
	["OC_PATH_02"] = {x = -6130.86865, y = -1044.36243, z = 3616.28809, adjacent =
	[
		"OC_ORK_MAIN_CAMP_12",
		"OC_ORK_MAIN_CAMP_02",
		"OC_PATH_01",
		"OC_PATH_03",
		"OC_PATH_04",
	]},
	["OC_ORK_MAIN_CAMP_12"] = {x = -5036.58154, y = -882.663635, z = 4461.59619, adjacent =
	[
		"OC_PATH_02",
		"OC_ORK_MAIN_CAMP_02",
		"OC_PATH_01",
	]},
	["OC_ORK_BACK_CAMP_03"] = {x = -339.279999, y = -1015.86731, z = -8668.56836, adjacent =
	[
		"OC9",
		"OC8_NAVIGATION2",
		"OC_ORK_BACK_CAMP_16",
		"OC_ORK_BACK_CAMP_04",
		"OC_ORK_BACK_CAMP_05",
	]},
	["OC8_NAVIGATION2"] = {x = -2341.3667, y = -1044.83301, z = -8959.14746, adjacent =
	[
		"OC8",
		"OC_ORK_BACK_CAMP_03",
	]},
	["OC13_NAVIGATION"] = {x = 949.319641, y = -1150.95862, z = 7209.05469, adjacent =
	[
		"OC13",
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT",
		"OC_ORK_MAIN_CAMP_08",
	]},
	["OC_ORK_BETWEEN_CAMPS_01_MOVEMENT"] = {x = 1374.19983, y = -764.715942, z = 5469.51807, adjacent =
	[
		"OC13_NAVIGATION",
		"OC_ORK_BETWEEN_CAMPS_01",
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT2",
	]},
	["OC_ORK_MAIN_CAMP_08"] = {x = -517.057678, y = -1229.59155, z = 7135.34814, adjacent =
	[
		"OC13_NAVIGATION",
		"OC_ORK_MAIN_CAMP_07",
		"OC_ORK_MAIN_CAMP_04",
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT3",
	]},
	["OC_ORK_LITTLE_CAMP_02"] = {x = 5411.81055, y = -956.533325, z = 4570.38135, adjacent =
	[
		"OC_ORK_LITTLE_CAMP_03_NAVIGATION",
		"OC_ORK_LITTLE_CAMP_06",
		"OC_ORK_LITTLE_CAMP_01",
	]},
	["OC_ORK_LITTLE_CAMP_03_NAVIGATION"] = {x = 7002.24951, y = -789.907532, z = 5077.31543, adjacent =
	[
		"OC_ORK_LITTLE_CAMP_02",
		"OC_ORK_LITTLE_CAMP_03",
	]},
	["OC_ORK_LITTLE_CAMP_03"] = {x = 8139.89209, y = -790.749817, z = 4911.46582, adjacent =
	[
		"OC12",
		"OC_ORK_LITTLE_CAMP_03_NAVIGATION",
		"OC_ORK_LITTLE_CAMP_04",
	]},
	["OC_ORK_BACK_CAMP_06"] = {x = -4185.33545, y = -769.792908, z = -7697.73975, adjacent =
	[
		"OC8",
		"OC8_NAVIGATION",
		"OC_ORK_BACK_CAMP_07_NAVIGATION2",
		"OC_ORK_BACK_CAMP_09",
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT11",
	]},
	["OC8_NAVIGATION"] = {x = -4347.45654, y = -844.183105, z = -8414.45898, adjacent =
	[
		"OC8",
		"OC_ORK_BACK_CAMP_06",
		"OC7_NAVIGATION2",
	]},
	["OC7_NAVIGATION2"] = {x = -5051.90088, y = -794.878479, z = -8159.35889, adjacent =
	[
		"OC8_NAVIGATION",
		"OC7_NAVIGATION",
		"OC_ORK_BACK_CAMP_07_NAVIGATION2",
	]},
	["OC7_NAVIGATION"] = {x = -5656.62646, y = -757.688721, z = -7976.74902, adjacent =
	[
		"OC7",
		"OC7_NAVIGATION2",
		"OC_ORK_BACK_CAMP_07_NAVIGATION",
	]},
	["OC_ORK_BACK_CAMP_07_NAVIGATION2"] = {x = -4892.71924, y = -744.586609, z = -7607.12158, adjacent =
	[
		"OC_ORK_BACK_CAMP_06",
		"OC7_NAVIGATION2",
		"OC_ORK_BACK_CAMP_07_NAVIGATION",
	]},
	["OC_ORK_BACK_CAMP_07_NAVIGATION"] = {x = -5366.31396, y = -738.705444, z = -7373.72217, adjacent =
	[
		"OC7_NAVIGATION",
		"OC_ORK_BACK_CAMP_07_NAVIGATION2",
		"OC_ORK_BACK_CAMP_07",
	]},
	["OC_ORK_BACK_CAMP_07"] = {x = -5786.3877, y = -712.669861, z = -7263.49365, adjacent =
	[
		"OC7",
		"OC_ORK_BACK_CAMP_07_NAVIGATION",
		"OC_ORK_BACK_CAMP_08",
	]},
	["OC_ORK_MAIN_CAMP_01"] = {x = -4696.43213, y = -1064.19531, z = 6390.80957, adjacent =
	[
		"OC14",
		"OC14_MOVEMENT",
		"OC_ORK_MAIN_CAMP_02",
	]},
	["OC14_MOVEMENT"] = {x = -5023.11182, y = -1132.01599, z = 6936.26318, adjacent =
	[
		"OC14",
		"OC_ORK_MAIN_CAMP_01",
		"OC_ORK_MAIN_CAMP_03",
	]},
	["OC_ORK_MAIN_CAMP_03"] = {x = -5869.85889, y = -1186.88403, z = 6625.38281, adjacent =
	[
		"OC1",
		"OC14_MOVEMENT",
		"OC_ORK_MAIN_CAMP_03_MOVEMENT",
	]},
	["OC_ORK_MAIN_CAMP_02"] = {x = -5328.53467, y = -1046.84448, z = 5750.72754, adjacent =
	[
		"OC_PATH_02",
		"OC_ORK_MAIN_CAMP_12",
		"OC_ORK_MAIN_CAMP_01",
		"OC_ORK_MAIN_CAMP_03_MOVEMENT",
		"OC_ORK_MAIN_CAMP_11",
	]},
	["OC_ORK_MAIN_CAMP_03_MOVEMENT"] = {x = -5737.31006, y = -1188.93848, z = 5978.6543, adjacent =
	[
		"OC_ORK_MAIN_CAMP_03",
		"OC_ORK_MAIN_CAMP_02",
	]},
	["OC_SMITH_COOL"] = {x = 1044.70068, y = 199.725922, z = 668.072754, adjacent =
	[
		"OC_SMITH_01",
		"OC_SMITH_ANVIL",
		"OC_SMITH_SHARP",
		"OC_SMITH_FIRE",
	]},
	["OC_GATE_GUARD_02"] = {x = 187.570221, y = 199.883896, z = -1196.51831, adjacent =
	[
		"OC_TO_MAGE",
		"OC_MAGE_ENTRANCE",
	]},
	["OC_TO_MAGE"] = {x = -375.832642, y = 151.121078, z = -648.35022, adjacent =
	[
		"OC_GATE_GUARD_02",
		"OC_MAGE_ENTRANCE",
		"OC_GUARD_ENTRANCE",
		"OC_CENTER_04",
		"OC_EBR_ENTRANCE",
	]},
	["OC_MAGE_ENTRANCE"] = {x = 1035.27209, y = 257.978333, z = -1178.30322, adjacent =
	[
		"OC_CENTER_05",
		"OC_GATE_GUARD_02",
		"OC_TO_MAGE",
		"OC_GATE_GUARD_03",
		"OC_MAGE_IN",
		"OC_CENTER_04",
		"OC_TRAIN_04",
	]},
	["OC_GATE_GUARD_03"] = {x = 1582.07776, y = 199.74968, z = -1215.58032, adjacent =
	[
		"OC_MAGE_ENTRANCE",
	]},
	["OC_CENTER_GUARD_03"] = {x = 3655.41919, y = 411.015442, z = -521.56842, adjacent =
	[
		"OC_CENTER_GUARD_02",
	]},
	["OC_GUARD_ENTRANCE"] = {x = -452.704895, y = 184.891678, z = 546.551147, adjacent =
	[
		"OC_TO_MAGE",
		"OC_GATE_GUARD_01",
		"OC_GUARD_IN_01",
		"OC_CENTER_04",
		"OC_TO_GUARD",
		"OC_GUARD_OUT_01",
	]},
	["OC_GATE_GUARD_01"] = {x = -767.52533, y = 199.50705, z = 2.63988495, adjacent =
	[
		"OC_GUARD_ENTRANCE",
	]},
	["OC_KNECHTCAMP_02"] = {x = -3695.49341, y = -272.522369, z = -2243.60938, adjacent =
	[
		"OC_KITCHEN_ENTRANCE",
		"OC_KNECHTCAMP_01",
	]},
	["OC_KITCHEN_ENTRANCE"] = {x = -4323.53613, y = -347.44043, z = -2657.40503, adjacent =
	[
		"OC_KNECHTCAMP_02",
		"OC_KNECHTCAMP_01",
		"OC_PRISON_TO_KITCHEN",
		"OC_FOLTER_IN",
		"OC_WALL_03",
		"OC_BRUTUS_MEATBUGS",
	]},
	["OC_KNECHTCAMP_01"] = {x = -3976.36694, y = -221.419434, z = -1653.06555, adjacent =
	[
		"OC_KNECHTCAMP_02",
		"OC_KITCHEN_ENTRANCE",
		"OC_CENTER_03",
		"OC_PRISON_TO_KITCHEN",
	]},
	["OC_CENTER_03"] = {x = -3522.50098, y = -168.901489, z = -474.529053, adjacent =
	[
		"OC_KNECHTCAMP_01",
		"OC_PRISON_ENTRANCE",
		"OC_TO_PRISON",
		"OC_EBR_ENTRANCE",
		"OC_CENTER_02",
	]},
	["OC_PRISON_TO_KITCHEN"] = {x = -4513.00977, y = -299.615295, z = -1810.54041, adjacent =
	[
		"OC_KITCHEN_ENTRANCE",
		"OC_KNECHTCAMP_01",
		"OC_PRISON_ENTRANCE",
		"OC_WALL_02",
	]},
	["OC_ORK_BACK_CAMP_16"] = {x = 1659.39099, y = -727.79071, z = -7771.88525, adjacent =
	[
		"OC9",
		"OC_ORK_BACK_CAMP_03",
		"OC_ORK_BACK_CAMP_02",
		"OC_ORK_BACK_CAMP_04",
	]},
	["OC_ORK_BACK_CAMP_02"] = {x = 1995.62549, y = -451.560486, z = -6188.13281, adjacent =
	[
		"OC9",
		"OC_ORK_BACK_CAMP_16",
		"OC_ORK_BACK_CAMP_04",
	]},
	["OC_ORK_BACK_CAMP_04"] = {x = 679.852356, y = -525.794434, z = -7074.27686, adjacent =
	[
		"OC_ORK_BACK_CAMP_03",
		"OC_ORK_BACK_CAMP_16",
		"OC_ORK_BACK_CAMP_02",
	]},
	["OC_ORK_BACK_CAMP_05"] = {x = -982.272888, y = -795.758301, z = -7612.46875, adjacent =
	[
		"OC_ORK_BACK_CAMP_03",
	]},
	["OC_ORK_LITTLE_CAMP_05"] = {x = 7144.77148, y = -725.549377, z = 3024.2417, adjacent =
	[
		"OC_ORK_LITTLE_CAMP_06",
		"OC_ORK_LITTLE_CAMP_04",
	]},
	["OC_ORK_LITTLE_CAMP_06"] = {x = 6245.37402, y = -891.962036, z = 3035.17749, adjacent =
	[
		"OC_ORK_LITTLE_CAMP_02",
		"OC_ORK_LITTLE_CAMP_05",
	]},
	["OC_ORK_LITTLE_CAMP_04"] = {x = 8440.61523, y = -708.014282, z = 2943.81543, adjacent =
	[
		"OC11",
		"OC_ORK_LITTLE_CAMP_03",
		"OC_ORK_LITTLE_CAMP_05",
	]},
	["OC_ORK_LITTLE_CAMP_01"] = {x = 5013.83594, y = -1084.39282, z = 6893.3252, adjacent =
	[
		"OC12",
		"OC13",
		"OC_ORK_LITTLE_CAMP_02",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING5",
	]},
	["OC_ORK_MAIN_CAMP_07"] = {x = -1588.14917, y = -1059.51392, z = 7273.32813, adjacent =
	[
		"OC_ORK_MAIN_CAMP_08",
		"OC_ORK_MAIN_CAMP_04",
		"OC_ORK_MAIN_CAMP_06",
	]},
	["OC_ORK_MAIN_CAMP_04"] = {x = -1900.36023, y = -959.597839, z = 6623.32031, adjacent =
	[
		"OC_ORK_MAIN_CAMP_08",
		"OC_ORK_MAIN_CAMP_07",
		"OC_ORK_MAIN_CAMP_10",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING",
	]},
	["OC_ORK_MAIN_CAMP_10"] = {x = -2621.57861, y = -594.920105, z = 5185.27246, adjacent =
	[
		"OC_ORK_MAIN_CAMP_04",
		"OC_ORK_MAIN_CAMP_05",
		"OC_ORK_MAIN_CAMP_09",
		"OC_ORK_MAIN_CAMP_11",
	]},
	["OC_ORK_MAIN_CAMP_06"] = {x = -2807.3479, y = -1005.14197, z = 7276.89209, adjacent =
	[
		"OC14",
		"OC_ORK_MAIN_CAMP_07",
		"OC_ORK_MAIN_CAMP_05",
	]},
	["OC_ORK_MAIN_CAMP_05"] = {x = -2755.72559, y = -959.056396, z = 6692.30664, adjacent =
	[
		"OC_ORK_MAIN_CAMP_10",
		"OC_ORK_MAIN_CAMP_06",
	]},
	["OC_ORK_MAIN_CAMP_09"] = {x = -3750.78101, y = -680.609802, z = 4176.47607, adjacent =
	[
		"OC_ORK_MAIN_CAMP_10",
		"OC_ORK_MAIN_CAMP_11",
		"OC_PATH_01",
	]},
	["OC_ORK_MAIN_CAMP_11"] = {x = -3927.75732, y = -821.130798, z = 5344.61377, adjacent =
	[
		"OC_ORK_MAIN_CAMP_02",
		"OC_ORK_MAIN_CAMP_10",
		"OC_ORK_MAIN_CAMP_09",
	]},
	["OC_PATH_01"] = {x = -4055.16064, y = -674.948669, z = 3510.37817, adjacent =
	[
		"OC_PATH_02",
		"OC_ORK_MAIN_CAMP_12",
		"OC_ORK_MAIN_CAMP_09",
	]},
	["OC_PATH_03"] = {x = -7116.01904, y = -1232.23108, z = 4947.85938, adjacent =
	[
		"OC1",
		"OC_PATH_02",
	]},
	["OC_CAMPFIRE_BARBQ"] = {x = 2014, y = 191.071655, z = -1045, adjacent =
	[
		"OC_CENTER_05",
	]},
	["OC_GUARD_ROOM_02_IN"] = {x = -1215, y = 699.525574, z = 2317, adjacent =
	[
		"OC_GUARD_UP_02",
		"OC_GUARD_FLOOR_UP_01",
		"OC_GUARD_ROOM_02_SLEEP_01",
		"OC_GUARD_ROOM_02_OUT",
	]},
	["OC_GUARD_UP_02"] = {x = -1243.85852, y = 699.52533, z = 2682.17578, adjacent =
	[
		"OC_GUARD_ROOM_02_IN",
		"OC_GUARD_UP_01",
	]},
	["OC_GUARD_FLOOR_UP_01"] = {x = -1507, y = 700.006836, z = 2345, adjacent =
	[
		"OC_GUARD_ROOM_02_IN",
		"OC_GUARD_FLOOR_UP_02",
	]},
	["OC_GUARD_FLOOR_UP_02"] = {x = -2361, y = 700.006836, z = 2033, adjacent =
	[
		"OC_GUARD_FLOOR_UP_01",
		"OC_GUARD_FLOOR_UP_03",
	]},
	["OC_GUARD_FLOOR_UP_03"] = {x = -3871, y = 700.004883, z = 1405, adjacent =
	[
		"OC_GUARD_FLOOR_UP_02",
		"OC_GUARD_FLOOR_UP_04",
	]},
	["OC_GUARD_FLOOR_UP_04"] = {x = -4433, y = 714.475891, z = 1027, adjacent =
	[
		"OC_GUARD_FLOOR_UP_03",
		"OC_GUARD_FLOOR_UP_05",
	]},
	["OC_GUARD_FLOOR_UP_05"] = {x = -4603, y = 719.587097, z = 263, adjacent =
	[
		"OC_GUARD_FLOOR_UP_04",
		"OC_GUARD_FLOOR_UP_06",
	]},
	["OC_GUARD_FLOOR_UP_06"] = {x = -4739, y = 719.337036, z = -108, adjacent =
	[
		"OC_GUARD_FLOOR_UP_05",
	]},
	["OC_GUARD_ROOM_01_SLEEP_02"] = {x = -894.393188, y = 199.527054, z = 1776.77881, adjacent =
	[
		"OC_GUARD_ROOM_01_CENTER",
		"OC_GUARD_ROOM_01_SLEEP_01",
		"OC_GUARD_ROOM_01_IN",
	]},
	["OC_GUARD_ROOM_01_CENTER"] = {x = -1077.68811, y = 199.527054, z = 2287.34814, adjacent =
	[
		"OC_GUARD_ROOM_01_SLEEP_02",
		"OC_GUARD_ROOM_01_SLEEP_01",
		"OC_GUARD_PALISADE_09",
		"OC_GUARD_IN_02",
	]},
	["OC_GUARD_ROOM_01_SLEEP_01"] = {x = -1188.01953, y = 199.526657, z = 1937.61523, adjacent =
	[
		"OC_GUARD_ROOM_01_SLEEP_02",
		"OC_GUARD_ROOM_01_CENTER",
		"OC_GUARD_ROOM_01_IN",
	]},
	["OC_GUARD_PALISADE_09"] = {x = -1831.49377, y = 200.00705, z = 2262.65649, adjacent =
	[
		"OC_GUARD_ROOM_01_CENTER",
		"OC_GATE_ROOM_IN",
		"OC_WALL_05",
	]},
	["OC_GATE_ROOM_IN"] = {x = -2377.59644, y = 199.626068, z = 2080.65723, adjacent =
	[
		"OC_GUARD_PALISADE_09",
	]},
	["OC_GUARD_IN_02"] = {x = -398.193329, y = 199.525497, z = 2226.95215, adjacent =
	[
		"OC_GUARD_ROOM_01_CENTER",
		"OC_GUARD_UP_01",
		"OC_GUARD_IN_01",
	]},
	["OC_GUARD_UP_01"] = {x = -500.360931, y = 299.515137, z = 2683.41357, adjacent =
	[
		"OC_GUARD_UP_02",
		"OC_GUARD_IN_02",
	]},
	["OC_GUARD_IN_01"] = {x = -376.010498, y = 199.526382, z = 1438.79346, adjacent =
	[
		"OC_GUARD_ENTRANCE",
		"OC_GUARD_IN_02",
		"OC_GUARD_ROOM_01_IN",
		"OC_GUARD_FLOOR_01",
	]},
	["OC_GUARD_ROOM_01_IN"] = {x = -973.382324, y = 199.525101, z = 1433.8562, adjacent =
	[
		"OC_GUARD_ROOM_01_SLEEP_02",
		"OC_GUARD_ROOM_01_SLEEP_01",
		"OC_GUARD_IN_01",
		"OC_GUARD_ROOM_01_FIRE",
	]},
	["OC_GUARD_ROOM_01_FIRE"] = {x = -1045.98486, y = 199.524384, z = 1354.03687, adjacent =
	[
		"OC_GUARD_ROOM_01_IN",
	]},
	["OC_MAGE_UP_03"] = {x = 1001.82214, y = 849.692505, z = -2756.62451, adjacent =
	[
		"OC_MAGE_STUDY",
		"OC_MAGE_CENTER",
		"OC_MAGE_UP_02",
		"OC_MAGE_UP_04",
	]},
	["OC_MAGE_STUDY"] = {x = 1481.78577, y = 849.692322, z = -3225.26855, adjacent =
	[
		"OC_MAGE_UP_03",
		"OC_MAGE_CENTER",
	]},
	["OC_MAGE_CENTER"] = {x = 1012.77209, y = 849.692444, z = -3140.58618, adjacent =
	[
		"OC_MAGE_UP_03",
		"OC_MAGE_STUDY",
	]},
	["OC_MAGE_TO_LIBRARY"] = {x = 730.597839, y = 299.863098, z = -2358.37598, adjacent =
	[
		"OC_MAGE_IN",
		"OC_MAGE_LIBRARY_IN",
	]},
	["OC_MAGE_IN"] = {x = 1002.68768, y = 306.311188, z = -1803.36035, adjacent =
	[
		"OC_MAGE_ENTRANCE",
		"OC_MAGE_TO_LIBRARY",
		"OC_MAGE_TO_LAB",
		"OC_MAGE_UP_05",
		"OC_MAGE_UP_01",
	]},
	["OC_MAGE_LIBRARY_IN"] = {x = 705.852844, y = 299.87323, z = -3032.20239, adjacent =
	[
		"OC_MAGE_TO_LIBRARY",
		"OC_MAGE_LIBRARY_BOOK_02",
		"OC_MAGE_LIBRARY_BOOK_01",
	]},
	["OC_MAGE_LIBRARY_BOOK_02"] = {x = 458.119385, y = 299.806458, z = -2991.53589, adjacent =
	[
		"OC_MAGE_LIBRARY_IN",
		"OC_MAGE_LIBRARY_BOOK_01",
	]},
	["OC_MAGE_LIBRARY_BOOK_01"] = {x = 790.12915, y = 299.407043, z = -3344.09253, adjacent =
	[
		"OC_MAGE_LIBRARY_IN",
		"OC_MAGE_LIBRARY_BOOK_02",
	]},
	["OC_MAGE_TO_LAB"] = {x = 1262.79895, y = 299.896484, z = -2415.60913, adjacent =
	[
		"OC_MAGE_IN",
		"OC_MAGE_LAB_IN",
	]},
	["OC_MAGE_LAB_IN"] = {x = 1267.88586, y = 299.889648, z = -3014.22021, adjacent =
	[
		"OC_MAGE_TO_LAB",
		"OC_MAGE_LAB_ALCHEMY",
	]},
	["OC_MAGE_LAB_ALCHEMY"] = {x = 1335, y = 299.407043, z = -3329, adjacent =
	[
		"OC_MAGE_LAB_IN",
	]},
	["OC_MAGE_UP_05"] = {x = 516.396973, y = 299.82843, z = -1697.53064, adjacent =
	[
		"OC_MAGE_IN",
		"OC_MAGE_UP_04",
	]},
	["OC_MAGE_UP_01"] = {x = 1486.01526, y = 299.835052, z = -1690.62329, adjacent =
	[
		"OC_MAGE_IN",
		"OC_MAGE_UP_02",
	]},
	["OC_MAGE_UP_02"] = {x = 1542.66406, y = 659.692505, z = -2562.29956, adjacent =
	[
		"OC_MAGE_UP_03",
		"OC_MAGE_UP_01",
	]},
	["OC_MAGE_UP_04"] = {x = 399.924316, y = 659.692505, z = -2591.74365, adjacent =
	[
		"OC_MAGE_UP_03",
		"OC_MAGE_UP_05",
	]},
	["OC_EBR_DARK_FLOOR"] = {x = -3927.10254, y = 740.006836, z = -3156.71216, adjacent =
	[
		"OC_EBR_DARK_ROOM_IN",
		"OC_EBR_ROOM_03_IN",
	]},
	["OC_EBR_DARK_ROOM_IN"] = {x = -4643.06445, y = 740.244629, z = -3199.45337, adjacent =
	[
		"OC_EBR_DARK_FLOOR",
	]},
	["OC_EBR_ROOM_03_IN"] = {x = -2957.62451, y = 740.258972, z = -3159.38306, adjacent =
	[
		"OC_EBR_DARK_FLOOR",
		"OC_EBR_ROOM_03_SIT",
		"OC_EBR_ROOM_03_SLEEP",
	]},
	["OC_EBR_ROOM_04_IN"] = {x = -3237.65259, y = 743.837524, z = -2048.18335, adjacent =
	[
		"OC_EBR_FLOOR_02",
		"OC_EBR_ROOM_04_SLEEP",
	]},
	["OC_EBR_FLOOR_02"] = {x = -3227.91675, y = 740.258667, z = -2477.28027, adjacent =
	[
		"OC_EBR_ROOM_04_IN",
		"OC_EBR_FLOOR_03",
		"OC_EBR_BASE_TO_FLOOR",
	]},
	["OC_EBR_ROOM_04_SLEEP"] = {x = -3187.31152, y = 742.267395, z = -1066.86548, adjacent =
	[
		"OC_EBR_ROOM_04_IN",
	]},
	["OC_EBR_FLOOR_05"] = {x = -2052.29004, y = 740.258972, z = -2735.55786, adjacent =
	[
		"OC_EBR_FLOOR_STAND",
		"OC_EBR_FLOOR_04",
		"OC_EBR_ROOM_02_IN",
		"OC_EBR_FLOOR_06",
	]},
	["OC_EBR_FLOOR_STAND"] = {x = -1973.12476, y = 740.026855, z = -2294.66455, adjacent =
	[
		"OC_EBR_FLOOR_05",
	]},
	["OC_EBR_FLOOR_04"] = {x = -2773.68164, y = 740.258972, z = -2721.29907, adjacent =
	[
		"OC_EBR_FLOOR_05",
		"OC_EBR_ROOM_03_SIT",
		"OC_EBR_FLOOR_03",
	]},
	["OC_EBR_ROOM_03_SIT"] = {x = -2721.21509, y = 740.258972, z = -3312.15747, adjacent =
	[
		"OC_EBR_ROOM_03_IN",
		"OC_EBR_FLOOR_04",
		"OC_EBR_ROOM_03_SLEEP",
	]},
	["OC_EBR_ROOM_03_SLEEP"] = {x = -2996.63232, y = 740.259033, z = -3500.60327, adjacent =
	[
		"OC_EBR_ROOM_03_IN",
		"OC_EBR_ROOM_03_SIT",
	]},
	["OC_EBR_ROOM_02_IN"] = {x = -2001.85681, y = 740.259094, z = -3156.53369, adjacent =
	[
		"OC_EBR_FLOOR_05",
		"OC_EBR_ROOM_02_CENTER",
		"OC_EBR_ROOM_02_SIT",
	]},
	["OC_EBR_ROOM_02_CENTER"] = {x = -1774.22388, y = 740.259766, z = -3418.33813, adjacent =
	[
		"OC_EBR_ROOM_02_IN",
		"OC_EBR_ROOM_02_SIT",
		"OC_EBR_ROOM_02_SLEEP",
	]},
	["OC_EBR_ROOM_02_SIT"] = {x = -2132.16797, y = 740.259094, z = -3297.20532, adjacent =
	[
		"OC_EBR_ROOM_02_IN",
		"OC_EBR_ROOM_02_CENTER",
	]},
	["OC_EBR_ROOM_02_SLEEP"] = {x = -823.628357, y = 744.083618, z = -3271.86182, adjacent =
	[
		"OC_EBR_ROOM_02_CENTER",
	]},
	["OC_EBR_FLOOR_01"] = {x = -780.781311, y = 740.260742, z = -2477.34155, adjacent =
	[
		"OC_EBR_ROOM_01_IN",
		"OC_EBR_FLOOR_06",
		"OC_EBR_BASE_TO_FLOOR",
	]},
	["OC_EBR_ROOM_01_IN"] = {x = -846.187866, y = 740.258972, z = -2021.43005, adjacent =
	[
		"OC_EBR_FLOOR_01",
		"OC_EBR_ROOM_01_SIT",
		"OC_EBR_ROOM_01_SLEEP",
	]},
	["OC_EBR_ROOM_01_SIT"] = {x = -1185.42432, y = 740.258972, z = -1967.49402, adjacent =
	[
		"OC_EBR_ROOM_01_IN",
		"OC_EBR_ROOM_01_SIT_WINDOW",
	]},
	["OC_EBR_ROOM_01_SIT_WINDOW"] = {x = -1228.14844, y = 740.258972, z = -1125.18115, adjacent =
	[
		"OC_EBR_ROOM_01_SIT",
		"OC_EBR_ROOM_01_SLEEP",
	]},
	["OC_EBR_ROOM_01_SLEEP"] = {x = -788.276306, y = 740.258972, z = -1154.005, adjacent =
	[
		"OC_EBR_ROOM_01_IN",
		"OC_EBR_ROOM_01_SIT_WINDOW",
	]},
	["OC_EBR_FLOOR_03"] = {x = -3238.92358, y = 740.258972, z = -2713.70679, adjacent =
	[
		"OC_EBR_FLOOR_02",
		"OC_EBR_FLOOR_04",
	]},
	["OC_EBR_FLOOR_06"] = {x = -832.651001, y = 740.258972, z = -2745.56689, adjacent =
	[
		"OC_EBR_FLOOR_05",
		"OC_EBR_FLOOR_01",
	]},
	["OC_EBR_BASE_TO_FLOOR"] = {x = -2033.55151, y = 200.043472, z = -2398.53467, adjacent =
	[
		"OC_EBR_FLOOR_02",
		"OC_EBR_FLOOR_01",
		"OC_EBR_IN",
	]},
	["OC_EBR_IN"] = {x = -2038.3457, y = 200.048004, z = -1453.89087, adjacent =
	[
		"OC_EBR_BASE_TO_FLOOR",
		"OC_EBR_WEAPON_IN",
		"OC_KITCHEN_IN",
		"OC_EBR_ENTRANCE",
	]},
	["OC_EBR_HALL_CENTER"] = {x = -1980.1947, y = 0.0367736816, z = -3120.63745, adjacent =
	[
		"OC_EBR_HALL_THRONE",
		"OC_EBR_HALL_TO_KITCHEN",
	]},
	["OC_EBR_HALL_THRONE"] = {x = -3236.02368, y = 47.9217072, z = -3167.39282, adjacent =
	[
		"OC_EBR_HALL_CENTER",
		"OC_EBR_HALL_IN_02",
		"OC_EBR_STAND_THRONE_02",
		"OC_EBR_STAND_THRONE_01",
	]},
	["OC_EBR_HALL_TO_KITCHEN"] = {x = -1993.89319, y = 0.0368766785, z = -2798.81714, adjacent =
	[
		"OC_EBR_HALL_CENTER",
		"OC_EBR_HALL_IN_01",
		"OC_EBR_HALL_IN_02",
	]},
	["OC_EBR_HALL_IN_01"] = {x = -839.774048, y = 0.0283470154, z = -2880.20239, adjacent =
	[
		"OC_EBR_HALL_TO_KITCHEN",
		"OC_EBR_WEAPON_TO_HALL",
	]},
	["OC_EBR_HALL_IN_02"] = {x = -3117.65381, y = 0.0451774597, z = -2745.74438, adjacent =
	[
		"OC_EBR_HALL_THRONE",
		"OC_EBR_HALL_TO_KITCHEN",
		"OC_EBR_KITCHEN_TO_HALL",
		"OC_EBR_STAND_THRONE_02",
	]},
	["OC_EBR_WEAPON_TO_HALL"] = {x = -986.939636, y = 100.047958, z = -2061.80322, adjacent =
	[
		"OC_EBR_HALL_IN_01",
		"OC_EBR_WEAPON_IN",
	]},
	["OC_EBR_KITCHEN_TO_HALL"] = {x = -3110.84717, y = 100.047951, z = -2104.05542, adjacent =
	[
		"OC_EBR_HALL_IN_02",
		"OC_EBR_KITCHEN_COOK",
		"OC_KITCHEN_IN",
	]},
	["OC_EBR_WEAPON_IN"] = {x = -1533.21533, y = 100.047958, z = -1600.85608, adjacent =
	[
		"OC_EBR_IN",
		"OC_EBR_WEAPON_TO_HALL",
	]},
	["OC_EBR_KITCHEN_COOK"] = {x = -3016.35522, y = 100.047943, z = -1584.10828, adjacent =
	[
		"OC_EBR_KITCHEN_TO_HALL",
		"OC_KITCHEN_IN",
	]},
	["OC_KITCHEN_IN"] = {x = -2398.06055, y = 100.047958, z = -1601.07715, adjacent =
	[
		"OC_EBR_IN",
		"OC_EBR_KITCHEN_TO_HALL",
		"OC_EBR_KITCHEN_COOK",
	]},
	["OC_FOLTER_SHARP"] = {x = -5013.67188, y = -359.99295, z = -3284.29321, adjacent =
	[
		"OC_FOLTER_IN",
	]},
	["OC_FOLTER_IN"] = {x = -4817.1001, y = -359.993134, z = -2805.78516, adjacent =
	[
		"OC_KITCHEN_ENTRANCE",
		"OC_FOLTER_SHARP",
	]},
	["OC_PRISON_ENTRANCE"] = {x = -4520.24512, y = -199.992889, z = -419.194427, adjacent =
	[
		"OC_CENTER_03",
		"OC_PRISON_TO_KITCHEN",
		"OC_PRISON_GUARD_01",
		"OC_WALL_02",
		"OC_TO_PRISON",
		"OC_PRISON_IN_01",
	]},
	["OC_PRISON_GUARD_01"] = {x = -4339.10303, y = -199.992905, z = -389.60318, adjacent =
	[
		"OC_PRISON_ENTRANCE",
	]},
	["OC_CENTER_04"] = {x = 818.871216, y = 199.545059, z = -278.443115, adjacent =
	[
		"OC_STORE_ENTRANCE",
		"OC_SMITH_01",
		"OC_TO_MAGE",
		"OC_MAGE_ENTRANCE",
		"OC_GUARD_ENTRANCE",
		"OC_TRAIN_03",
	]},
	["OC_CAMPFIRE_OUT_01"] = {x = 2401, y = 199.350494, z = -908, adjacent =
	[
		"OC_CENTER_05",
		"OC_CAMPFIRE_OUT_02",
		"OC_CENTER_GUARD_01",
	]},
	["OC_CAMPFIRE_OUT_02"] = {x = 2632, y = 196.737274, z = -1168, adjacent =
	[
		"OC_CAMPFIRE_OUT_01",
		"OC_WALL_04",
		"OC_CAMPFIRE_OUT_03",
	]},
	["OC_WALL_04"] = {x = 2768.54468, y = 199.71994, z = -1827.67383, adjacent =
	[
		"OC_CAMPFIRE_OUT_02",
		"OC_PALISADE_UP_03",
		"OC_CAMPFIRE_OUT_03",
	]},
	["OC_PALISADE_UP_03"] = {x = 2616.5166, y = 599.81604, z = -2175.00757, adjacent =
	[
		"OC_WALL_04",
		"OC_GUARD_PALISADE_06",
		"OC_GUARD_PALISADE_05",
	]},
	["OC_GUARD_PALISADE_06"] = {x = 2767.9751, y = 599.780945, z = -2228.53589, adjacent =
	[
		"OC_PALISADE_UP_03",
		"OC_GUARD_PALISADE_05",
		"OC_TOWER_BACK",
	]},
	["OC_GUARD_PALISADE_05"] = {x = 2121.98779, y = 599.925232, z = -2168.79053, adjacent =
	[
		"OC_PALISADE_UP_03",
		"OC_GUARD_PALISADE_06",
	]},
	["OC_SMITH_ANVIL"] = {x = 1288.82886, y = 199.859741, z = 613.944275, adjacent =
	[
		"OC_SMITH_01",
		"OC_SMITH_COOL",
		"OC_SMITH_SHARP",
	]},
	["OC_SMITH_SHARP"] = {x = 1368.40442, y = 199.876892, z = 827.624634, adjacent =
	[
		"OC_SMITH_COOL",
		"OC_SMITH_ANVIL",
		"OC_SMITH_FIRE",
	]},
	["OC_SMITH_FIRE"] = {x = 987.949768, y = 199.663208, z = 955.45752, adjacent =
	[
		"OC_SMITH_COOL",
		"OC_SMITH_SHARP",
	]},
	["OC_GUARD_STORE_01"] = {x = 2571.38696, y = 200.00705, z = 258.409668, adjacent =
	[
		"OC_STORE_ENTRANCE",
	]},
	["OC_PALISADE_UP_02"] = {x = -3850.4834, y = 199.950409, z = -3056.14722, adjacent =
	[
		"OC_GUARD_PALISADE_04",
		"OC_WALL_03",
	]},
	["OC_GUARD_PALISADE_04"] = {x = -4265.50684, y = 199.99353, z = -3221.72485, adjacent =
	[
		"OC_PALISADE_UP_02",
	]},
	["OC_WALL_03"] = {x = -4050.37524, y = -342.265106, z = -2892.61426, adjacent =
	[
		"OC_KITCHEN_ENTRANCE",
		"OC_PALISADE_UP_02",
	]},
	["OC_WALL_02"] = {x = -4832.89453, y = -307.520508, z = -1682.24463, adjacent =
	[
		"OC_PRISON_TO_KITCHEN",
		"OC_PRISON_ENTRANCE",
		"OC_PALISADE_UP_01",
	]},
	["OC_PALISADE_UP_01"] = {x = -5099.88184, y = 205.943253, z = -1942.93616, adjacent =
	[
		"OC_WALL_02",
		"OC_GUARD_PALISADE_02",
	]},
	["OC_GUARD_PALISADE_02"] = {x = -5220.39893, y = 208.324768, z = -1685.84167, adjacent =
	[
		"OC_PALISADE_UP_01",
		"OC_GUARD_PALISADE_03",
		"OC_OPEN_ROOM_IN_01",
	]},
	["OC_WALL_05"] = {x = -1552.32556, y = -175.526154, z = 1911.44312, adjacent =
	[
		"OC_GUARD_PALISADE_09",
		"OC_TO_GUARD",
	]},
	["OC_TO_GUARD"] = {x = -1833.23486, y = -63.8267517, z = 741.10083, adjacent =
	[
		"OC_GUARD_ENTRANCE",
		"OC_WALL_05",
		"OC_CENTER_02",
	]},
	["OC_GUARD_PALISADE_07"] = {x = 3630.91333, y = 601.657532, z = -1352.97607, adjacent =
	[
		"OC_TOWER_BACK",
	]},
	["OC_TOWER_BACK"] = {x = 3404.19287, y = 599.819946, z = -2361.49365, adjacent =
	[
		"OC_GUARD_PALISADE_06",
		"OC_GUARD_PALISADE_07",
		"OC_GUARD_TOWER_01",
	]},
	["OC_GUARD_TOWER_01"] = {x = 3777.23804, y = 596.890381, z = -2556.96118, adjacent =
	[
		"OC_TOWER_BACK",
	]},
	["OC_GUARD_PALISADE_03"] = {x = -5067.04785, y = 201.717804, z = -2435.24316, adjacent =
	[
		"OC_GUARD_PALISADE_02",
	]},
	["OC_TO_PRISON"] = {x = -3399.65039, y = -247.168701, z = 346.669189, adjacent =
	[
		"OC_CENTER_03",
		"OC_PRISON_ENTRANCE",
	]},
	["OC_EBR_ENTRANCE"] = {x = -2108.02905, y = 60.3483315, z = -456.843719, adjacent =
	[
		"OC_TO_MAGE",
		"OC_CENTER_03",
		"OC_EBR_IN",
		"OC_CENTER_02",
		"OC_EBR_GUARDPASSAGE_02",
		"OC_EBR_GUARDPASSAGE_01",
	]},
	["OC_CENTER_02"] = {x = -2371.58813, y = -56.9504547, z = 78.1051483, adjacent =
	[
		"OC_CENTER_03",
		"OC_TO_GUARD",
		"OC_EBR_ENTRANCE",
	]},
	["OC_PRISON_IN_01"] = {x = -4982.26221, y = -199.477356, z = -234.567764, adjacent =
	[
		"OC_PRISON_ENTRANCE",
		"OC_PRISON_IN_02",
		"OC_PRISON_CELL_01",
		"OC_PRISON_CELL_04",
	]},
	["OC_PRISON_IN_02"] = {x = -5446.14209, y = -200.15625, z = -122.005936, adjacent =
	[
		"OC_PRISON_IN_01",
		"OC_PRISON_CELL_02",
		"OC_PRISON_CELL_03",
	]},
	["OC_PRISON_CELL_01"] = {x = -5224.89453, y = -299.920044, z = -781.22937, adjacent =
	[
		"OC_PRISON_IN_01",
		"OC_PRISON_CELL_01_SIT",
	]},
	["OC_PRISON_CELL_04"] = {x = -4762.54736, y = -300.034027, z = 451.011475, adjacent =
	[
		"OC_PRISON_IN_01",
	]},
	["OC_PRISON_CELL_02"] = {x = -5680.21533, y = -300.137634, z = -723.787354, adjacent =
	[
		"OC_PRISON_IN_02",
		"OC_PRISON_CELL_02_SIT",
		"OC_PRISON_CELL_02_SIT_GROUND",
	]},
	["OC_PRISON_CELL_03"] = {x = -5274.03125, y = -300.186371, z = 574.370056, adjacent =
	[
		"OC_PRISON_IN_02",
		"OC_PRISON_CELL_03_SIT_GROUND",
	]},
	["OC_STORE_IN"] = {x = 2529.00488, y = 200.026413, z = 709.465149, adjacent =
	[
		"OC_STORE_ENTRANCE",
		"OC_STORE_CENTER",
	]},
	["OC_STORE_CENTER"] = {x = 2127, y = 200.007065, z = 1118, adjacent =
	[
		"OC_STORE_IN",
	]},
	["OC_GUARD_FLOOR_02"] = {x = 1114, y = 200.00705, z = 1418, adjacent =
	[
		"OC_STORE_TO_FLOOR",
		"OC_GUARD_FLOOR_01",
	]},
	["OC_STORE_TO_FLOOR"] = {x = 1923, y = 200.007065, z = 1329, adjacent =
	[
		"OC_GUARD_FLOOR_02",
	]},
	["OC_GUARD_FLOOR_01"] = {x = 301, y = 199.528564, z = 1436, adjacent =
	[
		"OC_GUARD_IN_01",
		"OC_GUARD_FLOOR_02",
		"OC_COUNT_01",
	]},
	["OC_COUNT_01"] = {x = 131.066513, y = 299.707794, z = 1883.151, adjacent =
	[
		"OC_GUARD_FLOOR_01",
		"OC_COUNT_SIT",
	]},
	["OC_COUNT_SIT"] = {x = 305, y = 299.524048, z = 2283, adjacent =
	[
		"OC_COUNT_01",
	]},
	["OC_GUARD_ROOM_02_SLEEP_01"] = {x = -1181, y = 699.524902, z = 1688, adjacent =
	[
		"OC_GUARD_ROOM_02_IN",
		"OC_GUARD_ROOM_02_SLEEP_02",
		"OC_GUARD_ROOM_02_OUT",
	]},
	["OC_GUARD_ROOM_02_SLEEP_02"] = {x = -1170, y = 699.524902, z = 1242, adjacent =
	[
		"OC_GUARD_ROOM_02_SLEEP_01",
		"OC_GUARD_ROOM_02_OUT",
	]},
	["OC_GUARD_ROOM_02_OUT"] = {x = -772, y = 699.52655, z = 1175, adjacent =
	[
		"OC_GUARD_ROOM_02_IN",
		"OC_GUARD_ROOM_02_SLEEP_01",
		"OC_GUARD_ROOM_02_SLEEP_02",
		"OC_GUARD_FLOOR_UP",
	]},
	["OC_GUARD_FLOOR_UP"] = {x = -333, y = 699.526855, z = 1149, adjacent =
	[
		"OC_GUARD_ROOM_02_OUT",
		"OC_GUARD_ROOM_03_IN",
	]},
	["OC_GUARD_ROOM_03_IN"] = {x = 136, y = 699.525879, z = 1183, adjacent =
	[
		"OC_GUARD_FLOOR_UP",
		"OC_GUARD_ROOM_03_SLEEP",
		"OC_GUARD_ROOM_03_SIT",
	]},
	["OC_GUARD_ROOM_03_SLEEP"] = {x = 126, y = 699.525269, z = 1949, adjacent =
	[
		"OC_GUARD_ROOM_03_IN",
	]},
	["OC_GUARD_ROOM_03_SIT"] = {x = 377, y = 699.526855, z = 1278, adjacent =
	[
		"OC_GUARD_ROOM_03_IN",
	]},
	["OC_TRAIN_01"] = {x = 1649.08191, y = 199.967865, z = -624.788208, adjacent =
	[
		"OC_CENTER_05",
		"OC_TRAIN_04",
		"OC_TRAIN_02",
	]},
	["OC_TRAIN_04"] = {x = 1280.59521, y = 200.00177, z = -659.06311, adjacent =
	[
		"OC_MAGE_ENTRANCE",
		"OC_TRAIN_01",
		"OC_TRAIN_03",
	]},
	["OC_TRAIN_02"] = {x = 1715.58264, y = 199.691986, z = -222.905273, adjacent =
	[
		"OC_CENTER_05",
		"OC_STORE_ENTRANCE",
		"OC_TRAIN_01",
		"OC_TRAIN_03",
	]},
	["OC_TRAIN_03"] = {x = 1203.71069, y = 199.587631, z = -308.678467, adjacent =
	[
		"OC_CENTER_04",
		"OC_TRAIN_04",
		"OC_TRAIN_02",
	]},
	["OC_EBR_STAND_THRONE_02"] = {x = -3050.03979, y = 4.16539383, z = -2955.30566, adjacent =
	[
		"OC_EBR_HALL_THRONE",
		"OC_EBR_HALL_IN_02",
		"OC_EBR_STAND_THRONE_01",
	]},
	["OC_EBR_STAND_THRONE_01"] = {x = -3086.07764, y = 4.16539383, z = -3317.11694, adjacent =
	[
		"OC_EBR_HALL_THRONE",
		"OC_EBR_STAND_THRONE_02",
	]},
	["OC_PRISON_CELL_02_SIT"] = {x = -5899.20508, y = -300.135498, z = -969.547607, adjacent =
	[
		"OC_PRISON_CELL_02",
		"OC_PRISON_CELL_02_SIT_GROUND",
	]},
	["OC_PRISON_CELL_02_SIT_GROUND"] = {x = -5879.44531, y = -300.272247, z = -634.602417, adjacent =
	[
		"OC_PRISON_CELL_02",
		"OC_PRISON_CELL_02_SIT",
	]},
	["OC_PRISON_CELL_03_SIT_GROUND"] = {x = -5314.69141, y = -300.121094, z = 776.620239, adjacent =
	[
		"OC_PRISON_CELL_03",
	]},
	["OC_PRISON_CELL_01_SIT"] = {x = -5171.74561, y = -300.007324, z = -1201.71399, adjacent =
	[
		"OC_PRISON_CELL_01",
	]},
	["OC_EBR_GUARDPASSAGE_02"] = {x = -2188.40137, y = 162.932526, z = -821.042297, adjacent =
	[
		"OC_EBR_ENTRANCE",
		"OC_EBR_GUARDPASSAGE_01",
	]},
	["OC_EBR_GUARDPASSAGE_01"] = {x = -1832.12317, y = 162.932526, z = -816.617432, adjacent =
	[
		"OC_EBR_ENTRANCE",
		"OC_EBR_GUARDPASSAGE_02",
	]},
	["OC_CENTER_GUARD_01"] = {x = 2681.22827, y = 199.940811, z = -775.9104, adjacent =
	[
		"OC_CENTER_GUARD_02",
		"OC_CENTER_05",
		"OC_CAMPFIRE_OUT_01",
		"OC_CENTER_GUARD_04",
	]},
	["OC_CAMPFIRE_OUT_03"] = {x = 2373.802, y = 199.730301, z = -1592.01794, adjacent =
	[
		"OC_CAMPFIRE_OUT_02",
		"OC_WALL_04",
	]},
	["OC_OPEN_ROOM_IN_02"] = {x = -4584.89404, y = 210.506866, z = 642.210754, adjacent =
	[
		"OC_OPEN_ROOM_CENTER",
		"OC_GUARD_PALISADE_01",
		"OC_OPEN_ROOM_REPAIR_01",
	]},
	["OC_OPEN_ROOM_CENTER"] = {x = -4999.60059, y = 210.506851, z = -448.808746, adjacent =
	[
		"OC_OPEN_ROOM_IN_02",
		"OC_OPEN_ROOM_IN_01",
		"OC_OPEN_ROOM_REPAIR_02",
		"OC_OPEN_ROOM_REPAIR_01",
		"OC_OPEN_ROOM_GUARD_01",
	]},
	["OC_GUARD_PALISADE_01"] = {x = -4235.43848, y = 204.23616, z = 1152.72034, adjacent =
	[
		"OC_OPEN_ROOM_IN_02",
	]},
	["OC_OPEN_ROOM_IN_01"] = {x = -5281.4668, y = 210.506836, z = -1072.33667, adjacent =
	[
		"OC_GUARD_PALISADE_02",
		"OC_OPEN_ROOM_CENTER",
		"OC_OPEN_ROOM_REPAIR_02",
	]},
	["OC_OPEN_ROOM_REPAIR_02"] = {x = -5759.58496, y = 210.506699, z = -396.108368, adjacent =
	[
		"OC_OPEN_ROOM_CENTER",
		"OC_OPEN_ROOM_IN_01",
		"OC_OPEN_ROOM_REPAIR_01",
	]},
	["OC_OPEN_ROOM_REPAIR_01"] = {x = -5534.49219, y = 210.507111, z = 121.77729, adjacent =
	[
		"OC_OPEN_ROOM_IN_02",
		"OC_OPEN_ROOM_CENTER",
		"OC_OPEN_ROOM_REPAIR_02",
	]},
	["OC_OPEN_ROOM_GUARD_01"] = {x = -5268.01563, y = 210.506622, z = -253.276276, adjacent =
	[
		"OC_OPEN_ROOM_CENTER",
	]},
	["OC_ORK_BACK_CAMP_08"] = {x = -5474.4209, y = -453.044861, z = -5736.77881, adjacent =
	[
		"OC_ORK_BACK_CAMP_07",
		"OC_ORK_BACK_CAMP_09",
		"OC_ORK_BACK_CAMP_10",
		"OC_ORK_BACK_CAMP_12",
	]},
	["OC_ORK_BACK_CAMP_09"] = {x = -3998.47437, y = -499.993164, z = -5684.55176, adjacent =
	[
		"OC_ORK_BACK_CAMP_06",
		"OC_ORK_BACK_CAMP_08",
	]},
	["OC_ORK_BACK_CAMP_10"] = {x = -7278.48926, y = -444.094238, z = -6132.22559, adjacent =
	[
		"OC7",
		"OC6",
		"OC_ORK_BACK_CAMP_08",
		"OC_ORK_BACK_CAMP_11",
	]},
	["OC_ORK_BACK_CAMP_11"] = {x = -8236.69727, y = -559.449646, z = -5470.73584, adjacent =
	[
		"OC6",
		"OC_ORK_BACK_CAMP_10",
		"OC_ORK_BACK_CAMP_12",
		"OC_ORK_BACK_CAMP_14",
	]},
	["OC_ORK_BACK_CAMP_12"] = {x = -7331.95264, y = -333.29248, z = -4622.82324, adjacent =
	[
		"OC_ORK_BACK_CAMP_08",
		"OC_ORK_BACK_CAMP_11",
		"OC_ORK_BACK_CAMP_14",
	]},
	["OC_ORK_BACK_CAMP_14"] = {x = -8229.72461, y = -586.39032, z = -3478.27222, adjacent =
	[
		"OC_ORK_BACK_CAMP_11",
		"OC_ORK_BACK_CAMP_12",
		"OC_ORK_BACK_CAMP_13",
	]},
	["OC_ORK_BACK_CAMP_13"] = {x = -9605.87891, y = -888.337646, z = -2834.70386, adjacent =
	[
		"OC5",
		"OC6",
		"OC_ORK_BACK_CAMP_14",
	]},
	["OC_GUARD_OUT_01"] = {x = 276.556152, y = 169.716446, z = 503.735291, adjacent =
	[
		"OC_SMITH_01",
		"OC_GUARD_ENTRANCE",
		"OC_JAN",
	]},
	["OC_JAN"] = {x = 210.344208, y = 171.044327, z = 606.41626, adjacent =
	[
		"OC_GUARD_OUT_01",
	]},
	["OC_CENTER_GUARD_04"] = {x = 3312.49902, y = 199.955139, z = 37.0427475, adjacent =
	[
		"OC_CENTER_GUARD_01",
	]},
	["OC_BRUTUS_MEATBUGS"] = {x = -4167.73877, y = -329.209595, z = -2485.24414, adjacent =
	[
		"OC_KITCHEN_ENTRANCE",
	]},
	["OC_ORK_BETWEEN_CAMPS_01"] = {x = 1923.34692, y = -696.451599, z = 5433.37939, adjacent =
	[
		"OC13",
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING3",
	]},
	["OC_ORK_BETWEEN_CAMPS_01_MOVEMENT2"] = {x = 105.697227, y = -961.816589, z = 4947.07031, adjacent =
	[
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT",
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT3",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING2",
	]},
	["OC_ORK_BETWEEN_CAMPS_01_MOVEMENT3"] = {x = -196.804108, y = -1100.14429, z = 6350.45752, adjacent =
	[
		"OC_ORK_MAIN_CAMP_08",
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT2",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING",
	]},
	["OC_ORK_BETWEEN_CAMPS_01_MOVEMENT11"] = {x = -3472.39453, y = -832.801697, z = -8069.67773, adjacent =
	[
		"OC8",
		"OC_ORK_BACK_CAMP_06",
	]},
	["OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING"] = {x = -915.851318, y = -1030.14453, z = 6225.90479, adjacent =
	[
		"OC_ORK_MAIN_CAMP_04",
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT3",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING2",
	]},
	["OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING2"] = {x = -756.885315, y = -835.078979, z = 5412.49072, adjacent =
	[
		"OC_ORK_BETWEEN_CAMPS_01_MOVEMENT2",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING",
	]},
	["OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING3"] = {x = 2512.89722, y = -833.977173, z = 5571.97266, adjacent =
	[
		"OC_ORK_BETWEEN_CAMPS_01",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING4",
	]},
	["OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING4"] = {x = 3648.62329, y = -1038.02466, z = 6024.18457, adjacent =
	[
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING3",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING5",
	]},
	["OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING5"] = {x = 4679.20996, y = -1024.31641, z = 6438.44385, adjacent =
	[
		"OC_ORK_LITTLE_CAMP_01",
		"OC_ORK_MAIN_CAMP_04_MOVEMENTTUNING4",
	]},
	["OC_PATH_04"] = {x = -8007.5166, y = -528.980896, z = 1964.57617, adjacent =
	[
		"OC3",
		"OC_PATH_02",
		"OC_ORK_BACK_CAMP_15",
	]},
	["OC_ORK_BACK_CAMP_15"] = {x = -7449.30078, y = -495.87439, z = -14.3103333, adjacent =
	[
		"OC5",
		"OC_PATH_04",
	]},
	["OC_ORK_BETWEEN_CAMPS_02_MOVEMENT"] = {x = 7327.91455, y = -736.460938, z = -1700.42493, adjacent =
	[
		"OC_ORK_BETWEEN_CAMPS_02",
	]},
	["OC_ORK_BETWEEN_CAMPS_02_MOVEMENT2"] = {x = 7356.99072, y = -771.074341, z = -988.451782, adjacent =
	[
		"OC_ORK_BETWEEN_CAMPS_02",
	]},
	["OC_ORK_BACK_CAMP_01"] = {x = 6089.23975, y = -615.573547, z = -4905.1333, adjacent =
	[
		"OC10",
	]},
	["OC11_OC12"] = {x = 8318.53711, y = -726.945923, z = -3546.88574, adjacent =
	[
		"OC10",
		"OC11",
	]},

}
