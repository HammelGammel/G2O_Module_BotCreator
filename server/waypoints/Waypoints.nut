// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

addEventHandler("onInit", function()
{
	WayPoints.InitWPs();
	WayPoints.InitFPs();
});

// ------------------------------------------------------------------- //

class WayPoints
{
	static function InitWPs()
	{
		local startTick = getTickCount();
		print ("Initializing WP cellsystems...");

		foreach (world, wpSystem in WayPoints.WPs)
		{
			print ("Initializing: '" + world + "'");
			local cells = Cell(3000);
			WayPoints.WPCells[world] <- cells;

			WPsArr[world] <- [];

			foreach (name, wp in wpSystem)
			{
				cells.addToCellByPos(wp.x, wp.z, name, wp);
				WPsArr[world].append(name);
			}
		}

		local deltaMS = getTickCount() - startTick;
		print ("Initialized WP cellsystems, took: " + deltaMS + "ms");
		print("-========================================-");
	}

	// ------------------------------------------------------------------- //

	static function InitFPs()
	{
		local startTick = getTickCount();
		print ("Initializing FP cellsystems...");

		foreach (world, wpSystem in WayPoints.FPs)
		{
			print ("Initializing: '" + world + "'");
			local cells = Cell(3000);
			WayPoints.FPCells[world] <- cells;

			foreach (name, wp in wpSystem)
				cells.addToCellByPos(wp.x, wp.z, name, wp);
		}

		local deltaMS = getTickCount() - startTick;
		print ("Initialized FP cellsystems, took: " + deltaMS + "ms");
		print("-========================================-");
	}

	// ------------------------------------------------------------------- //

	static function getNearestWP(x, y, z, world, radius = 5000)
	{
		if (world in WayPoints.WPCells)
		{
			local WPCells = WayPoints.WPCells[world];
			local wps = WPCells.getContentNear(x, z, radius);

			local lastDistance = 999999;
			local currWP = null;

			foreach(wpName, wp in wps)
			{
				local currDistance = getDistance3d(x, y, z, wp.x, wp.y, wp.z);
				if (currDistance < lastDistance && currDistance <= radius)
				{
					lastDistance = currDistance;
					currWP = wpName;
				}
			}

			return currWP;
		}
		else
			print("ERROR: World " + world + " is not initialized");

		return null;
	}

	// ------------------------------------------------------------------- //

	static function getWPsInRange(x, y, z, world, radius = 5000)
	{
		local wps = [];

		if (world in WayPoints.WPCells)
		{
			local WPCells = WayPoints.WPCells[world];
			local wpCells = WPCells.getContentNear(x, z, radius);

			foreach(wpName, wp in wpCells)
			{
				local currDistance = getDistance3d(x, y, z, wp.x, wp.y, wp.z);
				if (currDistance <= radius)
					wps.append(wpName);
			}
		}

		return wps;
	}

	// ------------------------------------------------------------------- //

	static function getRandomWP(world)
	{
		if (world in WPsArr && WPsArr[world].len() > 0)
			return wpSystem(rand() % WPsArr[world].len());
		return null;
	}

	// ------------------------------------------------------------------- //

	static function getRandomWPInRange(x, y, z, world, radius = 5000)
	{
		local wps = getWPsInRange(x, y, z, world, radius);
		return wps[rand() % wps.len()];
	}

	// ------------------------------------------------------------------- //

	static function getNearestFP(x, y, z, world, radius = 5000)
	{
		if (world in WayPoints.FPCells)
		{
			local FPCells = WayPoints.FPCells[world];
			local fps = FPCells.getContentNear(x, z, radius);

			local lastDistance = 999999;
			local currFP = null;

			foreach(fpName, fp in fps)
			{
				local currDistance = getDistance3d(x, y, z, fp.x, fp.y, fp.z);
				if (currDistance < lastDistance && currDistance <= radius)
				{
					lastDistance = currDistance;
					currFP = fpName;
				}
			}

			return currFP;
		}
		else
			print("ERROR: World " + world + " is not initialized");

		return null;
	}

	// ------------------------------------------------------------------- //

	static function	getWPByName(world, nodeName)
	{
		if (nodeName in WayPoints.WPs[world])
			return WayPoints.WPs[world][nodeName];
	}

	// ------------------------------------------------------------------- //

	static function	getFPByName(world, nodeName)
	{
		if (nodeName in WayPoints.FPs[world])
			return WayPoints.FPs[world][nodeName];
	}

	// ------------------------------------------------------------------- //

	static function getWPRoute(world, startNodeName, endNodeName)
	{
		local startTick = getTickCount();

		if (!(world in WayPoints.WPs))
			throw ("Trying to call getWPRoute with uninitialized world '" + world + "'");

		local wpSystem = WayPoints.WPs[world];
		if(!(startNodeName in wpSystem && endNodeName in wpSystem))
			throw ("Trying to call getWPRoute with uninitialized node");

		local endNode = wpSystem[endNodeName];
		local openList = {}
		local closedList = {};

		AddToOpenList(openList, startNodeName, 0, null);
		while (openList.len() > 0)
		{
			local currNodeName = getMin(openList);
			local currNode = openList[currNodeName];

			closedList[currNodeName] <- currNode.parent;

			// -- Found path -- //
			if (currNodeName == endNodeName)
			{
				local deltaMS = getTickCount() - startTick;
				return getPath(closedList, currNodeName, endNodeName);
			}

			ExpandNode(openList, closedList, currNodeName, endNode, wpSystem);
			delete openList[currNodeName];
		}
		// -- Found no path -- //
		return null;
	}

	// ------------------------------------------------------------------- //

	static function getPath(closedList, currNodeName, endNodeName)
	{
		local pathOut = [endNodeName];
		local currNodeName = closedList[endNodeName];
		while (currNodeName != null)
		{
			pathOut.append(currNodeName);
			currNodeName = closedList[currNodeName];
		}
		return pathOut;
	}

	// ------------------------------------------------------------------- //

	static function ExpandNode(openList, closedList, parentNodeName, endNode, wpSystem)
	{
		local parentNode = wpSystem[parentNodeName];
		foreach (adjacentNodeName in parentNode.adjacent)
		{
			if (adjacentNodeName in closedList)
				continue;

			local adjacentNode = wpSystem[adjacentNodeName];
			local distParent = getDistance3d(adjacentNode.x, adjacentNode.y, adjacentNode.z, parentNode.x, parentNode.y, parentNode.z);
			local distEnd = getDistance3d(adjacentNode.x, adjacentNode.y, adjacentNode.z, endNode.x, endNode.y, endNode.z);

			local dist = distParent + distEnd + openList[parentNodeName].dist;
			if (adjacentNodeName in openList && openList[adjacentNodeName].dist < dist)
				continue;

			AddToOpenList(openList, adjacentNodeName, dist, parentNodeName);
		}
	}

	// ------------------------------------------------------------------- //

	static function AddToOpenList(openList, nodeName, distance, parentNodeName)
	{
		openList[nodeName] <- {["dist"] = distance, ["parent"] = parentNodeName};
	}

	// ------------------------------------------------------------------- //

	static function getMin(openList)
	{
		local lastName = null;
		if (openList.len() > 0)
		{
			local lastDist = -1;
			foreach (name, node in openList)
			{
				if (lastDist == -1 || node.dist <= lastDist)
				{
					lastDist = node.dist;
					lastName = name;
				}
			}
		}
		return lastName;
	}

	// ------------------------------------------------------------------- //
	// ------------------------------------------------------------------- //

	static WPs = {};
	static FPs = {};

	static WPsArr = {};

	static WPCells = {};
	static FPCells = {};
}
