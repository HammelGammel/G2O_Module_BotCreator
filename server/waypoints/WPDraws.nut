// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

local REDRAW_LASTTICK = getTickCount();
local REDRAW_TICKINTERVAL = 1000;

local DRAW = false;

// ------------------------------------------------------------------- //

addEventHandler("onTick", function()
{
    if (DRAW)
    {
        local tick = getTickCount();

        if (tick > REDRAW_LASTTICK)
        {
            WPDraws.create();
            REDRAW_LASTTICK = tick + REDRAW_TICKINTERVAL;
        }
    }
});

// ------------------------------------------------------------------- //

addEventHandler("onPlayerCommand", function(pid, cmd, params)
{
    cmd = cmd.tolower();

    switch (cmd)
    {
        case "togglewpdraws":
        case "togglewpdraw":
            DRAW = !DRAW;
            if (!DRAW)
                WPDraws.clear();
            break;
    }
});

// ------------------------------------------------------------------- //

class WPDraws
{
    static function create()
    {
        clear();

        for (local pid = 0; pid < getMaxSlots(); pid++)
        {
            if (isPlayerConnected(pid))
            {
                local pos = getPlayerPosition(pid)
                local world = getPlayerWorld(pid);
                local wps = WayPoints.getWPsInRange(pos.x, pos.y, pos.z, world);

                foreach (wpName in wps)
                {
                    local wp = WayPoints.getWPByName(world, wpName);

                    if (wp != null)
                    {
                        local packet = Packet();
                        packet.writeUInt16(EPacketId3DDraws.packetIDCreate);
                        packet.writeFloat(wp.x);
                        packet.writeFloat(wp.y);
                        packet.writeFloat(wp.z);
                        packet.writeString(wpName);

                        packet.send(pid, UNRELIABLE);
                    }
                }
            }
        }
    }

    // ------------------------------------------------------------------- //

    static function clear()
    {
        local packet = Packet();
        packet.writeUInt16(EPacketId3DDraws.packetIDClear);

        for (local pid = 0; pid < getMaxSlots(); pid++)
            if (isPlayerConnected(pid))
                packet.send(pid, RELIABLE);
    }
}
