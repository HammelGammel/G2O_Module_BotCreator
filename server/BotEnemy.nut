// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class BotEnemy
{
    function receiveHit(killerId)
    {
        Bot.sendHit(getID(), killerId);
    }

    // ------------------------------------------------------------------- //

    function isValid()
    {
        return getID() != null && !isDead() && isConnected() && isSameWorld();
    }

    // ------------------------------------------------------------------- //

    function isSameWorld()
    {
        return getWorld() == getParent().getWorld();
    }

    // ------------------------------------------------------------------- //

    function isConnected()
    {
        if (m_IsBot)
            return true;
        return isPlayerConnected(getID());
    }

    // ------------------------------------------------------------------- //

    function isDead()
    {
        return getHealth() <= 0;
    }

    // ------------------------------------------------------------------- //

    function getHealth()
    {
        if (m_IsBot)
            return m_Bot.getHealth();
        else
            return getPlayerHealth(m_ID);
    }

    // ------------------------------------------------------------------- //

    function getAni()
    {
        if (m_IsBot)
            m_Bot.getAni();
        else
        {
            return null;
            // -- doesn't exist on server anymore -- //
            //return getPlayerAni(m_ID);
        }
    }

    // ------------------------------------------------------------------- //

    function getPosition()
    {
        if (m_IsBot)
            return m_Bot.getPosition();
        else
            return getPlayerPosition(m_ID);
    }

    // ------------------------------------------------------------------- //

    function getAngle()
    {
        if (m_IsBot)
            return m_Bot.getAngle();
        else
            return getPlayerAngle(m_ID);
    }

    // ------------------------------------------------------------------- //

    function getDistance()
    {
        local pos = getPosition();
        local otherPos = getParent().getPosition();
        return getDistance3d(pos.x, pos.y, pos.z, otherPos.x, otherPos.y, otherPos.z);
    }

    // ------------------------------------------------------------------- //

    function getWorld()
    {
        if (isBot())
            return m_Bot.getWorld();
        return getPlayerWorld(getID());
    }

    // ------------------------------------------------------------------- //

    function getID(){ return m_ID;}
    function getParent(){ return m_Parent;}
    function isBot(){ return m_IsBot;}
    function isPlayer(){ return !m_IsBot;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(id, parent)
    {
        m_ID = id;
        m_Parent = parent;

        if (id >= getMaxSlots())
        {
            m_IsBot = true;
            m_Bot = Bot.m_Bots[id];
        }
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_ID = null;
    m_IsBot = false;
    m_Bot = null;
    m_Parent = null;
}

// ------------------------------------------------------------------- //
