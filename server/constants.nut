// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

PRIORITY_HIGH <- true;
PRIORITY_LOW <- false;

// ------------------------------------------------------------------- //

enum EMovementMode
{
    walking,
    running,
    sprinting,
};

// ------------------------------------------------------------------- //

COMBAT_IDLE <- 0;

// ------------------------------------------------------------------- //

enum ECombatStatePredator
{
    idle,
    warnStart,
    warn,
    chaseStart,
    chase,
    evadeStart,
    evade,
    attackStart,
    attack,
    fleeStart,
    flee,
}

// ------------------------------------------------------------------- //

enum ECombatStatePrey
{
    idle,
    fleeStart,
    flee,
    fleeEnd,
}

// ------------------------------------------------------------------- //

enum ECombatStateHumanoid
{
    idle,
    warnStart,
    warn,
    drawStart,
    draw,
    sheathStart,
    sheath,
    chaseStart,
    chase,
    evadeStart,
    evade,
    attackStart,
    attack,
    fleeStart,
    flee,
}

// ------------------------------------------------------------------- //

enum EAttackState
{
    ready,
    swing,
    recover,
    jumpBack,
    strafe
}

// ------------------------------------------------------------------- //

PARRY_ANIMS <-
[
    "T_1HPARADE_0",
    "T_1HPARADE_0_A2",
    "T_1HPARADE_0_A3",
    "T_1HPARADEJUMPB",
    "T_1HJUMPB",

    "T_2HPARADE_0",
    "T_2HPARADE_0_A2",
    "T_2HPARADE_0_A3",
    "T_2HPARADEJUMPB",
    "T_2HJUMPB",
]
