// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

addEvent("onBotCreated");
addEvent("onPlayerHitBot");
addEvent("onBotHit");
addEvent("onBotTakeDamage");
addEvent("onBotDead");
addEvent("onBotRespawn");
addEvent("onBotInView");
addEvent("onBotOutOfView");
addEvent("onBotTakeUnknownDamage");
addEvent("onBotReceiveUnknownHeal");

// ------------------------------------------------------------------- //

local BOT_NEXTTICK = getTickCount();
local BOT_TICKINTERVAL = 100;

local BOT_NEXTSLOWTICK = getTickCount();
local BOT_SLOWTICKINTERVAL = 5000;

local BOT_NEXTSENDTICK = getTickCount();
local BOT_SENDTICKINTERVAL = 200;

local BOT_NEXTUPDATESYNCPIDTICK = getTickCount();
local BOT_UPDATESYNCPIDINTERVAL = 5000;

addEventHandler("onTick", function()
{
    local tick = getTickCount();

    if (tick > BOT_NEXTTICK)
    {
        local visibleBots = ManagedPlayer.getAllVisibleBots();

        // -- Update bots that are visible -- //
        foreach (bot in Bot.m_Bots)
        {
            if (bot.getID() in visibleBots)
            {
                local deltaTime = tick - bot.getLastTick();

                bot.setInView(true);
                bot.tick(deltaTime);
            }
            else
                bot.setInView(false);
        }

        BOT_NEXTTICK = tick + BOT_TICKINTERVAL;
    }

    if (tick > BOT_NEXTSLOWTICK)
    {
        local deltaTime = tick - BOT_NEXTSLOWTICK - BOT_SLOWTICKINTERVAL;

        // -- Update bots that aren't visible -- //
        foreach (bot in Bot.m_Bots)
        {
            if (!bot.isInView())
            {
                local deltaTime = tick - bot.getLastTick();
                bot.tick(deltaTime);
            }
        }

        BOT_NEXTSLOWTICK = tick + BOT_SLOWTICKINTERVAL;
    }

    if (tick > BOT_NEXTSENDTICK)
    {
        Bot.sendUpdatePacket();
        BOT_NEXTSENDTICK = tick + BOT_SENDTICKINTERVAL;
    }

    if (tick > BOT_NEXTUPDATESYNCPIDTICK)
    {
        Bot.updateSyncPlayers();
        BOT_NEXTUPDATESYNCPIDTICK = tick + BOT_UPDATESYNCPIDINTERVAL;
    }
});

// ------------------------------------------------------------------- //

addEventHandler("onInit", function()
{
    setTimer(function()
    {
        foreach (bot in Bot.m_Bots)
            bot.onLateInit();
    }, 100, 1);
});

// ------------------------------------------------------------------- //

addEventHandler("onBotCreated", function(bid, name)
{
    local bot = Bot.getBot(bid);

    Bot.updateSyncPlayers();
    BOT_NEXTUPDATESYNCPIDTICK = getTickCount() + BOT_UPDATESYNCPIDINTERVAL;

    for (local pid = 0; pid < getMaxSlots(); pid++)
        if (isPlayerConnected(pid))
            bot.addChangeAttributePlayer(pid);
});

// ------------------------------------------------------------------- //

addEventHandler("onPlayerJoin", function(pid)
{
    foreach (bot in Bot.m_Bots)
        bot.addChangeAttributePlayer(pid);

    Bot.updateSyncPlayers();
    BOT_NEXTUPDATESYNCPIDTICK = getTickCount() + BOT_UPDATESYNCPIDINTERVAL;

    Bot.createBots(pid);
});

// ------------------------------------------------------------------- //

addEventHandler("onPlayerDisconnect", function(pid)
{
    foreach (bot in Bot.m_Bots)
        bot.removeChangeAttributePlayer(pid);

    Bot.updateSyncPlayers();
    BOT_NEXTUPDATESYNCPIDTICK = getTickCount() + BOT_UPDATESYNCPIDINTERVAL;
});

// ------------------------------------------------------------------- //

addEventHandler("onPacket", function(pid, packet)
{
    local id = packet.readUInt16();
    switch (id)
    {
        case EPacketIdBots.TSUpdatePos:
            Bot.readPosUpdatePacket(packet);

            break;
        case EPacketIdBots.TSUpdateAni:
            Bot.readAniUpdatePacket(packet);
            break;

        case EPacketIdBots.TSBotHit:
            local bid = packet.readInt16();
            local dmg = packet.readInt32();
            callEvent("onPlayerHitBot", pid, bid, dmg);
            break;

        case EPacketIdBots.TSDamage:
            local bid = packet.readInt16();
            local attackerId = packet.readInt16();
            local dmg = packet.readInt32();

            local bot = Bot.getBot(bid);
            if (bot != null)
            {
                bot.setHealth(bot.getHealth() - dmg);
                callEvent("onBotTakeDamage", bid, attackerId, dmg);
            }
            break;

        case EPacketIdBots.TSUnknownDamage:
            local bid = packet.readInt16();
            local dmg = packet.readInt16();

            local bot = Bot.getBot(bid);
            bot.setHealth(bot.getHealth() - dmg);
            callEvent("onBotTakeDamage", bid, -1, dmg);
            callEvent("onBotTakeUnknown", bid, dmg);
            break;

        case EPacketIdBots.TSUnknownHeal:
            local bid = packet.readInt16();
            local amount = packet.readInt16();

            local bot = Bot.getBot(bid);
            bot.setHealth(bot.getHealth() + amount);
            callEvent("onBotReceiveUnknownHeal", bid, amount);
            break;
    }
});

// ------------------------------------------------------------------- //

addEventHandler("onPlayerHitBot", function(pid, bid, dmg)
{
    local bot = Bot.getBot(bid);
    if (bot != null)
        bot.damageBot(pid, dmg, type);
});

// ------------------------------------------------------------------- //

// -- synchronized bot without AI behaviour. Don't instanciate -- //
class Bot
{
    function onLateInit()
    {

    }

    // ------------------------------------------------------------------- //

    function tick(deltaTime)
    {
        m_LastTick = getTickCount();

        interpAngle(deltaTime);
    }

    // ------------------------------------------------------------------- //

    function interpAngle(deltaTime)
    {
        if (!isInView())
            return;

        // -- turn to player/bot -- //
        if (getInterpAngleId() >= 0)
        {
            m_InterpAngle = -1;
            interpAngleId(deltaTime);
            return;
        }

        // -- turn to angle -- //

        local currAngle = getAngle();
        local interpTargetAngle = getInterpAngle();

        if (interpTargetAngle >= 0 && interpTargetAngle != currAngle)
        {
            local newAngle = Util.lerpAngle(currAngle, interpTargetAngle, getInterpAngleSpeed() * deltaTime);

            m_Angle = newAngle;
        }
    }

    // ------------------------------------------------------------------- //

    function interpAngleId(deltaTime)
    {
        local currAngle = getAngle();
        local interpPos = null;
        local interpId = getInterpAngleId();
        if (isPlayer(interpId))
            interpPos = getPlayerPosition(interpId);
        else
            interpPos = getBot(interpId).getPosition();

        local interpTargetAngle = getVectorAngle(m_Pos.x, m_Pos.z, interpPos.x, interpPos.z);
        if (interpTargetAngle != currAngle)
        {
            local newAngle = Util.lerpAngle(currAngle, interpTargetAngle, getInterpAngleSpeed() * deltaTime);

            m_Angle = newAngle;
        }
    }

    // ------------------------------------------------------------------- //

    function damageBot(attackerId, dmg, type)
    {
        setHealth(m_Health - dmg, attackerId);
    }

    // ------------------------------------------------------------------- //

    function playAni(ani)
    {
        if (!isDead())
        {
            m_Ani = ani;
            setAttributeChanged("ani");
        }
    }

    // ------------------------------------------------------------------- //

    function stopAni()
    {
        m_Ani = "";
        setAttributeChanged("stopAni");
    }

    // ------------------------------------------------------------------- //

    function onDeath(killerId)
    {
        // -- Weapons are unequipped on death. Change attributes to reflect this -- //
        m_MeleeWeapon = -1;
        m_RangedWeapon = -1;

        setInterpAngleId(-1, getInterpAngleSpeed());
        setInterpAngle(-1, getInterpAngleSpeed());

        setWeaponMode(0);

        callEvent("onBotDead", getID(), killerId);
    }

    // ------------------------------------------------------------------- //

    function onRevival()
    {
        stopAni();
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    function setInView(mode)
    {
        if (mode != m_InView)
        {
            if (mode)
            {
                callEvent("onBotInView", getID());
            }
            else
            {
                local combatComp = getCombatComp();
                if (combatComp != null && combatComp.isFighting())
                    combatComp.stopCombat();

                callEvent("onBotOutOfView", getID());
            }

            m_InView = mode;

            setAttributeChanged("inView");
        }
    }

    // ------------------------------------------------------------------- //

    function setSyncPid(pid)
    {
        if (pid != m_SyncPid)
        {
            m_SyncPid = pid;

            setAttributeChanged("syncPid");
        }
    }

    // ------------------------------------------------------------------- //

    function setWorld(world)
    {
        if (world != m_World)
        {
            m_World = world;
            setAttributeChanged("world");
        }
    }

    // ------------------------------------------------------------------- //

    function setVirtualWorld(virtualWorld)
    {
        if (virtualWorld != m_VirtualWorld)
        {
            m_VirtualWorld = virtualWorld;
            setAttributeChanged("virtualWorld");
        }
    }

    // ------------------------------------------------------------------- //


    function setInstance(instance)
    {
        if (instance != m_Instance)
        {
            m_Instance = instance;

            setAttributeChanged("instance");
        }
    }

    // ------------------------------------------------------------------- //

    function setPosition(x, y, z)
    {
        local tick = getTickCount();
        local deltaTick = (tick - m_LastSpeedUpdateTick) / 1000.0;

        local pos = getPosition();
        local dist = getDistance3d(pos.x, pos.y, pos.z, x, y, z);

        setSpeed(dist/deltaTick);
        setMovementAngle(getVectorAngle(pos.x, pos.z, x, z));
        m_LastSpeedUpdateTick = tick;

        if (x != m_Pos.x || y != m_Pos.y || z != m_Pos.z)
        {
            m_Pos.x = x;
            m_Pos.y = y;
            m_Pos.z = z;

            updateCell();

            m_ReceivedClientPos = false;
            setAttributeChanged("pos");

            return true;
        }
        return false;
    }

    // ------------------------------------------------------------------- //

    // -- this has higher priority than position sent by client -- //
    function forcePosition(x, y, z)
    {
        setPosition(x, y, z);
    }

    // ------------------------------------------------------------------- //

    function setAngle(angle)
    {
        if (angle != m_Angle)
        {
            m_Angle = angle;
            setAttributeChanged("angle");
        }
    }

    // ------------------------------------------------------------------- //

    function setInterpAngleId(id, interpSpeed)
    {
        setInterpAngle(-1, interpSpeed);

        if (id != m_InterpAngleId)
        {
            m_InterpAngleId = id;

            setAttributeChanged("interpAngleId");
        }

        setInterpAngleSpeed(interpSpeed);
    }

    // ------------------------------------------------------------------- //

    function setInterpAngle(angle, interpSpeed)
    {
        if (angle != m_InterpAngle)
        {
            m_InterpAngle = angle;
            setAttributeChanged("interpAngle");
        }

        setInterpAngleSpeed(interpSpeed);
    }

    // ------------------------------------------------------------------- //

    function setInterpAngleSpeed(interpSpeed)
    {
        if (interpSpeed != m_InterpAngleSpeed)
        {
            m_InterpAngleSpeed = interpSpeed;
            setAttributeChanged("interpAngleSpeed");
        }
    }

    // ------------------------------------------------------------------- //

    function setWeaponMode(mode)
    {
        if (mode != m_WeaponMode)
        {
            m_WeaponMode = mode;
            setAttributeChanged("weaponMode");
        }
    }

    // ------------------------------------------------------------------- //

    function setMaxHealth(maxHealth)
    {
        if (maxHealth != m_MaxHealth)
        {
            m_MaxHealth = maxHealth;
            setAttributeChanged("maxHealth");
        }
    }

    // ------------------------------------------------------------------- //

    function setHealth(health, attackerId = null)
    {
        if (health < 0)
            health = 0;

        if (health != m_Health)
        {
            m_Health = health;
            setAttributeChanged("health");
            m_ReceivedClientHealth = false;

            if (isDead() && health > 0)
            {
                m_Dead = false;
                onRevival();
            }
            else if (m_Health == 0)
            {
                stopAni();
                onDeath(attackerId);
                m_Dead = true;
            }
        }
    }

    // ------------------------------------------------------------------- //

    function setStrength(stregth)
    {
        if (stregth != m_Strength)
        {
            m_Strength = stregth;
            setAttributeChanged("strength");
        }
    }

    // ------------------------------------------------------------------- //

    function setDexterity(dexterity)
    {
        if (dexterity != m_Dexterity)
        {
            m_Dexterity = dexterity;
            setAttributeChanged("dexterity");
        }
    }

    // ------------------------------------------------------------------- //

    function setOneH(oneH)
    {
        if (oneH != m_OneH)
        {
            m_OneH = oneH;
            setAttributeChanged("oneH");
        }
    }

    // ------------------------------------------------------------------- //

    function setTwoH(twoH)
    {
        if (twoH != m_TwoH)
        {
            m_TwoH = twoH;
            setAttributeChanged("twoH");
        }
    }

    // ------------------------------------------------------------------- //

    function setBow(bow)
    {
        if (bow != m_Bow)
        {
            m_Bow = bow;
            setAttributeChanged("bow");
        }
    }

    // ------------------------------------------------------------------- //

    function setCBow(cBow)
    {
        if (cBow != m_CBow)
        {
            m_CBow = cBow;
            setAttributeChanged("cBow");
        }
    }

    // ------------------------------------------------------------------- //

    function equipArmor(armor)
    {
        if (armor == null)
            armor = -1;

        if (armor != m_Armor)
        {
            m_Armor = armor;
            setAttributeChanged("armor");
        }
    }

    // ------------------------------------------------------------------- //

    function equipMeleeWeapon(meleeWeapon)
    {
        if (meleeWeapon == null)
            meleeWeapon = -1;

        if (meleeWeapon != m_MeleeWeapon)
        {
            m_MeleeWeapon = meleeWeapon;
            setAttributeChanged("meleeWeapon");
        }
    }

    // ------------------------------------------------------------------- //

    function equipRangedWeapon(rangedWeapon)
    {
        if (rangedWeapon == null)
            rangedWeapon = -1;

        if (rangedWeapon != m_RangedWeapon)
        {
            m_RangedWeapon = rangedWeapon;
            setAttributeChanged("rangedWeapon");
        }
    }

    // ------------------------------------------------------------------- //

    function equiShield(shield)
    {
        if (shield == null)
            shield = -1;

        if (shield != m_Shield)
        {
            m_Shield = shield;
            setAttributeChanged("shield");
        }
    }

    // ------------------------------------------------------------------- //

    function equiHelmet(helmet)
    {
        if (helmet == null)
            helmet = -1;

        if (helmet != m_Helmet)
        {
            m_Helmet = helmet;
            setAttributeChanged("helmet");
        }
    }

    // ------------------------------------------------------------------- //

    function setVisuals(bodyModel, bodyTexture, headModel, headTexture)
    {
        if (bodyModel != m_BodyModel || bodyTexture != m_BodyTexture || headModel != m_HeadModel || headTexture != m_HeadTexture)
        {
            m_BodyModel = bodyModel;
            m_BodyTexture = bodyTexture;
            m_HeadModel = headModel;
            m_HeadTexture = headTexture;
            setAttributeChanged("visuals");
        }
    }

    // ------------------------------------------------------------------- //

    function setFatness(fatness)
    {
        m_Fatness = fatness;
        if (fatness != m_Fatness)
        {
            m_Fatness = fatness;
            setAttributeChanged("fatness");
        }
    }

    // ------------------------------------------------------------------- //

    function setSpeed(speed){m_Speed = speed;}
    function setMovementAngle(angle){m_MovementAngle = angle;}

    // ------------------------------------------------------------------- //

    function ignoreStuckOnce()
    {
        m_IgnoreStuckEndTick = getTickCount() + m_IgnoreStuckTime;
    }

    // ------------------------------------------------------------------- //

    function updateCell()
    {
        local newCell = BotAI.m_CellSystem.getIndexByPos(m_Pos.x, m_Pos.z);
        if (m_Cell != newCell)
        {
            if (m_Cell != null)
                BotAI.m_CellSystem.removeFromCell(m_Cell, m_ID);

            onChangeCell();

            m_Cell = newCell;
            BotAI.m_CellSystem.addToCell(newCell, m_ID, this);
        }
    }

    // ------------------------------------------------------------------- //

    function onChangeCell()
    {
        updateVisiblePlayers();
    }

    // ------------------------------------------------------------------- //

    function updateVisiblePlayers()
    {
        local oldVisiblePlayers = m_VisiblePlayers;
        local pos = getPosition();

        m_VisiblePlayers = ManagedPlayer.m_CellSystem.getContentNear(pos.x, pos.z, ManagedPlayer.m_MaxInViewDistance);
        foreach (key, player in m_VisiblePlayers)
            if (getPlayerWorld(player.getID()) != getWorld())
                delete m_VisiblePlayers[key];

        foreach (player in oldVisiblePlayers)
        {
            if (!(player in m_VisiblePlayers))
                player.setBotNotVisible(this);
        }

        foreach (player in m_VisiblePlayers)
        {
            if (!(player in oldVisiblePlayers))
                player.setBotVisible(this);
        }
    }

    // ------------------------------------------------------------------- //

    function isPlayerVisible(pid)
    {
        return pid in m_VisiblePlayers;
    }

    // ------------------------------------------------------------------- //

    function setPlayerVisible(player)
    {
        local id = player.getID();
        if (!(id in m_VisiblePlayers))
            m_VisiblePlayers[id] <- player;
    }

    // ------------------------------------------------------------------- //

    function setPlayerNotVisible(player)
    {
        local id = player.getID();
        if (id in m_VisiblePlayers)
            delete m_VisiblePlayers[id];
    }

    // ------------------------------------------------------------------- //

    function getIdealSyncPlayer()
    {
        local visiblePlayers = getVisiblePlayers();

        local lastPlayer = null;
        local lastPing = 999999;

        foreach (player in visiblePlayers)
        {
            local id = player.getID();
            if (isPlayerConnected(id))
            {
                local currentPing = getPlayerPing(id);
                if (currentPing < lastPing)
                {
                    lastPing = currentPing;
                    lastPlayer = player;
                }
            }
        }

        return lastPlayer;
    }

    // ------------------------------------------------------------------- //

    function updateSyncPlayer()
    {
        local syncPlayer = getIdealSyncPlayer();

        if (syncPlayer == null)
            setSyncPid(-1);
        else
            setSyncPid(syncPlayer.getID());
    }

    // ------------------------------------------------------------------- //

    function hasAnyAttributeChanged(pid)
    {
        if (pid in m_ChangedAttributes)
        {
            foreach (attribute, value in m_ChangedAttributes[pid])
                if (value)
                    return true;
        }
        return false;
    }

    // ------------------------------------------------------------------- //

    function hasAttributeChanged(pid, attribute)
    {
        if (pid in m_ChangedAttributes)
            return m_ChangedAttributes[pid][attribute];
        return false;
    }

    // ------------------------------------------------------------------- //

    function setNoAttributeChanged(pid)
    {
        if (pid in m_ChangedAttributes)
            foreach (attribute, value in m_ChangedAttributes[pid])
                m_ChangedAttributes[pid][attribute] = false;
    }

    // ------------------------------------------------------------------- //

    function setAttributeChanged(attribute)
    {
        foreach (pidTable in m_ChangedAttributes)
            pidTable[attribute] = true;
    }

    // ------------------------------------------------------------------- //

    function setAttributeNotChanged(attribute)
    {
        foreach (pid, pidTable in m_ChangedAttributes)
            pidTable[attribute] = false;
    }

    // ------------------------------------------------------------------- //

    function removeChangeAttributePlayer(pid)
    {
        if (pid in m_ChangedAttributes)
            delete m_ChangedAttributes[pid];
    }

    // ------------------------------------------------------------------- //

    function addChangeAttributePlayer(pid)
    {
        m_ChangedAttributes[pid] <-
        {
            pos = false,
            angle = false,
            interpAngleId = false,
            interpAngle = false,
            interpAngleSpeed = false,
            syncPid = false,
            inView = false,
            instance = false,
            world = false,
            maxHealth = false,
            health = false,
            strength = false,
            dexterity = false,
            oneH = false,
            twoH = false,
            bow = false,
            cBow = false,
            weaponMode = false,
            armor = false,
            meleeWeapon = false,
            rangedWeapon = false,
            shield = false,
            helmet = false,
            visuals = false,
            fatness = false,
            virtualWorld = false,
            ani = false,
            stopAni = false,
        }
    }

    // ------------------------------------------------------------------- //

    function writeCreatePacket(packet)
    {
        packet.writeInt16(m_SyncPid);
        packet.writeInt16(getID());
        packet.writeString(getName());
        packet.writeFloat(m_Pos.x);
        packet.writeFloat(m_Pos.y);
        packet.writeFloat(m_Pos.z);
        packet.writeInt16(m_InterpAngleId);
        packet.writeFloat(m_InterpAngleSpeed);
        packet.writeFloat(m_Angle);
        packet.writeString(m_Instance);
        packet.writeString(m_World);
        packet.writeInt32(m_MaxHealth);
        packet.writeInt32(m_Health);
        packet.writeUInt8(m_Strength);
        packet.writeUInt8(m_Dexterity);
        packet.writeUInt8(m_OneH);
        packet.writeUInt8(m_TwoH);
        packet.writeUInt8(m_Bow);
        packet.writeUInt8(m_CBow);
        packet.writeUInt8(m_WeaponMode);
        packet.writeInt32(m_Armor);
        packet.writeInt32(m_MeleeWeapon);
        packet.writeInt32(m_RangedWeapon);
        packet.writeInt32(m_Shield);
        packet.writeInt32(m_Helmet);
        packet.writeString(m_BodyModel);
        packet.writeUInt8(m_BodyTexture);
        packet.writeString(m_HeadModel);
        packet.writeUInt8(m_HeadTexture);
        packet.writeFloat(m_Fatness);
        packet.writeBool(m_InView);
        packet.writeInt16(m_VirtualWorld);
        packet.writeString(m_Ani);
    }

    // ------------------------------------------------------------------- //

    function writeUpdatePacket(pid, packet)
    {
        local flags = 0;
        packet.writeInt16(getID());

        local sendSyncPid = hasAttributeChanged(pid, "syncPid");
        local sendPos = hasAttributeChanged(pid, "pos") && !(pid == m_SyncPid && m_ReceivedClientPos);
        local sendAngle = hasAttributeChanged(pid, "angle");
        local sendInterpAngleId = hasAttributeChanged(pid, "interpAngleId");
        local sendInterpAngle = hasAttributeChanged(pid, "interpAngle");
        local sendInterpAngleSpeed = hasAttributeChanged(pid, "interpAngleSpeed");
        local sendInstance = hasAttributeChanged(pid, "instance");
        local sendWorld = hasAttributeChanged(pid, "world");
        local sendMaxHealth = hasAttributeChanged(pid, "maxHealth");
        local sendHealth = hasAttributeChanged(pid, "health") && !(pid == m_SyncPid && m_ReceivedClientHealth);
        local sendStrength = hasAttributeChanged(pid, "strength");
        local sendDexterity = hasAttributeChanged(pid, "dexterity");
        local sendOneH = hasAttributeChanged(pid, "oneH");
        local sendTwoH = hasAttributeChanged(pid, "twoH");
        local sendBow = hasAttributeChanged(pid, "bow");
        local sendCBow = hasAttributeChanged(pid, "cBow");
        local sendWeaponMode = hasAttributeChanged(pid, "weaponMode");
        local sendArmor = hasAttributeChanged(pid, "armor");
        local sendMeleeWeapon = hasAttributeChanged(pid, "meleeWeapon");
        local sendRangedWeapon = hasAttributeChanged(pid, "rangedWeapon");
        local sendShield = hasAttributeChanged(pid, "shield");
        local sendHelmet = hasAttributeChanged(pid, "helmet");
        local sendVisuals = hasAttributeChanged(pid, "visuals");
        local sendFatness = hasAttributeChanged(pid, "fatness");
        local sendInView = hasAttributeChanged(pid, "inView");
        local sendVirtualWorld = hasAttributeChanged(pid, "virtualWorld");
        local sendAni = hasAttributeChanged(pid, "ani");
        local sendStopAni = hasAttributeChanged(pid, "stopAni");

        if (sendSyncPid)
            flags = flags | 1;
        if (sendPos)
            flags = flags | 2;
        if (sendAngle)
            flags = flags | 4;
        if (sendInterpAngleId)
            flags = flags | 8;
        if (sendInterpAngle)
            flags = flags | 16;
        if (sendInterpAngleSpeed)
            flags = flags | 32;
        if (sendInstance)
            flags = flags | 64;
        if (sendWorld)
            flags = flags | 128;
        if (sendMaxHealth)
            flags = flags | 256;
        if (sendHealth)
            flags = flags | 512;
        if (sendStrength)
            flags = flags | 1024;
        if (sendDexterity)
            flags = flags | 2048;
        if (sendOneH)
            flags = flags | 4096;
        if (sendTwoH)
            flags = flags | 8192;
        if (sendBow)
            flags = flags | 16384;
        if (sendCBow)
            flags = flags | 32768;
        if (sendWeaponMode)
            flags = flags | 65536;
        if (sendArmor)
            flags = flags | 131072;
        if (sendMeleeWeapon)
            flags = flags | 262144;
        if (sendRangedWeapon)
            flags = flags | 524288;
        if (sendShield)
            flags = flags | 1048576;
        if (sendHelmet)
            flags = flags | 2097152;
        if (sendVisuals)
            flags = flags | 4194304;
        if (sendFatness)
            flags = flags | 8388608;
        if (sendInView)
            flags = flags | 16777216;
        if (sendVirtualWorld)
            flags = flags | 33554432;
        if (sendAni)
            flags = flags | 67108864;
        if (sendStopAni)
            flags = flags | 134217728;
        packet.writeUInt32(flags);

        if (sendSyncPid)
            packet.writeInt16(m_SyncPid);
        if (sendPos)
        {
            packet.writeFloat(m_Pos.x);
            packet.writeFloat(m_Pos.y);
            packet.writeFloat(m_Pos.z);
        }
        if (sendAngle)
            packet.writeFloat(m_Angle);
        if (sendInterpAngleId)
            packet.writeInt16(m_InterpAngleId);
        if (sendInterpAngle)
            packet.writeFloat(m_InterpAngle);
        if (sendInterpAngleSpeed)
            packet.writeFloat(m_InterpAngleSpeed);
        if (sendInstance)
            packet.writeString(m_Instance);
        if (sendWorld)
            packet.writeString(m_World);
        if (sendMaxHealth)
            packet.writeInt32(m_MaxHealth);
        if (sendHealth)
            packet.writeInt32(m_Health);
        if (sendStrength)
            packet.writeUInt8(m_Strength);
        if (sendDexterity)
            packet.writeUInt8(m_Dexterity);
        if (sendOneH)
            packet.writeUInt8(m_OneH);
        if (sendTwoH)
            packet.writeUInt8(m_TwoH);
        if (sendBow)
            packet.writeUInt8(m_Bow);
        if (sendCBow)
            packet.writeUInt8(m_CBow);
        if (sendWeaponMode)
            packet.writeUInt8(m_WeaponMode);
        if (sendArmor)
            packet.writeInt32(m_Armor);
        if (sendMeleeWeapon)
            packet.writeInt32(m_MeleeWeapon);
        if (sendRangedWeapon)
            packet.writeInt32(m_RangedWeapon);
        if (sendShield)
            packet.writeInt32(m_Shield);
        if (sendHelmet)
            packet.writeInt32(m_Helmet);
        if (sendVisuals)
        {
            packet.writeString(m_BodyModel);
            packet.writeUInt8(m_BodyTexture);
            packet.writeString(m_HeadModel);
            packet.writeUInt8(m_HeadTexture);
        }
        if (sendFatness)
            packet.writeFloat(m_Fatness);
        if (sendInView)
            packet.writeBool(m_InView);
        if (sendVirtualWorld)
            packet.writeInt16(m_VirtualWorld);
        if (sendAni)
            packet.writeString(m_Ani);

        setNoAttributeChanged(pid);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static function createBots(pid)
    {
        local sentBots = 0;
        local botCount = getBotCount();

        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TCCreate);
        packet.writeUInt16(botCount);

        // -- order must be lowest id -> highest id -- //
        for (local bid = getMaxSlots(); sentBots != botCount; bid++)
        {
            local bot = getBot(bid);
            if (bot != null)
            {
                // -- write all bots in a single packet -- //
                bot.writeCreatePacket(packet);
                bot.setNoAttributeChanged(pid);
                sentBots++;
            }
        }

        packet.send(pid, RELIABLE);

    }

    // ------------------------------------------------------------------- //

    static function createBot(bot)
    {
        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TCCreate);
        packet.writeUInt16(1);
        bot.writeCreatePacket(packet);

        for (local pid = 0; pid < getMaxSlots(); pid++)
            if (isPlayerConnected(pid))
                packet.send(pid, RELIABLE);
    }

    // ------------------------------------------------------------------- //

    static function sendUpdatePacketTo(pid)
    {
        local numberBots = 0;

        foreach (bot in m_Bots)
            if (bot.hasAnyAttributeChanged(pid) && (bot.isPlayerVisible(pid) || bot.hasAttributeChanged(pid, "inView")))
                numberBots++;

        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TCUpdateBot);
        packet.writeUInt16(numberBots);

        foreach (bot in m_Bots)
            if (bot.hasAnyAttributeChanged(pid) && (bot.isPlayerVisible(pid) || bot.hasAttributeChanged(pid, "inView")))
                bot.writeUpdatePacket(pid, packet);

        packet.send(pid, RELIABLE);
    }

    // ------------------------------------------------------------------- //

    static function sendUpdatePacket()
    {
        for (local pid = 0; pid < getMaxSlots(); ++pid)
            if (isPlayerConnected(pid))
                sendUpdatePacketTo(pid);
    }

    // ------------------------------------------------------------------- //

    static function sendHit(id, killerId)
    {
        local bot = getBot(id);
        local syncPid = id;
        if (bot != null)
            syncPid = bot.m_SyncPid;

        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TCHit);
        packet.writeInt16(id);
        packet.writeInt16(killerId);

        local packetSendBack = Packet();
        packetSendBack.writeUInt16(EPacketIdBots.TCHitAndSendBack);
        packetSendBack.writeInt16(id);
        packetSendBack.writeInt16(killerId);

        // -- sync client sends calculated damage back to server -- //
        packetSendBack.send(syncPid, RELIABLE);

        for (local pid = 0; pid < getMaxSlots(); ++pid)
            if (isPlayerConnected(pid) && pid != syncPid)
                packet.send(pid, RELIABLE);
    }

    // ------------------------------------------------------------------- //

    static function readPosUpdatePacket(packet)
    {
        local botCount = packet.readUInt16();

        for (local i = 0; i < botCount; i++)
        {
            local bid = packet.readUInt16();
            local x = packet.readFloat();
            local y = packet.readFloat();
            local z = packet.readFloat();

            local bot = Bot.getBot(bid);
            bot.setPosition(x, y, z);
            bot.m_ReceivedClientPos = true;
        }
    }

    // ------------------------------------------------------------------- //

    static function readAniUpdatePacket(packet)
    {
        local botCount = packet.readUInt16();

        for (local i = 0; i < botCount; i++)
        {
            local bid = packet.readUInt16();
            local ani = packet.readString();

            local bot = Bot.getBot(bid);
            if (!bot.isDead())
                bot.m_Ani = ani;
        }
    }

    // ------------------------------------------------------------------- //

    static function getBot(bid)
    {
        if (bid in Bot.m_Bots)
            return Bot.m_Bots[bid];
        return null;
    }

    // ------------------------------------------------------------------- //

    static function isPlayer(bid)
    {
        return bid < getMaxSlots();
    }

    // ------------------------------------------------------------------- //

    static function isBot(bid)
    {
        return bid >= getMaxSlots();
    }

    // ------------------------------------------------------------------- //

    static function destroyBot(bid)
    {
        if (bid in m_Bots)
        {
            for (local pid = 0; pid < getMaxSlots(); ++pid)
            {
                if (isPlayerConnected(pid))
                {
                    local packet = Packet();
                    packet.writeUInt16(EPacketIdBots.TCDestroy);
                    packet.writeInt16(bid);
                    packet.send(pid, RELIABLE);
                }
            }

            delete m_Bots[bid];
        }
    }

    // ------------------------------------------------------------------- //

    static function updateSyncPlayers()
    {
        foreach (bot in m_Bots)
        {
            if (bot.isInView())
            {
                bot.updateSyncPlayer();
            }
        }
    }

    // ------------------------------------------------------------------- //

    static function getFreeID()
    {
        if (m_Bots.len() >= m_MaxBots)
            return -1;

        local bid = getMaxSlots();
        while (bid in m_Bots)
            bid++;

        return bid;
    }

    // ------------------------------------------------------------------- //

    static function getBotCount()
    {
        return m_Bots.len();
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- getters -- //
    function getID(){return m_ID;}
    function getName(){return m_Name;}
    function getInstance(){return m_Instance;}
    function getPosition(){return m_Pos;}
    function getAngle(){return m_Angle;}
    function getAni(){return m_Ani;}
    function getWorld(){return m_World;}
    function isDead(){return m_Dead;}
    function isInView(){return m_InView;}
    function getLastTick(){return m_LastTick;}
    function getWeaponMode(){return m_WeaponMode;}

    function getInterpAngleId(){return m_InterpAngleId;}
    function getInterpAngle(){return m_InterpAngle;}
    function getInterpAngleSpeed(){return m_InterpAngleSpeed;}

    function getMaxHealth(){return m_MaxHealth;}
    function getHealth(){return m_Health;}
    function getDexteriy(){return m_Dexterity;}
    function getStrength(){return m_Strength;}
    function getOneH(){return m_OneH;}
    function getTwoH(){return m_TwoH;}
    function getBow(){return m_Bow;}
    function getCBow(){return m_CBow;}

    function getArmor(){return m_Armor;}
    function getMeleeWeapon(){return m_MeleeWeapon;}
    function getRangedWeapon(){return m_RangedWeapon;}
    function getShield(){return m_Shield;}
    function getHelmet(){return m_Helmet;}

    function getHeadModel(){return m_HeadModel;}
    function getHeadTexture(){return m_HeadTexture;}
    function getBodyModel(){return m_BodyModel;}
    function getBodyTexture(){return m_BodyTexture;}
    function getFatness(){return m_Fatness;}

    function getVirtualWorld() {return m_VirtualWorld;}

    function getSpeed() {return m_Speed;}
    function getMovementAngle() {return m_MovementAngle;}
    function isStuck() {return m_Speed <= m_MaxStuckSpeed && getTickCount() >= m_IgnoreStuckEndTick;}

    function getVisiblePlayers() {return m_VisiblePlayers;}
    function getCell(){return m_Cell;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static function getVersion() {return m_Version;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(name, x = 0.0, y = 0.0, z = 0.0, angle = 0.0, world = "NEWWORLD\\NEWWORLD.ZEN")
    {
        m_ChangedAttributes = {};

        m_Pos = {["x"] = x, ["y"] = y, ["z"] = z};
        m_VisiblePlayers = [];
        setAngle(angle);
        setWorld(world);

        local bid = Bot.getFreeID();
        if (bid >= 0)
        {
            m_ID = bid;
            m_Name = name;

            m_Bots[bid] <- this;
            updateCell();

            Bot.createBot(this);
            callEvent("onBotCreated", bid, name);
        }
        else if (m_LogBots)
            print("cannot create bot '" + name + "' (reached max number of bots)");
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    // -- Cell within cellsystem of bots. Allows to get surrounding bots with low overhead -- //
    m_Cell = null;

    m_ID = -1;
    m_Name = "";
    m_Dead = false;
    m_InView = false;
    m_LastTick = getTickCount();

    m_SyncPid = -1;
    m_Instance = "PC_HERO";
    m_World = null;
    m_Pos = null;
    m_Angle = 0;
    m_Ani = "";
    m_VirtualWorld = 0;

    // -- interp angle towards player/bot -- //
    m_InterpAngleId = -1;
    m_InterpAngle = -1;
    m_InterpAngleSpeed = 0;

    // -- units/s. Updated every time, a player syncs the bot's position back to server -- //
    m_LastSpeedUpdateTick = getTickCount();
    m_Speed = 0;
    m_MovementAngle = 0;
    // -- bot doesn't count as stuck until enough time has passed -- //
    m_IgnoreStuckEndTick = getTickCount();

    // -- Stats -- //
        m_MaxHealth = 1000;
        m_Health = 1000;
        m_Strength = 15;
        m_Dexterity = 15;
        m_OneH = 30;
        m_TwoH = 30;
        m_Bow = 30;
        m_CBow = 30;
        m_WeaponMode = 0;

    // -- equiment -- //
        m_Armor = -1;
        m_MeleeWeapon = -1;
        m_RangedWeapon = -1;
        m_Shield = -1;
        m_Helmet = -1;

    // -- Visuals -- //
        m_BodyModel = "";
        m_BodyTexture = 0;
        m_HeadModel = "";
        m_HeadTexture = 0;
        m_Fatness = 1;

    // -- Don't send unchanged values to clients -- //

    m_ChangedAttributes = null;

    m_PosChanged = false;
    m_AngleChanged = false;

    // -- interpolate angle on client
    m_InterpAngleIdChanged = false;
    m_InterpAngleChanged = false;
    m_InterpAngleSpeedChanged = false;

    m_SyncPidChanged = false;
    m_InViewChanged = false;
    m_InstanceChanged = false;
    m_WorldChanged = false;
    m_MaxHealthChanged = false;
    m_HealthChanged = false;
    m_StrengthChanged = false;
    m_DexterityChanged = false;
    m_OneHChanged = false;
    m_TwoHChanged = false;
    m_BowChanged = false;
    m_CBowChanged = false;
    m_WeaponModeChanged = false;

    m_ArmorChanged = false;
    m_MeleeWeaponChanged = false;
    m_RangedWeaponChanged = false;
    m_ShieldChanged = false;
    m_HelmetChanged = false;

    m_VisualsChanged = false;
    m_FatnessChanged = false;

    m_VirtualWorldChanged = false;

    m_ReceivedClientPos = false;
    m_ReceivedClientHealth = false;

    m_AnyValueChanged = false;

    m_VisiblePlayers = null;

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static m_Bots = {};
    static m_MaxBots = 10000;
    static m_LogBots = true;

    static m_Version = "2.7.1";

    // -- if bot is slower than this, he is not moving or stuck -- //
    static m_MaxStuckSpeed = 10;
    static m_IgnoreStuckTime = 1000;

    // -- Cells to easily get close bots -- //
    static m_CellSystem = Cell(2000);
}
