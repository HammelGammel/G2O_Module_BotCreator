// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

addEvent("onBotCreated");

// ------------------------------------------------------------------- //

local SENDBOTPOS_TICKINTERVAL = 100;
local SENDBOTPOS_NEXTTICK = getTickCount() + SENDBOTPOS_TICKINTERVAL;

local SENDBOTANI_TICKINTERVAL = 500;
local SENDBOTANI_NEXTTICK = getTickCount() + SENDBOTANI_TICKINTERVAL;

addEventHandler("onRender", function()
{
    local tick = getTickCount();
    if (tick > SENDBOTPOS_NEXTTICK)
    {
        Bot.sendPos();
        SENDBOTPOS_NEXTTICK = tick + SENDBOTPOS_TICKINTERVAL;
    }

    if (tick > SENDBOTANI_NEXTTICK)
    {
        Bot.sendAni();
        SENDBOTANI_NEXTTICK = tick + SENDBOTANI_TICKINTERVAL;
    }
});

// ------------------------------------------------------------------- //

addEventHandler("onPlayerHit", function(killerid, bid, dmg)
{
    // -- This client hits bot -- //
    if (killerid == heroId && bid >= getMaxSlots())
    {
        local bot = Bot.getBot(bid);
        bot.setHealth(getPlayerHealth(bid) - dmg);

        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TSBotHit);
        packet.writeInt16(bid);
        packet.writeInt32(dmg);
        packet.send(RELIABLE);
    }
});

// ------------------------------------------------------------------- //

addEventHandler("onWorldChange", function(world)
{
    foreach (key, bot in Bot.m_Bots)
        bot.handleSpawnUnspawn();
});

// ------------------------------------------------------------------- //

addEventHandler("onPacket", function(packet)
{
    local packetID = packet.readUInt16();
    switch (packetID)
    {
        case EPacketIdBots.TCUpdateBot:
            Bot.readUpdatePacket(packet);
            break;

        case EPacketIdBots.TCCreate:
            Bot.readCreatePacket(packet);
            break;

        /*case EPacketIdBots.TCPlayAni:
            local id = packet.readInt16();
            local ani = packet.readString();
            local bot = Bot.getBot(id);

            bot.setAni(ani);
            playAni(id, ani);
            break;

        case EPacketIdBots.TCStopAni:
            local id = packet.readInt16();
            stopAni(id);
            break;*/

        case EPacketIdBots.TCSetPosImmediate:
            local id = packet.readInt16();
            local x = packet.readFloat();
            local y = packet.readFloat();
            local z = packet.readFloat();
            setPlayerPosition(id, x, y, z);
            break;

        case EPacketIdBots.TCDestroy:
            local id = packet.readInt16();
            Bot.destroyBot(id);
            break;

        case EPacketIdBots.TCRespawn:
            local id = packet.readInt16();
            local x = packet.readFloat();
            local y = packet.readFloat();
            local z = packet.readFloat();
            Bot.respawn(id, x, y, z);
            break;

        case EPacketIdBots.TCHit:
            local bid = packet.readInt16();
            local attackerId = packet.readInt16();
            Bot.hit(bid, attackerId);
            break;

        case EPacketIdBots.TCHitAndSendBack:
            local bid = packet.readInt16();
            local attackerId = packet.readInt16();
            Bot.hitAndSend(bid, attackerId);
            break;

        case EPacketIdBots.TCSetVirtualWorld:
            local vWorld = packet.readUInt16();
            ManagedPlayer.setVirtualWorld(vWorld);
            break;
        }
});

// ------------------------------------------------------------------- //
// ------------------------------------------------------------------- //

class Bot
{
    static function readUpdatePacket(packet)
    {
        local numberBots = packet.readUInt16();

        for(local i = 0; i < numberBots; i++)
        {
            local bid = packet.readInt16();
            local flags = packet.readUInt32();

            local bot = getBot(bid);
            if (bot == null)
            {
                print("ERROR: received update packet for bot " + bid + ", which doesn't exist");
                return;
            }

            local syncPidChanged = (flags &             1) == 1;
            local posChanged = (flags &                 2) == 2;
            local angleChanged = (flags &               4) == 4;
            local interpAngleIdChanged = (flags &       8) == 8;
            local interpAngleChanged = (flags &         16) == 16;
            local interpAngleSpeedChanged = (flags &    32) == 32;
            local instanceChanged = (flags &            64) == 64;
            local worldChanged = (flags &               128) == 128;
            local maxHealthChanged = (flags &           256) == 256;
            local healthChanged = (flags &              512) == 512;
            local strengthChanged = (flags &            1024) == 1024;
            local dexterityChanged = (flags &           2048) == 2048;
            local oneHChanged = (flags &                4092) == 4092;
            local twoHChanged = (flags &                8192) == 8192;
            local bowChanged = (flags &                 16384) == 16384;
            local cBowChanged = (flags &                32768) == 32768;
            local weaponModeChanged = (flags &          65536) == 65536;
            local armorChanged = (flags &               131072) == 131072;
            local meleeWeaponChanged = (flags &         262144) == 262144;
            local rangedWeaponChanged = (flags &        524288) == 524288;
            local shieldChanged = (flags &              1048576) == 1048576;
            local helmetChanged = (flags &              2097152) == 2097152;
            local visualsChanged = (flags &             4194304) == 4194304;
            local fatnessChanged = (flags &             8388608) == 8388608;
            local inViewChanged = (flags &              16777216) == 16777216
            local virtualWorldChanged = (flags &        33554432) == 33554432;
            local aniChanged = (flags &                 67108864) == 67108864;
            local stopAniChanged = (flags &             134217728) == 134217728;

            if (syncPidChanged)
            {
                local syncPid = packet.readInt16();
                bot.setSyncPid(syncPid);
            }
            if (posChanged)
            {
                local x = packet.readFloat();
                local y = packet.readFloat();
                local z = packet.readFloat();

                setPlayerPosition(bid, x, y, z);
                bot.setPosition(x, y, z);
            }
            if (angleChanged)
            {
                local angle = packet.readFloat();

                setPlayerAngle(bid, angle);
                bot.setAngle(angle);
            }
            if (interpAngleIdChanged)
            {
                local id = packet.readInt16();
                bot.setInterpAngleId(id);
            }
            if (interpAngleChanged)
            {
                local angle = packet.readFloat();
                bot.setInterpAngle(angle);
            }
            if (interpAngleSpeedChanged)
            {
                local speed = packet.readFloat();
                bot.setInterpAngleSpeed(speed);
            }
            if (instanceChanged)
            {
                local instance = packet.readString();
                setPlayerInstance(bid, instance);
                bot.setInstance(instance);
            }
            if (worldChanged)
            {
                local world = packet.readString();
                bot.setBotWorld(world);
            }
            if (maxHealthChanged)
            {
                local maxHealth = packet.readInt32();
                setPlayerMaxHealth(bid, maxHealth);
                bot.setMaxHealth(maxHealth);
            }
            if (healthChanged)
            {
                local health = packet.readInt32();
                setPlayerHealth(bid, health);
                bot.setHealth(health);
            }
            if (strengthChanged)
            {
                local strength = packet.readUInt8();
                setPlayerStrength(bid, strength);
                bot.setStrength(strength);
            }
            if (dexterityChanged)
            {
                local dexterity = packet.readUInt8();
                setPlayerDexterity(bid, dexterity);
                bot.setDexterity(dexterity);
            }
            if (oneHChanged)
            {
                local oneH = packet.readUInt8();
                setPlayerSkillWeapon(bid, WEAPON_1H, oneH);
                bot.setOneH(oneH);
            }
            if (twoHChanged)
            {
                local twoH = packet.readUInt8();
                setPlayerSkillWeapon(bid, WEAPON_2H, twoH);
                bot.setTwoH(twoH);
            }
            if (bowChanged)
            {
                local bow = packet.readUInt8();
                setPlayerSkillWeapon(bid, WEAPON_BOW, bow);
                bot.setBow(bow);
            }
            if (cBowChanged)
            {
                local cBow = packet.readUInt8();
                setPlayerSkillWeapon(bid, WEAPON_CBOW, cBow);
                bot.setCBow(cBow);
            }
            if (weaponModeChanged)
            {
                local weaponMode = packet.readUInt8();
                setPlayerWeaponMode(bid, weaponMode);
                bot.setWeaponMode(weaponMode);
            }
            if (armorChanged)
            {
                local armor = packet.readInt32();
                bot.setArmor(armor);
                if (armor < 0)
                    unequipArmor(bid);
                else
                    equipArmor(bid, armor);
            }
            if (meleeWeaponChanged)
            {
                local meleeWeapon = packet.readInt32();
                bot.setMeleeWeapon(meleeWeapon);
                if (meleeWeapon < 0)
                    unequipMeleeWeapon(bid);
                else
                    equipMeleeWeapon(bid, meleeWeapon);
            }
            if (rangedWeaponChanged)
            {
                local rangedWeapon = packet.readInt32();
                bot.setRangedWeapon(rangedWeapon);
                if (rangedWeapon < 0)
                    unequipRangedWeapon(bid);
                else
                    equipRangedWeapon(bid, rangedWeapon);
            }
            if (shieldChanged)
            {
                local shield = packet.readInt32();
                bot.setShield(shield);
                if (shield < 0)
                    unequipShield(bid);
                else
                    equipShield(bid, shield);
            }
            if (helmetChanged)
            {
                local helmet = packet.readInt32();
                bot.setHelmet(helmet);
                if (helmet < 0)
                    unequipHelmet(bid);
                else
                    equipHelmet(bid, helmet);
            }
            if (visualsChanged)
            {
                local bodyModel = packet.readString();
                local bodyTexture = packet.readUInt8();
                local headModel = packet.readString();
                local headTexture = packet.readUInt8();
                setPlayerVisual(bid, bodyModel, bodyTexture, headModel, headTexture);

                bot.setBodyModel(bodyModel);
                bot.setBodyTexture(bodyTexture);
                bot.setHeadModel(headModel);
                bot.setHeadTexture(headTexture);
            }
            if (fatnessChanged)
            {
                local fatness = packet.readUInt8();
                setPlayerFatness(bid, fatness);
                bot.setFatness(fatness);
            }
            if (inViewChanged)
            {
                local inView = packet.readBool();
                bot.setInView(inView);
            }
            if (virtualWorldChanged)
            {
                local virtualWorld = packet.readInt16();
                bot.setVirtualWorld(virtualWorld);
            }
            if (stopAniChanged)
            {
                bot.setAni("");
                stopAni(bid);
            }
            if (aniChanged)
            {
                local ani = packet.readString();
                bot.setAni(ani);

                if (ani != "" && ani != "NULL")
                    playAni(bid, ani);
            }

            bot.handleSpawnUnspawn();
        }
    }

    static function readCreatePacket(packet)
    {
        local numberBots = packet.readUInt16();

        for(local i = 0; i < numberBots; i++)
        {
            local syncPid = packet.readInt16();
            local id = packet.readInt16();
            local name = packet.readString();
            local x = packet.readFloat();
            local y = packet.readFloat();
            local z = packet.readFloat();
            local interpAngleId = packet.readInt16();
            local interpAngleSpeed = packet.readFloat();
            local angle = packet.readFloat();
            local instance = packet.readString();
            local world = packet.readString();
            local maxHealth = packet.readInt32();
            local health = packet.readInt32();
            local strength = packet.readUInt8();
            local dexterity = packet.readUInt8();
            local oneH = packet.readUInt8();
            local twoH = packet.readUInt8();
            local bow = packet.readUInt8();
            local cBow = packet.readUInt8();
            local weaponMode = packet.readUInt8();
            local armor = packet.readInt32();
            local meleeWeapon = packet.readInt32();
            local rangedWeapon = packet.readInt32();
            local shield = packet.readInt32();
            local helmet = packet.readInt32();
            local bodyModel = packet.readString();
            local bodyTexture = packet.readUInt8();
            local headModel = packet.readString();
            local headTexture = packet.readUInt8();
            local fatness = packet.readFloat();
            local inView = packet.readBool();
            local virtualWorld = packet.readInt16();
            local ani = packet.readString();

            local npcId = createNpc(name);

            if (npcId != id)
            {
                destroyNpc(npcId);
                print("ERROR: Creating bot id mismatch: " + npcId + " != " + id + " - " + name);
                return;
            }

            local bot = BotStruct(id);
            Bot.m_Bots[id] <- bot;

            bot.setSyncPid(syncPid);
            bot.setName(name);
            bot.setInstance(instance);
            bot.setBotWorld(world);
            bot.setPosition(x, y, z);
            bot.setInterpAngleId(interpAngleId);
            bot.setInterpAngleSpeed(interpAngleSpeed);
            bot.setAngle(angle);
            bot.setMaxHealth(maxHealth);
            bot.setHealth(health);
            bot.setStrength(strength);
            bot.setDexterity(dexterity);
            bot.setOneH(oneH);
            bot.setTwoH(twoH);
            bot.setBow(bow);
            bot.setCBow(cBow);
            bot.setArmor(armor);
            bot.setMeleeWeapon(meleeWeapon);
            bot.setRangedWeapon(rangedWeapon);
            bot.setShield(shield);
            bot.setHelmet(helmet);
            bot.setBodyModel(bodyModel);
            bot.setBodyTexture(bodyTexture);
            bot.setHeadModel(headModel);
            bot.setHeadTexture(headTexture);
            bot.setFatness(fatness);
            bot.setWeaponMode(weaponMode);
            bot.setInView(inView);
            bot.setVirtualWorld(virtualWorld);
            bot.setAni(ani);

            bot.handleSpawnUnspawn();

            callEvent("onBotCreated", id, name);
        }
    }

    // ------------------------------------------------------------------- //

    static function sendUnknownDamage(bot, dmg)
    {
        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TSUnknownDamage);
        packet.writeInt16(bot.getID());
        packet.writeInt16(dmg);
        packet.send(RELIABLE);
    }

    // ------------------------------------------------------------------- //

    static function sendUnknownHeal(bot, amount)
    {
        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TSUnknownHeal);
        packet.writeInt16(bot.getID());
        packet.writeInt16(amount);
        packet.send(RELIABLE);
    }

    // ------------------------------------------------------------------- //

    static function sendPos()
    {
        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TSUpdatePos);

        local botCount = 0;
        foreach (bot in m_Bots)
        {
            if (bot.getSyncBack() && bot.isSpawned())
            {
                local pos = bot.getEffectivePosition();
                local posChanged = getDistance3d(pos.x, pos.y, pos.z, bot.m_Pos.x, bot.m_Pos.y, bot.m_Pos.z) >= 1.0;
                if (posChanged || !bot.m_DontSendPosNextTime)
                    botCount++;
            }
        }

        packet.writeUInt16(botCount);

        foreach (bot in m_Bots)
        {
            // -- postpone sending to server if bot isn't synched by this client or not in view of any player -- //
            if (bot.getSyncBack() && bot.isSpawned())
            {
                local pos = bot.getEffectivePosition();

                local posChanged = getDistance3d(pos.x, pos.y, pos.z, bot.m_Pos.x, bot.m_Pos.y, bot.m_Pos.z) >= 1.0;

                // -- only send if bot has moved, or once if he hasn't so server registers that speed is 0 -- //
                if (posChanged || !bot.m_DontSendPosNextTime)
                {
                    packet.writeInt16(bot.getID());
                    packet.writeFloat(pos.x);
                    packet.writeFloat(pos.y);
                    packet.writeFloat(pos.z);
                }

                bot.m_DontSendPosNextTime = !posChanged;
                bot.m_Pos = pos;
            }
        }

        packet.send(UNRELIABLE);
    }

    // ------------------------------------------------------------------- //

    static function sendAni()
    {
        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TSUpdateAni);

        local botCount = 0;
        foreach (bot in m_Bots)
            if (bot.getSyncBack() && bot.m_AniChanged && bot.isSpawned())
                botCount++;

        packet.writeUInt16(botCount);

        foreach (bot in m_Bots)
        {
            // -- postpone sending to server if bot isn't synched by this client or not in view of any player -- //
            if (bot.getSyncBack() && bot.m_AniChanged && bot.isSpawned())
            {
                packet.writeInt16(bot.getID());
                packet.writeString(getPlayerAni(bot.getID()));
                bot.m_AniChanged = false;
            }
        }

        packet.send(UNRELIABLE);
    }

    // ------------------------------------------------------------------- //

    static function destroyBot(bid)
    {
        destroyNpc(bid);
        if (bid in m_Bots)
        {
            delete m_Bots[bid];
            callEvent("onBotDestroyed", bid);
        }
    }

    // ------------------------------------------------------------------- //

    static function isPlayer(bid)
    {
        return bid < getMaxSlots();
    }

    // ------------------------------------------------------------------- //

    static function isBot(bid)
    {
        return bid >= getMaxSlots();
    }

    // ------------------------------------------------------------------- //

    static function respawn(bid, x, y, z)
    {
        local bot = getBot(bid);

        bot.unspawn();

        bot.setPosition(x, y, z);
        bot.setAni(null);

        bot.handleSpawnUnspawn();

        stopAni(bid);
    }

    // ------------------------------------------------------------------- //

    function hit(bid, attackerId)
    {
        if (isBot(bid) && !getBot(bid).isSpawned())
            return;
        if (isBot(attackerId) && !getBot(attackerId).isSpawned())
            return;

        hitPlayer(attackerId, bid);
        getBot(bid).setHealth(getPlayerHealth(bid));
    }

    // ------------------------------------------------------------------- //

    function hitAndSend(bid, attackerId)
    {
        local bot = getBot(bid);

        // -- if one of the two isn't spawned, client crashes -- //
        if (isBot(bid) && !bot.isSpawned())
        {
            // -- this is a serious issue. Server will register no damage, other
            // -- clients might be ouf of sync -- //
            return;
        }
        if (isBot(attackerId) && !getBot(attackerId).isSpawned())
        {
            // -- this is a serious issue. Server will register no damage, other
            // -- clients might be ouf of sync -- //
            return;
        }

        local oldHealth = getPlayerHealth(bid);
        hitPlayer(attackerId, bid);
        local damage = oldHealth - getPlayerHealth(bid);

        if (bot != null)
            bot.setHealth(getPlayerHealth(bid));

        // -- Send calculated damage back to server -- //
        local packet = Packet();
        packet.writeUInt16(EPacketIdBots.TSDamage);
        packet.writeInt16(bid);
        packet.writeInt16(attackerId);
        packet.writeInt32(damage);

        packet.send(RELIABLE);
    }

    // ------------------------------------------------------------------- //

    static function getBot(bid)
    {
        if (bid in m_Bots)
            return m_Bots[bid];
        return null;
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static m_Bots = {};
    static m_LogBots = true;
}
